import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  constructor(private http: HttpClient) {}

  // user data
  getUserData(): Observable<any> {
    return this.http
      .get('content/data/profile/user.json')
      .pipe(map(data => data));
  }

  // notification admin
  getShipmentAdminData(): Observable<any> {
    return this.http
      .get('content/data/profile/notifications/shipment-admin.json')
      .pipe(map(data => data));
  }
  getFranchiseData(): Observable<any> {
    return this.http
      .get('content/data/profile/notifications/franchise.json')
      .pipe(map(data => data));
  }
  getInvoiceAdminData(): Observable<any> {
    return this.http
      .get('content/data/profile/notifications/invoice-admin.json')
      .pipe(map(data => data));
  }
  getPaymentCardsData(): Observable<any> {
    return this.http
      .get('content/data/profile/cards.json')
      .pipe(map(data => data));
  }
  getGeneralSettingsAdminData(): Observable<any> {
    return this.http
      .get('content/data/profile/general-settings-admin.json')
      .pipe(map(data => data));
  }
  // notification customer
  getShipmentCustomerData(): Observable<any> {
    return this.http
      .get('content/data/profile/notifications/shipment-customer.json')
      .pipe(map(data => data));
  }
  getInvoiceCustomerData(): Observable<any> {
    return this.http
      .get('content/data/profile/notifications/invoice-customer.json')
      .pipe(map(data => data));
  }
  getGeneralSettingsCustomerData(): Observable<any> {
    return this.http
      .get('content/data/profile/general-settings-customer.json')
      .pipe(map(data => data));
  }
}
