import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile-security',
  templateUrl: './profile-security.component.html',
  styleUrls: ['./profile-security.component.scss'],
})
export class ProfileSecurityComponent implements OnInit {
  hide = true;
  hideNew = true;
  hideConfirm = true;

  password = {
    old: '',
    new: '',
    confirm: '',
  };
  validation = {
    old: new FormControl(),
    new: new FormControl(),
    confirm: new FormControl(),
  };

  constructor() {}

  ngOnInit() {}

  savePassword() {
    console.log('save');
  }
}
