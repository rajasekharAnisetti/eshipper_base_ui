import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Settings } from 'src/app/interfaces/settings';

import { ProfileService } from '../../profile.service';

@Component({
  selector: 'app-notification-franchise',
  templateUrl: './notification-franchise.component.html',
  styleUrls: ['./notification-franchise.component.scss'],
})
export class NotificationFranchiseComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();

  settingsList: Array<Settings> = [];
  frequencies = ['Daily', 'Monthly', 'Yearly'];

  constructor(private getData: ProfileService) {}

  ngOnInit() {
    // get data
    this.subscribe.add(
      this.getData.getFranchiseData().subscribe(data => {
        this.settingsList = data.content.length ? data.content : [];
      })
    );
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
