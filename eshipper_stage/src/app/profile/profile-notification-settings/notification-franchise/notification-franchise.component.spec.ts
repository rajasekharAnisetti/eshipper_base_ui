import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationFranchiseComponent } from './notification-franchise.component';

describe('NotificationFranchiseComponent', () => {
  let component: NotificationFranchiseComponent;
  let fixture: ComponentFixture<NotificationFranchiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationFranchiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationFranchiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
