import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Settings } from 'src/app/interfaces/settings';

import { ProfileService } from '../../profile.service';

@Component({
  selector: 'app-notification-invoice',
  templateUrl: './notification-invoice.component.html',
  styleUrls: ['./notification-invoice.component.scss'],
})
export class NotificationInvoiceComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  account = localStorage.getItem('account');
  adminFlag = false;

  settingsList: Array<Settings> = [];
  frequencies = ['Daily', 'Monthly', 'Yearly'];

  constructor(private getData: ProfileService) {}

  ngOnInit() {
    // get admin flag
    this.adminFlag = this.account === 'admin' ? true : false;
    // get data
    if (this.adminFlag) {
      this.subscribe.add(
        this.getData.getInvoiceAdminData().subscribe(data => {
          this.settingsList = data.content.length ? data.content : [];
        })
      );
    } else {
      this.subscribe.add(
        this.getData.getInvoiceCustomerData().subscribe(data => {
          this.settingsList = data.content.length ? data.content : [];
        })
      );
    }
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
