import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-notification-settings',
  templateUrl: './profile-notification-settings.component.html',
  styleUrls: ['./profile-notification-settings.component.scss'],
})
export class ProfileNotificationSettingsComponent implements OnInit {
  account = localStorage.getItem('account');
  adminFlag = false;
  superAdminFlag = false;
  selectedIndex = 0;

  constructor() {}

  ngOnInit() {
    // get admin flag
    this.adminFlag = this.account === 'admin' ? true : false;
    // get superadmin flag
    this.superAdminFlag = this.account === 'superadmin' ? true : false;
  }

  selectedIndexChange(val: number) {
    this.selectedIndex = val;
  }
}
