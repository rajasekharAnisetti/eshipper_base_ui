import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationClaimComponent } from './notification-claim.component';

describe('NotificationClaimComponent', () => {
  let component: NotificationClaimComponent;
  let fixture: ComponentFixture<NotificationClaimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationClaimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationClaimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
