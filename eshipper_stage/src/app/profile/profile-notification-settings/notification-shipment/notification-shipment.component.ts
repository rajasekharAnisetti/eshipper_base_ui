import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Settings } from 'src/app/interfaces/settings';

import { ProfileService } from '../../profile.service';

@Component({
  selector: 'app-notification-shipment',
  templateUrl: './notification-shipment.component.html',
  styleUrls: ['./notification-shipment.component.scss'],
})
export class NotificationShipmentComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  account = localStorage.getItem('account');
  adminFlag = false;

  settingsList: Array<Settings> = [];
  frequencies = ['Daily', 'Monthly', 'Yearly'];

  constructor(private getData: ProfileService) {}

  ngOnInit() {
    // get admin flag
    this.adminFlag = this.account === 'admin' ? true : false;
    // get data
    if (this.adminFlag) {
      this.subscribe.add(
        this.getData.getShipmentAdminData().subscribe(data => {
          this.settingsList = data.content.length ? data.content : [];
        })
      );
    } else {
      this.subscribe.add(
        this.getData.getShipmentCustomerData().subscribe(data => {
          this.settingsList = data.content.length ? data.content : [];
        })
      );
    }
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
