import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationShipmentComponent } from './notification-shipment.component';

describe('NotificationShipmentComponent', () => {
  let component: NotificationShipmentComponent;
  let fixture: ComponentFixture<NotificationShipmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationShipmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationShipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
