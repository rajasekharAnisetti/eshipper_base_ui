import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notification-ecom',
  templateUrl: './notification-ecom.component.html',
  styleUrls: ['./notification-ecom.component.scss'],
})
export class NotificationEcomComponent implements OnInit {
  data = {
    newOrder: {
      name: 'New Order',
      system: true,
      email: true,
      frequency: 'Monthly',
      active: true,
      list: [
        {
          platform: '',
          store: '',
          notify: 'by Order',
          country: '',
        },
      ],
    },
    autoFulfilledOrder: {
      name: 'Auto-fulfilled Order',
      system: false,
      email: true,
      frequency: 'Monthly',
      active: true,
    },
    inventoryRunningLow: {
      name: 'Inventory Running Low',
      system: false,
      email: true,
      frequency: 'Monthly',
      active: true,
      list: [{ platform: '', store: '' }],
    },
  };

  frequencies = ['Daily', 'Monthly', 'Yearly'];
  notifyList = ['by Order', 'Option 2', 'Option 3'];
  platforms = ['Option 1', 'Option 2', 'Option 3'];
  stories = ['Option 1', 'Option 2', 'Option 3'];
  countries = ['Canada', 'USA', 'France'];

  // dynamic row
  addNewOrderRow() {
    this.data.newOrder.list.push({
      platform: '',
      store: '',
      notify: 'by Order',
      country: '',
    });
  }
  removeNewOrderRow(index) {
    this.data.newOrder.list.splice(index, 1);
  }

  addInventoryRunningLowRow() {
    this.data.inventoryRunningLow.list.push({ platform: '', store: '' });
  }
  removeInventoryRunningLowRow(index) {
    this.data.inventoryRunningLow.list.splice(index, 1);
  }
  // end of dynamic row

  constructor() {}

  ngOnInit() {}
}
