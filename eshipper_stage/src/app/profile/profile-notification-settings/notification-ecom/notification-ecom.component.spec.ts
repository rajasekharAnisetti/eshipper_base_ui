import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationEcomComponent } from './notification-ecom.component';

describe('NotificationEcomComponent', () => {
  let component: NotificationEcomComponent;
  let fixture: ComponentFixture<NotificationEcomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationEcomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationEcomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
