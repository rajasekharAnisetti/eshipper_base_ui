import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  flag = true;
  account = localStorage.getItem('account');
  adminFlag = false;
  superAdminFlag = false;

  constructor() { }

  ngOnInit() {
    this.adminFlag = this.account === 'admin' ? true : false;
    this.superAdminFlag = this.account === 'superadmin' ? true : false;
  }

  onActivate() {
    this.flag = false;
  }
  onDeactivate() {
    this.flag = true;
  }
}
