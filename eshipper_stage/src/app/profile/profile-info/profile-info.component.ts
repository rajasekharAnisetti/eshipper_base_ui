import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { FormControl, Validators } from '@angular/forms';

import { Countries } from 'src/app/shared/const/countries';
import { UserAvatar, User } from 'src/app/interfaces/user';
import { ValidationClassStateMatcher } from 'src/app/shared/classes/validation-class';
import { TIMEZONES } from 'src/app/shared/const/dropdowns';

import { ProfileService } from '../profile.service';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-profile-info',
  templateUrl: './profile-info.component.html',
  styleUrls: ['./profile-info.component.scss'],
})
export class ProfileInfoComponent implements OnInit, OnDestroy {
  avatar: UserAvatar = JSON.parse(localStorage.getItem('userAvatar'));
  filteredCityes: Observable<any[]>;
  account = localStorage.getItem('account');
  adminFlag = false;
  superAdminFlag = false;
  //
  staticData = {
    countries: Countries.COUNTRIES,
    states: Countries.USASTATES,
    cities: Countries.USACITIES,
    stateLabel: 'State',
    zipLabel: 'Zip Code',
  };
  subscribe = new Subscription();
  userData = <User>{};
  matcher = new ValidationClassStateMatcher();
  newFlag = false;

  validation = {
    company: new FormControl(),
    address1: new FormControl(),
    address2: new FormControl(),
    city: new FormControl(),
    stateOrProvince: new FormControl(),
    zipOrPostal: new FormControl(),
    country: new FormControl(),
    userName: new FormControl(),
    userEmail: new FormControl('', [Validators.email]),
    userPhone: new FormControl(),
    fax: new FormControl(),
    timeZone: new FormControl(),
    salesRep: new FormControl(),
  };

  timeZones = TIMEZONES;
  salesReps = ['Option 1', 'Option 2'];

  constructor(private getData: ProfileService) { }

  ngOnInit() {
    // get user data
    this.subscribe.add(
      this.getData.getUserData().subscribe(data => {
        this.userData = data;
      })
    );
    // get admin flag
    this.adminFlag = this.account === 'admin' ? true : false;
    // get superadmin flag
    this.superAdminFlag = this.account === 'superadmin' ? true : false;
    // for city
    this.filteredCityes = this.validation.city.valueChanges.pipe(
      startWith(''),
      map(city => city ? this._filter(city) : this.staticData.cities.slice())
    )
  }

  // city autocomplete
  displayFn(city?: string): string | undefined {
    return city ? city : undefined;
  }

  private _filter(city: string) {
    const filterValue = city.toLowerCase();

    return this.staticData.cities.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
  }
  // emd of city autocomplete

  // upload avatar
  upload() {
    console.log('Upload');
  }
  // end of upload avatar

  // change country
  changeCountry(country) {
    this.userData.city = '';
    switch (country) {
      case 'Canada':
        {
          this.staticData.stateLabel = 'Province';
          this.staticData.zipLabel = 'Postal Code';
          this.staticData.states = Countries.CANADAPROVINCES;
          this.staticData.cities = Countries.CANADACITIES;
        }
        break;
      case 'USA':
        {
          this.staticData.stateLabel = 'State';
          this.staticData.zipLabel = 'Zip Code';
          this.staticData.states = Countries.USASTATES;
          this.staticData.cities = Countries.USACITIES;
        }
        break;
      default:
        break;
    }
  }
  // end of change country

  ngOnDestroy() {
    // unsubscribe
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
