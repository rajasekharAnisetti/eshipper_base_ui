import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

import { PaymentCard } from 'src/app/interfaces/payment-card';
import { CreditCardDialogComponent } from 'src/app/dialogs/credit-card-dialog/credit-card-dialog.component';

import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-profile-credit-billing',
  templateUrl: './profile-credit-billing.component.html',
  styleUrls: ['./profile-credit-billing.component.scss'],
})
export class ProfileCreditBillingComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  cardList: Array<PaymentCard>;
  receiveTransactionReceipt = false;
  itemSettings = ['Edit', 'Delete'];

  constructor(private getData: ProfileService, private dialog: MatDialog) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData.getPaymentCardsData().subscribe(data => {
        this.cardList = data.content.length ? data.content : [];
      })
    );
  }

  // add payment card
  add() {
    const dialogRef = this.dialog.open(CreditCardDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Add Credit Card',
        actionButton: 'Add',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of add payment card

  // edit card
  editCard(item) {
    const dialogRef = this.dialog.open(CreditCardDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Edit Credit Card',
        actionButton: 'Edit',
        modalData: item,
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of edit card

  // change the card
  changeCard(item) {
    for (const i of this.cardList) {
      i.default = false;
    }
    item.default = true;
  }
  // end of change the card

  // delete card
  deteleCard(index) {
    if (index !== -1) {
      this.cardList.splice(index, 1);
    }
  }
  // end of delete card

  // item settings
  itemSettingsFunction(val, item, index) {
    switch (val) {
      case 'Edit':
        this.changeCard(item);
        break;
      case 'Delete':
        this.deteleCard(index);
        break;
      default:
        break;
    }
  }
  // end of item settings

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
