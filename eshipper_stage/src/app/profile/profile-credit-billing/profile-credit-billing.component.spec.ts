import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileCreditBillingComponent } from './profile-credit-billing.component';

describe('ProfileCreditBillingComponent', () => {
  let component: ProfileCreditBillingComponent;
  let fixture: ComponentFixture<ProfileCreditBillingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileCreditBillingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileCreditBillingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
