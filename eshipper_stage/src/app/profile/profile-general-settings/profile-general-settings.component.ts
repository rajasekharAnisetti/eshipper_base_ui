import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

import { CarrierSttings } from 'src/app/interfaces/carier-settings';
import { CarrierAccountDialogComponent } from 'src/app/dialogs/carrier-account-dialog/carrier-account-dialog.component';
import { CURRENCIES_$ } from 'src/app/shared/const/currencies';

import {
  Broker,
  InvoiceTax,
  Shipment,
  Reference,
  Print,
} from 'src/app/interfaces/user';

import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-profile-general-settings',
  templateUrl: './profile-general-settings.component.html',
  styleUrls: ['./profile-general-settings.component.scss'],
})
export class ProfileGeneralSettingsComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  account = localStorage.getItem('account');
  carrierList: Array<CarrierSttings> = [];
  customBrokerInfo = <Broker>{};
  invoiceTax = <InvoiceTax>{};
  shipment = <Shipment>{};
  referenceCodes = <Reference>{};
  printSettings = <Print>{};
  adminFlag = false;

  itemCarrierSettings = ['Edit', 'Delete'];
  currencies = CURRENCIES_$;
  generateInvoicesPerList = ['Per Shipment', 'Option 2', 'Option 3'];
  defaultPackageTypes = ['Envelope', 'Option 2', 'Option 3'];
  defaultPickupLocations = ['Value', 'Option 2', 'Option 3'];
  insuranceTypes = ['eShipper', 'Option 2', 'Option 3'];
  signatureRequiredList = ['No', 'Adult', 'Value'];
  shippingLabelSizeList = ['Regular Printer (8 x 11)', 'Option 2', 'Option 3'];

  validation = {
    brokerCompanyName: new FormControl(),
    taxIdBroker: new FormControl(),
    brokerContactName: new FormControl(),
    brokerEmail: new FormControl(),
    brokerPhoneNumber: new FormControl(),
    invoiceTaxCurrency: new FormControl(),
    generateInvoicesPer: new FormControl(),
    chargeLevel: new FormControl(),
    taxId: new FormControl(),
    defaultPackageType: new FormControl(),
    defaultPickupLocation: new FormControl(),
    insuranceType: new FormControl(),
    signatureRequired: new FormControl(),
    reference1: new FormControl(),
    reference2: new FormControl(),
    reference3: new FormControl(),
    shippingLabelSize: new FormControl(),
  };

  constructor(private getData: ProfileService, private dialog: MatDialog) { }

  ngOnInit() {
    // get admin flag
    this.adminFlag = this.account === 'admin' ? true : false;

    if (this.adminFlag) {
      // get admin data
      this.subscribe.add(
        this.getData.getGeneralSettingsAdminData().subscribe(data => {
          this.carrierList = data.carrierSettings;
        })
      );
    } else {
      // get customer data
      this.subscribe.add(
        this.getData.getGeneralSettingsCustomerData().subscribe(data => {
          this.carrierList = data.carrierSettings;
          this.customBrokerInfo = data.customBrokerInfo;
          this.invoiceTax = data.invoiceTax;
          this.shipment = data.shipment;
          this.referenceCodes = data.referenceCodes;
          this.printSettings = data.printSettings;
        })
      );
    }
  }

  // change currency
  changeCurrency(val, item) {
    item = val;
  }
  // end of change currency

  // item carrier settings function
  itemCarrierSettingsFunction(val, item) {
    console.log(val, item);

    switch (val) {
      case 'Edit':
        {
          const dialogRef = this.dialog.open(CarrierAccountDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              title: 'Edit Carrier Account',
              actionButton: 'Edit',
              modalData: item,
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                item = result;
              }
            })
          );
        }
        break;
      case 'Delete':
        {
          const index: number = this.carrierList.indexOf(item);
          if (index !== -1) {
            this.carrierList.splice(index, 1);
          }
        }
        break;
      default:
        break;
    }
  }
  // end of item carrier settings function

  // add carrier account function
  addCarrierAccount() {
    const dialogRef = this.dialog.open(CarrierAccountDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'New Carrier Account',
        actionButton: 'Add',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.carrierList.push(result);
        }
      })
    );
  }
  // end of add carrier account function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
