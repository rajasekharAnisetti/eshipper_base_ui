import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileComponent } from './profile.component';
import { ProfileInfoComponent } from './profile-info/profile-info.component';
import { ProfileSecurityComponent } from './profile-security/profile-security.component';
import { ProfileGeneralSettingsComponent } from './profile-general-settings/profile-general-settings.component';
import { ProfileNotificationSettingsComponent } from './profile-notification-settings/profile-notification-settings.component';
import { ProfileCreditBillingComponent } from './profile-credit-billing/profile-credit-billing.component';

const profileRoutes: Routes = [
  {
    path: 'info',
    component: ProfileInfoComponent,
    data: {
      title: 'Info',
      backBtn: true,
    },
  },
  {
    path: 'security',
    component: ProfileSecurityComponent,
    data: {
      title: 'Password',
      backBtn: true,
    },
  },
  {
    path: 'general-settings',
    component: ProfileGeneralSettingsComponent,
    data: {
      title: 'General Settings',
      backBtn: true,
    },
  },
  {
    path: 'notification-settings',
    component: ProfileNotificationSettingsComponent,
    data: {
      title: 'Notification Settings',
      backBtn: true,
    },
  },
  {
    path: 'credit-billing',
    component: ProfileCreditBillingComponent,
    data: {
      title: 'Credit & Billing',
      backBtn: true,
    },
  },
];

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
    data: { title: 'Profile', root: true },
    children: profileRoutes,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule {}
