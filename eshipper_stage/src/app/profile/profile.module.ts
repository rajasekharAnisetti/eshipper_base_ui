import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { ProfileRoutingModule } from './profile-routing.module';

import { ProfileComponent } from './profile.component';
import { ProfileInfoComponent } from './profile-info/profile-info.component';
import { ProfileSecurityComponent } from './profile-security/profile-security.component';
import { ProfileGeneralSettingsComponent } from './profile-general-settings/profile-general-settings.component';
import { ProfileNotificationSettingsComponent } from './profile-notification-settings/profile-notification-settings.component';
import { NotificationShipmentComponent } from './profile-notification-settings/notification-shipment/notification-shipment.component';
import { NotificationInvoiceComponent } from './profile-notification-settings/notification-invoice/notification-invoice.component';
import { NotificationEcomComponent } from './profile-notification-settings/notification-ecom/notification-ecom.component';
import { NotificationClaimComponent } from './profile-notification-settings/notification-claim/notification-claim.component';
import { ProfileCreditBillingComponent } from './profile-credit-billing/profile-credit-billing.component';
import { NotificationFranchiseComponent } from './profile-notification-settings/notification-franchise/notification-franchise.component';

@NgModule({
  declarations: [
    ProfileComponent,
    ProfileInfoComponent,
    ProfileSecurityComponent,
    ProfileGeneralSettingsComponent,
    ProfileNotificationSettingsComponent,
    NotificationShipmentComponent,
    NotificationInvoiceComponent,
    NotificationEcomComponent,
    NotificationClaimComponent,
    ProfileCreditBillingComponent,
    NotificationFranchiseComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    NgxMaskModule,
    MaterialModules,
    SharedModule,
    ProfileRoutingModule,
  ],
  exports: [ProfileComponent]
})
export class ProfileModule { }
