import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WoLibraryServicesComponent } from './wo-library-services.component';

describe('WoLibraryServicesComponent', () => {
  let component: WoLibraryServicesComponent;
  let fixture: ComponentFixture<WoLibraryServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WoLibraryServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WoLibraryServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
