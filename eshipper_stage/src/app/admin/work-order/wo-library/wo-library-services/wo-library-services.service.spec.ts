import { TestBed } from '@angular/core/testing';

import { WoLibraryServicesService } from './wo-library-services.service';

describe('WoLibraryServicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WoLibraryServicesService = TestBed.get(WoLibraryServicesService);
    expect(service).toBeTruthy();
  });
});
