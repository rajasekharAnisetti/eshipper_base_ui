import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class WoLibraryServicesService {
  constructor(private http: HttpClient) {}

  getWoLibraryServices(): Observable<any> {
    return this.http
      .get('../../content/data/wo-library-services.json')
      .pipe(map(data => data));
  }
}
