import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { ServicesData } from 'src/app/interfaces/wo';
import { ServiceDialogComponent } from 'src/app/dialogs/service-dialog/service-dialog.component';
import { WoLibraryServicesService } from './wo-library-services.service';

@Component({
  selector: 'app-wo-library-services',
  templateUrl: './wo-library-services.component.html',
  styleUrls: ['./wo-library-services.component.scss'],
})
export class WoLibraryServicesComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  services: ServicesData;
  dataEmpty = false;
  toolbarSettings = ['Option 1', 'Option 2'];
  itemSettings = ['Edit', 'Delete'];

  constructor(
    public dialog: MatDialog,
    private getData: WoLibraryServicesService
  ) {}

  ngOnInit() {
    // get services
    this.subscribe.add(
      this.getData.getWoLibraryServices().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        data.content ? (this.services = data.content) : console.log('Error!!!');
      })
    );
  }

  searchFunction(val) {
    console.log(val);
  }

  toolbarSettingsFunction(val) {
    console.log(val);
  }

  itemSettingsFunction(val, item) {
    switch (val) {
      case 'Edit':
        console.log(item.blocks);
        const dialogRef = this.dialog.open(ServiceDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
          data: {
            title: 'Edit Service',
          },
        });
        this.subscribe.add(
          dialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;

      default:
        break;
    }
  }

  addNewService() {
    const dialogRef = this.dialog.open(ServiceDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'New Service',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
