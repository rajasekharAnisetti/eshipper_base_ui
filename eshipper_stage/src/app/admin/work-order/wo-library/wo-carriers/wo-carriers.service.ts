import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class WoCarriersService {
  constructor(private http: HttpClient) {}

  getWoCarriers(): Observable<any> {
    return this.http
      .get('content/data/wo-carriers.json')
      .pipe(map(data => data));
  }
}
