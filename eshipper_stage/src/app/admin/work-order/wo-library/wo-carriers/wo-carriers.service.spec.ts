import { TestBed } from '@angular/core/testing';

import { WoCarriersService } from './wo-carriers.service';

describe('WoCarriersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WoCarriersService = TestBed.get(WoCarriersService);
    expect(service).toBeTruthy();
  });
});
