import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { Carrier } from 'src/app/interfaces/carrier';
import { CarrierDialogComponent } from 'src/app/dialogs/carrier-dialog/carrier-dialog.component';

import { WoCarriersService } from './wo-carriers.service';

@Component({
  selector: 'app-wo-carriers',
  templateUrl: './wo-carriers.component.html',
  styleUrls: ['./wo-carriers.component.scss'],
})
export class WoCarriersComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  carriers: Array<Carrier>;
  dataEmpty = true;
  toolbarSettings = ['Option 1', 'Option 2'];
  itemSettings = ['Edit', 'Delete'];

  constructor(private getData: WoCarriersService, public dialog: MatDialog) {}

  ngOnInit() {
    // get carriers;
    this.subscribe.add(
      this.getData.getWoCarriers().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        data.content ? (this.carriers = data.content) : console.log('Error!!!');
      })
    );
  }

  searchFunction(val) {
    console.log(val);
  }

  toolbarSettingsFunction(val) {
    console.log(val);
  }

  itemSettingsFunction(val, item) {
    console.log(val, item);
    switch (val) {
      case 'Edit':
        {
          const dialogRef = this.dialog.open(CarrierDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              title: 'Edit Carrier',
              carrier: item,
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                item = result;
              }
            })
          );
        }
        break;

      default:
        break;
    }
  }

  addNewCarier() {
    const dialogRef = this.dialog.open(CarrierDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'New Carrier',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.carriers.push(result);
        }
      })
    );
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
