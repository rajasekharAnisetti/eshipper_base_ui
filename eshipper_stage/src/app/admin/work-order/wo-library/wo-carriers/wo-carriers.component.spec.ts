import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WoCarriersComponent } from './wo-carriers.component';

describe('WoCarriersComponent', () => {
  let component: WoCarriersComponent;
  let fixture: ComponentFixture<WoCarriersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WoCarriersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WoCarriersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
