import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WoLibraryComponent } from './wo-library.component';

describe('WoLibraryComponent', () => {
  let component: WoLibraryComponent;
  let fixture: ComponentFixture<WoLibraryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WoLibraryComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WoLibraryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
