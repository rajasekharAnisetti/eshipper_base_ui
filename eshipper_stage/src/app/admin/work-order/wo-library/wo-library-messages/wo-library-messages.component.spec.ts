import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WoLibraryMessagesComponent } from './wo-library-messages.component';

describe('WoLibraryMessagesComponent', () => {
  let component: WoLibraryMessagesComponent;
  let fixture: ComponentFixture<WoLibraryMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WoLibraryMessagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WoLibraryMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
