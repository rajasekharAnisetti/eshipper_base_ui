import { TestBed } from '@angular/core/testing';

import { WoLibraryMessagesService } from './wo-library-messages.service';

describe('WoLibraryMessagesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WoLibraryMessagesService = TestBed.get(WoLibraryMessagesService);
    expect(service).toBeTruthy();
  });
});
