import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class WoLibraryMessagesService {
  constructor(private http: HttpClient) {}

  getWoLibraryMessages(): Observable<any> {
    return this.http
      .get('../../content/data/wo-library-messages.json')
      .pipe(map(data => data));
  }
}
