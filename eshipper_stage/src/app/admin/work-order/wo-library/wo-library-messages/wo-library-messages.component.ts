import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { Message } from 'src/app/interfaces/wo';
import { MessageDialogComponent } from 'src/app/dialogs/message-dialog/message-dialog.component';

import { WoLibraryMessagesService } from './wo-library-messages.service';

@Component({
  selector: 'app-wo-library-messages',
  templateUrl: './wo-library-messages.component.html',
  styleUrls: ['./wo-library-messages.component.scss'],
})
export class WoLibraryMessagesComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  dataEmpty = false;
  toolbarSettings = ['Option 1', 'Option 2'];
  itemSettings = ['Edit', 'Delete'];
  messages: Message;

  constructor(
    public dialog: MatDialog,
    private getData: WoLibraryMessagesService
  ) {}

  ngOnInit() {
    // get messages
    this.subscribe.add(
      this.getData.getWoLibraryMessages().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        this.messages = data.content.length
          ? data.content
          : console.log('Error Data');
      })
    );
  }

  addNewMessage() {
    const dialogRef = this.dialog.open(MessageDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'New Message',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }

  searchFunction(val) {
    console.log(val);
  }

  toolbarSettingsFunction(val) {
    console.log(val);
  }

  itemSettingsFunction(val, item) {
    switch (val) {
      case 'Edit':
        const dialogRef = this.dialog.open(MessageDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
          data: {
            title: 'Edit Message',
            modalData: item,
          },
        });
        this.subscribe.add(
          dialogRef.afterClosed().subscribe(result => {
            if (result) {
              item = result;
            }
          })
        );
        break;

      default:
        break;
    }
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
