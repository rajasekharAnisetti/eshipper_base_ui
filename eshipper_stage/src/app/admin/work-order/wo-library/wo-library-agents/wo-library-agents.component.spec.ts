import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WoLibraryAgentsComponent } from './wo-library-agents.component';

describe('WoLibraryAgentsComponent', () => {
  let component: WoLibraryAgentsComponent;
  let fixture: ComponentFixture<WoLibraryAgentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WoLibraryAgentsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WoLibraryAgentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
