import { TestBed } from '@angular/core/testing';

import { WoLibraryAgentsService } from './wo-library-agents.service';

describe('WoLibraryAgentsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WoLibraryAgentsService = TestBed.get(WoLibraryAgentsService);
    expect(service).toBeTruthy();
  });
});
