import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class WoLibraryAgentsService {
  constructor(private http: HttpClient) {}

  getWoLibraryAgents(): Observable<any> {
    return this.http
      .get('../../content/data/wo-library-agents.json')
      .pipe(map(data => data));
  }

  getWoLibraryAreas(): Observable<any> {
    return this.http
      .get('../../content/data/wo-library-areas.json')
      .pipe(map(data => data));
  }
}
