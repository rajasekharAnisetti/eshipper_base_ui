import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { Agent, Area } from 'src/app/interfaces/wo';
import { AgentDialogComponent } from 'src/app/dialogs/agent-dialog/agent-dialog.component';
import { WoLibraryAgentsService } from './wo-library-agents.service';

@Component({
  selector: 'app-wo-library-agents',
  templateUrl: './wo-library-agents.component.html',
  styleUrls: ['./wo-library-agents.component.scss'],
})
export class WoLibraryAgentsComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  dataEmpty = false;
  agents: Array<Agent> = [];
  countries = ['Canada', 'United States', 'Other'];
  itemSettings = ['Edit', 'Delete'];
  areasList: Array<Area> = [];

  constructor(
    public dialog: MatDialog,
    private getData: WoLibraryAgentsService
  ) {}

  ngOnInit() {
    // get agents
    this.subscribe.add(
      this.getData.getWoLibraryAgents().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        data.content.length
          ? (this.agents = data.content)
          : console.log('Error!!!');
      })
    );
    // get areas
    this.subscribe.add(
      this.getData.getWoLibraryAreas().subscribe(data => {
        this.areasList = data;
      })
    );
  }

  toolbarSelect(val) {
    console.log(val);
  }

  searchFunction(val) {
    console.log(val);
  }

  addNewAgent() {
    const dialogRef = this.dialog.open(AgentDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'New Agent',
        dataList: this.areasList,
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }

  itemSettingsFunction(val, item) {
    console.log(val, item);
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
