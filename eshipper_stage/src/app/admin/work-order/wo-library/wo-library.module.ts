import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModules } from '../../../material-modules';

import { WoLibraryServicesModule } from './wo-library-services/wo-library-services.module';
import { WoLibraryMessagesModule } from './wo-library-messages/wo-library-messages.module';
import { WoLibraryAgentsModule } from './wo-library-agents/wo-library-agents.module';
import { WoCarriresModule } from './wo-carriers/wo-carrires.module';

import { WoLibraryComponent } from './wo-library.component';
@NgModule({
  declarations: [WoLibraryComponent],
  imports: [
    CommonModule,
    MaterialModules,
    WoLibraryServicesModule,
    WoLibraryMessagesModule,
    WoLibraryAgentsModule,
    WoCarriresModule,
  ],
  exports: [WoLibraryComponent],
})
export class WoLibraryModule {}
