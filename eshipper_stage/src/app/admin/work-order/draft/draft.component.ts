import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { DraftFilterDialogComponent } from 'src/app/filters/draft-filter-dialog/draft-filter-dialog.component';
import { WarningDialogComponent } from 'src/app/dialogs/warning-dialog/warning-dialog.component';
import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';
import { ApprovedDialogComponent } from 'src/app/dialogs/approved-dialog/approved-dialog.component';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { PrintDialogComponent } from 'src/app/dialogs/print-dialog/print-dialog.component';
import { SIMPLESETTINGS, EDSETTINGS } from 'src/app/shared/const/dropdowns';

import { DraftService } from './draft.service';

@Component({
  selector: 'app-draft',
  templateUrl: './draft.component.html',
  styleUrls: ['./draft.component.scss'],
})
export class DraftComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  disabledToolbarBtn = true;
  dataEmpty = false;
  selection = false;
  toolbarIndeterminate = false;
  listData: Array<any>;
  selectAll = false;
  toolbarSettings = SIMPLESETTINGS;
  itemSettings = EDSETTINGS;
  buttonItems = ['Submit As Quote', 'Submit As Live WO'];

  constructor(
    private router: Router,
    private getData: DraftService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    // get drafts
    this.subscribe.add(
      this.getData.getDraft().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        data.content ? (this.listData = data.content) : console.log('Error!!!');
      })
    );
  }

  searchFunction(val) {
    console.log(val);
  }

  setTrackFlag() {
    localStorage.setItem('trackFlag', 'draft');
  }

  settingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print':
        const printDialogRef = this.dialog.open(PrintDialogComponent, {
          autoFocus: false,
          panelClass: 'small-dialog-width',
        });
        this.subscribe.add(
          printDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }

  // period filter function
  getPeriod(period) {
    console.log(period);
  }
  // end of period filter function

  itemSettingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print':
        const printDialogRef = this.dialog.open(PrintDialogComponent, {
          autoFocus: false,
          panelClass: 'small-dialog-width',
        });
        this.subscribe.add(
          printDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }

  doubleButtonClick(button) {
    switch (button) {
      case 'Submit As Quote':
        {
          const dialogRef = this.dialog.open(ApprovedDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              title: 'Quote WO# 12345678 Submitted',
              content:
                'We will send the quote notification to the customer shortly.',
              buttons: {
                buttonLeft: 'Check Quote',
                buttonRight: 'New WO',
              },
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
                switch (result) {
                  case 'Check Quote':
                    this.router.navigate(['/admin/work-order/job-board']);
                    break;

                  default:
                    break;
                }
              }
            })
          );
        }
        break;
      case 'Submit As Live WO':
        {
          const dialogRefLiveWO = this.dialog.open(ApprovedDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              title: 'Live WO# 12345678 Submitted',
              content: 'We will send the notification to the customer shortly.',
              buttons: {
                buttonLeft: 'New WO',
                buttonRight: 'Continue',
              },
            },
          });
          this.subscribe.add(
            dialogRefLiveWO.afterClosed().subscribe(result => {
              if (result) {
                switch (result) {
                  case 'Continue':
                    this.router.navigate([
                      '/admin/work-order/create-work-order-segments',
                    ]);
                    break;

                  default:
                    break;
                }
              }
            })
          );
        }
        break;
      default:
        break;
    }
  }

  // select functions
  selectAllFunction(flag) {
    const select = flag ? true : false;
    this.selection = select ? true : false;
    for (const i of this.listData) {
      i.selected = select;
    }
  }

  selectRow(val, el) {
    const n = this.listData.length;
    let item = 0;
    let quoteLine = 0;
    console.log(val, el);
    for (const i of this.listData) {
      if (i.quoteItem && i.selected) {
        quoteLine++;
      }
      if (i.selected) {
        item++;
      }
    }

    this.disabledToolbarBtn =
      item === quoteLine && quoteLine > 0 ? false : true;
    this.selectAll = n === item ? true : false;
    this.toolbarIndeterminate = n > item && item !== 0 ? true : false;
    this.selection = this.selectAll || this.toolbarIndeterminate ? true : false;
  }
  // end of select functions

  // quote details
  openQuoteDetailsDialog(data) {
    this.dialog.open(InfoDialogComponent, {
      data: {
        title: 'Quote Details',
        moreData: data,
      },
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
  }
  // end of quote details

  // double button function
  clickDoubleBtn(val, item) {
    console.log(item);
    switch (val) {
      case 'Quote':
        {
          const dialogRef = this.dialog.open(ApprovedDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              title: 'Quote WO# 12345678 Submitted',
              content:
                'We will send the quote notification to the customer shortly.',
              buttons: {
                buttonLeft: 'Check Quote',
                buttonRight: 'New WO',
              },
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
                switch (result) {
                  case 'Check Quote':
                    this.router.navigate(['/admin/work-order/job-board']);
                    break;

                  default:
                    break;
                }
              }
            })
          );
        }
        break;
      case 'Live WO':
        {
          const dialogRefLiveWO = this.dialog.open(ApprovedDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              title: 'Live WO# 12345678 Submitted',
              content: 'We will send the notification to the customer shortly.',
              buttons: {
                buttonLeft: 'New WO',
                buttonRight: 'Continue',
              },
            },
          });
          this.subscribe.add(
            dialogRefLiveWO.afterClosed().subscribe(result => {
              if (result) {
                switch (result) {
                  case 'Continue':
                    this.router.navigate([
                      '/admin/work-order/create-work-order-segments',
                    ]);
                    break;

                  default:
                    break;
                }
              }
            })
          );
        }
        break;
      default:
        break;
    }
  }
  // end of double button function

  // row click function
  rowClickFunction(item) {
    this.setTrackFlag();
    const url = '/admin/work-order/draft/draft-details';
    this.router.navigate([url]);
  }
  // end of row click function

  getItemSettingsValue(val) {
    switch (val) {
      case 'Edit':
        this.router.navigate(['/admin/work-order/accepted-work-order']);
        break;
      case 'Delete':
        {
          const dialogRef = this.dialog.open(WarningDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              message: 'Warning message for edit default markup goes here...',
              action: 'Delete',
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;
      default:
        break;
    }
  }

  // filter function
  filter() {
    const dialogRef = this.dialog.open(DraftFilterDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'New Service',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of filter function

  // get sort option
  getSortData(data) {
    console.log(data);
  }
  // end of get sort option

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
