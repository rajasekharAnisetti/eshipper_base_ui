import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DraftService {
  constructor(private http: HttpClient) {}

  getDraft(): Observable<any> {
    return this.http
      .get('content/data/work-order-draft.json')
      .pipe(map(data => data));
  }
}
