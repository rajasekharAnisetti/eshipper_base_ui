import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Segment } from 'src/app/interfaces/wo';

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.scss'],
})
export class AlertsComponent implements OnInit {
  minDate = new Date();
  newFlag = localStorage.getItem('newAlerts');
  agents = ['Peter Moss', 'Agent 2', 'Agent 3'];
  services = ['LTL', 'Service 2', 'Service 3'];
  locationTypes = ['Airport', 'Company'];
  missions = ['Pickup', 'Delivery'];
  data = <Segment>{};

  // delivery

  validation = {
    agent: {
      name: new FormControl(),
      service: new FormControl(),
      phone: new FormControl(),
      contactName: new FormControl(),
    },
    stops: [],
    pickup: {
      locationType: new FormControl(),
      location: new FormControl(),
      date: new FormControl(),
      time: new FormControl(),
      phone: new FormControl(),
      contactName: new FormControl(),
    },
    delivery: {
      locationType: new FormControl(),
      location: new FormControl(),
      date: new FormControl(),
      time: new FormControl(),
      phone: new FormControl(),
      contactName: new FormControl(),
      note: new FormControl(),
    },
  };

  constructor() { }

  ngOnInit() {
    const editData = localStorage.getItem('alertsData');
    if (editData) {
      this.data = JSON.parse(editData);
    }
  }

  getPickupTime = val => (this.data.pickup.time = val);

  getDeliveryTime = val => (this.data.delivery.time = val);

  getStopTime(val, item) {
    console.log(val, item);
  }

  // stop
  addStop() {
    this.data.stops.push({
      locationType: '',
      location: '',
      date: '',
      time: '',
      mission: '',
      active: false,
      current: false,
    });
  }

  closeStop(i) {
    this.data.stops.splice(i, 1);
  }
  // end of stop

  // save
  save() { }
  // end of save
}
