import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CreateWoSegmentsService {
  constructor(private http: HttpClient) {}

  // segments sidebar data
  getSegmentsDetailsSidebar(): Observable<any> {
    return this.http
      .get('content/data/work-order-segments-sidebar.json')
      .pipe(map(data => data));
  }

  // segments data
  getSegments(): Observable<any> {
    return this.http
      .get('content/data/work-order-segments.json')
      .pipe(map(data => data));
  }
}
