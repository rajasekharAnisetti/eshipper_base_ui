import {
  ChangeDetectorRef,
  Component,
  OnInit,
  OnDestroy,
  ElementRef,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MediaMatcher } from '@angular/cdk/layout';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { Segment } from 'src/app/interfaces/wo';
import { DetailsData } from 'src/app/interfaces/details-data';
import { SegmentDialogComponent } from 'src/app/dialogs/segment-dialog/segment-dialog.component';
import { GeneratePdfDialogComponent } from 'src/app/dialogs/generate-pdf-dialog/generate-pdf-dialog.component';
import { WarningDialogComponent } from 'src/app/dialogs/warning-dialog/warning-dialog.component';
import { SlideInOutAnimation } from 'src/app/shared/functions/animate';
import {
  SEGMENTSALARMSETTINGS,
  SEGMENTSITEMSETTINGS,
} from 'src/app/shared/const/dropdowns';

import { CreateWoSegmentsService } from './create-wo-segments.service';

@Component({
  selector: 'app-create-work-order-segments',
  templateUrl: './create-work-order-segments.component.html',
  styleUrls: ['./create-work-order-segments.component.scss'],
  animations: [SlideInOutAnimation],
})
export class CreateWorkOrderSegmentsComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  mobileQuery: MediaQueryList;
  detailsData = <DetailsData>{};
  nextDisable = true;
  additionalDetails = [
    {
      label: 'Service',
      value: 'LTL',
    },
    {
      label: 'Quote',
      value: 789.5,
      currency: true,
    },
  ];
  // steps
  steps = [
    {
      position: 'Details',
      state: true,
      link: '/admin/work-order/accepted-work-order',
    },
    {
      position: 'Segments',
      state: false,
      link: '',
    },
    {
      position: 'Cost & Docs',
      state: false,
      link: '',
    },
    {
      position: 'Summary',
      state: false,
      link: '',
    },
  ];

  itemSettings = SEGMENTSITEMSETTINGS;
  itemSettingsAlarm = SEGMENTSALARMSETTINGS;

  // segments data
  segments: Array<Segment>;

  innerSidebarHeight: number;
  @ViewChild('innerSidebar', { static: true }) elementView: ElementRef;

  // edit mode
  urlData: any;
  editMode = false;
  // end of edit mode

  private _mobileQueryListener: () => void;

  constructor(
    private getData: CreateWoSegmentsService,
    private dialog: MatDialog,
    public router: Router,
    private routerData: ActivatedRoute,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 1024px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    this.subscribe.add(
      this.getData
        .getSegmentsDetailsSidebar()
        .subscribe(data => (this.detailsData = data))
    );

    this.subscribe.add(
      this.getData
        .getSegments()
        .subscribe(data => (this.segments = data.content))
    );
    // get edit mode flag
    this.subscribe.add(
      (this.urlData = this.routerData.data.subscribe(data => {
        if (data) {
          this.editMode = data.editMode;
        }
      }))
    );
    // end of get edit mode flag

    setTimeout(() => {
      this.innerSidebarHeight = this.elementView.nativeElement.offsetHeight;
      console.log(this.innerSidebarHeight);
    }, 1);

    this.nextDisable = !localStorage.getItem('segmentsFlag');
  }

  // add segment function
  addSegment() {
    const dialogRef = this.dialog.open(SegmentDialogComponent, {
      autoFocus: false,
      panelClass: 'normal-dialog-width',
      data: {
        title: 'Add Segment',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
          localStorage.setItem('segmentsFlag', 'true');
          this.nextDisable = false;
        }
      })
    );
  }
  // end of add segment function

  itemSettingsFunction(val, item) {
    switch (val) {
      case 'Edit Segment':
        switch (item.carrier.type) {
          case 'air':
            const dialogRefFlight = this.dialog.open(SegmentDialogComponent, {
              autoFocus: false,
              panelClass: 'normal-dialog-width',
              data: {
                title: 'Edit Flight Segment',
                modalData: {
                  type: 'Air',
                  airwayBill: '',
                  tracking: '',
                  carrier: item.carrier.name,
                  flights: item.carrier.description,
                  service: '',
                  carrierSpotRate: '',
                  customerAccount: '',
                  departure: {
                    airport: '',
                    date: '',
                    time: '',
                  },
                  arrival: {
                    airport: '',
                    date: '',
                    time: '',
                  },
                },
              },
            });
            this.subscribe.add(
              dialogRefFlight.afterClosed().subscribe(result => {
                if (result) {
                  console.log(result);
                  this.nextDisable = false;
                }
              })
            );
            break;
          case 'truck':
            const dialogRefTruck = this.dialog.open(SegmentDialogComponent, {
              autoFocus: false,
              panelClass: 'normal-dialog-width',
              data: {
                title: 'Edit Truck Segment',
                modalData: {
                  type: 'Truck',
                  carrier: item.carrier.name,
                  bol: item.blocks[0] ? item.blocks[0].value : '',
                  paps: item.blocks[1] ? item.blocks[1].value : '',
                  pars: item.blocks[2] ? item.blocks[2].value : '',
                  pro: item.blocks[3] ? item.blocks[3].value : '',
                },
              },
            });
            this.subscribe.add(
              dialogRefTruck.afterClosed().subscribe(result => {
                if (result) {
                  console.log(result);
                }
              })
            );
            break;
          default:
            break;
        }
        break;
      case 'Edit Alert':
        {
          const url =
            '/admin/work-order/create-work-order-segments/edit-alerts';
          localStorage.setItem('newAlerts', 'false');
          this.router.navigate([url]);
        }
        break;
      case 'Delete Segment':
        {
          const warningDialogRef = this.dialog.open(WarningDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              message: 'Warning message for edit default markup goes here...',
              action: 'Delete',
            },
          });
          this.subscribe.add(
            warningDialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;
      case 'Delete Alert':
        {
          const warningDialogRef = this.dialog.open(WarningDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              message: 'Warning message for edit default markup goes here...',
              action: 'Delete',
            },
          });
          this.subscribe.add(
            warningDialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;
      case 'Export':
        const dialogRefPDF = this.dialog.open(GeneratePdfDialogComponent, {
          autoFocus: false,
          panelClass: 'normal-dialog-width',
        });
        this.subscribe.add(
          dialogRefPDF.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }

  addAlarm(item) {
    const url = '/admin/work-order/create-work-order-segments/add-alerts';
    localStorage.setItem('newAlerts', 'false');
    localStorage.setItem('alertsData', JSON.stringify(item));
    this.router.navigate([url]);
  }

  ngOnDestroy() {
    this.mobileQuery.removeListener(this._mobileQueryListener);
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
