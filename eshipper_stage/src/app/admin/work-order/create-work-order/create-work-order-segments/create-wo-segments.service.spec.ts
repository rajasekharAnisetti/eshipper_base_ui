import { TestBed } from '@angular/core/testing';

import { CreateWoSegmentsService } from './create-wo-segments.service';

describe('CreateWoSegmentsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreateWoSegmentsService = TestBed.get(CreateWoSegmentsService);
    expect(service).toBeTruthy();
  });
});
