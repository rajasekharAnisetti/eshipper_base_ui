import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateWorkOrderSegmentsComponent } from './create-work-order-segments.component';

describe('CreateWorkOrderSegmentsComponent', () => {
  let component: CreateWorkOrderSegmentsComponent;
  let fixture: ComponentFixture<CreateWorkOrderSegmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateWorkOrderSegmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateWorkOrderSegmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
