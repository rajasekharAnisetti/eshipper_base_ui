import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

import { WOConfirmedData } from 'src/app/interfaces/wo';
import { EmailDialogComponent } from 'src/app/dialogs/email-dialog/email-dialog.component';
import { PrintDialogComponent } from 'src/app/dialogs/print-dialog/print-dialog.component';

import { WoConfirmedService } from './wo-confirmed.service';

@Component({
  selector: 'app-work-order-confirmed',
  templateUrl: './work-order-confirmed.component.html',
  styleUrls: ['./work-order-confirmed.component.scss'],
})
export class WorkOrderConfirmedComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  confirmedData = <WOConfirmedData>{};

  constructor(private getData: WoConfirmedService, public dialog: MatDialog) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData
        .getWOConfirmed()
        .subscribe(data => (this.confirmedData = data))
    );
  }

  // email function
  emailFunction() {
    const dialogRef = this.dialog.open(EmailDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {},
    });

    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of email function

  // print function
  printFunction() {
    const printDialogRef = this.dialog.open(PrintDialogComponent, {
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
    this.subscribe.add(
      printDialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of print function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
