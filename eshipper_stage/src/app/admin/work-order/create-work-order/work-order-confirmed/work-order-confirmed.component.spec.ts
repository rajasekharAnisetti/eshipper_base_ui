import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkOrderConfirmedComponent } from './work-order-confirmed.component';

describe('WorkOrderConfirmedComponent', () => {
  let component: WorkOrderConfirmedComponent;
  let fixture: ComponentFixture<WorkOrderConfirmedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkOrderConfirmedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkOrderConfirmedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
