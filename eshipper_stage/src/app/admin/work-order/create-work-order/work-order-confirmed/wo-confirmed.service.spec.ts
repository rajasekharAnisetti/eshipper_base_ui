import { TestBed } from '@angular/core/testing';

import { WoConfirmedService } from './wo-confirmed.service';

describe('WoConfirmedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WoConfirmedService = TestBed.get(WoConfirmedService);
    expect(service).toBeTruthy();
  });
});
