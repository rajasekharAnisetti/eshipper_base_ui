import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class WoConfirmedService {
  constructor(private http: HttpClient) {}

  getWOConfirmed(): Observable<any> {
    return this.http
      .get('content/data/wo-confirmed.json')
      .pipe(map(data => data));
  }
}
