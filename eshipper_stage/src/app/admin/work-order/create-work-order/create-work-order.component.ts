import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';

import { LocationData } from 'src/app/interfaces/location';
import { AddressBookDialogComponent } from 'src/app/dialogs/address-book-dialog/address-book-dialog.component';
import { ApprovedDialogComponent } from 'src/app/dialogs/approved-dialog/approved-dialog.component';
import { CURRENCIES_$ } from 'src/app/shared/const/currencies';
import { TIMEZONES } from 'src/app/shared/const/dropdowns';
import { QtyMatcher } from 'src/app/shared/classes/validation-class';

import { CreateWorkOrderService } from './create-work-order.service';

@Component({
  selector: 'app-create-work-order',
  templateUrl: './create-work-order.component.html',
  styleUrls: ['./create-work-order.component.scss'],
})
export class CreateWorkOrderComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  minDate = new Date();
  fromFilteredLocations: Observable<LocationData[]>;
  toFilteredLocations: Observable<LocationData[]>;
  locations = [];
  buttonItms = ['Submit As Quote', 'Submit As Live WO'];

  // steps
  steps = [];
  stepsCreate = [
    {
      position: 'Details',
      state: false,
    },
    {
      position: 'Segments',
      state: false,
    },
    {
      position: 'Cost & Docs',
      state: false,
    },
    {
      position: 'Summary',
      state: false,
    },
  ];
  stepsEdit = [
    {
      position: 'Details',
      state: true,
      link: '/admin/work-order/accepted-work-order',
    },
    {
      position: 'Segments',
      state: true,
      link: '/admin/work-order/accepted-work-order-segments',
    },
    {
      position: 'Cost & Docs',
      state: true,
      link: '/admin/work-order/accepted-work-order-cost-docs',
    },
    {
      position: 'Summary',
      state: true,
      link: '/admin/work-order/accepted-work-order-confirmed',
    },
  ];
  // location types
  locationTypes = [
    {
      value: 'address',
      viewValue: 'Address',
    },
    {
      value: 'airport',
      viewValue: 'Airport',
    },
    {
      value: 'option 3',
      viewValue: 'Option 3',
    },
  ];
  // work data
  workingData = {
    from: {
      values: {
        value: '',
        locationType: '',
        data: {
          companyName: '',
          address1: '',
          address2: '',
          city: '',
          province: '',
          zip: '',
          country: '',
          attention: '',
          phone: '',
          email: '',
          taxId: '',
        },
      },
      locationTypeControl: new FormControl(),
      locationControl: new FormControl(),
      checkboxes: [],
    },
    to: {
      values: {
        value: '',
        locationType: '',
        data: {
          companyName: '',
          address1: '',
          address2: '',
          city: '',
          province: '',
          zip: '',
          country: '',
          attention: '',
          phone: '',
          email: '',
          taxId: '',
        },
      },
      locationTypeControl: new FormControl(),
      locationControl: new FormControl(),
      checkboxes: [
        {
          checked: false,
          label: 'Residential',
        },
      ],
    },
  };
  fromFlag = false;
  toFlag = false;

  // job details
  jobDetailsServices = [
    {
      value: 'Next Flight Out',
      viewValue: 'Next Flight Out',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];
  timeZones = TIMEZONES;
  salesList = [
    {
      value: 'Option 1',
      viewValue: 'Option 1',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];

  jobDetails = {
    date: '',
    time: '', // format 00:00
    specialInstructions: '',
    deliveryDeadline: '',
    deliveryDeadlineControl: new FormControl(),
    customer: '',
    customerControl: new FormControl(),
    service: '',
    serviceControl: new FormControl(),
    job: '',
    jobControl: new FormControl(),
    assignTo: '',
    assignToControl: new FormControl(),
    timeZone: '',
    timeZoneControl: new FormControl(),
    sales: '',
    salesControl: new FormControl(),
    originalCost: '',
    originalCostControl: new FormControl(),
    quotedAmount: '',
    quotedAmountControl: new FormControl(),
  };
  // end of job details

  // shipment
  currencyItems = CURRENCIES_$;
  systemItems = ['Metric', 'Imperial'];
  insuranceTypes = [
    {
      value: 'eshipper',
      viewValue: 'eShipper',
    },
    {
      value: 'option2',
      viewValue: 'Option 2',
    },
    {
      value: 'option3',
      viewValue: 'Option 2',
    },
  ];
  freightClasses = [
    {
      value: '50',
      viewValue: '50',
    },
    {
      value: '100',
      viewValue: '100',
    },
    {
      value: '150',
      viewValue: '150',
    },
  ];
  types = [
    {
      value: 'Type 1',
      viewValue: 'Type 1',
    },
    {
      value: 'Type 2',
      viewValue: 'Type 2',
    },
    {
      value: 'Type 3',
      viewValue: 'Type 3',
    },
  ];

  shipment = {
    currencyValue: '',
    systemValue: '',
    shippingDateValue: '',
    shippingDateControl: new FormControl(),
    qty: '1',
    qtyControl: new FormControl(),
    insuranceType: '',
    insuranceTypeControl: new FormControl(),
    totalWeight: 10,
    palletForm: [
      {
        lengthControl: new FormControl(),
        length: '',
        widthControl: new FormControl(),
        width: '',
        heightControl: new FormControl(),
        height: '',
        weightControl: new FormControl(),
        weight: '',
        freightClassControl: new FormControl(),
        freightClass: '',
        nmfc: '',
        typeControl: new FormControl(),
        typeClass: '',
        insurance: '',
        insuranceTooltip: 'Info about the field',
        description: '',
        button: '',
      },
    ],
  };
  // end of shipment

  // edit mode
  urlData: any;
  editMode = false;
  // end of edit mode

  qtyMatcher = new QtyMatcher();

  constructor(
    public dialog: MatDialog,
    private getData: CreateWorkOrderService,
    private router: Router,
    private routerData: ActivatedRoute
  ) { }

  ngOnInit() {
    // from to functions
    this.subscribe.add(
      this.getData.getShippingLocations().subscribe(data => {
        this.locations = data;
        this.initAutocomplete();
      })
    );
    // end of from to functions
    // get edit mode flag
    this.subscribe.add(
      (this.urlData = this.routerData.data.subscribe(data => {
        if (data) {
          this.editMode = data.editMode;
        }
        this.steps = this.editMode ? this.stepsEdit : this.stepsCreate;
      }))
    );
    // end of get edit mode flag
  }

  // from to functions
  private _fromFilterStates(value: string): LocationData[] {
    const filterValue = value.toLowerCase();
    this.workingData.from.values.data = this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    )[0];
    return this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    );
  }

  private _toFilterStates(value: string): LocationData[] {
    const filterValue = value.toLowerCase();
    this.workingData.to.values.data = this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    )[0];
    return this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    );
  }

  initAutocomplete() {
    this.fromFilteredLocations = this.workingData.from.locationControl.valueChanges.pipe(
      startWith(''),
      map(location =>
        location ? this._fromFilterStates(location) : this.locations.slice()
      )
    );
    this.toFilteredLocations = this.workingData.to.locationControl.valueChanges.pipe(
      startWith(''),
      map(location =>
        location ? this._toFilterStates(location) : this.locations.slice()
      )
    );
  }

  newLocation(val) {
    const dialogRef = this.dialog.open(AddressBookDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'New Location',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          switch (val) {
            case 'from':
              this.locations.push(result);
              this.workingData.from.values.data = result;
              break;
            case 'to':
              this.locations.push(result);
              this.workingData.to.values.data = result;
              break;
            default:
              break;
          }
          this.initAutocomplete();
        }
      })
    );
  }

  changePosition() {
    const values = this.workingData.from.values;
    this.workingData.from.values = this.workingData.to.values;
    this.workingData.to.values = values;
  }

  // change flom location field
  changeFrom() {
    this.fromFlag = false;
    for (const item of this.locations) {
      if (this.workingData.from.values.value === item.companyName) {
        this.fromFlag = true;
      }
    }
  }
  // change To location field
  changeTo() {
    this.toFlag = false;
    for (const item of this.locations) {
      if (this.workingData.to.values.value === item.companyName) {
        this.toFlag = true;
      }
    }
  }
  // end of from to functions

  // job details functions
  jobDetailsTime(val) {
    console.log(val);
  }
  getDeliveryDeadline(val) {
    console.log(val);
  }
  // end of job details functions

  // shipment functions
  shipmentCurrencyChange(val) {
    console.log(val);
  }
  shipmentSystemChange(val) {
    console.log(val);
  }
  getShippingDate() { }

  getShippingTime(time) {
    console.log(time)
  }
  // end of shipment functions

  // main buttons
  doubleButtonClick(button) {
    switch (button) {
      case 'Submit As Quote':
        const dialogRef = this.dialog.open(ApprovedDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
          data: {
            title: 'Quote WO# 12345678 Submitted',
            content:
              'We will send the quote notification to the customer shortly.',
            buttons: {
              buttonRight: 'Close',
            },
          },
        });
        this.subscribe.add(
          dialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
              switch (result) {
                case 'Check Quote':
                  this.router.navigate(['/admin/work-order/job-board']);
                  break;

                default:
                  break;
              }
            }
          })
        );
        break;
      case 'Submit As Live WO':
        const dialogRefLiveWO = this.dialog.open(ApprovedDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
          data: {
            title: 'Live WO# 12345678 Submitted',
            content: 'We will send the notification to the customer shortly.',
            buttons: {
              buttonLeft: 'Cancel',
              buttonRight: 'Continue',
            },
          },
        });
        this.subscribe.add(
          dialogRefLiveWO.afterClosed().subscribe(result => {
            if (result) {
              switch (result) {
                case 'Continue':
                  this.router.navigate([
                    '/admin/work-order/create-work-order-segments',
                  ]);
                  break;

                default:
                  break;
              }
            }
          })
        );
        break;
      default:
        break;
    }
  }

  // change qty
  qtyFunction(qty) {
    setTimeout(() => {
      this.shipment.palletForm = [
        {
          lengthControl: new FormControl(),
          length: '',
          widthControl: new FormControl(),
          width: '',
          heightControl: new FormControl(),
          height: '',
          weightControl: new FormControl(),
          weight: '',
          freightClassControl: new FormControl(),
          freightClass: '',
          nmfc: '',
          typeControl: new FormControl(),
          typeClass: '',
          insurance: '',
          insuranceTooltip: 'Info about the field',
          description: '',
          button: 'All the Same',
        },
      ];
      if (qty > 1 && qty <= 35) {
        for (let i = 1; i <= qty - 1; i++) {
          this.shipment.palletForm.push({
            lengthControl: new FormControl(),
            length: '',
            widthControl: new FormControl(),
            width: '',
            heightControl: new FormControl(),
            height: '',
            weightControl: new FormControl(),
            weight: '',
            freightClassControl: new FormControl(),
            freightClass: '',
            nmfc: '',
            typeControl: new FormControl(),
            typeClass: '',
            insurance: '',
            insuranceTooltip: 'Info about the field',
            description: '',
            button: 'Same As Above',
          });
        }
      }
    }, 100);
  }
  // end of change qty

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
