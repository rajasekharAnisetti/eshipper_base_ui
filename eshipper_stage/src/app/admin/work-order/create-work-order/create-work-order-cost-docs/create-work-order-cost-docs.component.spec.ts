import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateWorkOrderCostDocsComponent } from './create-work-order-cost-docs.component';

describe('CreateWorkOrderCostDocsComponent', () => {
  let component: CreateWorkOrderCostDocsComponent;
  let fixture: ComponentFixture<CreateWorkOrderCostDocsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateWorkOrderCostDocsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateWorkOrderCostDocsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
