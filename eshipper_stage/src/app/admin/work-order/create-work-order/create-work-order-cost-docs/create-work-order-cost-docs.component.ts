import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { ServiceDialogComponent } from 'src/app/dialogs/service-dialog/service-dialog.component';
import { RenameDialogComponent } from 'src/app/dialogs/rename-dialog/rename-dialog.component';
import { CURRENCIES_$ } from 'src/app/shared/const/currencies';

@Component({
  selector: 'app-create-work-order-cost-docs',
  templateUrl: './create-work-order-cost-docs.component.html',
  styleUrls: ['./create-work-order-cost-docs.component.scss'],
})
export class CreateWorkOrderCostDocsComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  minDate = new Date();
  // steps
  steps = [];
  stepsCreate = [
    {
      position: 'Details',
      state: true,
      link: '/admin/work-order/accepted-work-order',
    },
    {
      position: 'Segments',
      state: true,
      link: '/admin/work-order/accepted-work-order-segments',
    },
    {
      position: 'Cost & Docs',
      state: false,
    },
    {
      position: 'Summary',
      state: false,
    },
  ];
  stepsEdit = [
    {
      position: 'Details',
      state: true,
      link: '/admin/work-order/accepted-work-order',
    },
    {
      position: 'Segments',
      state: true,
      link: '/admin/work-order/accepted-work-order-segments',
    },
    {
      position: 'Cost & Docs',
      state: true,
      link: '/admin/work-order/accepted-work-order-cost-docs',
    },
    {
      position: 'Summary',
      state: true,
      link: '/admin/work-order/accepted-work-order-confirmed',
    },
  ];

  pod = {
    signedBy: '',
    reportTo: '',
    date: '',
    time: '',
    internalComments: '',
  };

  // validation
  validation = {
    pod: {
      signedBy: new FormControl(),
      reportTo: new FormControl(),
      date: new FormControl(),
      internalComments: new FormControl(),
    },
  };

  // doc settings
  docSettings = ['Download', 'Rename', 'Delete'];

  docs = [
    {
      name: 'Doc Name.pdf',
      state: 'Ptivate',
    },
    {
      name: 'Doc Name.pdf',
      state: 'Ptivate',
    },
    {
      name: 'Doc Name.pdf',
      state: 'Ptivate',
    },
  ];

  // Price Breakdown
  currencyValue = 'CAD $';
  currencyItems = CURRENCIES_$;

  select1List = [
    {
      value: 'Pick Up & Delivery',
      viewValue: 'Pick Up & Delivery',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];
  serviseNameList = [
    {
      value: 'Deliver Docs to Terminal',
      viewValue: 'Deliver Docs to Terminal',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];
  select3List = [
    {
      value: '1',
      viewValue: '1',
    },
    {
      value: '2',
      viewValue: '2',
    },
    {
      value: '3',
      viewValue: '3',
    },
  ];

  priceBreakdown = [
    {
      select1: 'Pick Up & Delivery',
      serviseName: 'Deliver Docs to Terminal',
      cost: {
        label: 'Cost',
        value: 500.0,
        currency: true,
        right: true,
      },
      charge: {
        label: 'Charge',
        value: 600.0,
        currency: true,
        right: true,
      },
      select3: '3',
      totalPrice: {
        label: 'Total Price',
        value: 600.0,
        currency: true,
        right: true,
      },
    },
  ];

  totalBox = {
    subtotal: '$ 154,500.00',
    taxes: [
      {
        tax: 'Tax 1',
        taxValue: '$ 500.00',
      },
      {
        tax: 'Tax 1',
        taxValue: '$ 500.00',
      },
      {
        tax: 'Tax 1',
        taxValue: '$ 500.00',
      },
    ],
    total: '$ 154,500.00',
  };

  taxes = [
    {
      value: 'Tax 1',
      viewValue: 'Tax 1',
    },
    {
      value: 'Tax 2',
      viewValue: 'Tax 2',
    },
    {
      value: 'Tax 3',
      viewValue: 'Tax 3',
    },
  ];

  // edit mode
  urlData: any;
  editMode = false;
  // end of edit mode

  constructor(public dialog: MatDialog, private routerData: ActivatedRoute) { }

  ngOnInit() {
    // get edit mode flag
    this.subscribe.add(
      (this.urlData = this.routerData.data.subscribe(data => {
        if (data) {
          this.editMode = data.editMode;
        }
        this.steps = this.editMode ? this.stepsEdit : this.stepsCreate;
      }))
    );
    // end of get edit mode flag
  }

  docSettingsFunction(val) {
    switch (val) {
      case 'Rename':
        {
          const dialogRef = this.dialog.open(RenameDialogComponent, {
            autoFocus: false,
            panelClass: 'normal-dialog-width',
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;

      default:
        break;
    }
  }

  changePublic = item =>
    item.state === 'Public'
      ? (item.state = 'Private')
      : (item.state = 'Public');

  getTime = time => (this.pod.time = time);

  currencyChange = val => console.log(val);

  addService() {
    this.priceBreakdown.push({
      select1: 'Pick Up & Delivery',
      serviseName: 'Deliver Docs to Terminal',
      cost: {
        label: 'Cost',
        value: 500.0,
        currency: true,
        right: true,
      },
      charge: {
        label: 'Charge',
        value: 600.0,
        currency: true,
        right: true,
      },
      select3: '3',
      totalPrice: {
        label: 'Total Price',
        value: 600.0,
        currency: true,
        right: true,
      },
    });
  }
  removeService(item) {
    const index = this.priceBreakdown.indexOf(item);
    if (index) {
      this.priceBreakdown.splice(index, 1);
    }
  }

  newService() {
    const dialogRef = this.dialog.open(ServiceDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'New Service',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
