import { TestBed } from '@angular/core/testing';

import { CreateWorkOrderService } from './create-work-order.service';

describe('CreateWorkOrderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreateWorkOrderService = TestBed.get(CreateWorkOrderService);
    expect(service).toBeTruthy();
  });
});
