import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgxMaskModule } from 'ngx-mask';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { WorkOrderConfirmedModule } from './work-order-confirmed/wo-confirmed.module';
import { CreateWorkOrderSegmentsModule } from './create-work-order-segments/create-wo-segments.module';
import { CreateWorkOrderCostDocsModule } from './create-work-order-cost-docs/create-wo-cost-docs.module';
import { CreateWorkOrderComponent } from './create-work-order.component';
import { AlertsComponent } from './alerts/alerts.component';

@NgModule({
  declarations: [CreateWorkOrderComponent, AlertsComponent],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgxMaskModule,
    MaterialModules,
    WorkOrderConfirmedModule,
    CreateWorkOrderSegmentsModule,
    CreateWorkOrderCostDocsModule,
    SharedModule,
  ],
  exports: [CreateWorkOrderComponent],
})
export class CreateWorkOrderModule {}
