import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WorkOrderComponent } from './work-order.component';
import { TrackComponent } from './track/track.component';
//
import { CreateWorkOrderComponent } from './create-work-order/create-work-order.component';
import { CreateWorkOrderSegmentsComponent } from './create-work-order/create-work-order-segments/create-work-order-segments.component';
import { AlertsComponent } from './create-work-order/alerts/alerts.component';
import { CreateWorkOrderCostDocsComponent } from './create-work-order/create-work-order-cost-docs/create-work-order-cost-docs.component';
import { WorkOrderConfirmedComponent } from './create-work-order/work-order-confirmed/work-order-confirmed.component';
import { JobBoardComponent } from './job-board/job-board.component';
import { WoLibraryComponent } from './wo-library/wo-library.component';
import { DraftComponent } from './draft/draft.component';
import { TrackDetailsComponent } from './job-board/track-details/track-details.component';

const workOrderRoutes: Routes = [
  {
    path: '',
    redirectTo: 'create-work-order',
    pathMatch: 'full',
  },
  {
    path: 'wo-library',
    component: WoLibraryComponent,
    data: { title: 'WO Library' },
  },
  {
    path: 'draft',
    component: DraftComponent,
    data: { title: 'Draft' },
  },
  {
    path: 'draft/draft-details',
    component: TrackDetailsComponent,
    data: {
      title: 'WO #SDJ123456789012',
      backBtn: true,
    },
  },
  {
    path: 'job-board',
    component: JobBoardComponent,
    data: { title: 'Job Board' },
  },
  {
    path: 'job-board/ltl-track',
    component: TrackComponent,
    data: {
      title: 'Tracking',
      backBtn: true,
    },
  },
  {
    path: 'job-board/ltl-track-details',
    component: TrackDetailsComponent,
    data: {
      title: 'WO #SDJ123456789012',
      backBtn: true,
    },
  },
  {
    path: 'job-board/work-order-track',
    component: TrackComponent,
    data: {
      title: 'Tracking',
      backBtn: true,
    },
  },
  {
    path: 'job-board/work-order-track-details',
    component: TrackDetailsComponent,
    data: {
      title: 'WO #SDJ123456789012',
      backBtn: true,
    },
  },
  // create work order
  {
    path: 'create-work-order',
    component: CreateWorkOrderComponent,
    data: { title: 'Create Work Order' },
  },
  {
    path: 'create-work-order-segments',
    component: CreateWorkOrderSegmentsComponent,
    data: { title: 'Create Work Order', backBtn: true },
  },
  {
    path: 'create-work-order-segments/add-alerts',
    component: AlertsComponent,
    data: { title: 'Add Alerts', backBtn: true },
  },
  {
    path: 'create-work-order-segments/edit-alerts',
    component: AlertsComponent,
    data: { title: 'Edit Alerts', backBtn: true },
  },
  {
    path: 'create-work-order-cost-docs',
    component: CreateWorkOrderCostDocsComponent,
    data: { title: 'Create Work Order', backBtn: true },
  },
  {
    path: 'create-work-order-confirmed',
    component: WorkOrderConfirmedComponent,
    data: { title: 'Create Work Order' },
  },
  // work order accepted
  {
    path: 'accepted-work-order',
    component: CreateWorkOrderComponent,
    data: { title: 'WO # 123456', backBtn: true, editMode: true },
  },
  {
    path: 'accepted-work-order-segments',
    component: CreateWorkOrderSegmentsComponent,
    data: { title: 'WO # 123456', backBtn: true, editMode: true },
  },
  {
    path: 'accepted-work-order-cost-docs',
    component: CreateWorkOrderCostDocsComponent,
    data: { title: 'WO # 123456', backBtn: true, editMode: true },
  },
  {
    path: 'accepted-work-order-confirmed',
    component: WorkOrderConfirmedComponent,
    data: { title: 'WO # 123456', backBtn: true, editMode: true },
  },
];

const routes: Routes = [
  {
    path: '',
    component: WorkOrderComponent,
    data: { title: 'Work Order' },
    children: workOrderRoutes,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WoRoutingModule {}
