import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModules } from '../../../material-modules';
import { TrackDetailsModule } from './track-details/track-details.module';
import { JobBoardQuoteModule } from './job-board-quote/job-board-quote.module';
import { JobBoardLtlModule } from './job-board-ltl/job-board-ltl.module';
import { JobBoardWorkOrderModule } from './job-board-work-order/job-board-work-order.module';

import { JobBoardComponent } from './job-board.component';

@NgModule({
  declarations: [JobBoardComponent],
  imports: [
    CommonModule,
    MaterialModules,
    TrackDetailsModule,
    JobBoardQuoteModule,
    JobBoardLtlModule,
    JobBoardWorkOrderModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  exports: [JobBoardComponent],
  entryComponents: [],
})
export class JobBoardModule {}
