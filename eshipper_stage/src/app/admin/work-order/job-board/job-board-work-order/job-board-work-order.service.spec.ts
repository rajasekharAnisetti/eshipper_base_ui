import { TestBed } from '@angular/core/testing';

import { JobBoardWorkOrderService } from './job-board-work-order.service';

describe('JobBoardWorkOrderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JobBoardWorkOrderService = TestBed.get(JobBoardWorkOrderService);
    expect(service).toBeTruthy();
  });
});
