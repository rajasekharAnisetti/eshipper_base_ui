import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class JobBoardWorkOrderService {
  constructor(private http: HttpClient) {}

  getJobBoardWorkOrder(): Observable<any> {
    return this.http
      .get('../../content/data/job-board-work-order.json')
      .pipe(map(data => data));
  }
}
