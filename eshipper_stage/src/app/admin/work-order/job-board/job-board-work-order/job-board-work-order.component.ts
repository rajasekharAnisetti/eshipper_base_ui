import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { JobBoardWorkOrderFilterComponent } from 'src/app/filters/job-board-work-order-filter/job-board-work-order-filter.component';
import { StatusDialogComponent } from 'src/app/dialogs/status-dialog/status-dialog.component';
import { PrintDialogComponent } from 'src/app/dialogs/print-dialog/print-dialog.component';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { WarningDialogComponent } from 'src/app/dialogs/warning-dialog/warning-dialog.component';
import { SIMPLESETTINGS } from 'src/app/shared/const/dropdowns';

import { JobBoardWorkOrderService } from './job-board-work-order.service';
import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';

@Component({
  selector: 'app-job-board-work-order',
  templateUrl: './job-board-work-order.component.html',
  styleUrls: ['./job-board-work-order.component.scss'],
})
export class JobBoardWorkOrderComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  listData = [];
  dataEmpty = false;
  contentToolbarSettings = SIMPLESETTINGS;
  itemSettings = ['Edit', 'Cancel'];
  jbStatuses = [
    'Pending',
    'Dispatch',
    'Checkout',
    'In Transit',
    'Delivered',
    'Cancelled',
    'Closed',
  ];

  constructor(
    private router: Router,
    private getData: JobBoardWorkOrderService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    // get agents
    this.subscribe.add(
      this.getData.getJobBoardWorkOrder().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        data.content ? (this.listData = data.content) : console.log('Error!!!');
      })
    );
  }

  // click on row
  setLtlTrackFlag() {
    localStorage.setItem('trackFlag', 'workOrder');
    this.router.navigate(['/admin/work-order/job-board/work-order-track']);
  }
  // end of click on row

  // toolbar settings
  settingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print':
        const printDialogRef = this.dialog.open(PrintDialogComponent, {
          autoFocus: false,
          panelClass: 'small-dialog-width',
        });
        this.subscribe.add(
          printDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }
  // end of toolbar settings

  // filter
  filterOpen() {
    const dialogRef = this.dialog.open(JobBoardWorkOrderFilterComponent, {
      autoFocus: false,
      panelClass: 'normal-dialog-width',
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of filter

  // item settings
  getItemSettingsValue(val) {
    switch (val) {
      case 'Edit':
        this.router.navigate(['/admin/work-order/accepted-work-order']);
        break;
      case 'Cancel':
        {
          const dialogRef = this.dialog.open(WarningDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              message: 'Warning message for edit default markup goes here...',
              action: 'Cancel',
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;
      default:
        break;
    }
  }
  // end of filter

  // status function
  statusFunction(status, item) {
    const dialogRef = this.dialog.open(StatusDialogComponent, {
      autoFocus: false,
      panelClass: 'huge-dialog-width',
      data: {
        onlyForm: false,
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          item.status = {
            name: status,
            color: status === 'Closed' ? 'red' : '',
          };
        } else {
          item.status = {
            name: 'In Process',
            color: '',
          };
        }
        console.log(item, item.status);
      })
    );
  }
  // end of status function

  // cost details
  openCostDetailsDialog(data) {
    this.dialog.open(InfoDialogComponent, {
      data: {
        title: 'Cost Details',
        moreData: data,
      },
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
  }
  // end of cost details

  // get period
  getPeriod(period) {
    console.log(period);
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
