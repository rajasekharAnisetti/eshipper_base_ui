import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobBoardWorkOrderComponent } from './job-board-work-order.component';

describe('JobBoardWorkOrderComponent', () => {
  let component: JobBoardWorkOrderComponent;
  let fixture: ComponentFixture<JobBoardWorkOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobBoardWorkOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobBoardWorkOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
