import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobBoardQuoteComponent } from './job-board-quote.component';

describe('JobBoardQuoteComponent', () => {
  let component: JobBoardQuoteComponent;
  let fixture: ComponentFixture<JobBoardQuoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobBoardQuoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobBoardQuoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
