import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { JobBoardQuoteComponent } from './job-board-quote.component';

@NgModule({
  declarations: [JobBoardQuoteComponent],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    FormsModule,
    RouterModule,
    MaterialModules,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [JobBoardQuoteComponent],
})
export class JobBoardQuoteModule {}
