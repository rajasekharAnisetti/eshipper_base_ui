import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { PrintDialogComponent } from 'src/app/dialogs/print-dialog/print-dialog.component';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { WarningDialogComponent } from 'src/app/dialogs/warning-dialog/warning-dialog.component';
import { JobBoardWorkOrderFilterComponent } from 'src/app/filters/job-board-work-order-filter/job-board-work-order-filter.component';
import { SIMPLESETTINGS } from 'src/app/shared/const/dropdowns';

import { JobBoardQuoteService } from './job-board-quote.service';

@Component({
  selector: 'app-job-board-quote',
  templateUrl: './job-board-quote.component.html',
  styleUrls: ['./job-board-quote.component.scss'],
})
export class JobBoardQuoteComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  listData = [];
  dataEmpty = false;
  contentToolbarSettings = SIMPLESETTINGS;
  itemSettings = ['Edit', 'Delete'];
  expiredItemSettings = ['Activate', 'Delete'];
  constructor(
    private router: Router,
    private getData: JobBoardQuoteService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    // get agents
    this.subscribe.add(
      this.getData.getJobBoardQuote().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        data.content ? (this.listData = data.content) : console.log('Error!!!');
      })
    );
  }

  filterOpen() {
    const dialogRef = this.dialog.open(JobBoardWorkOrderFilterComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }

  setLtlTrackFlag() {
    this.router.navigate(['/admin/work-order/accepted-work-order']);
  }


  openCostDetailsDialog() { }

  settingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print':
        const printDialogRef = this.dialog.open(PrintDialogComponent, {
          autoFocus: false,
          panelClass: 'small-dialog-width',
        });
        this.subscribe.add(
          printDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }

  getItemSettingsValue(val) {
    switch (val) {
      case 'Activate':
        break;
      case 'Edit':
        this.router.navigate(['/admin/work-order/accepted-work-order']);
        break;
      case 'Delete':
        {
          const dialogRef = this.dialog.open(WarningDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              message: 'Warning message for edit default markup goes here...',
              action: 'Delete',
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;
      default:
        break;
    }
  }

  // get period
  getPeriod(period) {
    console.log(period);
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
