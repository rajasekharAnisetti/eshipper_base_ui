import { TestBed } from '@angular/core/testing';

import { JobBoardQuoteService } from './job-board-quote.service';

describe('JobBoardQuoteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JobBoardQuoteService = TestBed.get(JobBoardQuoteService);
    expect(service).toBeTruthy();
  });
});
