import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class JobBoardQuoteService {
  constructor(private http: HttpClient) {}

  getJobBoardQuote(): Observable<any> {
    return this.http
      .get('../../content/data/job-board-quote.json')
      .pipe(map(data => data));
  }
}
