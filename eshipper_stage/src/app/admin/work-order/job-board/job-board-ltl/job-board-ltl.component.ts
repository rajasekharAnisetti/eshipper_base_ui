import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { PrintDialogComponent } from 'src/app/dialogs/print-dialog/print-dialog.component';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { StatusDialogComponent } from 'src/app/dialogs/status-dialog/status-dialog.component';
import { JobBoardLtlFilterComponent } from 'src/app/filters/job-board-ltl-filter/job-board-ltl-filter.component';
import { WarningDialogComponent } from 'src/app/dialogs/warning-dialog/warning-dialog.component';
import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';
import { SIMPLESETTINGS } from 'src/app/shared/const/dropdowns';

import { JobBoardLtlService } from './job-board-ltl.service';

@Component({
  selector: 'app-job-board-ltl',
  templateUrl: './job-board-ltl.component.html',
  styleUrls: ['./job-board-ltl.component.scss'],
})
export class JobBoardLtlComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  listData = [];
  dataEmpty = false;
  contentToolbarSettings = SIMPLESETTINGS;
  itemSettings = ['Edit', 'Cancel'];
  jbStatuses = [
    'Pending',
    'Dispatch',
    'Checkout',
    'In Transit',
    'Delivered',
    'Cancelled',
    'Closed',
  ];

  constructor(
    private router: Router,
    private getData: JobBoardLtlService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    // get agents
    this.subscribe.add(
      this.getData.getJobBoardLtl().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        data.content ? (this.listData = data.content) : console.log('Error!!!');
      })
    );
  }

  // click on row
  setLtlTrackFlag(item) {
    localStorage.setItem('trackFlag', 'ltl');
    this.router.navigate(['/admin/work-order/job-board/ltl-track']);
    console.log(item);
  }
  // end of click on row

  // toolbar settings
  settingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print':
        const printDialogRef = this.dialog.open(PrintDialogComponent, {
          autoFocus: false,
          panelClass: 'small-dialog-width',
        });
        this.subscribe.add(
          printDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }
  // end of toolbar sttings

  // filter function
  filterOpen() {
    const dialogRef = this.dialog.open(JobBoardLtlFilterComponent, {
      autoFocus: false,
      panelClass: 'normal-dialog-width',
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  //  end of filter function

  // itm settings
  getItemSettingsValue(val) {
    switch (val) {
      case 'Edit':
        this.router.navigate(['/admin/work-order/accepted-work-order']);
        break;
      case 'Cancel':
        {
          const dialogRef = this.dialog.open(WarningDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              message: 'Warning message for edit default markup goes here...',
              action: 'Cancel',
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;
      default:
        break;
    }
  }
  // end of item settings

  // cost details
  openCostDetailsDialog(data) {
    this.dialog.open(InfoDialogComponent, {
      data: {
        title: 'Cost Details',
        moreData: data,
      },
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
  }
  // end of cost details

  // status function
  statusFunction(status, item) {
    const dialogRef = this.dialog.open(StatusDialogComponent, {
      autoFocus: false,
      panelClass: 'huge-dialog-width',
      data: {
        onlyForm: false,
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          item.status = {
            name: status,
            color: status === 'Closed' ? 'red' : '',
          };
        } else {
          item.status = {
            name: 'In Process',
            color: '',
          };
        }
        console.log(item, item.status);
      })
    );
  }
  // end of status function

  // get period
  getPeriod(period) {
    console.log(period);
  }
  // end of get period

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
