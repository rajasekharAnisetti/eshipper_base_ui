import { TestBed } from '@angular/core/testing';

import { JobBoardLtlService } from './job-board-ltl.service';

describe('JobBoardLtlService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JobBoardLtlService = TestBed.get(JobBoardLtlService);
    expect(service).toBeTruthy();
  });
});
