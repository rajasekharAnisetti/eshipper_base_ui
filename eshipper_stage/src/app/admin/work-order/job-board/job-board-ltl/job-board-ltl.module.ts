import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { JobBoardLtlComponent } from './job-board-ltl.component';

@NgModule({
  declarations: [JobBoardLtlComponent],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MaterialModules,
    SharedModule,
  ],
  exports: [JobBoardLtlComponent],
  entryComponents: [],
})
export class JobBoardLtlModule {}
