import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class JobBoardLtlService {
  constructor(private http: HttpClient) {}

  getJobBoardLtl(): Observable<any> {
    return this.http
      .get('../../content/data/job-board-ltl.json')
      .pipe(map(data => data));
  }
}
