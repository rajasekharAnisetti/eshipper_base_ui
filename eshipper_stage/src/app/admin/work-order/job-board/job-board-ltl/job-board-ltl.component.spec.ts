import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobBoardLtlComponent } from './job-board-ltl.component';

describe('JobBoardLtlComponent', () => {
  let component: JobBoardLtlComponent;
  let fixture: ComponentFixture<JobBoardLtlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobBoardLtlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobBoardLtlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
