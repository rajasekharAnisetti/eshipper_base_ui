import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { TrackDetails } from 'src/app/interfaces/wo';
import { StatusDialogComponent } from 'src/app/dialogs/status-dialog/status-dialog.component';

import { TrackDetailsService } from './track-details.service';
import { ApprovedDialogComponent } from 'src/app/dialogs/approved-dialog/approved-dialog.component';
import { PrintDialogComponent } from 'src/app/dialogs/print-dialog/print-dialog.component';
import { EmailDialogComponent } from 'src/app/dialogs/email-dialog/email-dialog.component';

@Component({
  selector: 'app-track-details',
  templateUrl: './track-details.component.html',
  styleUrls: ['./track-details.component.scss'],
})
export class TrackDetailsComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  trackFlag = localStorage.getItem('trackFlag');
  trackDetailsData = <TrackDetails>{};
  draftFlag = false;
  buttonItems = ['Submit As Quote', 'Submit As Live WO'];
  jbStatuses = [
    'Pending',
    'Dispatch',
    'Checkout',
    'In Transit',
    'Delivered',
    'Cancelled',
    'Closed',
  ];

  constructor(
    private getData: TrackDetailsService,
    private dialog: MatDialog,
    public router: Router
  ) { }

  ngOnInit() {
    switch (this.trackFlag) {
      case 'ltl':
        {
          this.draftFlag = false
          // get ltl track details
          this.subscribe.add(
            this.getData.getLtlTrackDetails().subscribe(data => {
              data
                ? (this.trackDetailsData = data)
                : console.log('Data File Error!!!');
            })
          );
        }
        break;
      case 'workOrder':
        {
          this.draftFlag = false
          // get work order track details
          this.subscribe.add(
            this.getData.getWorkOrderTrackDetails().subscribe(data => {
              data
                ? (this.trackDetailsData = data)
                : console.log('Data File Error!!!');
            })
          );
        }
        break;

      case 'draft':
        {
          this.draftFlag = true;
          // get draft track details
          this.subscribe.add(
            this.getData.getWorkOrderDraftTrackDetails().subscribe(data => {
              data
                ? (this.trackDetailsData = data)
                : console.log('Data File Error!!!');
            })
          );
        }
        break;

      default:
        this.subscribe.add(
          this.getData.getWorkOrderTrackDetails().subscribe(data => {
            data
              ? (this.trackDetailsData = data)
              : console.log('Data File Error!!!');
          })
        );
        break;
    }
  }

  // status function
  statusFunction(status) {
    const dialogRef = this.dialog.open(StatusDialogComponent, {
      autoFocus: false,
      panelClass: 'huge-dialog-width',
      data: {
        onlyForm: false,
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result, status)
        }
      })
    );
  }
  // end of status function

  // email function
  emailFunction() {
    const dialogRef = this.dialog.open(EmailDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {},
    });

    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of email function

  // print function
  printFunction() {
    const printDialogRef = this.dialog.open(PrintDialogComponent, {
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
    this.subscribe.add(
      printDialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of print function

  // quote button function
  doubleButtonClick(button) {
    switch (button) {
      case 'Submit As Quote':
        {
          const dialogRef = this.dialog.open(ApprovedDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              title: 'Quote WO# 12345678 Submitted',
              content:
                'We will send the quote notification to the customer shortly.',
              buttons: {
                buttonLeft: 'Check Quote',
                buttonRight: 'New WO',
              },
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
                switch (result) {
                  case 'Check Quote':
                    this.router.navigate(['/admin/work-order/job-board']);
                    break;

                  default:
                    break;
                }
              }
            })
          );
        }
        break;
      case 'Submit As Live WO':
        {
          const dialogRefLiveWO = this.dialog.open(ApprovedDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              title: 'Live WO# 12345678 Submitted',
              content: 'We will send the notification to the customer shortly.',
              buttons: {
                buttonLeft: 'New WO',
                buttonRight: 'Continue',
              },
            },
          });
          this.subscribe.add(
            dialogRefLiveWO.afterClosed().subscribe(result => {
              if (result) {
                switch (result) {
                  case 'Continue':
                    this.router.navigate([
                      '/admin/work-order/create-work-order-segments',
                    ]);
                    break;

                  default:
                    break;
                }
              }
            })
          );
        }
        break;
      default:
        break;
    }
  }
  // end of quote button function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
