import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TrackDetailsService {
  constructor(private http: HttpClient) {}

  getLtlTrackDetails(): Observable<any> {
    return this.http
      .get('content/data/ltl-track-details.json')
      .pipe(map(data => data));
  }

  getWorkOrderTrackDetails(): Observable<any> {
    return this.http
      .get('content/data/work-order-track-details.json')
      .pipe(map(data => data));
  }
  getWorkOrderDraftTrackDetails(): Observable<any> {
    return this.http
      .get('content/data/work-order-draft-track-details.json')
      .pipe(map(data => data));
  }
}
