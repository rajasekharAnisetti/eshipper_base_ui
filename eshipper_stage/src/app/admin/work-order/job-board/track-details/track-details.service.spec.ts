import { TestBed } from '@angular/core/testing';

import { TrackDetailsService } from './track-details.service';

describe('TrackDetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TrackDetailsService = TestBed.get(TrackDetailsService);
    expect(service).toBeTruthy();
  });
});
