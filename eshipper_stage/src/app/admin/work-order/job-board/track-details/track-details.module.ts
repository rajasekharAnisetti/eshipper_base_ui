import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { TrackDetailsComponent } from './track-details.component';

@NgModule({
  declarations: [TrackDetailsComponent],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    FormsModule,
    RouterModule,
    MaterialModules,
    SharedModule,
  ],
  exports: [TrackDetailsComponent],
})
export class TrackDetailsModule {}
