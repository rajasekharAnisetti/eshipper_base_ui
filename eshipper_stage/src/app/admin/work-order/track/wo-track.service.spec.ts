import { TestBed } from '@angular/core/testing';

import { WoTrackService } from './wo-track.service';

describe('WoTrackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WoTrackService = TestBed.get(WoTrackService);
    expect(service).toBeTruthy();
  });
});
