import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { TrackComponent } from './track.component';

@NgModule({
  declarations: [TrackComponent],
  imports: [
    CommonModule,
    MaterialModules,
    PerfectScrollbarModule,
    RouterModule,
    FormsModule,
    AgmCoreModule,
    SharedModule,
  ],
  exports: [TrackComponent],
})
export class TrackModule {}
