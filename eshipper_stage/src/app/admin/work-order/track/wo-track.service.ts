import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class WoTrackService {
  constructor(private http: HttpClient) {}

  // get ltl track data
  getLtlTrack(): Observable<any> {
    return this.http
      .get('content/data/ltl-track.json')
      .pipe(map(data => data));
  }

  // get work order track data
  getWorkOrderTrack(): Observable<any> {
    return this.http
      .get('content/data/work-order-track.json')
      .pipe(map(data => data));
  }
}
