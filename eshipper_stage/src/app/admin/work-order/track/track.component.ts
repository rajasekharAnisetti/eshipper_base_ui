import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Track } from 'src/app/interfaces/wo';
import { WoTrackService } from './wo-track.service';

@Component({
  selector: 'app-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.scss'],
})
export class TrackComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  trackFlag = localStorage.getItem('trackFlag');

  // coordinates for example only
  lat = 43.727022;
  lng = -79.327631;
  // end of coordinates

  trackData = <Track>{};

  constructor(private getData: WoTrackService) {}

  ngOnInit() {
    switch (this.trackFlag) {
      case 'ltl':
        // get ltl track
        this.subscribe.add(
          this.getData.getLtlTrack().subscribe(data => {
            data ? (this.trackData = data) : console.log('Data File Error!!!');
          })
        );
        break;
      case 'workOrder':
        // get work order track
        this.subscribe.add(
          this.getData.getWorkOrderTrack().subscribe(data => {
            data ? (this.trackData = data) : console.log('Data File Error!!!');
          })
        );
        break;

      default:
        break;
    }
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
