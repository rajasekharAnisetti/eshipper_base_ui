import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WoRoutingModule } from './wo-routing.module';
import { WoLibraryModule } from './wo-library/wo-library.module';
import { DraftModule } from './draft/draft.modules';
import { JobBoardModule } from './job-board/job-board.module';
import { TrackModule } from './track/track.module';
import { CreateWorkOrderModule } from './create-work-order/create-wo.module';

import { WorkOrderComponent } from './work-order.component';

@NgModule({
  declarations: [WorkOrderComponent],
  imports: [
    CommonModule,
    WoRoutingModule,
    WoLibraryModule,
    DraftModule,
    JobBoardModule,
    TrackModule,
    CreateWorkOrderModule,
  ],
  exports: [WorkOrderComponent],
})
export class WoModule {}
