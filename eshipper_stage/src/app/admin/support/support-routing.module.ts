import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TicketsComponent } from './tickets/tickets.component';
import { ClaimsComponent } from './claims/claims.component';
import { ClaimComponent } from './claim/claim.component';
import { EditableClaimComponent } from './editable-claim/editable-claim.component';

const routes: Routes = [
  {
    path: 'tickets',
    component: TicketsComponent,
    data: {
      title: 'Tickets',
    },
  },
  {
    path: 'claims',
    component: ClaimsComponent,
    data: {
      title: 'Claims',
    },
  },
  {
    path: 'claim',
    component: ClaimComponent,
    data: {
      title: 'Claim 12345678',
      backBtn: true,
    },
  },
  {
    path: 'new-claim',
    component: EditableClaimComponent,
    data: {
      title: 'Add Claim',
      backBtn: true,
    },
  },
  {
    path: 'edit-claim',
    component: EditableClaimComponent,
    data: {
      title: 'Edit Claim',
      backBtn: true,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SupportRoutingModule {}
