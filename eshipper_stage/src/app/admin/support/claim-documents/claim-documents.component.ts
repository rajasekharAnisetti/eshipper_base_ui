import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { AddMissingDocDialogComponent } from 'src/app/dialogs/add-missing-doc-dialog/add-missing-doc-dialog.component';

@Component({
  selector: 'app-claim-documents',
  templateUrl: './claim-documents.component.html',
  styleUrls: ['./claim-documents.component.scss'],
})
export class ClaimDocumentsComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  toolbarSettings = ['Export'];
  itemSettings = ['Download', 'Print', 'Delete'];
  missingItemSettings = ['Upload', 'Notify', 'Delete'];
  claimDocs = [
    {
      name: 'Document name',
      url: '/admin/support/claim',
    },
    {
      name: 'Document name',
      url: '/admin/support/claim',
    },
    {
      name: 'Document name',
      url: '/admin/support/claim',
    },
    {
      name: 'Document name',
      url: '',
    },
  ];
  constructor(public dialog: MatDialog) {}

  ngOnInit() {}

  // toolbar settings
  settingsFunction(val) {
    switch (val) {
      case 'Export':
        {
          const exportDialogRef = this.dialog.open(ExportDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
          });
          this.subscribe.add(
            exportDialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;

      default:
        break;
    }
  }
  // end of toolbar settings

  // item settings
  itemSettingsFunction(val, index) {
    switch (val) {
      case 'Upload':
        {
        }
        break;
      case 'Delete':
        {
          this.claimDocs.splice(index, 1);
        }
        break;

      default:
        break;
    }
  }
  // end of item settings

  // add missing document function
  addMissingDocument() {
    const exportDialogRef = this.dialog.open(AddMissingDocDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      exportDialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of add missing document function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
