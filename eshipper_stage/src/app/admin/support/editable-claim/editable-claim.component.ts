import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription, Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { MatDialog } from '@angular/material';

import { AddCommentComponent } from 'src/app/dialogs/add-comment/add-comment.component';
import { Claim } from 'src/app/interfaces/claim';
import { CURRENCIES } from 'src/app/shared/const/currencies';
import { STATUSES } from 'src/app/shared/const/dropdowns';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editable-claim',
  templateUrl: './editable-claim.component.html',
  styleUrls: ['./editable-claim.component.scss'],
})
export class EditableClaimComponent implements OnInit, OnDestroy {
  @Input() readonlyFlag = false;
  minDate = new Date();
  subscribe = new Subscription();
  claim = <Claim>{};
  shipmentFlag = false;
  currencyItems = CURRENCIES;
  statuses = STATUSES;
  trackings = ['CXDT1234567800', 'WTX890890654'];
  trackingsFiltered: Observable<any>;
  solutions = ['Refund via Check', 'Option 2', 'Option 2'];
  reasons = ['Service Failure', 'Option 2', 'Option 2'];
  assignToList = ['Clifford Mitchell', 'Option 2', 'Option 2'];
  toolbarSettings = ['Export'];
  validation = {
    receivedDate: new FormControl(),
    mailedDate: new FormControl(),
    status: new FormControl(),
    assignTo: new FormControl(),
    reason: new FormControl(),
    solution: new FormControl(),
    statusAlert: new FormControl(),
    tracking: new FormControl(),
    description: new FormControl(),
    carrierRefundCheck: new FormControl(),
    carrierClaim: new FormControl(),
    carrierRefundStatus: new FormControl(),
    carrierRefundAmount: new FormControl(),
    carrierRefundCheckNumber: new FormControl(),
    carrierRefundDate: new FormControl(),
    eShipperRefundAmount: new FormControl(),
    eShipperRefundCheck: new FormControl(),
    eShipperRefundDate: new FormControl(),
  };
  shipment = {
    logo: 'content/images/logos/ups.png',
    service: 'Express International Parcel',
    trans: 'WTX890890654',
    linkToTrans: '/#/admin/tracking/track',
    cost: 789900.0,
    charge: 789900.0,
    insurance: 'eshipper',
    amount: 789900.0,
    shipData: '03/27/2017',
    valueOfGoods: 789900.0,
    customer: 'Anthony Wilgeorwiseceol',
    email: 'brigitte_eichmann@mafalda.me',
    phone: '111-060-9732',
    shipping: '700 Mitchell Flats Suite 253, Mississauga, L5B 1B8, CA',
    billing: 'Same as shipping address',
  };

  constructor(private router: Router, public dialog: MatDialog) {
    if (this.router.getCurrentNavigation().extras.state) {
      // get data from url
      this.claim = this.router.getCurrentNavigation().extras.state.data;

      // updated dates
      this.validation.receivedDate = new FormControl(
        new Date(this.claim.receivedDate).toISOString()
      );
      this.validation.mailedDate = new FormControl(
        new Date(this.claim.mailedDate).toISOString()
      );
      this.validation.statusAlert = new FormControl(
        new Date(this.claim.statusAlert).toISOString()
      );
      this.validation.carrierRefundDate = new FormControl(
        new Date(this.claim.carrierRefundDate).toISOString()
      );
      this.validation.eShipperRefundDate = new FormControl(
        new Date(this.claim.eShipperRefundDate).toISOString()
      );
    } else {
      this.claim.status = {
        name: '',
        color: '',
      };
      this.claim.comments = [
        {
          type: 'In Process',
          date: '04/22/2019',
          comment: '',
        },
      ];
    }
    console.log(this.claim);
  }

  ngOnInit() {
    this.trackingsFiltered = this.validation.tracking.valueChanges.pipe(
      startWith(''),
      map(name => (name ? this._filter(name) : this.trackings.slice()))
    );
    this.shipmentFlag = this.readonlyFlag ? true : false;
  }

  // add a new comment
  addNewComment() {
    const dialogRef = this.dialog.open(AddCommentComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
          this.claim.comments.push({
            type: 'In Process',
            date: '04/22/2019',
            comment: result.comment,
          });
        }
      })
    );
  }
  // end of add a new comment

  // change currency
  customsCurrencyChange(val) {
    console.log(val);
  }
  // end of change currency

  // submit claim
  submitClaim() {
    const url = '/admin/support/claims';
    this.router.navigate([url]);
  }
  // end of submit claim

  // trackings autocomplete filter
  private _filter(item: string) {
    const filterValue = item.toLowerCase();

    return this.trackings.filter(
      option => option.toLowerCase().indexOf(filterValue) === 0
    );
  }
  // end of trackings autocomplete filter

  // change status button
  changeStatus(status) {
    console.log(status);
  }
  // end of change status button

  // toolbar setting function
  settingsFunction(val) {
    console.log(val);
  }
  // end of toolbar setting function

  // edit claim function
  editClaim() {
    this.readonlyFlag = false;
  }
  // end of edit claim function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
