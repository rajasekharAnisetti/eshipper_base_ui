import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditableClaimComponent } from './editable-claim.component';

describe('EditableClaimComponent', () => {
  let component: EditableClaimComponent;
  let fixture: ComponentFixture<EditableClaimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditableClaimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableClaimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
