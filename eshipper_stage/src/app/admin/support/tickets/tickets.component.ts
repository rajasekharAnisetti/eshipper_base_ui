import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';

import { Ticket } from 'src/app/interfaces/ticket';
import { AdminTicketsFilterComponent } from 'src/app/filters/admin-tickets-filter/admin-tickets-filter.component';

import { TicketsService } from './tickets.service';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss'],
})
export class TicketsComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  dataEmpty = true;
  ticketsList: Array<Ticket>;
  toolbarSettings = ['Sync', 'Export'];
  itemSettings = ['Sync', 'Export', 'Delete'];

  constructor(
    public router: Router,
    public dialog: MatDialog,
    private getData: TicketsService
  ) {}

  ngOnInit() {
    this.subscribe.add(
      this.getData.getTickets().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        this.ticketsList = data.content.length ? data.content : [];
      })
    );
  }

  // period function
  period(period) {
    console.log(period);
  }
  // end of period function

  // search function
  searchFunction(val) {
    console.log(val);
  }
  // end of search function

  // filter
  filter() {
    const dialogRef = this.dialog.open(AdminTicketsFilterComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of filter

  // toolbar settings
  settingsFunction(val) {
    switch (val) {
      case 'Export':
        {
          const exportDialogRef = this.dialog.open(ExportDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
          });
          this.subscribe.add(
            exportDialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;

      default:
        break;
    }
  }
  // end of toolbar settings

  // get sort option
  getSortData(data) {
    console.log(data);
  }
  // end of get sort option

  // item settings
  itemSettingsFunction(val, item) {
    console.log(val, item);
    switch (val) {
      case 'Export':
        {
          const exportDialogRef = this.dialog.open(ExportDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
          });
          this.subscribe.add(
            exportDialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;

      default:
        break;
    }
  }
  // end of item settings

  // click on row
  clickOnRow(item) {
    console.log(item);
  }
  // end of click on row

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
