import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, NavigationExtras } from '@angular/router';
import { MatDialog } from '@angular/material';

import { ClaimsFilterDialogComponent } from 'src/app/filters/claims-filter-dialog/claims-filter-dialog.component';
import { ClaimStatusComponent } from 'src/app/dialogs/claim-status/claim-status.component';
import { Claim } from 'src/app/interfaces/claim';

import { ClaimsService } from './claims.service';

@Component({
  selector: 'app-claims',
  templateUrl: './claims.component.html',
  styleUrls: ['./claims.component.scss'],
})
export class ClaimsComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  dataEmpty = true;
  claimsList: Array<Claim>;
  toolbarSettings = ['Export'];
  selectionToolbarSettings = ['Export', 'Delete'];
  itemSettings = ['Edit', 'Export', 'Delete'];
  toolbarIndeterminate = false;
  selectAll = false;
  selection = false;

  constructor(
    private dataService: ClaimsService,
    public dialog: MatDialog,
    public router: Router
  ) { }

  ngOnInit() {
    // get claims data
    this.subscribe.add(
      this.dataService.getClaims().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        this.claimsList = data.content.length ? data.content : [];
      })
    );
  }

  // period function
  period(period) {
    console.log(period);
  }
  // end of period function

  // search function
  searchFunction(val) {
    console.log(val);
  }
  // end of search function

  // filter function
  filter() {
    const dialogRef = this.dialog.open(ClaimsFilterDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of filter function

  // toolbar settings
  settingsFunction(val) {
    console.log(val);
  }
  // end of toolbar settings

  // selection
  selectAllFunction(flag) {
    const select = flag ? true : false;
    this.selection = select ? true : false;
    for (const i of this.claimsList) {
      i.selected = select;
    }
  }

  selectRow() {
    const n = this.claimsList.length;
    let item = 0;
    for (const i of this.claimsList) {
      if (i.selected) {
        item++;
      }
    }
    this.selectAll = n === item ? true : false;
    this.toolbarIndeterminate = n > item && item !== 0 ? true : false;
    this.selection = this.toolbarIndeterminate || this.selectAll ? true : false;
  }
  // end of selection

  // update status function
  changeStatus(status, item) {
    if (status === 'Approved' || status === 'Denied') {
      const dialogRef = this.dialog.open(ClaimStatusComponent, {
        panelClass: 'normal-dialog-width',
        autoFocus: false,
        data: {
          title: 'Claim # 1234567890',
          status: status,
        },
      });
      this.subscribe.add(
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            item.status = {
              name: status,
              color:
                status === 'Approved'
                  ? 'green'
                  : status === 'Denied'
                    ? 'red'
                    : '',
            };
          } else {
            item.status = {
              name: 'In Process',
              color: '',
            };
          }
          console.log(item, item.status);
        })
      );
    }
  }
  // end of update status function

  // get sort option
  getSortData(data) {
    console.log(data);
  }
  // end of get sort option

  // click on row function
  clickRowFunction(item) {
    const navigationExtras: NavigationExtras = {
      state: {
        data: item,
      },
    };
    const url = '/admin/support/claim';
    this.router.navigate([url], navigationExtras);
  }
  // end of click on row function

  // item settings
  itemSettingsFunction(val, item) {
    console.log(val, item);
  }
  // end of item settings

  // add claim function
  addClaim() {
    const url = '/admin/support/new-claim';
    this.router.navigate([url]);
  }
  // end of add claim function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
