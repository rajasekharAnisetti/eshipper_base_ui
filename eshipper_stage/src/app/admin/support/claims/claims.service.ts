import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ClaimsService {
  constructor(private http: HttpClient) {}

  getClaims(): Observable<any> {
    return this.http
      .get('content/data/support/claims.json')
      .pipe(map(data => data));
  }
}
