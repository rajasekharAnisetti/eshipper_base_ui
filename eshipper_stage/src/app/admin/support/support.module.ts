import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { SupportRoutingModule } from './support-routing.module';

import { SupportComponent } from './support.component';
import { TicketsComponent } from './tickets/tickets.component';
import { ClaimsComponent } from './claims/claims.component';
import { EditableClaimComponent } from './editable-claim/editable-claim.component';
import { ClaimComponent } from './claim/claim.component';
import { ClaimDocumentsComponent } from './claim-documents/claim-documents.component';

@NgModule({
  declarations: [SupportComponent, TicketsComponent, ClaimsComponent, EditableClaimComponent, ClaimComponent, ClaimDocumentsComponent],
  imports: [
    CommonModule,
    RouterModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    SupportRoutingModule,
    NgxMaskModule,
    MaterialModules,
    SharedModule,
  ],
  exports: [SupportComponent],
})
export class SupportModule {}
