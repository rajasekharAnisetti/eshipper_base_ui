import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './admin.component';

const adminRoutes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: 'dashboard',
    loadChildren: () =>
      import('../dashboard/dashboard.module').then(mod => mod.DashboardModule),
  },
  {
    path: 'profile',
    loadChildren: () =>
      import('../profile/profile.module').then(mod => mod.ProfileModule),
  },
  {
    path: 'tracking',
    loadChildren: () =>
      import('../tracking/tracking.module').then(mod => mod.TrackingModule),
  },
  {
    path: 'tracking/order-details',
    loadChildren: () =>
      import('../order-details/order-details.module').then(
        mod => mod.OrderDetailsModule
      ),
    data: {
      title: 'Trans # SDJ123456789012',
      backBtn: true,
    },
  },
  {
    path: 'invoicing',
    loadChildren: () =>
      import('./invoicing/invoicing.module').then(mod => mod.InvoicingModule),
  },
  {
    path: 'edi',
    loadChildren: () => import('./edi/edi.module').then(mod => mod.EdiModule),
  },
  {
    path: 'work-order',
    loadChildren: () =>
      import('./work-order/wo.module').then(mod => mod.WoModule),
  },
  {
    path: 'reports',
    loadChildren: () =>
      import('./admin-reports/admin-reports.module').then(
        mod => mod.AdminReportsModule
      ),
  },
  {
    path: 'account',
    loadChildren: () =>
      import('./admin-account/admin-account.module').then(
        mod => mod.AdminAccountModule
      ),
  },
  {
    path: 'support',
    loadChildren: () =>
      import('./support/support.module').then(mod => mod.SupportModule),
  },
  {
    path: '**',
    loadChildren: () =>
      import('../unavailable-server/unavailable-server.module').then(mod => mod.UnavailableServerModule),
    data: {
      title: 'Unavailable',
    },
  }
];

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: adminRoutes,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule { }
