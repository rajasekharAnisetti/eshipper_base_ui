import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadEdiDialogComponent } from './upload-edi-dialog.component';

describe('UploadEdiDialogComponent', () => {
  let component: UploadEdiDialogComponent;
  let fixture: ComponentFixture<UploadEdiDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadEdiDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadEdiDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
