import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-upload-edi-dialog',
  templateUrl: './upload-edi-dialog.component.html',
  styleUrls: ['./upload-edi-dialog.component.scss'],
})
export class UploadEdiDialogComponent implements OnInit {
  modalData = {
    carrier: '',
  };
  carriers = ['Option 1', 'Option 2'];
  carrierFormControl = new FormControl('', [Validators.required]);
  // upload files
  progress = true;
  @ViewChild('file', { static: false }) file;
  public files: Set<File> = new Set();
  // end of upload files

  constructor(
    public dialogRef: MatDialogRef<UploadEdiDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  // add files functions
  addFiles() {
    this.file.nativeElement.click();
  }
  onFilesAdded() {
    const files: { [key: string]: File } = this.file.nativeElement.files;
    for (const key in files) {
      if (!isNaN(parseInt(key, 10))) {
        this.files.add(files[key]);
      }
    }
    // only for example
    setTimeout(() => {
      this.progress = false;
    }, 3000);
    // end of example
  }
  // end of add files functions

  closeDialog = () => this.dialogRef.close();
  process = () => this.dialogRef.close(this.modalData);
}
