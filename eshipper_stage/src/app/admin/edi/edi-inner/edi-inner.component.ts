import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { EdiInnerService } from './edi-inner.service';

import { PrintDialogComponent } from 'src/app/dialogs/print-dialog/print-dialog.component';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { SIMPLESETTINGS } from 'src/app/shared/const/dropdowns';
import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edi-inner',
  templateUrl: './edi-inner.component.html',
  styleUrls: ['./edi-inner.component.scss'],
})
export class EdiInnerComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  ediOrders: Array<any>;
  smallWidgetItems: Array<any>;

  dataEmpty = false;
  toolbarSettings = SIMPLESETTINGS;
  itemSettings = SIMPLESETTINGS;

  constructor(public dialog: MatDialog, private getData: EdiInnerService, public router: Router) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData.getEdiOrdersData().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        data.content ? (this.ediOrders = data.content) : console.log('Error!');
      })
    );

    this.subscribe.add(
      this.getData
        .getEdiSmallWIdgets()
        .subscribe(data =>
          data ? (this.smallWidgetItems = data) : console.log('Data Error!!!')
        )
    );
  }

  // settings function
  settingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print':
        const printDialogRef = this.dialog.open(PrintDialogComponent, {
          autoFocus: false,
          panelClass: 'small-dialog-width',
        });
        this.subscribe.add(
          printDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }
  // end of settings function

  // item settings function
  getItemSettingsValue(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print':
        const printDialogRef = this.dialog.open(PrintDialogComponent, {
          autoFocus: false,
          panelClass: 'small-dialog-width',
        });
        this.subscribe.add(
          printDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }
  // end of item settings function

  openInvInfoDialog(data) {
    this.dialog.open(InfoDialogComponent, {
      data: {
        title: 'INV Amt Details',
        moreData: data,
      },
      autoFocus: false,
      panelClass: 'normal-dialog-width',
    });
  }

  clickOnRow() {
    const url = '/admin/tracking/track';
    this.router.navigate([url]);
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
