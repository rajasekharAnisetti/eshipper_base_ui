import { TestBed } from '@angular/core/testing';

import { EdiInnerService } from './edi-inner.service';

describe('EdiInnerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EdiInnerService = TestBed.get(EdiInnerService);
    expect(service).toBeTruthy();
  });
});
