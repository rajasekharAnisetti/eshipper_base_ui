import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdiInnerComponent } from './edi-inner.component';

describe('EdiInnerComponent', () => {
  let component: EdiInnerComponent;
  let fixture: ComponentFixture<EdiInnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdiInnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdiInnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
