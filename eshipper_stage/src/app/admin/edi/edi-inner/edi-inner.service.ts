import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class EdiInnerService {
  constructor(private http: HttpClient) {}

  getEdiSmallWIdgets(): Observable<any> {
    return this.http
      .get('content/data/edi-small-widgets.json')
      .pipe(map(data => data));
  }

  getEdiOrdersData(): Observable<any> {
    return this.http
      .get('content/data/edi-orders-data.json')
      .pipe(map(data => data));
  }
}
