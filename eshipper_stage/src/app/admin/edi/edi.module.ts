import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialModules } from 'src/app/material-modules';

import { EdiRoutingModule } from './edi-routing.module';

import { UploadEdiDialogComponent } from './upload-edi-dialog/upload-edi-dialog.component';

import { EdiComponent } from './edi.component';
import { OrphanEdiComponent } from './orphan-edi/orphan-edi.component';
import { EdiHeaderComponent } from './edi-header/edi-header.component';
import { EdiInnerComponent } from './edi-inner/edi-inner.component';

@NgModule({
  declarations: [
    EdiComponent,
    OrphanEdiComponent,
    EdiHeaderComponent,
    EdiInnerComponent,
    UploadEdiDialogComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    MaterialModules,
    RouterModule,
    EdiRoutingModule,
    SharedModule,
  ],
  exports: [EdiComponent],
  entryComponents: [UploadEdiDialogComponent],
})
export class EdiModule {}
