import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrphanEdiComponent } from './orphan-edi.component';

describe('OrphanEdiComponent', () => {
  let component: OrphanEdiComponent;
  let fixture: ComponentFixture<OrphanEdiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrphanEdiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrphanEdiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
