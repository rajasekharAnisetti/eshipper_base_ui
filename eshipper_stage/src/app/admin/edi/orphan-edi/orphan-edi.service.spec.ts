import { TestBed } from '@angular/core/testing';

import { OrphanEdiService } from './orphan-edi.service';

describe('OrphanEdiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OrphanEdiService = TestBed.get(OrphanEdiService);
    expect(service).toBeTruthy();
  });
});
