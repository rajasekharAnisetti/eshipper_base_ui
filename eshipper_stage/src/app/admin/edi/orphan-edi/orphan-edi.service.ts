import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class OrphanEdiService {
  constructor(private http: HttpClient) {}

  getOrphanData(): Observable<any> {
    return this.http
      .get('content/data/edi-orphan-data.json')
      .pipe(map(data => data));
  }
}
