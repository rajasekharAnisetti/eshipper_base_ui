import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

import { EdiFilterDialogComponent } from 'src/app/filters/edi-filter-dialog/edi-filter-dialog.component';
import { LinkDialogComponent } from 'src/app/dialogs/link-dialog/link-dialog.component';
import { PrintDialogComponent } from 'src/app/dialogs/print-dialog/print-dialog.component';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import {
  SIMPLESETTINGS,
  EDIORPHANITEMSETTINGS,
} from 'src/app/shared/const/dropdowns';
import { EdiOrphan } from 'src/app/interfaces/edi';

import { OrphanEdiService } from './orphan-edi.service';

@Component({
  selector: 'app-orphan-edi',
  templateUrl: './orphan-edi.component.html',
  styleUrls: ['./orphan-edi.component.scss'],
})
export class OrphanEdiComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  dataEmpty = false;
  selection = false;
  selectAll = false;
  toolbarIndeterminate = false;

  toolbarSettings = SIMPLESETTINGS;
  itemSettings = EDIORPHANITEMSETTINGS;

  orders: Array<EdiOrphan>;

  constructor(private getData: OrphanEdiService, public dialog: MatDialog) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData.getOrphanData().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        data.content ? (this.orders = data.content) : console.log('Error!');
      })
    );
  }

  // search
  searchFunction(val) {
    console.log(val);
  }
  // end of search

  // filter
  filter() {
    const dialogRef = this.dialog.open(EdiFilterDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of filter

  // get sort option
  getSortData(data) {
    console.log(data);
  }
  // end of get sort option

  // page settings
  settingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print':
        const printDialogRef = this.dialog.open(PrintDialogComponent, {
          autoFocus: false,
          panelClass: 'small-dialog-width',
        });
        this.subscribe.add(
          printDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }
  // end of page settings

  // link function
  linkFunction() {
    const dialogRef = this.dialog.open(LinkDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of link function

  // selection
  selectAllFunction(flag) {
    const select = flag ? true : false;
    this.selection = select ? true : false;
    for (const i of this.orders) {
      i.selected = select;
    }
  }
  selectRow() {
    const n = this.orders.length;
    let item = 0;
    for (const i of this.orders) {
      if (i.selected) {
        item++;
      }
    }
    this.selectAll = n === item ? true : false;

    this.toolbarIndeterminate = n > item && item !== 0 ? true : false;

    this.selection = this.toolbarIndeterminate || this.selectAll ? true : false;
  }
  // end of selection

  itemSettingsFunction(val, item) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print':
        const printDialogRef = this.dialog.open(PrintDialogComponent, {
          autoFocus: false,
          panelClass: 'small-dialog-width',
        });
        this.subscribe.add(
          printDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }

  // click on row
  clickRowFunction(item) {
    console.log(item)
  }
  // end of click on row

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
