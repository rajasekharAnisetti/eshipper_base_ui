import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EdiComponent } from './edi.component';
import { OrphanEdiComponent } from './orphan-edi/orphan-edi.component';
import { EdiHeaderComponent } from './edi-header/edi-header.component';
import { EdiInnerComponent } from './edi-inner/edi-inner.component';

const ediRoutes: Routes = [
  {
    path: '',
    redirectTo: 'edi-header',
    pathMatch: 'full',
  },
  {
    path: 'orphan-edi',
    component: OrphanEdiComponent,
    data: { title: 'Orphan EDI' },
  },
  {
    path: 'edi-header',
    component: EdiHeaderComponent,
    data: { title: 'EDI Header' },
  },
  {
    path: 'edi-header/edi-inner',
    component: EdiInnerComponent,
    data: {
      title: 'EDI # 27773',
      backBtn: true,
    },
  },
];

const routes: Routes = [
  {
    path: '',
    component: EdiComponent,
    data: { title: 'EDI' },
    children: ediRoutes,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EdiRoutingModule {}
