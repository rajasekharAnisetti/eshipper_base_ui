import { TestBed } from '@angular/core/testing';

import { EdiHeaderService } from './edi-header.service';

describe('EdiHeaderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EdiHeaderService = TestBed.get(EdiHeaderService);
    expect(service).toBeTruthy();
  });
});
