import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class EdiHeaderService {
  constructor(private http: HttpClient) {}

  getHeaderData(): Observable<any> {
    return this.http
      .get('content/data/edi-header-data.json')
      .pipe(map(data => data));
  }
}
