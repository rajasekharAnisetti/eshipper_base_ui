import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdiHeaderComponent } from './edi-header.component';

describe('EdiHeaderComponent', () => {
  let component: EdiHeaderComponent;
  let fixture: ComponentFixture<EdiHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdiHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdiHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
