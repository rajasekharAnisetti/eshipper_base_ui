import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { EdiFilterDialogComponent } from 'src/app/filters/edi-filter-dialog/edi-filter-dialog.component';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';
import { EdiHeader } from 'src/app/interfaces/edi';
import { SIMPLESETTINGS } from 'src/app/shared/const/dropdowns';

import { EdiHeaderService } from './edi-header.service';
import { UploadEdiDialogComponent } from '../upload-edi-dialog/upload-edi-dialog.component';

@Component({
  selector: 'app-edi-header',
  templateUrl: './edi-header.component.html',
  styleUrls: ['./edi-header.component.scss'],
})
export class EdiHeaderComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  dataEmpty = false;
  toolbarIndeterminate = false;
  selectAll = false;
  selection = false;
  itemSettings = SIMPLESETTINGS;
  toolbarSettings = SIMPLESETTINGS;
  edis: Array<EdiHeader>;

  constructor(
    private getData: EdiHeaderService,
    public dialog: MatDialog,
    public router: Router
  ) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData.getHeaderData().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        data.content ? (this.edis = data.content) : console.log('Error!');
      })
    );
  }

  // click row function
  clickRowFunction(data) {
    console.log(data);
    const url = '/admin/edi/edi-header/edi-inner';
    this.router.navigate([url]);
  }
  // end of click row function

  // selection
  selectAllFunction(flag) {
    const select = flag ? true : false;
    this.selection = select ? true : false;
    for (const i of this.edis) {
      i.selected = select;
    }
  }

  selectRow() {
    const n = this.edis.length;
    let item = 0;
    for (const i of this.edis) {
      if (i.selected) {
        item++;
      }
    }
    this.selectAll = n === item ? true : false;
    this.toolbarIndeterminate = n > item && item !== 0 ? true : false;
    this.selection = this.toolbarIndeterminate || this.selectAll ? true : false;
  }
  // end of selection

  deleteSelectedRows() { }

  // release
  releaseFunction() { }
  // end of release

  // total info dialog
  openTotalDetailsDialog(data) {
    this.dialog.open(InfoDialogComponent, {
      data: {
        title: 'Charge Details',
        moreData: data,
      },
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
  }
  // end of total info dialog

  // settings
  settingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }
  // end of settings

  // search
  searchFunction(val) {
    console.log(val);
  }
  // end of search

  // filter
  filter() {
    const dialogRef = this.dialog.open(EdiFilterDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of filter

  // get sort option
  getSortData(data) {
    console.log(data);
  }
  // end of get sort option

  // item settings
  itemSettingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }
  // end of item settings

  // upload EDI
  uploadEdi() {
    const DialogRef = this.dialog.open(UploadEdiDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      DialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of upload EDI

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
