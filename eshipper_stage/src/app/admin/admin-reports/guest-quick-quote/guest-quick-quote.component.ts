import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';

import { AdminReportsService } from '../admin-reports.service';
import { GuestQuickQuote } from '../../../interfaces/guest-quick-quote';

@Component({
  selector: 'app-guest-quick-quote',
  templateUrl: './guest-quick-quote.component.html',
  styleUrls: ['./guest-quick-quote.component.scss']
})
export class GuestQuickQuoteComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  toolbarSettings = ['Option 1', 'Option 2'];
  quotesList: Array<GuestQuickQuote>;
  itemSettings = ['Option 1', 'Option 2'];

  widget = {
    name: 'Widget Name',
    settings: {
      chartValue: false,
      chartTypes: ['Type 1', 'Type 2', 'Type 3']
    }
  };

  // chart
  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end'
      }
    },
    maintainAspectRatio: false
  };
  public barChartLabels: Label[] = [
    '2006',
    '2007',
    '2008',
    '2009',
    '2010',
    '2011',
    '2012'
  ];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' }
  ];
  // end of chart

  constructor(private getData: AdminReportsService, private router: Router) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData.getGuestInstantQuotes().subscribe(data => {
        this.quotesList = data.content;
      })
    );
  }

  // get period function
  getPeriod(val) {
    console.log(val);
  }
  // end of get period function

  // search function
  searchFunction(val) {
    console.log(val);
  }
  // end of search function

  // toolbar settings
  toolbarSettingsFunction(val) {
    console.log(val);
  }
  // ens of toolbar settings

  // chart type click function
  chartTypeClick(val) {
    console.log(val);
  }
  // end of chart type click function

  // click on quote item function
  clickOnQouteItem(item) {
    console.log(item);
    const url = '/admin/reports/guest-quick-quote/inner';
    this.router.navigate([url]);
  }
  //  click on quote item function

  // item settings function
  itemSettingsFunction(item) {
    console.log(item);
  }
  // end of item settings function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
