import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestQuickQuoteComponent } from './guest-quick-quote.component';

describe('GuestQuickQuoteComponent', () => {
  let component: GuestQuickQuoteComponent;
  let fixture: ComponentFixture<GuestQuickQuoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestQuickQuoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestQuickQuoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
