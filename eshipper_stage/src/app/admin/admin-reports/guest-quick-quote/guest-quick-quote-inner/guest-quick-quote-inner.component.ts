import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, from } from 'rxjs';
import { AdminReportsService } from '../../admin-reports.service';

@Component({
  selector: 'app-guest-quick-quote-inner',
  templateUrl: './guest-quick-quote-inner.component.html',
  styleUrls: ['./guest-quick-quote-inner.component.scss'],
})
export class GuestQuickQuoteInnerComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  smallWidgetItems = [];
  data = <any>{};

  constructor(private getData: AdminReportsService) {}

  ngOnInit() {
    this.subscribe.add(
      this.getData.getGuestInstantQuotesInner().subscribe(data => {
        this.smallWidgetItems = [
          {
            label: 'Contact',
            value: data.contactPerson,
            bold: true,
          },
          {
            label: 'Phone',
            value: data.phoneNumber,
            bold: true,
          },
          {
            label: 'Email',
            value: data.phoneNumber,
            bold: true,
          },
          {
            label: 'Created',
            value: data.dateCreated,
            bold: true,
          },
          {
            label: 'Register',
            value: data.registered,
            bold: true,
          },
          {
            label: 'Check Out',
            value: data.checkOut,
            bold: true,
          },
        ];
        this.data = data;
      })
    );
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
