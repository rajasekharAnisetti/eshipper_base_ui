import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuestQuickQuoteInnerComponent } from './guest-quick-quote-inner.component';

describe('GuestQuickQuoteInnerComponent', () => {
  let component: GuestQuickQuoteInnerComponent;
  let fixture: ComponentFixture<GuestQuickQuoteInnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuestQuickQuoteInnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestQuickQuoteInnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
