import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AdminReportsService {
  constructor(private http: HttpClient) {}

  // guest instant quotes
  getGuestInstantQuotes(): Observable<any> {
    return this.http
      .get('content/data/reports/guest-instant-quotes.json')
      .pipe(map(data => data));
  }

  // guest instant quotes
  getGuestInstantQuotesInner(): Observable<any> {
    return this.http
      .get('content/data/reports/guest-instant-quotes-inner.json')
      .pipe(map(data => data));
  }

  // insurance list
  getInsuranceList(): Observable<any> {
    return this.http
      .get('content/data/reports/insurance.json')
      .pipe(map(data => data));
  }

  // cc auth data
  getCcAuthData(): Observable<any> {
    return this.http
      .get('content/data/reports/cc-auth.json')
      .pipe(map(data => data));
  }

  // profit data
  getProfitData(): Observable<any> {
    return this.http
      .get('content/data/reports/profit-loss.json')
      .pipe(map(data => data));
  }

  // carrier data
  getCarrierData(): Observable<any> {
    return this.http
      .get('content/data/reports/carrier.json')
      .pipe(map(data => data));
  }

  // dormant inner data
  getDormantInnerData(): Observable<any> {
    return this.http
      .get('content/data/reports/dormant-inner.json')
      .pipe(map(data => data));
  }
}
