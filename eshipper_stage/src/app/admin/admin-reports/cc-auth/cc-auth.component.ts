import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { CcAuthFilterDialogComponent } from 'src/app/filters/cc-auth-filter-dialog/cc-auth-filter-dialog.component';
import { CCAuth } from 'src/app/interfaces/reports';

import { AdminReportsService } from '../admin-reports.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cc-auth',
  templateUrl: './cc-auth.component.html',
  styleUrls: ['./cc-auth.component.scss'],
})
export class CcAuthComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  dataEmpty = true;
  smallWidgetItems = [];
  ccAuthList: Array<CCAuth>;
  toolbarSettings = ['Option 1', 'Option 2'];
  itemSettings = ['Option 1', 'Option 2'];

  constructor(public dialog: MatDialog, private getData: AdminReportsService, private router: Router) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData.getCcAuthData().subscribe(data => {
        if (data) {
          this.smallWidgetItems = data.widgets;
          this.ccAuthList = data.content;
          this.dataEmpty = data.content.length ? false : true;
        }
      })
    );
  }

  // get period
  getPeriod(val) {
    console.log(val);
  }
  // end of get period

  // search function
  searchFunction(val) {
    console.log(val);
  }
  // end of search function

  // filter
  filter() {
    const dialogRef = this.dialog.open(CcAuthFilterDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of filter

  // get sort option
  getSortData(data) {
    console.log(data);
  }
  // end of get sort option

  // toolbar settings function
  toolbarSettingsFunction(val) {
    console.log(val);
  }
  // end of toolbar settings function

  // item settings
  itemSettingsFunction(val) {
    console.log(val);
  }
  // end of item settings

  // open order function
  openOrder() {
    const url = '/admin/tracking/order-details';
    this.router.navigate([url]);
  }
  // end of open order function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
