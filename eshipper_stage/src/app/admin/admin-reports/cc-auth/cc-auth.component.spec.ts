import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcAuthComponent } from './cc-auth.component';

describe('CcAuthComponent', () => {
  let component: CcAuthComponent;
  let fixture: ComponentFixture<CcAuthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcAuthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcAuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
