import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { AdminReportsRoutingModule } from './admin-reports-routing.modules';

import { AdminReportsComponent } from './admin-reports.component';
import { GuestQuickQuoteComponent } from './guest-quick-quote/guest-quick-quote.component';
import { InsuranceComponent } from './insurance/insurance.component';
import { CcAuthComponent } from './cc-auth/cc-auth.component';
import { ProfitLossComponent } from './profit-loss/profit-loss.component';
import { CarriersComponent } from './carriers/carriers.component';
import { GuestQuickQuoteInnerComponent } from './guest-quick-quote/guest-quick-quote-inner/guest-quick-quote-inner.component';
import { DormantInnerComponent } from './dormant-inner/dormant-inner.component';

@NgModule({
  declarations: [
    AdminReportsComponent,
    GuestQuickQuoteComponent,
    InsuranceComponent,
    CcAuthComponent,
    ProfitLossComponent,
    CarriersComponent,
    GuestQuickQuoteInnerComponent,
    DormantInnerComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    MaterialModules,
    RouterModule,
    SharedModule,
    AdminReportsRoutingModule,
    ChartsModule,
  ],
  exports: [],
  entryComponents: [],
})
export class AdminReportsModule {}
