import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminReportsComponent } from './admin-reports.component';
import { GuestQuickQuoteComponent } from './guest-quick-quote/guest-quick-quote.component';
import { InsuranceComponent } from './insurance/insurance.component';
import { CcAuthComponent } from './cc-auth/cc-auth.component';
import { ProfitLossComponent } from './profit-loss/profit-loss.component';
import { CarriersComponent } from './carriers/carriers.component';
import { GuestQuickQuoteInnerComponent } from './guest-quick-quote/guest-quick-quote-inner/guest-quick-quote-inner.component';
import { DormantInnerComponent } from './dormant-inner/dormant-inner.component';

const adminReportsRoutes: Routes = [
  {
    path: '',
    redirectTo: 'guest-quick-quote',
    pathMatch: 'full',
  },
  {
    path: 'guest-quick-quote',
    component: GuestQuickQuoteComponent,
    data: { title: 'Guest Quick Quote' },
  },
  {
    path: 'guest-quick-quote/inner',
    component: GuestQuickQuoteInnerComponent,
    data: { title: 'Company Name', backBtn: true },
  },
  {
    path: 'insurance',
    component: InsuranceComponent,
    data: { title: 'Insurance' },
  },
  {
    path: 'cc-auth',
    component: CcAuthComponent,
    data: { title: 'CC Auth' },
  },
  {
    path: 'reports-account',
    loadChildren: () =>
      import('./reports-account/reports-account.module').then(
        mod => mod.ReportsAccountModule
      ),
  },
  {
    path: 'reports-account/dormant/inner',
    component: DormantInnerComponent,
    data: { title: 'Company Name', backBtn: true },
  },
  {
    path: 'profit-loss',
    component: ProfitLossComponent,
    data: { title: 'Profit & Loss' },
  },
  {
    path: 'carriers',
    component: CarriersComponent,
    data: { title: 'Carriers' },
  },
];

const routes: Routes = [
  {
    path: '',
    component: AdminReportsComponent,
    data: { title: 'Reports' },
    children: adminReportsRoutes,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminReportsRoutingModule {}
