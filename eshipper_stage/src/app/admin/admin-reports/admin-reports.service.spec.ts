import { TestBed } from '@angular/core/testing';

import { AdminReportsService } from './admin-reports.service';

describe('AdminReportsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdminReportsService = TestBed.get(AdminReportsService);
    expect(service).toBeTruthy();
  });
});
