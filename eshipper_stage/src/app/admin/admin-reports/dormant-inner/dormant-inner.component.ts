import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { AdminReportsService } from '../admin-reports.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dormant-inner',
  templateUrl: './dormant-inner.component.html',
  styleUrls: ['./dormant-inner.component.scss'],
})
export class DormantInnerComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  dataEmpty = true;
  smallWidgetItems = [];
  itemSettings = ['Option 1', 'Option 2'];
  dormantInnerList = [];

  constructor(private getData: AdminReportsService, private router: Router) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData.getDormantInnerData().subscribe(data => {
        this.smallWidgetItems = data.smallWidgets.length
          ? data.smallWidgets
          : [];
        this.dataEmpty = data.content.length ? false : true;
        this.dormantInnerList = data.content.length ? data.content : [];
      })
    );
  }

  // item settings function
  getItemSettingsValue(val) {
    console.log(val);
  }
  // end of item settings function

  // click on row
  clickOnRow() {
    const url = '/admin/tracking/track';
    this.router.navigate([url]);
  }
  // end of click on row

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
