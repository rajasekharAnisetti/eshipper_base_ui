import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DormantInnerComponent } from './dormant-inner.component';

describe('DormantInnerComponent', () => {
  let component: DormantInnerComponent;
  let fixture: ComponentFixture<DormantInnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DormantInnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DormantInnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
