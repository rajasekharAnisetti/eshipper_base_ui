import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';

import { ProfitLossFiterComponent } from 'src/app/filters/profit-loss-fiter/profit-loss-fiter.component';
import { StatusDialogComponent } from 'src/app/dialogs/status-dialog/status-dialog.component';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { PrintDialogComponent } from 'src/app/dialogs/print-dialog/print-dialog.component';
import { SIMPLESETTINGS } from 'src/app/shared/const/dropdowns';

import { AdminReportsService } from '../admin-reports.service';
import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';

@Component({
  selector: 'app-profit-loss',
  templateUrl: './profit-loss.component.html',
  styleUrls: ['./profit-loss.component.scss'],
})
export class ProfitLossComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  toolbarSettings = SIMPLESETTINGS;
  smallWidgetItems = [];
  listData: Array<any>;
  itemSettings = ['Option 1', 'Option 2'];

  widget = {
    name: 'Widget Name',
    settings: {
      chartValue: false,
      chartTypes: ['Type 1', 'Type 2', 'Type 3'],
    },
  };

  // chart
  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      },
    },
    maintainAspectRatio: false,
  };
  public barChartLabels: Label[] = [
    '2006',
    '2007',
    '2008',
    '2009',
    '2010',
    '2011',
    '2012',
  ];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
  ];
  // end of chart

  constructor(
    private dialog: MatDialog,
    private getData: AdminReportsService
  ) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData.getProfitData().subscribe(data => {
        this.smallWidgetItems = data.smallWidgets.length
          ? data.smallWidgets
          : [];

        this.listData = data.content.length ? data.content : [];
      })
    );
  }

  // get period function
  getPeriod(val) {
    console.log(val);
  }
  // end of get period function

  // search function
  searchFunction(val) {
    console.log(val);
  }
  // end of search function

  // cost info
  penCostInfoDialog(data) {
    this.dialog.open(InfoDialogComponent, {
      data: {
        title: 'Cost Details',
        moreData: data,
      },
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
  }
  // end of cost info

  // filter function
  filter() {
    const dialogRef = this.dialog.open(ProfitLossFiterComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of filter function

  // get sort option
  getSortData(data) {
    console.log(data);
  }
  // end of get sort option

  // toolbar settings
  toolbarSettingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print':
        const printDialogRef = this.dialog.open(PrintDialogComponent, {
          autoFocus: false,
          panelClass: 'small-dialog-width',
        });
        this.subscribe.add(
          printDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }
  // end of toolbar settings

  // click on row
  clickRowFunction(item) {
    console.log(item);
  }
  // end of click on row

  // chart type click function
  chartTypeClick(val) {
    console.log(val);
  }
  // end of chart type click function

  // item settings function
  itemSettingsFunction(item) {
    console.log(item);
  }
  // end of item settings function

  // row status function
  statusFunction() {
    const dialogRef = this.dialog.open(StatusDialogComponent, {
      autoFocus: false,
      panelClass: 'huge-dialog-width',
      data: {
        onlyForm: false,
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of row status function

  // item settings
  getItemSettingsValue(val) {
    console.log(val);
  }
  // end of item settings

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
