import { TestBed } from '@angular/core/testing';

import { ReportsAccountService } from './reports-account.service';

describe('ReportsAccountService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReportsAccountService = TestBed.get(ReportsAccountService);
    expect(service).toBeTruthy();
  });
});
