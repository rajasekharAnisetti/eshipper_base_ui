import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsAccountComponent } from './reports-account.component';

describe('ReportsAccountComponent', () => {
  let component: ReportsAccountComponent;
  let fixture: ComponentFixture<ReportsAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
