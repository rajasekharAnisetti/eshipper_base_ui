import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsPayableComponent } from './reports-payable.component';

describe('ReportsPayableComponent', () => {
  let component: ReportsPayableComponent;
  let fixture: ComponentFixture<ReportsPayableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsPayableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsPayableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
