import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';

import { Payable } from 'src/app/interfaces/invoicing';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { PayDialogComponent } from 'src/app/dialogs/pay-dialog/pay-dialog.component';
import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';
import { ReportsPayableFilterComponent } from 'src/app/filters/reports-payable-filter/reports-payable-filter.component';

import { ReportsAccountService } from '../reports-account.service';

@Component({
  selector: 'app-reports-payable',
  templateUrl: './reports-payable.component.html',
  styleUrls: ['./reports-payable.component.scss'],
})
export class ReportsPayableComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  payablesList: Array<Payable>;
  categoryList: Array<any> = [];
  itemSettings = ['Activities', 'Export', 'Cancel'];
  selection = false;
  dataEmpty = true;
  toolbarIndeterminate = false;
  selectAll = false;
  totalAmount = 0;

  // widget data
  widget = {
    name: 'Widget Name',
    settings: {
      chartValue: false,
      chartTypes: ['Type 1', 'Type 2', 'Type 3'],
    },
  };

  // chart
  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      },
    },
    maintainAspectRatio: false
  };
  public barChartLabels: Label[] = [
    '2006',
    '2007',
    '2008',
    '2009',
    '2010',
    '2011',
    '2012',
  ];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
  ];
  // end of chart

  constructor(
    public dialog: MatDialog,
    private getData: ReportsAccountService,
    private router: Router
  ) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData.getReportPayableList().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        this.payablesList = data.content.length ? data.content : [];
      })
    );
    // get categories
    this.subscribe.add(
      this.getData
        .getApCatigories()
        .subscribe(data => (this.categoryList = data))
    );
  }

  // get period
  getPeriod(val) {
    console.log(val);
  }
  // end of get period

  // search function
  searchFunction(val) {
    console.log(val);
  }
  // end of search function

  // filter function
  filter() {
    const dialogRef = this.dialog.open(ReportsPayableFilterComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of filter function

  // select functions
  selectAllFunction(flag) {
    const select = flag ? true : false;
    this.selection = select ? true : false;
    this.totalAmount = 0;
    for (const i of this.payablesList) {
      i.selected = select;
      if (select) {
        this.totalAmountCount(i.totalAmount, i.selected);
      }
    }
  }

  selectRow(data) {
    const n = this.payablesList.length;
    let item = 0;
    for (const i of this.payablesList) {
      if (i.selected) {
        item++;
      }
    }
    this.selectAll = n === item ? true : false;
    this.toolbarIndeterminate = n > item && item !== 0 ? true : false;
    this.selection = this.selectAll || this.toolbarIndeterminate ? true : false;
    this.totalAmountCount(data.totalAmount, data.selected);
  }
  // end of select functions

  // total amount count
  totalAmountCount(val, check) {
    this.totalAmount = check ? this.totalAmount + val : this.totalAmount - val;
  }
  // end of total amount count

  // get sort option
  getSortData(data) {
    console.log(data);
  }
  // end of get sort option

  // download function
  download() {
    const dialogRef = this.dialog.open(ExportDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of download function

  // chart type click function
  chartTypeClick(val) {
    console.log(val);
  }
  // end of chart type click function

  // click on row
  clickOnPayableItem() {
    const url = '/admin/invoicing/ap/inner/details';
    this.router.navigate([url]);
  }
  // end of click on row

  // get category color
  convertPayeeCategory(category) {
    for (const item of this.categoryList) {
      if (item.name === category) {
        return item.color;
      }
    }
    return 'red';
  }
  // end of get category color

  // get warn
  getWarnFunction(item) {
    switch (item.status.name) {
      case 'Unpaid':
        return true;
      case 'Partially':
        return true;
      case 'Partially Paid':
        return true;
      case 'Released':
        return true;
      default:
        break;
    }
  }
  // end of get warn

  // open total details
  openTotalInfoDialog(data) {
    this.dialog.open(InfoDialogComponent, {
      data: {
        title: 'Price Details',
        moreData: data,
      },
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
  }
  // end of open total details

  // pay row function
  payFunction() {
    const dialogRef = this.dialog.open(PayDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'AP # CWS12345678',
        attachFlag: true,
        checkboxLabel: 'Notify payee by email',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of pay row function

  // row settings
  itemSettingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;

      default:
        break;
    }
  }
  // end of row settings

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
