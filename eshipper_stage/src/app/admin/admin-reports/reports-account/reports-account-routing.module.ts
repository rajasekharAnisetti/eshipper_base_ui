import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportsAccountComponent } from './reports-account.component';
import { ReceivableComponent } from './receivable/receivable.component';
import { ReportsPayableComponent } from './reports-payable/reports-payable.component';
import { ReportsDormantComponent } from './reports-dormant/reports-dormant.component';

const reportsAccountRoutes: Routes = [
  {
    path: '',
    redirectTo: 'receivable',
    pathMatch: 'full',
  },
  {
    path: 'receivable',
    component: ReceivableComponent,
  },
  {
    path: 'payable',
    component: ReportsPayableComponent,
  },
  {
    path: 'dormant',
    component: ReportsDormantComponent,
  },
];

const routes: Routes = [
  {
    path: '',
    component: ReportsAccountComponent,
    data: { title: 'Account' },
    children: reportsAccountRoutes,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportsAccountRoutingModule {}
