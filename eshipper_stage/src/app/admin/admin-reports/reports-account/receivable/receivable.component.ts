import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';

import { AR } from 'src/app/interfaces/invoicing';

import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { CreditInfoDialogComponent } from 'src/app/dialogs/credit-info-dialog/credit-info-dialog.component';
import { ReportsAccountService } from '../reports-account.service';

@Component({
  selector: 'app-receivable',
  templateUrl: './receivable.component.html',
  styleUrls: ['./receivable.component.scss'],
})
export class ReceivableComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  arList: Array<AR>;
  itemSettings = ['Export'];
  // widget data
  widget = {
    name: 'Widget Name',
    settings: {
      chartValue: false,
      chartTypes: ['Type 1', 'Type 2', 'Type 3'],
    },
  };

  // chart
  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      },
    },
    maintainAspectRatio: false
  };
  public barChartLabels: Label[] = [
    '2006',
    '2007',
    '2008',
    '2009',
    '2010',
    '2011',
    '2012',
  ];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
  ];
  // end of chart

  constructor(
    public dialog: MatDialog,
    private getData: ReportsAccountService,
    private router: Router
  ) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData.getReceivableList().subscribe(data => {
        this.arList = data.content.length ? data.content : [];
      })
    );
  }

  // get period
  getPeriod(val) {
    console.log(val);
  }
  // end of get period

  // search function
  searchFunction(val) {
    console.log(val);
  }
  // end of search function

  // download function
  download() {
    const dialogRef = this.dialog.open(ExportDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of download function

  // chart type click function
  chartTypeClick(val) {
    console.log(val);
  }
  // end of chart type click function

  // click on row function
  rowClick(item) {
    console.log(item);
    const url = '/admin/invoicing/ar/customer';
    this.router.navigate([url]);
  }
  // end of click on row function

  // open credit info dialog
  openCreditInfoDialog(data) {
    this.dialog.open(CreditInfoDialogComponent, {
      data: {
        title: 'Credit',
        // check cretil data
        moreData: data,
      },
      autoFocus: false,
      panelClass: 'normal-dialog-width',
    });
  }
  // end of open credit info dialog

  // item settings
  itemSettingsFunction(val, item) {
    switch (val) {
      // export dialog
      case 'Export':
        {
          const dialogRef = this.dialog.open(ExportDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;

      default:
        break;
    }
  }
  // end of item settings

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
