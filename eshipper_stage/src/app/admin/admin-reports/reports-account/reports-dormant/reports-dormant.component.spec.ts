import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsDormantComponent } from './reports-dormant.component';

describe('ReportsDormantComponent', () => {
  let component: ReportsDormantComponent;
  let fixture: ComponentFixture<ReportsDormantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsDormantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsDormantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
