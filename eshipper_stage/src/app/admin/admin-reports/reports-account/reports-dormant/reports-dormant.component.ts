import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { CustomerDetails } from 'src/app/interfaces/customer';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { DormantFilterComponent } from 'src/app/filters/dormant-filter/dormant-filter.component';

import { ReportsAccountService } from '../reports-account.service';

@Component({
  selector: 'app-reports-dormant',
  templateUrl: './reports-dormant.component.html',
  styleUrls: ['./reports-dormant.component.scss'],
})
export class ReportsDormantComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  smallWidgetItems = [];
  dormantList: Array<CustomerDetails> = [];

  constructor(
    private dialog: MatDialog,
    private getData: ReportsAccountService,
    private router: Router
  ) {}

  ngOnInit() {
    this.subscribe.add(
      this.getData.getDormantData().subscribe(data => {
        this.smallWidgetItems = data.smallWidgets.length
          ? data.smallWidgets
          : [];
        this.dormantList = data.content.length ? data.content : [];
      })
    );
  }

  // get period
  getPeriod(val) {
    console.log(val);
  }
  // end of get period

  // search function
  searchFunction(val) {
    console.log(val);
  }
  // end of search function

  // filter function
  filter() {
    const dialogRef = this.dialog.open(DormantFilterComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of filter function

  // get sort option
  getSortData(data) {
    console.log(data);
  }
  // end of get sort option

  // download function
  download() {
    const dialogRef = this.dialog.open(ExportDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of download function

  // click on row
  clickOnCustomerItem(item) {
    console.log(item);
    const url = '/admin/reports/reports-account/dormant/inner';
    this.router.navigate([url]);
  }
  // end of click on row

  // login to customer
  loginToCustomer(item) {
    console.log(item, 'Need to login and go to the current customer dashboard');
    // need to login and go to the current customer dashboard
    const url = '/dashboard';
    this.router.navigate([url]);
  }
  // end of login to customer

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
