import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reports-account',
  templateUrl: './reports-account.component.html',
  styleUrls: ['./reports-account.component.scss'],
})
export class ReportsAccountComponent implements OnInit {
  tabLinks = [
    {
      route: 'receivable',
      name: 'Receivable',
    },
    {
      route: 'payable',
      name: 'Payable',
    },
    {
      route: 'dormant',
      name: 'Dormant',
    },
  ];
  activeLink = this.tabLinks[0];

  constructor() {}

  ngOnInit() {}
}
