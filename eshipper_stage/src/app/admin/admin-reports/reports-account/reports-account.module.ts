import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReportsAccountRoutingModule } from './reports-account-routing.module';

import { ReportsAccountComponent } from './reports-account.component';
import { ReceivableComponent } from './receivable/receivable.component';
import { ReportsPayableComponent } from './reports-payable/reports-payable.component';
import { ReportsDormantComponent } from './reports-dormant/reports-dormant.component';

@NgModule({
  declarations: [
    ReportsAccountComponent,
    ReceivableComponent,
    ReportsPayableComponent,
    ReportsDormantComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    MaterialModules,
    RouterModule,
    SharedModule,
    ReportsAccountRoutingModule,
    ChartsModule,
  ],
  exports: [],
  entryComponents: [],
})
export class ReportsAccountModule {}
