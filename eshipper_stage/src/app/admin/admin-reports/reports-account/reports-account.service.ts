import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ReportsAccountService {
  constructor(private http: HttpClient) {}

  // receivable list
  getReceivableList(): Observable<any> {
    return this.http
      .get('content/data/reports/account-receivable.json')
      .pipe(map(data => data));
  }

  // report payable
  getReportPayableList(): Observable<any> {
    return this.http
      .get('content/data/reports/report-payable.json')
      .pipe(map(data => data));
  }

  // AP Categories
  getApCatigories(): Observable<any> {
    return this.http
      .get('content/data/invoicing/ap-categories.json')
      .pipe(map(data => data));
  }

  // dormant data
  // AP Categories
  getDormantData(): Observable<any> {
    return this.http
      .get('content/data/reports/dormant.json')
      .pipe(map(data => data));
  }
}
