import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
} from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';
import { AdminAccountRoutingModule } from './admin-account-routing.module';

import { AdminAccountComponent } from './admin-account.component';
import { NewsComponent } from './news/news.component';
import { MarkupsComponent } from './markups/markups.component';
import { UsersComponent } from './users/users.component';
import { CustomersComponent } from './customers/customers.component';
import { AdminRolesComponent } from './admin-roles/admin-roles.component';
import { NewsDialogComponent } from './news-dialog/news-dialog.component';
import { GoalCompletionDialogComponent } from './goal-completion-dialog/goal-completion-dialog.component';

@NgModule({
  declarations: [
    AdminAccountComponent,
    NewsComponent,
    MarkupsComponent,
    UsersComponent,
    CustomersComponent,
    AdminRolesComponent,
    NewsDialogComponent,
    GoalCompletionDialogComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    MaterialModules,
    RouterModule,
    SharedModule,
    NgxMaskModule,
    AdminAccountRoutingModule,
  ],
  exports: [],
  entryComponents: [NewsDialogComponent, GoalCompletionDialogComponent],
  providers: [
    // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
    // `MatMomentDateModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
})
export class AdminAccountModule {}
