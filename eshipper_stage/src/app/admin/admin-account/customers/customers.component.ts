import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { CustomerDetails } from 'src/app/interfaces/customer';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { PrintDialogComponent } from 'src/app/dialogs/print-dialog/print-dialog.component';
import { CustomerFilterComponent } from 'src/app/filters/customer-filter/customer-filter.component';
import { SIMPLESETTINGS } from 'src/app/shared/const/dropdowns';

import { AdminAccountService } from '../admin-account.service';
import { GoalCompletionDialogComponent } from '../goal-completion-dialog/goal-completion-dialog.component';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss'],
})
export class CustomersComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  customersList: Array<CustomerDetails>;
  dataEmpty = true;
  toolbarSettings = SIMPLESETTINGS;
  itemSettings = ['Goal Completion', 'Delete'];

  constructor(
    private getData: AdminAccountService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.subscribe.add(
      this.getData.getCustomersList().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        this.customersList = data.content.length ? data.content : [];
      })
    );
  }

  // period filter function
  getPeriod(period) {
    console.log(period);
  }
  // end of period filter function

  // search function
  searchFunction(val) {
    console.log(val);
  }
  // end of search function

  // get sort option
  getSortData(data) {
    console.log(data);
  }
  // end of get sort option

  // filter function
  filterOpen() {
    const dialogRef = this.dialog.open(CustomerFilterComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of filter function

  // toolbar settings function
  toolbarSettingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print':
        const printDialogRef = this.dialog.open(PrintDialogComponent, {
          autoFocus: false,
          panelClass: 'small-dialog-width',
        });
        this.subscribe.add(
          printDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }
  // end of toolbar settings function

  // item settings function
  itemSettingsFunction(val, item) {
    console.log(val, item);
    switch (val) {
      case 'Goal Completion':
        const exportDialogRef = this.dialog.open(
          GoalCompletionDialogComponent,
          {
            panelClass: 'small-dialog-width',
            autoFocus: false,
            data: {
              goalCompletionList: item.goalCompletionList,
            },
          }
        );
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }
  // end of item settings function

  // click on customer item
  clickOnCustomerItem(item) {
    console.log(item);
    const url = '/admin/account/customers/edit/details';
    this.router.navigate([url]);
  }
  // end of click on customer item

  // new customer function
  newCustomer() {
    const url = '/admin/account/customers/new/details';
    this.router.navigate([url]);
  }
  // end of new customer function

  // login to customer
  loginToCustomer(item) {
    console.log(item, 'Need to login and go to the current customer dashboard');
    // need to login and go to the current customer dashboard
    const url = '/dashboard';
    this.router.navigate([url]);
  }
  // end of login to customer

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
