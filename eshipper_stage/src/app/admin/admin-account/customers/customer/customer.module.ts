import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerComponent } from './customer.component';
import { DetailsComponent } from './details/details.component';
import { SettingsComponent } from './settings/settings.component';
import { RolesComponent } from './roles/roles.component';
import { UsersComponent } from './users/users.component';
import { MarkupsComponent } from './markups/markups.component';

@NgModule({
  declarations: [
    CustomerComponent,
    DetailsComponent,
    SettingsComponent,
    RolesComponent,
    UsersComponent,
    MarkupsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModules,
    NgxMaskModule,
    SharedModule,
    CustomerRoutingModule,
  ],
})
export class CustomerModule {}
