import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomerComponent } from './customer.component';
import { DetailsComponent } from './details/details.component';
import { SettingsComponent } from './settings/settings.component';
import { RolesComponent } from './roles/roles.component';
import { UsersComponent } from './users/users.component';
import { MarkupsComponent } from './markups/markups.component';

const routes: Routes = [
  {
    path: '',
    component: CustomerComponent,
    children: [
      {
        path: '',
        redirectTo: 'details',
        pathMatch: 'full',
      },
      {
        path: 'details',
        component: DetailsComponent,
      },
      {
        path: 'settings',
        component: SettingsComponent,
      },
      {
        path: 'roles',
        component: RolesComponent,
      },
      {
        path: 'roles/role/edit',
        loadChildren: () =>
          import('src/app/role/role.module').then(mod => mod.RoleModule),
        data: { title: 'Edit Role', backBtn: true, },
      },
      {
        path: 'roles/role/new',
        loadChildren: () =>
          import('src/app/role/role.module').then(mod => mod.RoleModule),
        data: { title: 'Create Role', backBtn: true },
      },
      {
        path: 'users',
        component: UsersComponent,
      },
      {
        path: 'markups',
        component: MarkupsComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerRoutingModule { }
