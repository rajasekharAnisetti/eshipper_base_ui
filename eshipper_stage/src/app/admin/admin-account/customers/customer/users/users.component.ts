import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { UserDialogComponent } from 'src/app/dialogs/user-dialog/user-dialog.component';
import { User } from 'src/app/interfaces/user';

import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  dataEmpty = true;
  toolbarSettings = ['Option 1', 'Option 2'];
  usersList: Array<User>;
  itemSettings = ['Edit', 'Delete'];

  constructor(public dialog: MatDialog, private getData: CustomerService) {}

  ngOnInit() {
    // get users list
    this.subscribe.add(
      this.getData.getUsersList().subscribe(data => {
        this.dataEmpty = data.content ? false : true;
        this.usersList = data.content;
      })
    );
  }

  // search
  searchFunction(val) {
    console.log(val);
  }
  // end of search

  // toolbar settings
  settingsFunction(val) {
    console.log(val);
  }
  // end of toolbar settings

  // item settings
  itemSettingsFunction(val, item) {
    switch (val) {
      case 'Edit':
        {
          const dialogRef = this.dialog.open(UserDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              title: 'Edit User',
              actionButton: 'Edit',
              uresData: item,
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;

      default:
        break;
    }
  }
  // end of item settings

  // add user function
  addUser() {
    const dialogRef = this.dialog.open(UserDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Add User',
        actionButton: 'Add',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of add user function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
