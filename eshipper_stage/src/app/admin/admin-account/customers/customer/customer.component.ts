import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss'],
})
export class CustomerComponent implements OnInit {
  tabLinks = [
    {
      route: 'details',
      name: 'Details',
    },
    {
      route: 'settings',
      name: 'Settings',
    },
    {
      route: 'roles',
      name: 'Roles',
    },
    {
      route: 'users',
      name: 'Users',
    },
    {
      route: 'markups',
      name: 'Markups',
    },
  ];
  activeLink = this.tabLinks[0];

  constructor() {}

  ngOnInit() {}
}
