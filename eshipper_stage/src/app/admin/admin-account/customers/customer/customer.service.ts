import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  constructor(private http: HttpClient) {}

  // customer details
  getCustomerDetails(): Observable<any> {
    return this.http
      .get('content/data/admin-account/customer/details.json')
      .pipe(map(data => data));
  }

  // customer settings
  getCustomerSettings(): Observable<any> {
    return this.http
      .get('content/data/admin-account/customer/settings.json')
      .pipe(map(data => data));
  }

  // role permissions
  getRolePermissions(): Observable<any> {
    return this.http
      .get('content/data/account/role-permissions.json')
      .pipe(map(data => data));
  }
  // customer roles
  getCustomerRoles(): Observable<any> {
    return this.http
      .get('content/data/account/roles-list.json')
      .pipe(map(data => data));
  }

  // users list
  getUsersList(): Observable<any> {
    return this.http
      .get('content/data/account/users-list.json')
      .pipe(map(data => data));
  }

  // markups list
  getMarkupsList(): Observable<any> {
    return this.http
      .get('content/data/admin-account/customer/markups.json')
      .pipe(map(data => data));
  }
}
