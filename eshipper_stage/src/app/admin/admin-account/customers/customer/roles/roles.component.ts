import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { Role, RolePermissions } from 'src/app/interfaces/roles';

import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss'],
})
export class RolesComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  dataEmpty = true;
  itemSettings = ['Edit', 'Delete'];
  rolePermissions: Array<RolePermissions>;
  rolesList: Array<Role>;

  constructor(public router: Router, private getData: CustomerService) { }

  ngOnInit() {
    // get role permissions
    this.subscribe.add(
      this.getData
        .getRolePermissions()
        .subscribe(data => (this.rolePermissions = data))
    );
    // get roles list
    this.subscribe.add(
      this.getData.getCustomerRoles().subscribe(data => {
        this.rolesList = data.content;
        this.dataEmpty = data.content ? false : true;
      })
    );
  }

  // edit role
  editRole(item) {
    const url = '/admin/account/customers/edit/roles/role/edit';
    localStorage.setItem('roleAdminFlag', 'customer');
    localStorage.setItem('roleData', JSON.stringify(item));
    this.router.navigate([url]);
  }
  // end of edit role

  // item settings function
  itemSettingsFunction(val, item) {
    switch (val) {
      case 'Edit':
        this.editRole(item);
        break;

      default:
        break;
    }
  }
  // end of item settings function

  // add role function
  addRole() {
    const url = '/admin/account/customers/edit/roles/role/new';
    const newData = {
      permissions: this.rolePermissions,
      roleName: ''
    };
    localStorage.setItem('roleAdminFlag', 'customer');
    localStorage.setItem('roleData', JSON.stringify(newData));
    this.router.navigate([url]);
  }
  // end of add role function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
