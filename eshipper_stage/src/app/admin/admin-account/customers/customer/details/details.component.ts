import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { FormControl, Validators } from '@angular/forms';

import { CustomerDetails } from 'src/app/interfaces/customer';
import { Countries } from 'src/app/shared/const/countries';
import { ValidationClassStateMatcher } from 'src/app/shared/classes/validation-class';
import { CURRENCIES_$ } from 'src/app/shared/const/currencies';

import { CustomerService } from '../customer.service';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  customerDetails = <CustomerDetails>{};
  currencyValue = 'CAD $';
  currencyItems = CURRENCIES_$;
  filteredCityes: Observable<any[]>;
  staticData = {
    countries: Countries.COUNTRIES,
    states: Countries.USASTATES,
    cities: Countries.USACITIES,
    stateLabel: 'State',
    zipLabel: 'Zip Code',
  };
  matcher = new ValidationClassStateMatcher();
  validation = {
    customerName: new FormControl(),
    customerType: new FormControl(),
    address1: new FormControl(),
    address2: new FormControl(),
    city: new FormControl(),
    stateOrProvince: new FormControl(),
    zipOrPostal: new FormControl(),
    country: new FormControl(),
    contactPerson: new FormControl(),
    phoneNumber: new FormControl(),
    email: new FormControl('', [Validators.required, Validators.email]),
    shipmentDefaultPackageType: new FormControl(),
    shipmentDefaultPickupLocation: new FormControl(),
    shipmentServiceCountryExcluded: new FormControl(),
    insuranceType: new FormControl(),
    insurance: new FormControl(),
    salesRep: new FormControl(),
    reference1: new FormControl(),
    reference2: new FormControl(),
    reference3: new FormControl(),
    subAffiliateRebate: new FormControl(),
    subAffiliateDiscount: new FormControl(),
    linkToAffiliate: new FormControl(),
  };

  customerTypes = ['Internal', 'Type 2', 'Type 3'];
  shipmentDefaultPackageTypes = ['Envelope', 'Type 2', 'Type 3'];
  shipmentDefaultPickupLocations = ['Location', 'Location 2', 'Location 3'];
  shipmentServiceCountryExcludedList = ['Option 1', 'Option 2', 'Option 3'];
  salesRepList = ['Option 1', 'Option 2', 'Option 3'];
  linkToAffiliateList = ['Option 1', 'Option 2', 'Option 3'];
  insuranceTypes = ['eShipper', 'Option 2', 'Option 3'];

  constructor(
    private getData: CustomerService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    const newFlag = this.activatedRoute.parent.snapshot.data.newFlag;
    if (!newFlag) {
      this.subscribe.add(
        this.getData.getCustomerDetails().subscribe(data => {
          this.customerDetails = data;
          console.log(this.customerDetails);
        })
      );
    }

    // for city
    this.filteredCityes = this.validation.city.valueChanges.pipe(
      startWith(''),
      map(city => city ? this._filter(city) : this.staticData.cities.slice())
    )
  }

  // city autocomplete
  displayFn(city?: string): string | undefined {
    return city ? city : undefined;
  }

  private _filter(city: string) {
    const filterValue = city.toLowerCase();

    return this.staticData.cities.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
  }
  // emd of city autocomplete

  // change country
  changeCountry(country) {
    switch (country) {
      case 'Canada':
        {
          this.staticData.stateLabel = 'Province';
          this.staticData.zipLabel = 'Postal Code';
          this.staticData.states = Countries.CANADAPROVINCES;
          this.staticData.cities = Countries.CANADACITIES;
        }
        break;
      case 'USA':
        {
          this.staticData.stateLabel = 'State';
          this.staticData.zipLabel = 'Zip Code';
          this.staticData.states = Countries.USASTATES;
          this.staticData.cities = Countries.USACITIES;
        }
        break;
      default:
        break;
    }
  }
  // end of change country

  // upload avatar
  upload() {
    console.log('Upload');
  }
  // end of upload avatar

  // save details function
  saveDetails() { }
  // end of save details function

  // change currency
  currencyChange = val => console.log(val);
  // end of change currency

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
