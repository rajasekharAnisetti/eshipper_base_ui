import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormControl, Validators } from '@angular/forms';

import { ValidationClassStateMatcher } from 'src/app/shared/classes/validation-class';
import { AccountSettings } from 'src/app/interfaces/account';
import { CURRENCIES_$ } from 'src/app/shared/const/currencies';

import { CustomerService } from '../customer.service';
import { PACKAGETYPES } from 'src/app/shared/const/dropdowns';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  customerSettings = <AccountSettings>{};
  matcher = new ValidationClassStateMatcher();
  validation = {
    netTerms: new FormControl(),
    netWarning: new FormControl(),
    uspsAccountType: new FormControl(),
    customerType: new FormControl(),
    carrier: new FormControl(),
    service: new FormControl(),
    flatCharge: new FormControl(),
    flatCost: new FormControl(),
    generateInvoicesPer: new FormControl(),
    chargeLevel: new FormControl(),
    customsInvoiceNotRequired: new FormControl(),
    brokerCompanyName: new FormControl(),
    taxId: new FormControl(),
    brokerContactName: new FormControl(),
    brokerEmail: new FormControl('', [Validators.email]),
    brokerPhoneNumber: new FormControl(),
    operationalExpensesCouriers: new FormControl(),
    operationalExpensesPallets: new FormControl(),
    operationalExpensesPostal: new FormControl(),
    affiliateCommissionFor: new FormControl(),
    transactionCommissionFor: new FormControl(),
    apiUsername: new FormControl(),
    apiPassword: new FormControl(),
  };

  currencies = CURRENCIES_$;
  uspsAccountTypes = ['Option 1', 'Option 2', 'Option 3'];
  customerTypes = ['Credit Card', 'Option 2', 'Option 3'];
  carriers = ['Option 1', 'Option 2', 'Option 3'];
  services = ['Option 1', 'Option 2', 'Option 3'];
  generateInvoicesPerList = ['Per Shipment', 'Option 2', 'Option 3'];
  customsInvoiceNotRequiredList = PACKAGETYPES;
  commissionsList = ['Courier', 'Pallet', 'Postal'];

  constructor(
    private getData: CustomerService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.customerSettings.invoiceEmail = [];
    // get new flag from routing
    const newFlag = this.activatedRoute.parent.snapshot.data.newFlag;
    // gel data for edit state
    if (!newFlag) {
      this.subscribe.add(
        this.getData.getCustomerSettings().subscribe(data => {
          this.customerSettings = data;
        })
      );
    }
  }

  // change currency
  changeCurrency(val, item) {
    item = val;
  }
  // end of change currency

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
