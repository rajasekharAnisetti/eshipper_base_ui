import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

import { News } from 'src/app/interfaces/news';

import { AdminAccountService } from '../admin-account.service';
import { NewsDialogComponent } from '../news-dialog/news-dialog.component';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
})
export class NewsComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  newsList: Array<News>;
  dataEmpty = true;
  itemSettings = ['Edit', 'Delete'];

  constructor(
    private getData: AdminAccountService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.subscribe.add(
      this.getData.getNewsList().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        this.newsList = data.content.length ? data.content : [];
      })
    );
  }

  // search function
  searchFunction(val) {
    console.log(val);
  }
  // end of search function

  // item settings function
  itemSettingsFunction(val, item) {
    console.log(val, item);
    switch (val) {
      case 'Edit':
        {
          const dialogRef = this.dialog.open(NewsDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              title: 'Edit News',
              news: item,
              actionButton: 'Edit',
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                item = result;
              }
            })
          );
        }
        break;

      default:
        break;
    }
  }
  // end of item settings function

  // add news function
  addNews() {
    const dialogRef = this.dialog.open(NewsDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Add News',
        actionButton: 'Add',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of add news function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
