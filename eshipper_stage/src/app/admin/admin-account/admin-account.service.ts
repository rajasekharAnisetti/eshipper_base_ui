import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AdminAccountService {
  constructor(private http: HttpClient) {}

  // admin account news
  getNewsList(): Observable<any> {
    return this.http
      .get('content/data/admin-account/news-list.json')
      .pipe(map(data => data));
  }

  // customers list
  getCustomersList(): Observable<any> {
    return this.http
      .get('content/data/admin-account/customers-list.json')
      .pipe(map(data => data));
  }

  // markup list
  getMarkupsList(): Observable<any> {
    return this.http
      .get('content/data/admin-account/customer/markups.json')
      .pipe(map(data => data));
  }

  // users list
  getUsersList(): Observable<any> {
    return this.http
      .get('content/data/admin-account/users-list.json')
      .pipe(map(data => data));
  }

  // user data
  getUserData(): Observable<any> {
    return this.http
      .get('content/data/admin-account/user.json')
      .pipe(map(data => data));
  }

  // role permissions
  getRolePermissions(): Observable<any> {
    return this.http
      .get('content/data/account/role-permissions.json')
      .pipe(map(data => data));
  }

  // roles list
  getRolesList(): Observable<any> {
    return this.http
      .get('content/data/admin-account/roles-list.json')
      .pipe(map(data => data));
  }
}
