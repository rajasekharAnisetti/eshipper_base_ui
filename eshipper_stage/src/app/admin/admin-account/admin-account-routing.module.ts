import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminAccountComponent } from './admin-account.component';
import { NewsComponent } from './news/news.component';
import { MarkupsComponent } from './markups/markups.component';
import { UsersComponent } from './users/users.component';
import { CustomersComponent } from './customers/customers.component';
import { AdminRolesComponent } from './admin-roles/admin-roles.component';
import { UserComponent } from 'src/app/shared/user/user.component';

const accountRoutes: Routes = [
  {
    path: '',
    redirectTo: 'customers',
    pathMatch: 'full',
  },
  {
    path: 'customers',
    component: CustomersComponent,
    data: { title: 'Customers' },
  },
  {
    path: 'customers/new',
    loadChildren: () =>
      import('./customers/customer/customer.module').then(
        mod => mod.CustomerModule
      ),
    data: { title: 'Add New Customer', backBtn: true, newFlag: true },
  },
  {
    path: 'customers/edit',
    loadChildren: () =>
      import('./customers/customer/customer.module').then(
        mod => mod.CustomerModule
      ),
    data: { title: 'Company Name', backBtn: true },
  },
  {
    path: 'roles',
    component: AdminRolesComponent,
    data: { title: 'Roles' },
  },
  {
    path: 'roles/role/edit',
    loadChildren: () =>
      import('src/app/role/role.module').then(mod => mod.RoleModule),
    data: { title: 'Edit Role', backBtn: true, },
  },
  {
    path: 'roles/role/new',
    loadChildren: () =>
      import('src/app/role/role.module').then(mod => mod.RoleModule),
    data: { title: 'Create Role', backBtn: true },
  },
  {
    path: 'users',
    component: UsersComponent,
    data: { title: 'Users' },
  },
  {
    path: 'users/new/user',
    component: UserComponent,
    data: { title: 'Add New User', backBtn: true, newFlag: true },
  },
  {
    path: 'users/edit/user',
    component: UserComponent,
    data: { title: 'User Name', backBtn: true },
  },
  {
    path: 'markups',
    component: MarkupsComponent,
    data: { title: 'Markups' },
  },
  {
    path: 'news',
    component: NewsComponent,
    data: { title: 'News' },
  },
  {
    path: 'markup',
    loadChildren: () =>
      import('../../markup/markup.module').then(mod => mod.MarkupModule),
  },
];

const adminAccountRoutes: Routes = [
  {
    path: '',
    component: AdminAccountComponent,
    children: accountRoutes,
  },
];

@NgModule({
  imports: [RouterModule.forChild(adminAccountRoutes)],
  exports: [RouterModule],
})
export class AdminAccountRoutingModule { }
