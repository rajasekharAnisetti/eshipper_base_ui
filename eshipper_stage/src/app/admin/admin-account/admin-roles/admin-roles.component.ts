import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Router } from '@angular/router';

import { RolePermissions, AdminRole } from 'src/app/interfaces/roles';

import { AdminAccountService } from '../admin-account.service';

@Component({
  selector: 'app-admin-roles',
  templateUrl: './admin-roles.component.html',
  styleUrls: ['./admin-roles.component.scss'],
})
export class AdminRolesComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  dataEmpty = true;
  toolbarSettings = ['Option 1', 'Option 2'];
  sectionItemSettings = ['Edit', 'Delete'];
  itemSettings = ['Edit', 'Delete'];
  rolePermissions: Array<RolePermissions>;
  adminRolesList: Array<AdminRole>;

  constructor(private getData: AdminAccountService, public router: Router) { }

  ngOnInit() {
    // get role permissions
    this.subscribe.add(
      this.getData
        .getRolePermissions()
        .subscribe(data => (this.rolePermissions = data))
    );
    // get roles list
    this.subscribe.add(
      this.getData.getRolesList().subscribe(data => {
        this.adminRolesList = data.content;
        this.dataEmpty = data.content ? false : true;
      })
    );
  }

  // search
  searchFunction(val) {
    console.log(val);
  }
  // end of search

  // toolbar settings
  settingsFunction(val) {
    console.log(val);
  }
  // end of toolbar settings

  // section settings
  sectionItemSettingsFunction(val, section) {
    console.log(val, section);
  }
  // end of section settings

  // edit role
  editRole(item, role) {
    const url = '/customer/account/roles/role/edit';
    localStorage.setItem('roleAdminFlag', 'admin');
    const data = {
      permissions: role.permissions,
      roleName: role.roleName,
      sectionName: item.sectionTitle
    };
    localStorage.setItem('roleData', JSON.stringify(data));
    this.router.navigate([url]);
  }
  // end of edit role

  // item settings function
  itemSettingsFunction(val, item, role) {
    switch (val) {
      case 'Edit':
        this.editRole(item, role);
        break;

      default:
        break;
    }
  }
  // end of item settings function

  // add role function
  addRole() {
    const url = '/customer/account/roles/role/new';
    const newData = {
      permissions: this.rolePermissions,
      roleName: '',
      sectionName: ''
    };
    localStorage.setItem('roleAdminFlag', 'admin');
    localStorage.setItem('roleData', JSON.stringify(newData));
    this.router.navigate([url]);
  }
  // end of add role function

  // drop animation
  dropList(event: CdkDragDrop<string[]>, section) {
    moveItemInArray(section, event.previousIndex, event.currentIndex);
  }
  // end of drop animation

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
