import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import * as moment from 'moment';

import { News } from 'src/app/interfaces/news';

@Component({
  selector: 'app-news-dialog',
  templateUrl: './news-dialog.component.html',
  styleUrls: ['./news-dialog.component.scss'],
})
export class NewsDialogComponent implements OnInit {
  modalData = <News>{};
  setData = <News>{};
  minDate = new Date();
  locations = ['All', 'CA', 'USA'];
  validation = {
    newsTitle: new FormControl(),
    newsMessage: new FormControl(),
    location: new FormControl(),
    expired: new FormControl(),
  };

  constructor(
    public dialogRef: MatDialogRef<NewsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data.news) {
      this.modalData = data.news;
    }
  }

  ngOnInit() {
    if (this.modalData.expired) {
      this.validation.expired = new FormControl(moment(this.modalData.expired));
    }
  }

  closeDialog = () => this.dialogRef.close();

  save() {
    this.setData = this.modalData;
    this.dialogRef.close(this.setData);
  }
}
