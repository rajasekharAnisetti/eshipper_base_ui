import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { User } from 'src/app/interfaces/user';
import { AdminAccountService } from 'src/app/admin/admin-account/admin-account.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  usersList: Array<User>;
  dataEmpty = true;
  roleFilterItems = ['All Roles', 'User Role 1', 'User Role 2'];
  toolbarSettings = ['Option 1', 'Option 2', 'Option 3'];
  itemSettings = ['Delete'];

  constructor(private router: Router, private getData: AdminAccountService) {}

  ngOnInit() {
    // get users list
    this.subscribe.add(
      this.getData.getUsersList().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        this.usersList = data.content.length ? data.content : [];
      })
    );
  }

  // user role filter
  getRoleFilter(val) {
    console.log(val);
  }
  // end of user role filter

  //  search function
  searchFunction(val) {
    console.log(val);
  }
  // end of search function

  // toolbar settings function
  toolbarSettingsFunction(val) {
    console.log(val);
  }
  // end of toolbar settings function

  // item settings function
  itemSettingsFunction(val, item) {
    console.log(val, item);
  }
  // end of item settings function

  // click on row function
  clickOnRow() {
    const url = '/admin/account/users/edit/user';
    this.router.navigate([url]);
  }
  // end of click on row function

  // add new user
  newUser() {
    const url = '/admin/account/users/new/user';
    this.router.navigate([url]);
  }
  // end of add new user

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
