import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoalCompletionDialogComponent } from './goal-completion-dialog.component';

describe('GoalCompletionDialogComponent', () => {
  let component: GoalCompletionDialogComponent;
  let fixture: ComponentFixture<GoalCompletionDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoalCompletionDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoalCompletionDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
