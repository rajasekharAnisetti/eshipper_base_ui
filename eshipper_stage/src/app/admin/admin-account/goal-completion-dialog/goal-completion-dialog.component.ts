import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-goal-completion-dialog',
  templateUrl: './goal-completion-dialog.component.html',
  styleUrls: ['./goal-completion-dialog.component.scss'],
})
export class GoalCompletionDialogComponent implements OnInit {
  modalData = [];

  constructor(
    public dialogRef: MatDialogRef<GoalCompletionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data) {
      this.modalData = data.goalCompletionList;
    }
  }

  ngOnInit() {}

  closeDialog() {
    this.dialogRef.close();
  }
}
