import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxMaskModule } from 'ngx-mask';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialModules } from 'src/app/material-modules';

import { ApRoutingModule } from './ap-routing.module';

import { ApComponent } from './ap.component';
import { ApPayablesComponent } from './ap-payables/ap-payables.component';
import { ApPayeesComponent } from './ap-payees/ap-payees.component';
import { ApCategoriesComponent } from './ap-categories/ap-categories.component';
import { CreatePayableComponent } from './create-payable/create-payable.component';
import { CreatePayeeDialogComponent } from './create-payee-dialog/create-payee-dialog.component';

@NgModule({
  declarations: [
    ApComponent,
    ApPayablesComponent,
    ApPayeesComponent,
    ApCategoriesComponent,
    CreatePayableComponent,
    CreatePayeeDialogComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    MaterialModules,
    RouterModule,
    SharedModule,
    NgxMaskModule,
    ApRoutingModule,
  ],
  exports: [ApComponent],
  entryComponents: [CreatePayeeDialogComponent],
})
export class ApModule {}
