import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ap',
  templateUrl: './ap.component.html',
  styleUrls: ['./ap.component.scss'],
})
export class ApComponent implements OnInit {
  tabLinks = [
    {
      route: 'payables',
      name: 'Payables',
    },
    {
      route: 'payees',
      name: 'Payees',
    },
    {
      route: 'categories',
      name: 'Categories',
    },
  ];
  activeLink = this.tabLinks[0];

  constructor() {}

  ngOnInit() {}
}
