import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';

import { Countries } from 'src/app/shared/const/countries';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-create-payee-dialog',
  templateUrl: './create-payee-dialog.component.html',
  styleUrls: ['./create-payee-dialog.component.scss'],
})
export class CreatePayeeDialogComponent implements OnInit {
  fromFlag = false;
  filteredCityes: Observable<any[]>;
  province = 'Province';
  zip = 'Postal';
  cities = Countries.CANADACITIES;
  provinces = Countries.CANADAPROVINCES;
  countries = Countries.COUNTRIES;
  modalData = {
    from: '',
    payee: '',
    payeeType: '',
    paymentMethod: '',
    address1: '',
    address2: '',
    country: 'Canada',
    city: '',
    province: '',
    zip: '',
    phone: '',
    email: '',
    comment: '',
    remember: false,
    days: '',
  };
  validation = {
    from: new FormControl(),
    payee: new FormControl(),
    payeeType: new FormControl(),
    paymentMethod: new FormControl(),
    address1: new FormControl(),
    address2: new FormControl(),
    country: new FormControl(),
    city: new FormControl(),
    province: new FormControl(),
    zip: new FormControl(),
    phone: new FormControl(),
    email: new FormControl('', [Validators.email]),
    comment: new FormControl(),
    days: new FormControl(),
  };
  fromItems = [
    {
      value: 'customer',
      viewValue: 'Customer',
    },
    {
      value: 'other',
      viewValue: 'Other',
    },
  ];
  payees = [
    {
      value: 'Option 1',
      viewValue: 'Option 1',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];
  payeeTypes = [
    {
      value: 'Option 1',
      viewValue: 'Option 1',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];
  paymentMethods = [
    {
      value: 'Option 1',
      viewValue: 'Option 1',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];

  constructor(
    public dialogRef: MatDialogRef<CreatePayeeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    // for city
    this.filteredCityes = this.validation.city.valueChanges.pipe(
      startWith(''),
      map(city => city ? this._filter(city) : this.cities.slice())
    )
  }

  // from function
  from(val) {
    switch (val) {
      case 'customer':
        this.fromFlag = false;
        break;
      case 'other':
        this.fromFlag = true;
        break;
      default:
        break;
    }
  }
  // end of from function

  // city autocomplete
  displayFn(city?: string): string | undefined {
    return city ? city : undefined;
  }

  private _filter(city: string) {
    const filterValue = city.toLowerCase();

    return this.cities.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
  }
  // emd of city autocomplete

  // change country
  changeCountry(country) {
    switch (country) {
      case 'Canada':
        {
          this.province = 'Province';
          this.zip = 'Postal';
          this.cities = Countries.CANADACITIES;
          this.provinces = Countries.CANADAPROVINCES;
        }
        break;
      case 'USA':
        {
          this.province = 'State';
          this.zip = 'Zip Code';
          this.cities = Countries.USACITIES;
          this.provinces = Countries.USASTATES;
        }
        break;
      default:
        break;
    }
  }
  // end of change country

  closeDialog = () => this.dialogRef.close();

  create() {
    this.dialogRef.close(this.modalData);
  }
}
