import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePayeeDialogComponent } from './create-payee-dialog.component';

describe('CreatePayeeDialogComponent', () => {
  let component: CreatePayeeDialogComponent;
  let fixture: ComponentFixture<CreatePayeeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePayeeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePayeeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
