import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { Category, CategoryItem } from 'src/app/interfaces/invoicing';
import { SetingDialogComponent } from 'src/app/dialogs/seting-dialog/seting-dialog.component';

import { ApService } from '../ap.service';

@Component({
  selector: 'app-ap-categories',
  templateUrl: './ap-categories.component.html',
  styleUrls: ['./ap-categories.component.scss'],
})
export class ApCategoriesComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  dataEmpty = true;
  categoriesList: Array<Category>;
  categoriesContentList: Array<CategoryItem>;

  constructor(public dialog: MatDialog, private getData: ApService) {}

  ngOnInit() {
    // get categories
    this.subscribe.add(
      this.getData.getApCatigories().subscribe(data => {
        this.categoriesList = data;
      })
    );
    // get categories content list
    this.subscribe.add(
      this.getData.getApCatigoriesList().subscribe(data => {
        this.categoriesContentList = data.content;
        this.dataEmpty = data.content ? false : true;
      })
    );
  }

  // category settings
  categorySettings() {
    const dialogRef = this.dialog.open(SetingDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Categories Management',
        dataList: this.categoriesList,
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.categoriesList = result;
        }
      })
    );
  }
  // end of category settings

  // add new category
  addNewCategory(item: Array<Category>) {
    const dialogRef = this.dialog.open(SetingDialogComponent, {
      panelClass: 'normal-dialog-width',
      data: {
        title: 'Categories Management',
        dataList: this.categoriesList,
        new: true,
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          const l = result.length;
          this.categoriesList = result;
          item.push(result[l - 1]);
        }
      })
    );
  }
  // end of add new category

  // change categories
  changeCategories(category, list) {
    list.push(category);
  }
  // end of change categories

  // remove category
  removeCategory(item, index) {
    item.splice(index, 1);
  }
  // end of remove category

  // get category color
  convertPayeeCategory(category) {
    for (const item of this.categoriesList) {
      if (item.name === category) {
        return item.color;
      }
    }
    return 'red';
  }
  // end of get category color

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
