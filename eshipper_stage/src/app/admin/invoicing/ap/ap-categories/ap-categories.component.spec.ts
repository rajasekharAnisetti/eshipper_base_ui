import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApCategoriesComponent } from './ap-categories.component';

describe('ApCategoriesComponent', () => {
  let component: ApCategoriesComponent;
  let fixture: ComponentFixture<ApCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
