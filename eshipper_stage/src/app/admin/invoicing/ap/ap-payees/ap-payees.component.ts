import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { Payee } from 'src/app/interfaces/invoicing';

import { PrintDialogComponent } from 'src/app/dialogs/print-dialog/print-dialog.component';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { PayeeFilterDialogComponent } from 'src/app/filters/payee-filter-dialog/payee-filter-dialog.component';
import { SIMPLESETTINGS } from 'src/app/shared/const/dropdowns';

import { ApService } from '../ap.service';
import { CreatePayeeDialogComponent } from '../create-payee-dialog/create-payee-dialog.component';

@Component({
  selector: 'app-ap-payees',
  templateUrl: './ap-payees.component.html',
  styleUrls: ['./ap-payees.component.scss'],
})
export class ApPayeesComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  payeeList: Array<Payee>;
  dataEmpty = true;
  toolbarSettings = SIMPLESETTINGS;
  itemSettings = ['Edit', 'Delete'];

  constructor(public dialog: MatDialog, private getData: ApService) {}

  ngOnInit() {
    // get payees list
    this.subscribe.add(
      this.getData.getApPayees().subscribe(data => {
        this.payeeList = data.content;
        this.dataEmpty = data.content ? false : true;
      })
    );
  }

  // search functioon
  searchFunction(val) {
    console.log(val);
  }
  // end of search functioon

  // filter function
  filterOpen() {
    const dialogRef = this.dialog.open(PayeeFilterDialogComponent, {
      autoFocus: false,
      panelClass: 'normal-dialog-width',
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of filter function

  // get sort option
  getSortData(data) {
    console.log(data);
  }
  // end of get sort option

  // settings function
  settingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print':
        const printDialogRef = this.dialog.open(PrintDialogComponent, {
          autoFocus: false,
          panelClass: 'small-dialog-width',
        });
        this.subscribe.add(
          printDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }
  // end of settings function

  // item settings function
  itemSettingsFunction(val) {
    console.log(val);
    switch (val) {
      case 'Edit':
        {
          const dialogRef = this.dialog.open(CreatePayeeDialogComponent, {
            autoFocus: false,
            panelClass: 'normal-dialog-width',
            data: {
              title: 'Edit Payee',
              actionButton: 'Edit',
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;

      default:
        break;
    }
  }
  // end of item settings function

  // create payee
  createPayee() {
    const dialogRef = this.dialog.open(CreatePayeeDialogComponent, {
      autoFocus: false,
      panelClass: 'normal-dialog-width',
      data: {
        title: 'Create Payee',
        actionButton: 'Create',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of create payee

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
