import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApPayeesComponent } from './ap-payees.component';

describe('ApPayeesComponent', () => {
  let component: ApPayeesComponent;
  let fixture: ComponentFixture<ApPayeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApPayeesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApPayeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
