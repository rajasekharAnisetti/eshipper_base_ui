import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApService {
  constructor(private http: HttpClient) {}

  // AP Payables
  getApPayables(): Observable<any> {
    return this.http
      .get('content/data/invoicing/ap-payables.json')
      .pipe(map(data => data));
  }
  getDefaultApPayable(): Observable<any> {
    return this.http
      .get('content/data/invoicing/default-ap-payable.json')
      .pipe(map(data => data));
  }

  // AP Payees
  getApPayees(): Observable<any> {
    return this.http
      .get('content/data/invoicing/ap-payees-list.json')
      .pipe(map(data => data));
  }

  // AP Categories
  getApCatigories(): Observable<any> {
    return this.http
      .get('content/data/invoicing/ap-categories.json')
      .pipe(map(data => data));
  }

  // categories content list
  getApCatigoriesList(): Observable<any> {
    return this.http
      .get('content/data/invoicing/ap-categories-list.json')
      .pipe(map(data => data));
  }
}
