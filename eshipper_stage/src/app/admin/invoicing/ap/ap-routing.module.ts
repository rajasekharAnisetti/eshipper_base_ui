import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApComponent } from './ap.component';
import { ApPayablesComponent } from './ap-payables/ap-payables.component';
import { ApPayeesComponent } from './ap-payees/ap-payees.component';
import { ApCategoriesComponent } from './ap-categories/ap-categories.component';
import { CreatePayableComponent } from './create-payable/create-payable.component';

const apRoutesChild: Routes = [
  {
    path: '',
    redirectTo: 'payables',
    pathMatch: 'full',
  },
  {
    path: 'payables',
    component: ApPayablesComponent,
    data: { title: 'AP' },
  },
  {
    path: 'payees',
    component: ApPayeesComponent,
    data: { title: 'AP' },
  },
  {
    path: 'categories',
    component: ApCategoriesComponent,
    data: { title: 'AP' },
  },
];

const apRoutes: Routes = [
  {
    path: '',
    component: ApComponent,
    children: apRoutesChild,
  },
  {
    path: 'create-payable',
    component: CreatePayableComponent,
    data: { title: 'Create Payable', backBtn: true },
  },
  {
    path: 'inner',
    loadChildren: () =>
      import('./ap-inner/ap-inner.module').then(mod => mod.ApInnerModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(apRoutes)],
  exports: [RouterModule],
})
export class ApRoutingModule {}
