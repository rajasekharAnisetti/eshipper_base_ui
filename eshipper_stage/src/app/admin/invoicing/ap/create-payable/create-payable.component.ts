import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';

import { Details } from 'src/app/interfaces/invoicing';

import { ApService } from '../ap.service';

@Component({
  selector: 'app-create-payable',
  templateUrl: './create-payable.component.html',
  styleUrls: ['./create-payable.component.scss'],
})
export class CreatePayableComponent implements OnInit, OnDestroy {
  minDate = new Date();
  subscribe: Subscription = new Subscription();
  data = <Details>{};
  taxes = ['Tax 1', 'Tax 2', 'Tax 3'];
  payeeTypes = ['Type 1', 'Type 2', 'Type 3'];
  payees = ['Payee 1', 'Payee 2', 'Payee 3'];
  paymentMethods = ['Cheque', 'Creditcard'];
  payeeCategories = [];

  validation = {
    due: new FormControl(),
    paid: new FormControl(),
    payeeType: new FormControl(),
    payee: new FormControl(),
    payeeCategory: new FormControl(),
    payment: new FormControl(),
    invoiceId: new FormControl(),
    invoiceDate: new FormControl(),
    invoiceComment: new FormControl(),
    invoiceTotal: new FormControl(),
  };

  constructor(private getData: ApService) { }

  ngOnInit() {
    // get payee categories
    this.subscribe.add(
      this.getData
        .getApCatigories()
        .subscribe(data => (this.payeeCategories = data))
    );

    // get default pagable data
    this.subscribe.add(
      this.getData.getDefaultApPayable().subscribe(data => (this.data = data))
    );
  }

  // invoice upload function
  invoiceUpload() {
    console.log('upload');
  }
  // end of invoice upload function

  // submit function
  submit() {
    console.log('submit');
  }
  // end of submit function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
