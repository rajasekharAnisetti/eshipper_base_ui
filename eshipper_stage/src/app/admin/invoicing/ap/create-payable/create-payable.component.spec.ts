import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePayableComponent } from './create-payable.component';

describe('CreatePayableComponent', () => {
  let component: CreatePayableComponent;
  let fixture: ComponentFixture<CreatePayableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePayableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePayableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
