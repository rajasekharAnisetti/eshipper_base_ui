import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApPayablesComponent } from './ap-payables.component';

describe('ApPayablesComponent', () => {
  let component: ApPayablesComponent;
  let fixture: ComponentFixture<ApPayablesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApPayablesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApPayablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
