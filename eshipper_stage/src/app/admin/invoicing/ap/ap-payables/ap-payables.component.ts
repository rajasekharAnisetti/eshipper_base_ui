import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { Category, Payable } from 'src/app/interfaces/invoicing';
import { PayDialogComponent } from 'src/app/dialogs/pay-dialog/pay-dialog.component';
import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';
import { PrintDialogComponent } from 'src/app/dialogs/print-dialog/print-dialog.component';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { PayableFilterDialogComponent } from 'src/app/filters/payable-filter-dialog/payable-filter-dialog.component';
import {
  SIMPLESETTINGS,
  APPAYABLESITEMSETTINGS,
} from 'src/app/shared/const/dropdowns';

import { ApService } from '../ap.service';

@Component({
  selector: 'app-ap-payables',
  templateUrl: './ap-payables.component.html',
  styleUrls: ['./ap-payables.component.scss'],
})
export class ApPayablesComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  totalAmount = 0;
  payablesList: Array<Payable> = [];
  categoryList: Array<Category> = [];
  selection = false;
  dataEmpty = true;
  toolbarIndeterminate = false;
  selectAll = false;
  toolbarSettings = SIMPLESETTINGS;
  itemSettings = APPAYABLESITEMSETTINGS;

  constructor(
    public dialog: MatDialog,
    private getData: ApService,
    private router: Router
  ) {}

  ngOnInit() {
    // get AP payables data
    this.subscribe.add(
      this.getData.getApPayables().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        this.payablesList = data.content;
        for (const item of this.payablesList) {
          item['selected'] = false;
        }
      })
    );
    // get categories
    this.subscribe.add(
      this.getData
        .getApCatigories()
        .subscribe(data => (this.categoryList = data))
    );
  }

  // select period
  selectPeriod(period) {
    console.log(period);
  }
  // end of select period

  // search functioon
  searchFunction(val) {
    console.log(val);
  }
  // end of search functioon

  // filter function
  filterOpen() {
    const dialogRef = this.dialog.open(PayableFilterDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of filter function

  // get sort option
  getSortData(data) {
    console.log(data);
  }
  // end of get sort option

  // settings function
  settingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print':
        const printDialogRef = this.dialog.open(PrintDialogComponent, {
          autoFocus: false,
          panelClass: 'small-dialog-width',
        });
        this.subscribe.add(
          printDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }
  // end of settings function

  // click on row
  clickOnPayableItem() {
    const url = '/admin/invoicing/ap/inner/details';
    this.router.navigate([url]);
  }
  // end of click on row

  // get category color
  convertPayeeCategory(category) {
    for (const item of this.categoryList) {
      if (item.name === category) {
        return item.color;
      }
    }
    return 'red';
  }
  // end of get category color

  // open total details
  openTotalInfoDialog(data) {
    this.dialog.open(InfoDialogComponent, {
      data: {
        title: 'Price Details',
        moreData: data,
      },
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
  }
  // end of open total details

  // pay row function
  payFunction() {
    const dialogRef = this.dialog.open(PayDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'AP # CWS12345678',
        attachFlag: true,
        checkboxLabel: 'Notify payee by email',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of pay row function

  // get warn
  getWarnFunction(item) {
    switch (item.status.name) {
      case 'Unpaid':
        return true;
      case 'Partially':
        return true;
      case 'Partially Paid':
        return true;
      case 'Released':
        return true;
      default:
        break;
    }
  }
  // end of get warn

  // select functions
  selectAllFunction(flag) {
    const select = flag ? true : false;
    this.selection = select ? true : false;
    this.totalAmount = 0;
    for (const i of this.payablesList) {
      i.selected = select;
      if (select) {
        this.totalAmountCount(i.totalAmount, i.selected);
      }
    }
  }

  selectRow(data) {
    const n = this.payablesList.length;
    let item = 0;
    for (const i of this.payablesList) {
      if (i.selected) {
        item++;
      }
    }
    this.selectAll = n === item ? true : false;
    this.toolbarIndeterminate = n > item && item !== 0 ? true : false;
    this.selection = this.selectAll || this.toolbarIndeterminate ? true : false;
    this.totalAmountCount(data.totalAmount, data.selected);
  }
  // end of select functions

  // row settings
  itemSettingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;

      default:
        break;
    }
  }
  // end of row settings

  // total amount count
  totalAmountCount(val, check) {
    this.totalAmount = check ? this.totalAmount + val : this.totalAmount - val;
  }
  // end of total amount count

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
