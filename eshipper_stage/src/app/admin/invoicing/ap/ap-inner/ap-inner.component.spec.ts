import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApInnerComponent } from './ap-inner.component';

describe('ApInnerComponent', () => {
  let component: ApInnerComponent;
  let fixture: ComponentFixture<ApInnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApInnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApInnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
