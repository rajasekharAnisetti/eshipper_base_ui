import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApInnerService {
  constructor(private http: HttpClient) {}

  // AP Categories
  getApCatigories(): Observable<any> {
    return this.http
      .get('content/data/invoicing/ap-categories.json')
      .pipe(map(data => data));
  }

  // details data
  getApInnerDetails(): Observable<any> {
    return this.http
      .get('content/data/invoicing/ap-inner-details.json')
      .pipe(map(data => data));
  }

  // cheques data
  getApInnerCheques(): Observable<any> {
    return this.http
      .get('content/data/invoicing/ap-inner-cheques.json')
      .pipe(map(data => data));
  }
}
