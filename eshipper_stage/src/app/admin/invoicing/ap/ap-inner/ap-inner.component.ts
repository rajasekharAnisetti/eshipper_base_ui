import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ap-inner',
  templateUrl: './ap-inner.component.html',
  styleUrls: ['./ap-inner.component.scss'],
})
export class ApInnerComponent implements OnInit {
  tabLinks = [
    {
      route: 'details',
      name: 'Details',
    },
    {
      route: 'cheques',
      name: 'Cheques',
    },
  ];
  activeLink = this.tabLinks[0];

  constructor() {}

  ngOnInit() {}
}
