import { TestBed } from '@angular/core/testing';

import { ApInnerService } from './ap-inner.service';

describe('ApInnerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApInnerService = TestBed.get(ApInnerService);
    expect(service).toBeTruthy();
  });
});
