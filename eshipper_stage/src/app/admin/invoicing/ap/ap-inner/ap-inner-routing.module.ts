import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApInnerComponent } from './ap-inner.component';
import { ApInnerDetailsComponent } from './ap-inner-details/ap-inner-details.component';
import { ApInnerChequesComponent } from './ap-inner-cheques/ap-inner-cheques.component';

const apInnerRoutesChild: Routes = [
  {
    path: '',
    redirectTo: 'details',
    pathMatch: 'full',
  },
  {
    path: 'details',
    component: ApInnerDetailsComponent,
    data: { title: 'AP # 12345678', backBtn: true },
  },
  {
    path: 'cheques',
    component: ApInnerChequesComponent,
    data: { title: 'AP # 12345678', backBtn: true },
  },
];

const apInnerRoutes: Routes = [
  {
    path: '',
    component: ApInnerComponent,
    children: apInnerRoutesChild,
  },
];

@NgModule({
  imports: [RouterModule.forChild(apInnerRoutes)],
  exports: [RouterModule],
})
export class ApInnerRoutingModule {}
