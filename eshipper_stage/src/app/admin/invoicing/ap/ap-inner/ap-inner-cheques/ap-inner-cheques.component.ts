import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Cheque } from 'src/app/interfaces/invoicing';

import { ApInnerService } from '../ap-inner.service';

@Component({
  selector: 'app-ap-inner-cheques',
  templateUrl: './ap-inner-cheques.component.html',
  styleUrls: ['./ap-inner-cheques.component.scss'],
})
export class ApInnerChequesComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  chequeData: Array<Cheque> = [];
  dataEmpty = true;
  itemSettings = ['Option 1', 'Option 2', 'Option 3'];

  constructor(private getData: ApInnerService) {}

  ngOnInit() {
    this.subscribe.add(
      this.getData.getApInnerCheques().subscribe(data => {
        if (data.content) {
          this.chequeData = data.content;
          this.dataEmpty = data.content.length ? false : true;
        }
      })
    );
  }

  // click on row function
  clickOnChequeItem() {}
  // end of click on row function

  // item settings function
  itemSettingsFunction(val) {
    console.log(val);
  }
  // end of item settings function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
