import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApInnerChequesComponent } from './ap-inner-cheques.component';

describe('ApInnerChequesComponent', () => {
  let component: ApInnerChequesComponent;
  let fixture: ComponentFixture<ApInnerChequesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApInnerChequesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApInnerChequesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
