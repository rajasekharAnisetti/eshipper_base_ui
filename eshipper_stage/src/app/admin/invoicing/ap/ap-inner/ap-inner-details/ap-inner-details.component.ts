import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Category, Details } from 'src/app/interfaces/invoicing';

import { ApInnerService } from '../ap-inner.service';

@Component({
  selector: 'app-ap-inner-details',
  templateUrl: './ap-inner-details.component.html',
  styleUrls: ['./ap-inner-details.component.scss'],
})
export class ApInnerDetailsComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  detailsData = <Details>{};
  categoryList: Array<Category> = [];
  detailsSettings = ['Option 1', 'Option 2'];
  taxes = ['Tax 1', 'Tax 2', 'Tax 3'];
  payments = ['Check', 'Option 2'];

  constructor(private getData: ApInnerService) { }

  ngOnInit() {
    // get details data
    this.subscribe.add(
      this.getData.getApInnerDetails().subscribe(data => {
        this.detailsData = data;
      })
    );

    // get categories
    this.subscribe.add(
      this.getData
        .getApCatigories()
        .subscribe(data => (this.categoryList = data))
    );
  }

  // change due date
  changeDueDate(data) {
    setTimeout(() => {
      this.detailsData.dueDate = data.target.value ? data.target.value : 0;
    });
  }
  // end of change due date

  // details settings
  detailsSettingsFunction(val) {
    console.log(val);
  }
  // end of details settings

  // get category color
  convertPayeeCategory(category) {
    for (const item of this.categoryList) {
      if (item.name === category) {
        return item.color;
      }
    }
    return 'red';
  }
  // end of get category color

  // save function
  save() {
    console.log('save');
  }
  // end of save function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
