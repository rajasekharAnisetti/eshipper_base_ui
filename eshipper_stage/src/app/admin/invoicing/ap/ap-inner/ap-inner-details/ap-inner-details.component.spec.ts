import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApInnerDetailsComponent } from './ap-inner-details.component';

describe('ApInnerDetailsComponent', () => {
  let component: ApInnerDetailsComponent;
  let fixture: ComponentFixture<ApInnerDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApInnerDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApInnerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
