import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

import { Invoice } from 'src/app/interfaces/invoice';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { InvoiceHistoryDialogComponent } from 'src/app/dialogs/invoice-history-dialog/invoice-history-dialog.component';

import { InvoicingService } from '../invoicing.service';
import { InvoiceActivitiesDialogComponent } from '../invoice-activities-dialog/invoice-activities-dialog.component';

@Component({
  selector: 'app-invoice-preview',
  templateUrl: './invoice-preview.component.html',
  styleUrls: ['./invoice-preview.component.scss'],
})
export class InvoicePreviewComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  data = <Invoice>{};
  invoiceSettings = ['Histories', 'Activities', 'Export', 'Cancel'];
  workOrderSettings = ['Export'];

  constructor(public dialog: MatDialog, private getData: InvoicingService) { }

  ngOnInit() {
    // get invoice data
    this.subscribe.add(
      this.getData.getInvoice().subscribe(data => {
        this.data = data;
      })
    );
    // end of get invoice data
  }

  // settings
  invoiceSettingsFunction(val) {
    switch (val) {
      case 'Histories':
        {
          this.dialog.open(InvoiceHistoryDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              files: this.data.docs,
            },
          });
        }
        break;
      case 'Activities':
        {
          this.subscribe.add(
            this.getData.getDraftInvoiceActivities().subscribe(activities => {
              this.dialog.open(InvoiceActivitiesDialogComponent, {
                panelClass: 'huge-dialog-width',
                autoFocus: false,
                data: {
                  title: 'Invoice #123456',
                  activities: activities,
                },
              });
            })
          );
        }
        break;
      case 'Export':
        {
          const dialogRef = this.dialog.open(ExportDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;
      default:
        break;
    }
  }
  // end of settings

  // work order settings
  workOrderSettingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;

      default:
        break;
    }
  }
  // end of work order settings

  // pay invoice
  payInvoice() {
    console.log('Pay');
  }
  // end of pay invoice

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
