import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { AR } from 'src/app/interfaces/invoicing';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { CreditInfoDialogComponent } from 'src/app/dialogs/credit-info-dialog/credit-info-dialog.component';

import { InvoicingService } from '../invoicing.service';
import { NewDepositDialogComponent } from '../new-deposit-dialog/new-deposit-dialog.component';
import { AddDepositDialogComponent } from '../add-deposit-dialog/add-deposit-dialog.component';

@Component({
  selector: 'app-ar',
  templateUrl: './ar.component.html',
  styleUrls: ['./ar.component.scss'],
})
export class ArComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  toolbarSettings = ['Export', 'New Deposit'];

  arList = <AR>{};
  dataEmpty = true;
  itemSettings = ['Export', 'Add Deposit'];

  constructor(
    public dialog: MatDialog,
    private getData: InvoicingService,
    private router: Router
  ) {}

  ngOnInit() {
    this.subscribe.add(
      this.getData.getAr().subscribe(data => {
        this.arList = data.content ? data.content : {};
        this.dataEmpty = data.content ? false : true;
      })
    );
  }

  // search function
  searchFunction(val) {
    console.log(val);
  }
  // end of search function

  // toolbar settings
  settingsFunction(val) {
    switch (val) {
      // export dialog
      case 'Export':
        {
          const dialogRef = this.dialog.open(ExportDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;
      // add deposit dialog
      case 'New Deposit':
        {
          const dialogRef = this.dialog.open(NewDepositDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              title: 'New Deposit',
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;
      default:
        break;
    }
  }
  // end of toolbar settings

  // click on row function
  rowClick(item) {
    console.log(item);
    const url = '/admin/invoicing/ar/customer';
    this.router.navigate([url]);
  }
  // end of click on row function

  // item settings
  itemSettingsFunction(val, item) {
    switch (val) {
      // export dialog
      case 'Export':
        {
          const dialogRef = this.dialog.open(ExportDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;
      // add deposit dialog
      case 'Add Deposit':
        {
          const dialogRef = this.dialog.open(AddDepositDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              title: 'Add Deposit',
              moreData: item.depositDetails,
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;
      default:
        break;
    }
  }
  // end of item settings

  // open credit info dialog
  openCreditInfoDialog(data) {
    this.dialog.open(CreditInfoDialogComponent, {
      data: {
        title: 'Credit',
        // check cretil data
        moreData: data,
      },
      autoFocus: false,
      panelClass: 'normal-dialog-width',
    });
  }
  // end of open credit info dialog

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
