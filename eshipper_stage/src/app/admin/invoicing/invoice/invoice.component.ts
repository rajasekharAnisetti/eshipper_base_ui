import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { MatDialog } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

import { Invoice } from 'src/app/interfaces/invoice';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { InvoiceHistoryDialogComponent } from 'src/app/dialogs/invoice-history-dialog/invoice-history-dialog.component';

import { InvoicingService } from '../invoicing.service';
import { InvoiceActivitiesDialogComponent } from '../invoice-activities-dialog/invoice-activities-dialog.component';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss'],
})
export class InvoiceComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  data = <Invoice>{};
  invoiceSettings = ['Histories', 'Activities', 'Export', 'Cancel'];
  workOrderSettings = ['Option 1', 'Option 2'];
  discountTypes = ['Discount %', 'Discount $'];

  validation = {
    discount: new FormControl(),
    discountValue: new FormControl(),
    comment: new FormControl(),
    depositAmount: new FormControl(),
  };

  constructor(public dialog: MatDialog, private getData: InvoicingService) {}

  ngOnInit() {
    // get invoice data
    this.subscribe.add(
      this.getData.getInvoice().subscribe(data => {
        this.data = data;
      })
    );
    // end of get invoice data
  }

  // invoice history
  openInvoiceHistory() {}
  // end of invoice history

  // change due date
  changeDueDate(data) {
    setTimeout(() => {
      this.data.dueDate = data.target.value ? data.target.value : 0;
    });
  }
  // end of change due date

  // settings
  invoiceSettingsFunction(val) {
    switch (val) {
      case 'Histories':
        {
          // InvoiceHistoryDialogComponent;
          this.dialog.open(InvoiceHistoryDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              files: this.data.docs,
            },
          });
        }
        break;
      case 'Activities':
        {
          this.subscribe.add(
            this.getData.getDraftInvoiceActivities().subscribe(activities => {
              this.dialog.open(InvoiceActivitiesDialogComponent, {
                panelClass: 'huge-dialog-width',
                autoFocus: false,
                data: {
                  title: 'Invoice #123456',
                  activities: activities,
                },
              });
            })
          );
        }
        break;
      case 'Export':
        {
          const dialogRef = this.dialog.open(ExportDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;
      default:
        break;
    }
  }
  // end of settings

  // work order settings
  workOrderSettingsFunction(val) {
    console.log(val);
  }
  // end of work order settings

  // submit invoice
  submitInvoice() {
    console.log('Submit');
  }
  // end of submit invoice

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
