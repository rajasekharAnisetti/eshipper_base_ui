import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewDepositDialogComponent } from './new-deposit-dialog.component';

describe('NewDepositDialogComponent', () => {
  let component: NewDepositDialogComponent;
  let fixture: ComponentFixture<NewDepositDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewDepositDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewDepositDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
