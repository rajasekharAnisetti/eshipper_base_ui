import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

import { CURRENCIES } from 'src/app/shared/const/currencies';

@Component({
  selector: 'app-new-deposit-dialog',
  templateUrl: './new-deposit-dialog.component.html',
  styleUrls: ['./new-deposit-dialog.component.scss'],
})
export class NewDepositDialogComponent implements OnInit {
  minDate = new Date();
  modalData = {
    amount: null,
    customer: '',
    paymentMethod: '',
    checkNumber: '',
    currency: '',
    datePayment: '',
  };

  validation = {
    customer: new FormControl(),
    paymentMethod: new FormControl(),
    checkNumber: new FormControl(),
    currency: new FormControl(),
    datePayment: new FormControl(),
  };

  customers = ['Customer 1', 'Customer 2', 'Customer 3'];
  paymentMethods = ['Value 1', 'Value 2', 'Value 3'];
  currencies = CURRENCIES;

  constructor(
    public dialogRef: MatDialogRef<NewDepositDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() { }

  // close dialog
  closeDialog = () => this.dialogRef.close();

  // save data
  save = () => this.dialogRef.close(this.modalData);
}
