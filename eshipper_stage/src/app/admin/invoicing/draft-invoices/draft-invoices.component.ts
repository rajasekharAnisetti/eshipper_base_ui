import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

import { Invoice } from 'src/app/interfaces/invoice';
import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { DraftInvoicesFilterDialogComponent } from 'src/app/filters/draft-invoices-filter-dialog/draft-invoices-filter-dialog.component';

import { InvoicingService } from '../invoicing.service';
import { InvoiceActivitiesDialogComponent } from '../invoice-activities-dialog/invoice-activities-dialog.component';

@Component({
  selector: 'app-draft-invoices',
  templateUrl: './draft-invoices.component.html',
  styleUrls: ['./draft-invoices.component.scss'],
})
export class DraftInvoicesComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  dataEmpty = true;
  toolbarIndeterminate = false;
  selectAll = false;
  selection = false;
  invoicesList: Array<Invoice>;
  toolbarSettings = ['Details PDF', 'Summary PDF'];
  itemSettings = ['Activities', 'Export', 'Delete'];

  constructor(public dialog: MatDialog, private getData: InvoicingService) {}

  ngOnInit() {
    this.subscribe.add(
      this.getData.getDraftInvoices().subscribe(data => {
        if (data) {
          this.invoicesList = data.content;
          this.dataEmpty = false;
        }
      })
    );
  }

  // filter function
  filterOpen() {
    const dialogRef = this.dialog.open(DraftInvoicesFilterDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of filter function

  // settings data
  settingsFunction(val) {
    console.log(val);
  }
  // end of settings data

  // selection
  selectAllFunction(flag) {
    const select = flag ? true : false;
    this.selection = select ? true : false;
    for (const i of this.invoicesList) {
      i.selected = select;
    }
  }

  selectRow() {
    const n = this.invoicesList.length;
    let item = 0;
    for (const i of this.invoicesList) {
      if (i.selected) {
        item++;
      }
    }
    this.selectAll = n === item ? true : false;
    this.toolbarIndeterminate = n > item && item !== 0 ? true : false;
    this.selection = this.toolbarIndeterminate || this.selectAll ? true : false;
  }
  // end of selection

  // get sort data
  getSortData(data) {
    console.log(data);
  }
  // end of get sort data

  // get period
  getPeriod(period) {
    console.log(period);
  }
  // end of get period

  // search data
  getSearch(val) {
    console.log(val);
  }
  // end of search data

  // item settings
  itemSettingsFunction(val) {
    switch (val) {
      case 'Activities':
        {
          this.subscribe.add(
            this.getData.getDraftInvoiceActivities().subscribe(activities => {
              this.dialog.open(InvoiceActivitiesDialogComponent, {
                panelClass: 'huge-dialog-width',
                autoFocus: false,
                data: {
                  title: 'Invoice #123456',
                  activities: activities,
                },
              });
            })
          );
        }
        break;
      case 'Export':
        {
          const dialogRef = this.dialog.open(ExportDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;
      default:
        break;
    }
  }
  // end of item settings

  // open total details
  openTotalDetails(data) {
    this.dialog.open(InfoDialogComponent, {
      data: {
        title: 'Price Details',
        moreData: data,
      },
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
  }
  // end of open total details

  // submit
  submitFunction() {
    console.log('submit');
  }
  // end of submit

  // submit all function
  submitAll() {
    console.log('submit all');
  }
  // end of submit all function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
