import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DraftInvoicesComponent } from './draft-invoices.component';

describe('DraftInvoicesComponent', () => {
  let component: DraftInvoicesComponent;
  let fixture: ComponentFixture<DraftInvoicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DraftInvoicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DraftInvoicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
