import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

import { COD } from 'src/app/interfaces/invoicing';
import { CURRENCIES_$ } from 'src/app/shared/const/currencies';
import { RenameDialogComponent } from 'src/app/dialogs/rename-dialog/rename-dialog.component';
import { currency } from 'src/app/shared/functions/currency';

import { InvoicingService } from '../invoicing.service';

@Component({
  selector: 'app-cod',
  templateUrl: './cod.component.html',
  styleUrls: ['./cod.component.scss'],
})
export class CodComponent implements OnInit, OnDestroy {
  minDate = new Date();
  subscribe: Subscription = new Subscription();
  data = <COD>{};
  detailsData: any = {};
  docSettings = ['Download', 'Rename', 'Delete'];
  docs = [
    {
      name: 'Doc Name.pdf',
      state: 'Ptivate',
    },
    {
      name: 'Doc Name.pdf',
      state: 'Ptivate',
    },
    {
      name: 'Doc Name.pdf',
      state: 'Ptivate',
    },
  ];
  deliveryList = ['Pick Up & Delivery', 'Option 2', 'Option 3'];
  deliveryDocsList = ['Deliver Docs to Terminal', 'Option 2', 'Option 3'];
  shipments = ['1', '2', '3', '4'];
  taxes = ['Tax 1', 'Tax 2', 'Tax 3'];

  currencies = CURRENCIES_$;

  validation = {
    signedBy: new FormControl(),
    reportTo: new FormControl(),
    date: new FormControl(),
    time: new FormControl(),
    internalComments: new FormControl(),
    chargesOrginalCost: new FormControl(),
    chargesOrginalCharge: new FormControl(),
    chargesFuelCost: new FormControl(),
    chargesFuelCharge: new FormControl(),
  };

  constructor(private getData: InvoicingService, public dialog: MatDialog) { }

  ngOnInit() {
    // get cod data
    this.subscribe.add(
      this.getData.getCod().subscribe(data => {
        this.data.additionalServices = data.additionalServices;
        this.data = data;

        this.detailsData = {
          from: {
            label: 'From',
            title: this.data.details.fromComapnyName,
            description: this.data.details.fromAddress,
            secondDescription: this.data.details.fromDate,
          },
          to: {
            label: 'To',
            title: this.data.details.toComapnyName,
            description: this.data.details.toAddress,
            secondDescription: this.data.details.toDate,
          },
          package: {
            label: 'Package',
            value: this.data.details.package,
          },
          quantity: {
            label: 'Quantity',
            value: this.data.details.quantity,
          },
          dimension: {
            label: 'Dimension & Weight (LxWxH)',
            value: this.data.details.dimensionWeight,
            infoFlag: 'weight',
            info: {
              title: 'Dimension Details',
              moreData: this.data.details.dimensionWeightDetais,
            },
          },
          instructions: {
            label: 'Special Instructions',
            value: this.data.details.specialInstructions,
          },
        };
        // end of details data
      })
    );
    // end of get cod data
  }

  // get time function
  getTime(time) {
    this.data.podTime = time;
  }
  // end of get time function

  // save function
  save() {
    console.log('Save');
  }
  // end of save function

  // upload function
  uplod() { }
  // end of upload function

  // doc menu
  docSettingsFunction(val) {
    switch (val) {
      case 'Rename':
        {
          const dialogRef = this.dialog.open(RenameDialogComponent, {
            autoFocus: false,
            panelClass: 'normal-dialog-width',
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;

      default:
        break;
    }
  }

  changePublic = item =>
    item.state === 'Public'
      ? (item.state = 'Private')
      : (item.state = 'Public');

  // end of doc menu

  // change currency function
  changeCurrency(val) {
    this.data.currency = val;
  }
  // end of change currency function

  // dynamic row
  addRow() {
    this.data.additionalServices.push({
      delivery: 'Pick Up & Delivery',
      deliveryDocs: 'Deliver Docs to Terminal',
      cost: 0,
      charge: 0,
      noOfShipments: '1',
      totalPrice: 0,
    });
  }
  removeRow(index) {
    this.data.additionalServices.splice(index, 1);
  }
  // end of dynamic row

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
