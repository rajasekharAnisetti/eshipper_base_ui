import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { Invoice } from 'src/app/interfaces/invoice';
import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';
import { InvoicesFilterDialogComponent } from 'src/app/filters/invoices-filter-dialog/invoices-filter-dialog.component';
import { PayDialogComponent } from 'src/app/dialogs/pay-dialog/pay-dialog.component';
import {
  ARCUSTOMERTOOLBARSETTINGS,
  ARCUSTOMERITEMSETTINGS,
  ARCUSTOMERITEMSETTINGSMORE,
} from 'src/app/shared/const/dropdowns';

import { InvoicingService } from '../invoicing.service';
import { InvoiceActivitiesDialogComponent } from '../invoice-activities-dialog/invoice-activities-dialog.component';
import { ReleaseDialogComponent } from '../release-dialog/release-dialog.component';

@Component({
  selector: 'app-ar-customer',
  templateUrl: './ar-customer.component.html',
  styleUrls: ['./ar-customer.component.scss'],
})
export class ArCustomerComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  dataEmpty = true;
  invoicesList: Array<Invoice> = [];
  smallWidgetItems: Array<any>;

  selection = false;
  toolbarIndeterminate = false;
  selectAll = false;
  toolbarSettings = ARCUSTOMERTOOLBARSETTINGS;
  itemSettings = ARCUSTOMERITEMSETTINGS;
  itemSettingsMoreOptions = ARCUSTOMERITEMSETTINGSMORE;

  creditReleased = {
    currency: 'CAD',
    list: [
      {
        date: '04/22/2019',
        amount: 300.0,
      },
      {
        date: '04/22/2019',
        amount: 500.0,
      },
    ],
  };

  constructor(
    public dialog: MatDialog,
    private getData: InvoicingService,
    private router: Router
  ) { }

  ngOnInit() {
    // get invoices
    this.subscribe.add(
      this.getData.getArCustomer().subscribe(data => {
        this.invoicesList = data.invoicesList;
        for (const item of this.invoicesList) {
          item['selected'] = false;
        }
        this.dataEmpty = data.invoicesList.length ? false : true;
        this.smallWidgetItems = data.widgets;
      })
    );
  }

  // filter function
  filterOpen() {
    const dialogRef = this.dialog.open(InvoicesFilterDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of filter function

  // get sort option
  getSortData(data) {
    console.log(data);
  }
  // end of get sort option

  // select functions
  selectAllFunction(flag) {
    const select = flag ? true : false;
    this.selection = select ? true : false;
    for (const i of this.invoicesList) {
      i.selected = select;
    }
  }

  selectRow(val, el) {
    const n = this.invoicesList.length;
    let item = 0;
    console.log(val, el);
    for (const i of this.invoicesList) {
      if (i.selected) {
        item++;
      }
    }

    this.selectAll = n === item ? true : false;
    this.toolbarIndeterminate = n > item && item !== 0 ? true : false;
    this.selection = this.selectAll || this.toolbarIndeterminate ? true : false;
  }
  // end of select functions

  // get period
  getPeriod(period) {
    console.log(period);
  }
  // end of get period

  // search data
  getSearch(val) {
    console.log(val);
  }
  // enf of search data

  // settings data
  getSettings(val) {
    console.log(val);
  }
  // end of settings data

  // item settings function
  itemSettingsFunction(val) {
    switch (val) {
      case 'Activities':
        {
          this.subscribe.add(
            this.getData.getDraftInvoiceActivities().subscribe(activities => {
              this.dialog.open(InvoiceActivitiesDialogComponent, {
                panelClass: 'huge-dialog-width',
                autoFocus: false,
                data: {
                  title: 'Invoice #123456',
                  activities: activities,
                },
              });
            })
          );
        }
        break;
      case 'Release Credit Limit':
        {
          const dialogRef = this.dialog.open(ReleaseDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              title: 'Invoice # CWS12345678',
              creditReleased: this.creditReleased,
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;
      default:
        break;
    }
  }
  // end of item settings function

  // item pay function
  payFunction() {
    const dialogRef = this.dialog.open(PayDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Invoice # CWS12345678',
        checkboxLabel: 'Notify customer by Email',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of item pay function

  // click on invoice row function
  clickOnInvoiceItem() {
    const url = '/admin/invoicing/invoice';
    this.router.navigate([url]);
  }
  // end of click on invoice row function

  // open total info dialog
  openTotalInfoDialog(data) {
    this.dialog.open(InfoDialogComponent, {
      data: {
        title: 'Price Details',
        moreData: data,
      },
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
  }
  // end of open total info dialog

  // get warn
  getWarnFunction(item) {
    switch (item.status.name) {
      case 'Unpaid':
        return true;
      case 'Partially':
        return true;
      case 'Partially Paid':
        return true;
      case 'Released':
        return true;
      default:
        break;
    }
  }
  // end of get warn

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
