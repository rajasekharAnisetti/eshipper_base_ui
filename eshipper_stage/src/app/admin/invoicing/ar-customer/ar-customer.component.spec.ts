import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArCustomerComponent } from './ar-customer.component';

describe('ArCustomerComponent', () => {
  let component: ArCustomerComponent;
  let fixture: ComponentFixture<ArCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
