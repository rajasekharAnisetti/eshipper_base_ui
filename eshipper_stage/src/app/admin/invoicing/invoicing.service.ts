import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class InvoicingService {
  constructor(private http: HttpClient) {}

  // draft invoices list
  getDraftInvoices(): Observable<any> {
    return this.http
      .get('content/data/invoicing/draft-invoices-list.json')
      .pipe(map(data => data));
  }

  // draft invoice activities
  getDraftInvoiceActivities(): Observable<any> {
    return this.http
      .get('content/data/invoicing/invoice-activities.json')
      .pipe(map(data => data));
  }

  // draft invoice
  getDraftInvoice(): Observable<any> {
    return this.http
      .get('content/data/invoicing/draft-invoice.json')
      .pipe(map(data => data));
  }

  // COD data
  getCod(): Observable<any> {
    return this.http
      .get('content/data/invoicing/cod.json')
      .pipe(map(data => data));
  }

  // get invoices
  getInvoices(): Observable<any> {
    return this.http
      .get('content/data/invoicing/invoices-list.json')
      .pipe(map(data => data));
  }

  // get invoice
  getInvoice(): Observable<any> {
    return this.http
      .get('content/data/invoicing/invoice.json')
      .pipe(map(data => data));
  }

  // get ar list
  getAr(): Observable<any> {
    return this.http
      .get('content/data/invoicing/ar.json')
      .pipe(map(data => data));
  }

  // get ar customer
  getArCustomer(): Observable<any> {
    return this.http
      .get('content/data/invoicing/ar-customer.json')
      .pipe(map(data => data));
  }
}
