import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-add-deposit-dialog',
  templateUrl: './add-deposit-dialog.component.html',
  styleUrls: ['./add-deposit-dialog.component.scss'],
})
export class AddDepositDialogComponent implements OnInit {
  minDate = new Date();
  total = 0;
  modalData = {
    amount: null,
    paymentMethod: '',
    checkNumber: '',
    datePayment: '',
  };

  validation = {
    paymentMethod: new FormControl(),
    checkNumber: new FormControl(),
    datePayment: new FormControl(),
  };

  paymentMethods = ['Value 1', 'Value 2', 'Value 3'];

  constructor(
    public dialogRef: MatDialogRef<AddDepositDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    for (const item of data.moreData) {
      this.total = this.total + item.amount;
    }
  }

  ngOnInit() { }

  // close dialog
  closeDialog = () => this.dialogRef.close();

  // save data
  save = () => this.dialogRef.close(this.modalData);
}
