import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InvoicingComponent } from './invoicing.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { DraftInvoicesComponent } from './draft-invoices/draft-invoices.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { InvoicePreviewComponent } from './invoice-preview/invoice-preview.component';
import { ArComponent } from './ar/ar.component';
import { DraftInvoiceComponent } from './draft-invoice/draft-invoice.component';
import { CodComponent } from './cod/cod.component';
import { ArCustomerComponent } from './ar-customer/ar-customer.component';

const invoicingRoutes: Routes = [
  {
    path: '',
    redirectTo: 'draft-invoices',
    pathMatch: 'full',
  },
  {
    path: 'draft-invoices',
    component: DraftInvoicesComponent,
    data: { title: 'Draft Invoices' },
  },
  {
    path: 'draft-invoice',
    component: DraftInvoiceComponent,
    data: { title: 'Draft # 123456', backBtn: true },
  },
  {
    path: 'invoices',
    component: InvoicesComponent,
    data: { title: 'Invoices' },
  },
  {
    path: 'invoice',
    component: InvoiceComponent,
    data: { title: 'Invoice # 123456', backBtn: true },
  },
  {
    path: 'invoice-preview',
    component: InvoicePreviewComponent,
    data: { title: 'Invoice # 123456', backBtn: true },
  },
  {
    path: 'cod',
    component: CodComponent,
    data: { title: 'COD', backBtn: true },
  },
  {
    path: 'ar',
    component: ArComponent,
    data: { title: 'AR' },
  },
  {
    path: 'ar/customer',
    component: ArCustomerComponent,
    data: { title: 'Customer Name', backBtn: true },
  },
  {
    path: 'ap',
    loadChildren: () => import('./ap/ap.module').then(mod => mod.ApModule),
  },
];

const routes: Routes = [
  {
    path: '',
    component: InvoicingComponent,
    data: { title: 'Invoicing' },
    children: invoicingRoutes,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InvoicingRoutingModule {}
