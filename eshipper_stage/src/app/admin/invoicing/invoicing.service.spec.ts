import { TestBed } from '@angular/core/testing';

import { InvoicingService } from './invoicing.service';

describe('InvoicingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InvoicingService = TestBed.get(InvoicingService);
    expect(service).toBeTruthy();
  });
});
