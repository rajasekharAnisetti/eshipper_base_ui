import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import * as moment from 'moment';

import { DraftInvoice } from 'src/app/interfaces/invoicing';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';

import { InvoicingService } from '../invoicing.service';
import { InvoiceActivitiesDialogComponent } from '../invoice-activities-dialog/invoice-activities-dialog.component';

@Component({
  selector: 'app-draft-invoice',
  templateUrl: './draft-invoice.component.html',
  styleUrls: ['./draft-invoice.component.scss'],
})
export class DraftInvoiceComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  invoiceSettings = ['Activities', 'Export', 'Delete'];
  workOrderSettings = ['Option 1', 'Option 2'];
  data = <DraftInvoice>{};
  discountTypes = ['Discount %', 'Discount $'];

  validation = {
    due: new FormControl(),
    discount: new FormControl(),
    discountValue: new FormControl(),
    comment: new FormControl(),
    depositAmount: new FormControl(),
  };

  constructor(public dialog: MatDialog, private getData: InvoicingService) {}

  ngOnInit() {
    this.subscribe.add(
      this.getData.getDraftInvoice().subscribe(data => {
        this.data = data;
        this.validation.due = new FormControl(moment(this.data.dueDate));
      })
    );
  }

  // settings
  invoiceSettingsFunction(val) {
    switch (val) {
      case 'Activities':
        {
          this.subscribe.add(
            this.getData.getDraftInvoiceActivities().subscribe(activities => {
              this.dialog.open(InvoiceActivitiesDialogComponent, {
                panelClass: 'huge-dialog-width',
                autoFocus: false,
                data: {
                  title: 'Invoice #123456',
                  activities: activities,
                },
              });
            })
          );
        }
        break;
      case 'Export':
        {
          const dialogRef = this.dialog.open(ExportDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;
      default:
        break;
    }
  }
  // end of settings

  // change due date
  changeDueDate(data) {
    setTimeout(() => {
      this.data.dueDate = data.target.value ? data.target.value : 0;
    });
  }
  // end of change due date

  // work order settings
  workOrderSettingsFunction(val) {
    console.log(val);
  }
  // end of work order settings

  // submit invoice
  submitInvoice() {}
  // end of submit invoice

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
