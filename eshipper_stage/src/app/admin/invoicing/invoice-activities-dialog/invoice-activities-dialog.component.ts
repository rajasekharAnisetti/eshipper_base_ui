import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-invoice-activities-dialog',
  templateUrl: './invoice-activities-dialog.component.html',
  styleUrls: ['./invoice-activities-dialog.component.scss'],
})
export class InvoiceActivitiesDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<InvoiceActivitiesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();
}
