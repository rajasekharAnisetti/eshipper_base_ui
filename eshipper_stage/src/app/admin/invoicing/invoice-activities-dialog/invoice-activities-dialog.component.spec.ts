import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceActivitiesDialogComponent } from './invoice-activities-dialog.component';

describe('InvoiceActivitiesDialogComponent', () => {
  let component: InvoiceActivitiesDialogComponent;
  let fixture: ComponentFixture<InvoiceActivitiesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceActivitiesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceActivitiesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
