import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxMaskModule } from 'ngx-mask';
import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
} from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialModules } from 'src/app/material-modules';

import { InvoicingRoutingModule } from './invoicing-routing.module';

import { InvoicingComponent } from './invoicing.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { DraftInvoicesComponent } from './draft-invoices/draft-invoices.component';
import { ArComponent } from './ar/ar.component';
import { DraftInvoiceComponent } from './draft-invoice/draft-invoice.component';
import { CodComponent } from './cod/cod.component';
import { InvoiceActivitiesDialogComponent } from './invoice-activities-dialog/invoice-activities-dialog.component';
import { ReleaseDialogComponent } from './release-dialog/release-dialog.component';
import { NewDepositDialogComponent } from './new-deposit-dialog/new-deposit-dialog.component';
import { AddDepositDialogComponent } from './add-deposit-dialog/add-deposit-dialog.component';
import { ArCustomerComponent } from './ar-customer/ar-customer.component';
import { InvoicePreviewComponent } from './invoice-preview/invoice-preview.component';

@NgModule({
  declarations: [
    InvoicingComponent,
    InvoicesComponent,
    DraftInvoicesComponent,
    ArComponent,
    InvoiceComponent,
    CodComponent,
    InvoiceActivitiesDialogComponent,
    DraftInvoiceComponent,
    ReleaseDialogComponent,
    NewDepositDialogComponent,
    AddDepositDialogComponent,
    ArCustomerComponent,
    InvoicePreviewComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    MaterialModules,
    RouterModule,
    SharedModule,
    InvoicingRoutingModule,
    NgxMaskModule.forRoot(),
  ],
  exports: [InvoicingComponent],
  entryComponents: [
    InvoiceActivitiesDialogComponent,
    ReleaseDialogComponent,
    NewDepositDialogComponent,
    AddDepositDialogComponent,
  ],
  providers: [
    // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
    // `MatMomentDateModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
})
export class InvoicingModule {}
