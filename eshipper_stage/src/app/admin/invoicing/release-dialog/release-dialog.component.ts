import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-release-dialog',
  templateUrl: './release-dialog.component.html',
  styleUrls: ['./release-dialog.component.scss'],
})
export class ReleaseDialogComponent implements OnInit {
  modalData = {
    amount: null,
    releaseDay: '',
  };

  validation = {
    releaseDay: new FormControl(),
  };

  total = 0;

  releaseDays = ['Option 1', 'Option 2', 'Option 3'];

  constructor(
    public dialogRef: MatDialogRef<ReleaseDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    for (const item of data.creditReleased.list) {
      this.total = this.total + item.amount;
    }
  }

  ngOnInit() {}

  // close dialog
  closeDialog = () => this.dialogRef.close();

  // send release
  release = () => this.dialogRef.close(this.modalData);
}
