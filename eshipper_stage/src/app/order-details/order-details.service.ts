import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class OrderDetailsService {
  constructor(private http: HttpClient) {}

  getCustomerOrderDetails(): Observable<any> {
    return this.http
      .get('content/data/customer-order-details.json')
      .pipe(map(data => data));
  }

  getAdminOrderDetails(): Observable<any> {
    return this.http
      .get('content/data/admin-order-details.json')
      .pipe(map(data => data));
  }

  getProducts(): Observable<any> {
    return this.http
      .get('content/data/product-list.json')
      .pipe(map(data => data));
  }
}
