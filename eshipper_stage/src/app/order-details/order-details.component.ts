import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';

import { OrderDetails } from 'src/app/interfaces/order-details';
import { EmailDialogComponent } from 'src/app/dialogs/email-dialog/email-dialog.component';
import { Product } from 'src/app/interfaces/product';
import { OrderDetailsService } from './order-details.service';
import { PrintDialogComponent } from '../dialogs/print-dialog/print-dialog.component';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss'],
})
export class OrderDetailsComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  account = localStorage.getItem('account');
  orderDetailsData = <OrderDetails>{};
  products: Array<Product>;
  eComFlag = false;

  constructor(private getData: OrderDetailsService, public dialog: MatDialog) { }

  ngOnInit() {
    if (this.account === 'admin' || this.account === 'superadmin') {
      // get admin track
      this.subscribe.add(
        this.getData.getAdminOrderDetails().subscribe(data => {
          this.orderDetailsData = data;
          this.eComFlag = false;
        })
      );
    } else {
      // get customer track
      this.subscribe.add(
        this.getData.getCustomerOrderDetails().subscribe(data => {
          this.orderDetailsData = data;
          this.eComFlag = true;
        })
      );
      // get products
      this.subscribe.add(
        this.getData.getProducts().subscribe(data => {
          this.products = data.content;
        })
      );
    }
  }

  // email function
  emailFunction() {
    const dialogRef = this.dialog.open(EmailDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {},
    });

    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of email function

  // print function
  printFunction() {
    const printDialogRef = this.dialog.open(PrintDialogComponent, {
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
    this.subscribe.add(
      printDialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of print function

  // file function
  docFunction(url) {
    console.log(url);
  }
  // end of file function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
