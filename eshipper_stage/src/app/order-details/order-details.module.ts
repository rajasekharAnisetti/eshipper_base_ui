import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { OrderDetailsRoutingModule } from './order-details-routing.module';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { OrderDetailsComponent } from './order-details.component';

@NgModule({
  declarations: [OrderDetailsComponent],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MaterialModules,
    OrderDetailsRoutingModule,
    SharedModule,
  ],
  exports: [OrderDetailsComponent],
})
export class OrderDetailsModule {}
