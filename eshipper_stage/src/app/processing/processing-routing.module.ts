import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// global pages
import { ProcessingComponent } from './processing.component';

const routes: Routes = [
  {
    path: 'processing',
    component: ProcessingComponent,
    data: { loginFlag: true },
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class ProcessingRoutingModule {}
