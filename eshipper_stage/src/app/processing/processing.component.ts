import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-processing',
  templateUrl: './processing.component.html',
  styleUrls: ['./processing.component.scss'],
})
export class ProcessingComponent implements OnInit {
  url = localStorage.getItem('processing');

  constructor(private router: Router) {}

  ngOnInit() {
    setTimeout(() => {
      this.router.navigate([this.url]);
    }, 3000);
  }
}
