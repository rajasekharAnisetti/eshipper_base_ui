import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MaterialModules } from '../material-modules';
import { ProcessingRoutingModule } from './processing-routing.module';

import { ProcessingComponent } from './processing.component';

@NgModule({
  declarations: [ProcessingComponent],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModules,
    ProcessingRoutingModule,
  ],
  exports: [ProcessingComponent],
})
export class ProcessingModule {}
