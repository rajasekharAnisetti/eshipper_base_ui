import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./login/login.module').then(mod => mod.LoginModule),
  },
  {
    path: 'customer',
    loadChildren: () =>
      import('./customer/customer.module').then(mod => mod.CustomerModule),
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('./admin/admin.module').then(mod => mod.AdminModule),
  },
  {
    path: 'super-admin',
    loadChildren: () =>
      import('./super-admin/super-admin.module').then(
        mod => mod.SuperAdminModule
      ),
  },
  {
    path: 'instant-quote',
    loadChildren: () =>
      import('./instant-quote/instant-quote.module').then(
        mod => mod.InstantQuoteModule
      ),
  },
  {
    path: 'file-a-claim',
    loadChildren: () =>
      import('./file-a-claim/file-a-claim.module').then(
        mod => mod.FileAClaimModule
      ),
    data: {
      title: 'File A Claim',
      backBtn: true,
    },
  },
  {
    path: '**',
    loadChildren: () =>
      import('./unavailable-server/unavailable-server.module').then(mod => mod.UnavailableServerModule),
    data: {
      title: 'Unavailable',
    },
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
