import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-dynamic-imags',
  templateUrl: './dynamic-imags.component.html',
  styleUrls: ['./dynamic-imags.component.scss'],
})
export class DynamicImagsComponent implements OnInit {
  @Input() data = [
    {
      name: '',
      url: '',
    },
  ];
  @Output() click = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  // add image
  addImage() {
    console.log('click');
    this.data.push({
      name: 'image',
      url: 'content/images/products/product-1.jpg',
    });
  }
  // end of add image
}
