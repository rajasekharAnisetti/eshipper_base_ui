import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  HostListener,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
} from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';

// Depending on whether rollup is used, moment needs to be imported differently.
// Since Moment.js doesn't have a default export, we normally need to import using the `* as`
// syntax. However, rollup creates a synthetic default module and we thus need to import it using
// the `default as` syntax.
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import * as _rollupMoment from 'moment';

const moment = _rollupMoment || _moment;

import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';

import { currency } from '../functions/currency';
import { CreditInfoDialogComponent } from '../../dialogs/credit-info-dialog/credit-info-dialog.component';

@Component({
  selector: 'app-data-block',
  templateUrl: './data-block.component.html',
  styleUrls: ['./data-block.component.scss'],
  providers: [
    // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
    // `MatMomentDateModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
})
export class DataBlockComponent implements OnInit {
  @Output() infoClick = new EventEmitter();
  @Output() change = new EventEmitter<string>();
  @Input() data = <any>{};
  // need use or data or single elements
  // for single elements label & valie is required
  @Input() label = '';
  @Input() value = '';
  @Input() strong = false;
  @Input() right = false;
  @Input() bold = false;
  @Input() wrongFlag = false;
  @Input() details = [];
  @Input() big = false;
  @Input() badge: string;
  @Input() editable = false;
  @Input() editablePiker = false;

  editField = false;
  dateControl = new FormControl();

  // @HostListener('document:keydown', ['$event.target'])
  // onKeydownHandler(event: KeyboardEvent) {
  //   if (event.keyCode === 27) {
  //     this.editField = false;
  //   }
  // }
  constructor(public dialog: MatDialog) {}

  ngOnInit() {}

  // edit due date
  editableField() {
    this.editField = this.editable ? true : false;
    if (this.editField && this.editablePiker) {
      this.dateControl = new FormControl(moment(this.value));
    }
  }
  // end of edit due date

  cangeValue(value) {
    setTimeout(() => {
      this.value = value;
      this.change.emit(value);
      this.editField = false;
    }, 1);
  }

  openDetails(data, singleData) {
    if (singleData.length) {
      //  for single element
      this.infoClick.emit(singleData);
    } else {
      // this.data.infoFlag -> '', 'credit', 'weight'
      if (this.data.infoFlag) {
        switch (this.data.infoFlag) {
          case 'weight':
            {
              this.dialog.open(InfoDialogComponent, {
                data: {
                  title: 'Weight Details',
                  moreData: data,
                  infoFlag: 'weight',
                },
                autoFocus: false,
                panelClass: 'small-dialog-width',
              });
            }
            break;
          case 'credit':
            {
              this.dialog.open(CreditInfoDialogComponent, {
                data: {
                  title: 'Credit Details',
                  moreData: data,
                },
                autoFocus: false,
                panelClass: 'normal-dialog-width',
              });
            }
            break;
          default:
            break;
        }
      } else {
        this.dialog.open(InfoDialogComponent, {
          data: {
            title: 'Price Details',
            moreData: data,
          },
          autoFocus: false,
          panelClass: 'small-dialog-width',
        });
      }
    }
  }

  currencyConvert = val => currency(val);

  isNumber = val => typeof val === 'number';
}
