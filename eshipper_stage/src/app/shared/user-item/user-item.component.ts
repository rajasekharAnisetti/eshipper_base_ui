import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.scss'],
})
export class UserItemComponent implements OnInit {
  userPosition = localStorage.getItem('userPosition');
  profileUrl = localStorage.getItem('profileUrl');
  userAvatar = JSON.parse(localStorage.getItem('userAvatar'));

  constructor() { }

  ngOnInit() { }
}
