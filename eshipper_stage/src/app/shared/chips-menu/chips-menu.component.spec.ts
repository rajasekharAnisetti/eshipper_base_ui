import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChipsMenuComponent } from './chips-menu.component';

describe('ChipsMenuComponent', () => {
  let component: ChipsMenuComponent;
  let fixture: ComponentFixture<ChipsMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChipsMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChipsMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
