import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

export interface Chip {
  name: string;
  color: string;
}

@Component({
  selector: 'app-chips-menu',
  templateUrl: './chips-menu.component.html',
  styleUrls: ['./chips-menu.component.scss'],
})
export class ChipsMenuComponent implements OnInit {
  @Input() items: Array<Chip>;
  @Input() fullItems: Array<Chip>;
  @Input() action: string;
  @Output() change = new EventEmitter();
  @Output() addNew = new EventEmitter();

  showItems: Array<Chip> = [];

  constructor() {}

  ngOnInit() {
    this.showItems = [];
  }

  openMenu() {
    this.showItems = this.fullItems.filter(
      item => this.items.filter(el => item.name === el.name).length === 0
    );
  }

  // click on item
  clickFunction(item) {
    this.change.emit(item);
  }
  // end of click on item

  // add new
  addNewChip() {
    this.addNew.emit(true);
  }
  // end of add new
}
