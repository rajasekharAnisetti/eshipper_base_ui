import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-content-toolbar',
  templateUrl: './content-toolbar.component.html',
  styleUrls: ['./content-toolbar.component.scss'],
})
export class ContentToolbarComponent implements OnInit {
  openAdvansedSearch = false;
  @Input() filterBtn = false;
  @Input() downloadBtn = false;
  @Input() period = false;
  @Input() searchFlag = true;
  @Input() toolbarSettings = [];
  @Input() sortMenu = [];
  @Output() filterOpen = new EventEmitter<any>();
  @Output() backSettingsData: EventEmitter<string> = new EventEmitter();
  @Output() searchData: EventEmitter<string> = new EventEmitter();
  @Output() sortData: EventEmitter<string> = new EventEmitter();
  @Output() periodData: EventEmitter<string> = new EventEmitter();
  @Output() downloadData = new EventEmitter();

  constructor() { }

  ngOnInit() { }

  filter = () => this.filterOpen.emit();

  searchFunction = val => this.searchData.emit(val);

  selectPeriod = period => this.periodData.emit(period);

  settingsFunction = val => this.backSettingsData.emit(val);

  download = () => this.downloadData.emit();

  // get sort option
  getSortData = (data) => this.sortData.emit(data)
  // end of get sort option
}
