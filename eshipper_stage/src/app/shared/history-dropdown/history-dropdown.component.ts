import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-history-dropdown',
  templateUrl: './history-dropdown.component.html',
  styleUrls: ['./history-dropdown.component.scss'],
})
export class HistoryDropdownComponent implements OnInit {
  @Input() historyData = [
    {
      title: '',
      innerData: [
        {
          label: '',
          value: '',
        },
      ],
    },
  ];

  constructor() {}

  ngOnInit() {}
}
