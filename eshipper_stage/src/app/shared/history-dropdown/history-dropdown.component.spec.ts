import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryDropdownComponent } from './history-dropdown.component';

describe('HistoryDropdownComponent', () => {
  let component: HistoryDropdownComponent;
  let fixture: ComponentFixture<HistoryDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
