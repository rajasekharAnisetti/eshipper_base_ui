import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';

import { User } from 'src/app/interfaces/user';
import { AdminAccountService } from 'src/app/admin/admin-account/admin-account.service';
import { ValidationClassStateMatcher } from 'src/app/shared/classes/validation-class';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  userData = <User>{};
  userRoles = ['User Role', 'User Role', 'User Role', 'User Role'];
  matcher = new ValidationClassStateMatcher();
  newFlag = false;
  validation = {
    userName: new FormControl(),
    userRole: new FormControl(),
    userEmail: new FormControl('', [Validators.email]),
    userPhone: new FormControl(),
  };

  constructor(
    private getData: AdminAccountService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    // get user data
    this.newFlag = this.activatedRoute.snapshot.data.newFlag;
    if (!this.newFlag) {
      this.subscribe.add(
        this.getData.getUserData().subscribe(data => {
          this.userData = data;
        })
      );
    }
  }

  // change role
  changeRole(val) {
    if (val) {
      this.userData.permissions = ['Shipping', 'Tracking', 'Invoicing', 'Value']
    }
  }
  // end of change role

  // upload avatar
  upload() {
    console.log('Upload');
  }
  // end of upload avatar

  // add user function
  addUser() {
    console.log('Add New User');
  }
  // end of add user function

  ngOnDestroy() {
    // unsubscribe
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
