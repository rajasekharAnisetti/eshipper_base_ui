import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmedBlockComponent } from './confirmed-block.component';

describe('ConfirmedBlockComponent', () => {
  let component: ConfirmedBlockComponent;
  let fixture: ComponentFixture<ConfirmedBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmedBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmedBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
