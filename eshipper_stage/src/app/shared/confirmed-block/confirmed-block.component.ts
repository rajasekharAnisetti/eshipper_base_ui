import { Component, OnInit, Input } from '@angular/core';
import { ConfirmedData } from 'src/app/interfaces/shared';

@Component({
  selector: 'app-confirmed-block',
  templateUrl: './confirmed-block.component.html',
  styleUrls: ['./confirmed-block.component.scss'],
})
export class ConfirmedBlockComponent implements OnInit {
  @Input() data: ConfirmedData;

  constructor() {}

  ngOnInit() {}
}
