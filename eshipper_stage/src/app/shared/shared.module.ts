import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { RouterModule } from '@angular/router';

import { NgxMaskModule } from 'ngx-mask';
import { WebStorageModule } from 'ngx-store';
import { GridsterModule } from 'angular-gridster2';
import { ChartsModule } from 'ng2-charts';

import { MaterialModules } from '../material-modules';

// directives
import { AutoFocusDirective } from './directives/auto-focus.directive';
import { ClickOutsideDirective } from './directives/click-outside.directive';
import { CreditCardMaskPipe } from './pipes/credit-card-mask.pipe';

// components
import { ContentToolbarComponent } from './content-toolbar/content-toolbar.component';
import { SimpleSearchComponent } from './simple-search/simple-search.component';
import { MainToolbarComponent } from './main-toolbar/main-toolbar.component';
import { ImageComponent } from './image/image.component';
import { DotMenuComponent } from './dot-menu/dot-menu.component';
import { SmallWidgetsComponent } from './small-widgets/small-widgets.component';
import { CustomDropdownComponent } from './custom-dropdown/custom-dropdown.component';
import { InfoBlockComponent } from './info-block/info-block.component';
import { InfoListComponent } from './info-list/info-list.component';
import { MessageComponent } from './message/message.component';
import { TimeFieldComponent } from './time-field/time-field.component';
import { UploadFilesComponent } from './upload-files/upload-files.component';
import { StepsComponent } from './steps/steps.component';
import { ShipmentCardComponent } from './shipment-card/shipment-card.component';
import { HistoryDropdownComponent } from './history-dropdown/history-dropdown.component';
import { ButtonSelectComponent } from './button-select/button-select.component';
import { ComplexChipsListComponent } from './complex-chips-list/complex-chips-list.component';
import { AdditionalServicesCardComponent } from './additional-services-card/additional-services-card.component';
import { CodServiceCardComponent } from './cod-service-card/cod-service-card.component';
import { SortBtnComponent } from './sort-btn/sort-btn.component';
import { RateLineComponent } from './rate-line/rate-line.component';
import { ShipDetailsSidebarComponent } from './ship-details-sidebar/ship-details-sidebar.component';
import { DataItemComponent } from './data-item/data-item.component';
import { CardDateComponent } from './card-date/card-date.component';
import { ImgBoxComponent } from './img-box/img-box.component';
import { NumberRowBoxComponent } from './number-row-box/number-row-box.component';
import { DataBlockComponent } from './data-block/data-block.component';
import { DoubleButtonComponent } from './double-button/double-button.component';
import { ConfirmedBlockComponent } from './confirmed-block/confirmed-block.component';
import { ChipMultiselectComponent } from './chip-multiselect/chip-multiselect.component';
import { AmountComponent } from './amount/amount.component';
import { BackButtonComponent } from './back-button/back-button.component';
import { MultiEmailFieldComponent } from './multi-email-field/multi-email-field.component';
import { DropLabelComponent } from './drop-label/drop-label.component';
import { UploadAvatarComponent } from './upload-avatar/upload-avatar.component';
import { DashboardWidgetsComponent } from './dashboard-widgets/dashboard-widgets.component';
import { SmallCompareWidgetsComponent } from './small-compare-widgets/small-compare-widgets.component';
import { UserComponent } from './user/user.component';
import { StatusButtonComponent } from './status-button/status-button.component';
import { ChipsMenuComponent } from './chips-menu/chips-menu.component';
import { KeywordListComponent } from './keyword-list/keyword-list.component';
import { DynamicImagsComponent } from './dynamic-imags/dynamic-imags.component';

@NgModule({
  declarations: [
    // directives
    ClickOutsideDirective,
    AutoFocusDirective,
    // componnts
    SimpleSearchComponent,
    MainToolbarComponent,
    ImageComponent,
    ContentToolbarComponent,
    DotMenuComponent,
    SmallWidgetsComponent,
    CustomDropdownComponent,
    InfoBlockComponent,
    InfoListComponent,
    MessageComponent,
    TimeFieldComponent,
    UploadFilesComponent,
    ConfirmedBlockComponent,
    DoubleButtonComponent,
    DataBlockComponent,
    ImgBoxComponent,
    NumberRowBoxComponent,
    ShipmentCardComponent,
    HistoryDropdownComponent,
    ButtonSelectComponent,
    ComplexChipsListComponent,
    AdditionalServicesCardComponent,
    CodServiceCardComponent,
    SortBtnComponent,
    RateLineComponent,
    CardDateComponent,
    ShipDetailsSidebarComponent,
    DataItemComponent,
    StepsComponent,
    ChipMultiselectComponent,
    DashboardWidgetsComponent,
    UserComponent,
    SmallCompareWidgetsComponent,
    AmountComponent,
    BackButtonComponent,
    MultiEmailFieldComponent,
    DropLabelComponent,
    UploadAvatarComponent,
    CreditCardMaskPipe,
    StatusButtonComponent,
    ChipsMenuComponent,
    KeywordListComponent,
    DynamicImagsComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModules,
    PerfectScrollbarModule,
    RouterModule,
    NgxMaskModule,
    WebStorageModule,
    GridsterModule,
    ChartsModule,
  ],
  exports: [
    SimpleSearchComponent,
    MainToolbarComponent,
    ImageComponent,
    ContentToolbarComponent,
    DotMenuComponent,
    SmallWidgetsComponent,
    CustomDropdownComponent,
    InfoBlockComponent,
    InfoListComponent,
    MessageComponent,
    TimeFieldComponent,
    UploadFilesComponent,
    ConfirmedBlockComponent,
    DoubleButtonComponent,
    DataBlockComponent,
    ImgBoxComponent,
    NumberRowBoxComponent,
    ShipmentCardComponent,
    HistoryDropdownComponent,
    ButtonSelectComponent,
    ComplexChipsListComponent,
    AdditionalServicesCardComponent,
    CodServiceCardComponent,
    SortBtnComponent,
    RateLineComponent,
    CardDateComponent,
    ShipDetailsSidebarComponent,
    DataItemComponent,
    StepsComponent,
    ChipMultiselectComponent,
    AmountComponent,
    MultiEmailFieldComponent,
    DropLabelComponent,
    UploadAvatarComponent,
    DashboardWidgetsComponent,
    CreditCardMaskPipe,
    UserComponent,
    SmallCompareWidgetsComponent,
    StatusButtonComponent,
    ChipsMenuComponent,
    KeywordListComponent,
    DynamicImagsComponent,
  ],
})
export class SharedModule { }
