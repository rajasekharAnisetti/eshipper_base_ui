import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-dot-menu',
  templateUrl: './dot-menu.component.html',
  styleUrls: ['./dot-menu.component.scss'],
})
export class DotMenuComponent implements OnInit {
  @Input() items = ['Option 1', 'Option 2'];
  @Output() backData: EventEmitter<string> = new EventEmitter();

  clickFunction = name => this.backData.emit(name);

  constructor() {}

  ngOnInit() {}
}
