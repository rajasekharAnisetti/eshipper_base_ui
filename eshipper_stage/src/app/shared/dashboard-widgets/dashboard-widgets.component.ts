import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  OnDestroy,
} from '@angular/core';
import {
  GridsterConfig,
  GridsterItem,
  DisplayGrid,
  GridType,
  CompactType,
} from 'angular-gridster2';

import * as pluginDataLabels from 'chartjs-plugin-datalabels';

@Component({
  selector: 'app-dashboard-widgets',
  templateUrl: './dashboard-widgets.component.html',
  styleUrls: ['./dashboard-widgets.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class DashboardWidgetsComponent implements OnInit, OnDestroy {
  @Input() charts = [
    {
      title: 'Guest Instant Quote',
      chart: {
        type: 'bar',
        labels: ['2006', '2007', '2008', '2009', '2010', '2011', '2012'],
        datasets: [
          { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
          { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
        ],
        options: {
          responsive: true,
          // We use these empty structures as placeholders for dynamic theming.
          scales: { xAxes: [{}], yAxes: [{}] },
          plugins: {
            datalabels: {
              anchor: 'end',
              align: 'end',
            },
          },
          maintainAspectRatio: false,
        },
        plugins: [pluginDataLabels],
        legend: true,
      },
    },
    {
      title: 'Shipment',
      chart: {
        type: 'doughnut',
        labels: ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'],
        data: [
          [350, 450, 100],
          [50, 150, 120],
          [250, 130, 70],
        ],
        options: {
          maintainAspectRatio: false,
        }
      },
    },
    {
      title: 'Invoices',
      chart: {
        type: 'doughnut',
        labels: ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'],
        data: [
          [350, 450, 100],
          [50, 150, 120],
          [250, 130, 70],
        ],
        options: {
          maintainAspectRatio: false,
        }
      },
    },
    {
      title: 'Work Order',
      chart: {
        type: 'doughnut',
        labels: ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'],
        data: [
          [350, 450, 100],
          [50, 150, 120],
          [250, 130, 70],
        ],
        options: {
          maintainAspectRatio: false,
        }
      },
    },

  ];
  @Input() widgets = [
    {
      cols: 6,
      rows: 4,
      y: 0,
      x: 0,
      hasContent: true,
    },
    {
      cols: 2,
      rows: 3,
      y: 4,
      x: 0,
      hasContent: true,
    },
    {
      cols: 2,
      rows: 3,
      y: 4,
      x: 2,
      hasContent: true,
    },
    {
      cols: 2,
      rows: 3,
      y: 4,
      x: 4,
      hasContent: true,
    },

  ];

  options: GridsterConfig;

  dashboard: Array<GridsterItem>;

  chartFilter = ['View Option', 'View Option', 'View Option'];
  dateFilter = ['Day', 'Week', 'Month'];

  constructor() { }

  ngOnInit() {
    this.options = {
      gridType: GridType.Fit,
      compactType: CompactType.CompactLeftAndUp,
      margin: 16,
      outerMargin: true,
      outerMarginTop: null,
      outerMarginRight: null,
      outerMarginBottom: null,
      outerMarginLeft: null,
      useTransformPositioning: false,
      mobileBreakpoint: 720,
      minCols: 1,
      maxCols: 12,
      minRows: 1,
      maxRows: 100,
      maxItemCols: 12,
      minItemCols: 1,
      maxItemRows: 100,
      minItemRows: 1,
      maxItemArea: 1000,
      minItemArea: 1,
      defaultItemCols: 1,
      defaultItemRows: 1,
      fixedColWidth: 105,
      fixedRowHeight: 105,
      keepFixedHeightInMobile: false,
      keepFixedWidthInMobile: false,
      destroyCallback: () => true,
      scrollSensitivity: 10,
      scrollSpeed: 20,
      enableEmptyCellClick: false,
      enableEmptyCellContextMenu: false,
      enableEmptyCellDrop: false,
      enableEmptyCellDrag: false,
      emptyCellDragMaxCols: 50,
      emptyCellDragMaxRows: 1000,
      ignoreMarginInRow: false,
      itemRemovedCallback: undefined,
      gridsterComponent: undefined,
      draggable: {
        enabled: true,
        dragHandleClass: 'drag-handle',
        ignoreContentClass: 'drag-ignore',
      },
      resizable: {
        enabled: true,
      },
      swap: false,
      pushItems: true,
      disablePushOnDrag: false,
      disablePushOnResize: false,
      pushDirections: { north: true, east: true, south: true, west: true },
      pushResizeItems: false,
      displayGrid: DisplayGrid.OnDragAndResize,
      disableWindowResize: false,
      disableWarnings: false,
      scrollToNewItems: false,
    };
    this.dashboard = this.widgets;
  }

  changedOptions() {
    if (this.options.api && this.options.api.optionsChanged) {
      this.options.api.optionsChanged();
    }
  }

  destroy() { }

  // events
  public chartClicked({
    event,
    active,
  }: {
    event: MouseEvent;
    active: {}[];
  }): void {
    console.log(event, active);
  }

  public chartHovered({
    event,
    active,
  }: {
    event: MouseEvent;
    active: {}[];
  }): void {
    console.log(event, active);
  }

  getFilterData(val) {
    console.log(val);
  }

  ngOnDestroy(): void { }
}
