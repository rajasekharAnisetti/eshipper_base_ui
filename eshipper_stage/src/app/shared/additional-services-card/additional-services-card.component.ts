import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-additional-services-card',
  templateUrl: './additional-services-card.component.html',
  styleUrls: ['./additional-services-card.component.scss'],
})
export class AdditionalServicesCardComponent implements OnInit {
  // Additional Services
  dangerousGoodsList = [
    {
      value: 'None',
      viewValue: 'None',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];

  signatureRequiredList = [
    {
      value: 'No',
      viewValue: 'No',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];

  additionalServicesMenu = [
    {
      value: 'Pickup Tailgate',
      selected: false,
    },
    {
      value: 'Delivery Tailgate',
      selected: false,
    },
    {
      value: 'Inside Pickup',
      selected: false,
    },
    {
      value: 'Inside Delivery',
      selected: false,
    },
    {
      value: 'Saturday Pickup',
      selected: false,
    },
    {
      value: 'Saturday Delivery',
      selected: false,
    },
    {
      value: 'Appointment Pickup',
      selected: false,
    },
    {
      value: 'Appointment Delivery',
      selected: false,
    },
    {
      value: 'Sort and Segregate',
      selected: false,
    },
    {
      value: 'Single Shipment',
      selected: false,
    },
  ];

  constructor() {}

  ngOnInit() {}
}
