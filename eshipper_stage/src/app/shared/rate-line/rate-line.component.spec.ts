import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RateLineComponent } from './rate-line.component';

describe('RateLineComponent', () => {
  let component: RateLineComponent;
  let fixture: ComponentFixture<RateLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RateLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
