import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';
import { Rate } from 'src/app/interfaces/shared';

@Component({
  selector: 'app-rate-line',
  templateUrl: './rate-line.component.html',
  styleUrls: ['./rate-line.component.scss'],
})
export class RateLineComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  @Input() rateItem: Rate;
  @Input() quoteBtn = false;
  @Output() quoteBtnClick = new EventEmitter();

  constructor(public dialog: MatDialog) { }

  ngOnInit() { }

  // info function
  info(data) {
    const dialogData = {
      title: 'Price Details',
      moreData: data,
    };
    const dialogRef = this.dialog.open(InfoDialogComponent, {
      data: dialogData,
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of info function

  // quote button function
  quoteBtnFunction() {
    this.quoteBtnClick.emit();
  }
  // end of quote button function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
