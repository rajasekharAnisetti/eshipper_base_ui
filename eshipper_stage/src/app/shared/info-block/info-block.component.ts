import { Component, OnInit, Input } from '@angular/core';
import { InfoBlock } from 'src/app/interfaces/shared';

@Component({
  selector: 'app-info-block',
  templateUrl: './info-block.component.html',
  styleUrls: ['./info-block.component.scss'],
})
export class InfoBlockComponent implements OnInit {
  @Input() data: InfoBlock;

  constructor() {}

  ngOnInit() {}
}
