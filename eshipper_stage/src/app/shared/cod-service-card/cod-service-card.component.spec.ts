import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodServiceCardComponent } from './cod-service-card.component';

describe('CodServiceCardComponent', () => {
  let component: CodServiceCardComponent;
  let fixture: ComponentFixture<CodServiceCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodServiceCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodServiceCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
