import { Component, OnInit } from '@angular/core';

import { CURRENCIES_$ } from 'src/app/shared/const/currencies';

@Component({
  selector: 'app-cod-service-card',
  templateUrl: './cod-service-card.component.html',
  styleUrls: ['./cod-service-card.component.scss'],
})
export class CodServiceCardComponent implements OnInit {
  showCurrency = false;
  // Currency
  currencyValue = 'CAD $';
  currencyItems = CURRENCIES_$;

  cods = [
    {
      value: 'Option 1',
      viewValue: 'Option 1',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];

  CODCurrency = () => (this.showCurrency = !this.showCurrency);

  currencyChange = val => console.log(val);

  constructor() { }

  ngOnInit() { }
}
