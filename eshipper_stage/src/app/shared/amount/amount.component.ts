import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-amount',
  templateUrl: './amount.component.html',
  styleUrls: ['./amount.component.scss'],
})
export class AmountComponent implements OnInit {
  private localvalue: string;
  public valueControl = new FormControl();

  @Input()
  get value() {
    return this.localvalue;
  }

  @Output() valueChange = new EventEmitter();

  set value(value) {
    this.localvalue = value;
    this.valueChange.emit(this.localvalue);
  }

  constructor() {}

  ngOnInit() {}
}
