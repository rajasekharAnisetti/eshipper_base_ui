import { Component, OnInit, Input } from '@angular/core';
import { NumberBox } from 'src/app/interfaces/shared';

@Component({
  selector: 'app-number-row-box',
  templateUrl: './number-row-box.component.html',
  styleUrls: ['./number-row-box.component.scss'],
})
export class NumberRowBoxComponent implements OnInit {
  @Input() data: NumberBox;
  @Input() noLogo = false;

  constructor() {}

  ngOnInit() {}
}
