import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumberRowBoxComponent } from './number-row-box.component';

describe('NumberRowBoxComponent', () => {
  let component: NumberRowBoxComponent;
  let fixture: ComponentFixture<NumberRowBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NumberRowBoxComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberRowBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
