import { Component, OnInit, Input } from '@angular/core';
import { Chip } from 'src/app/interfaces/shared';

@Component({
  selector: 'app-complex-chips-list',
  templateUrl: './complex-chips-list.component.html',
  styleUrls: ['./complex-chips-list.component.scss'],
})
export class ComplexChipsListComponent implements OnInit {
  @Input() data: Array<Chip>;
  selectable = false;
  removable = true;

  constructor() {}

  ngOnInit() {}
}
