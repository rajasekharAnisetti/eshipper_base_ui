import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComplexChipsListComponent } from './complex-chips-list.component';

describe('ComplexChipsListComponent', () => {
  let component: ComplexChipsListComponent;
  let fixture: ComponentFixture<ComplexChipsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComplexChipsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComplexChipsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
