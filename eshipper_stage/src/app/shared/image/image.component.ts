import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss'],
})
export class ImageComponent implements OnInit, OnChanges {
  localData = {};
  @Input() size = 32;
  @Input()
  set data(data: any) {
    if (data) {
      // reset previous data
      this.localImageURL = null;
      // init with new data
      this.localData = data;
      this.initImage();
    }
  }
  get data() {
    return this.localData;
  }

  @Input() imageUrl: string;
  @Input() placeholderName = 'Guest';

  initialColor: string;
  initial: string;

  imgWidth = 0;
  imgHeight = 0;
  imgClass = 'square';

  localImageURL = null;

  constructor() {}

  ngOnInit() {
    this.initImage();
  }
  ngOnChanges(): void {
    this.initImage();
  }

  initImage() {
    if (this.data && this.data.imageUrl && this.data.imageUrl.length > 0) {
      this.localImageURL = this.data.imageUrl;
    } else if (this.imageUrl && this.imageUrl.length > 0) {
      this.localImageURL = this.imageUrl;
    }

    if (this.localImageURL) {
      const image = new Image();
      image.addEventListener('load', e => this.handleImageLoad(e));
      image.src = this.localImageURL;
    }

    if (this.data && this.data.name && this.data.name.length > 0) {
      this.generateInitial(this.data.name);
    } else if (this.placeholderName && this.placeholderName.length > 0) {
      this.generateInitial(this.placeholderName);
    }
  }

  handleImageLoad(event) {
    this.imgWidth = event.target.width;
    this.imgHeight = event.target.height;
    if (this.imgWidth > this.imgHeight) {
      this.imgClass = 'landscape-img';
    }
    if (this.imgWidth < this.imgHeight) {
      this.imgClass = 'portrait-img';
    }
  }

  generateInitial(userName) {
    if (userName) {
      const colors = [
        'orange',
        'dark-orange',
        'red',
        'pink',
        'green',
        'light-green',
        'blue',
        'darken-blue',
        'dark-blue',
        'purple',
        'brown',
        'dark',
        'gray',
      ];
      this.initialColor = colors[Math.floor(Math.random() * colors.length)];
      this.initial = this.getInitial(userName);
    }
  }

  getInitial(name) {
    const matches = name.match(/\b(\w)/g);
    return matches.join('');
  }
}
