export const SIMPLESETTINGS = ['Export', 'Print'];

export const EDSETTINGS = ['Edit', 'Delete'];

export const EDIORPHANITEMSETTINGS = ['Export', 'Print', 'Delete'];

export const BATCHITEMSETTINGS = ['Export', 'Print', 'Delete'];

export const APPAYABLESITEMSETTINGS = ['Activities', 'Export', 'Cancel'];

// ar customer
export const ARCUSTOMERTOOLBARSETTINGS = ['Details PDF', 'Summary PDF'];

export const ARCUSTOMERITEMSETTINGS = [
  'Activities',
  'Details PDF',
  'Summary PDF',
];

export const ARCUSTOMERITEMSETTINGSMORE = [
  'Activities',
  'Details PDF',
  'Summary PDF',
  'Release Credit Limit',
  'Cancel',
];
// end of ar customer

// batch inner
export const BATCHINNERTOOLBARSETTINGS = ['Option 1', 'Option 2'];

export const BATCHINNERINNERSETTINGS = [
  'Edit',
  'Print Label',
  'Schedule Pickup',
  'divider',
  'File a Claim',
  'Cancel',
];
// end of batch inner

// rates batch inner
export const RATESBATCHINNERTOOLBARSETTINGS = [
  'Option 1',
  'Option 2',
  'Option 3',
];

export const RATESBATCHINNERITEMSETTINGS = ['Edit', 'Remove'];
// end of rates batch inner

// tracking batch
export const ADMINBATCHITEMSETTINGS = [
  'Invoice',
  'Edit',
  'Print Label',
  'Schedule Pickup',
  'divider',
  'File a Claim',
  'Cancel',
];

export const CUSTOMERBATCHITEMSETTINGS = [
  'Edit',
  'Print Label',
  'Schedule Pickup',
  'divider',
  'File a Claim',
  'Cancel',
];
// end of tracking batch

export const TIMEZONES = [
  {
    value: 'GMT -12:00',
    viewValue: 'GMT -12:00',
  },
  {
    value: 'GMT -11:00',
    viewValue: 'GMT -11:00',
  },
  {
    value: 'GMT -10:00',
    viewValue: 'GMT -10:00',
  },
  {
    value: 'GMT -9:00',
    viewValue: 'GMT -9:00',
  },
  {
    value: 'GMT -8:00',
    viewValue: 'GMT -8:00',
  },
  {
    value: 'GMT -7:00',
    viewValue: 'GMT -7:00',
  },
  {
    value: 'GMT -6:00',
    viewValue: 'GMT -6:00',
  },
  {
    value: 'GMT -5:00',
    viewValue: 'GMT -5:00',
  },
  {
    value: 'GMT -4:00',
    viewValue: 'GMT -4:00',
  },
  {
    value: 'GMT -3:00',
    viewValue: 'GMT -3:00',
  },
  {
    value: 'GMT -2:00',
    viewValue: 'GMT -2:00',
  },
  {
    value: 'GMT -1:00',
    viewValue: 'GMT -1:00',
  },
  {
    value: 'GMT',
    viewValue: 'GMT',
  },
  {
    value: 'GMT 1:00',
    viewValue: 'GMT 1:00',
  },
  {
    value: 'GMT 2:00',
    viewValue: 'GMT 2:00',
  },
  {
    value: 'GMT 3:00',
    viewValue: 'GMT 3:00',
  },
  {
    value: 'GMT 4:00',
    viewValue: 'GMT 4:00',
  },
  {
    value: 'GMT 5:00',
    viewValue: 'GMT 5:00',
  },
  {
    value: 'GMT 6:00',
    viewValue: 'GMT 6:00',
  },
  {
    value: 'GMT 7:00',
    viewValue: 'GMT 7:00',
  },
  {
    value: 'GMT 8:00',
    viewValue: 'GMT 8:00',
  },
  {
    value: 'GMT 9:00',
    viewValue: 'GMT 9:00',
  },
  {
    value: 'GMT 10:00',
    viewValue: 'GMT 10:00',
  },
  {
    value: 'GMT 11:00',
    viewValue: 'GMT 11:00',
  },
  {
    value: 'GMT 12:00',
    viewValue: 'GMT 12:00',
  },
];

// WO
export const SEGMENTSITEMSETTINGS = [
  'Edit Segment',
  'Delete Segment',
  'Export',
];

export const SEGMENTSALARMSETTINGS = [
  'Edit Segment',
  'Delete Segment',
  'Edit Alert',
  'Delete Alert',
  'Export',
];
// end of WO

export const STATUSES = ['Pending', 'In Process', 'Approved', 'Denied'];

export const PACKAGETYPES = ['Package', 'Envelope', 'Pak', 'Pallet'];
