export const COLORS = [
  'light-blue',
  'blue',
  'dark-blue',
  'dark-green',
  'green',
  'light-purple',
  'purple',
  'red',
  'yellow',
  'light-orange',
  'orange',
  'brown',
];
