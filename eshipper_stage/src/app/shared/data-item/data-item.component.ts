import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';
import { DataItem } from 'src/app/interfaces/shared';
import { currency } from 'src/app/shared/functions/currency';

@Component({
  selector: 'app-data-item',
  templateUrl: './data-item.component.html',
  styleUrls: ['./data-item.component.scss'],
})
export class DataItemComponent implements OnInit {
  @Input() data: DataItem;

  constructor(public dialog: MatDialog) {}

  ngOnInit() {}

  openInfoDialog() {
    this.dialog.open(InfoDialogComponent, {
      data: {
        infoFlag: this.data.infoFlag,
        moreData: this.data.info.moreData,
        title: this.data.info.title,
      },
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
  }

  currencyConvert = val => currency(val);
  isNumber = val => typeof val === 'number';
}
