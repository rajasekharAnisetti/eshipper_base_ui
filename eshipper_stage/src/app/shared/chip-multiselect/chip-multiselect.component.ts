import {
  ElementRef,
  ViewChild,
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import {
  MatAutocompleteSelectedEvent,
  MatAutocomplete,
  MatAutocompleteTrigger,
} from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { ChipColor } from 'src/app/interfaces/shared';

@Component({
  selector: 'app-chip-multiselect',
  templateUrl: './chip-multiselect.component.html',
  styleUrls: ['./chip-multiselect.component.scss'],
})
export class ChipMultiselectComponent implements OnInit {
  @Input() placeholder: string;
  @Input() required = false;
  @Input() data: Array<ChipColor> = [];

  @Output() change = new EventEmitter();

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  colors = [
    'light-blue',
    'blue',
    'dark-blue',
    'dark-green',
    'green',
    'light-purple',
    'purple',
    'red',
    'yellow',
    'light-orange',
    'orange',
    'brown',
  ];
  validation = {
    chips: new FormControl(),
  };

  finalChipsList: Array<ChipColor> = [];

  filteredChips: Observable<any>;
  @ViewChild('chipInput', { static: false }) chipInput: ElementRef<
    HTMLInputElement
  >;
  @ViewChild('autoChip', { static: false }) matAutocomplete: MatAutocomplete;
  @ViewChild('chipInput', { static: false, read: MatAutocompleteTrigger })
  autoComplete;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  constructor() {}

  ngOnInit() {
    this.filteredChips = this.validation.chips.valueChanges.pipe(
      startWith(null),
      map((chip: string | null) =>
        chip ? this._filter(chip) : this.data.slice()
      )
    );
  }

  private _filter(value: string) {
    const filterValue = value.toLowerCase();
    return this.data.filter(
      chip => chip.name.toLowerCase().indexOf(filterValue) === 0
    );
  }

  addChip(event: MatChipInputEvent): void {
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;
      const regionColor = this.colors[
        Math.floor(Math.random() * this.colors.length)
      ];

      // Add
      if ((value || '').trim()) {
        this.finalChipsList.push({
          name: value.trim(),
          color: regionColor,
        });
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      this.validation.chips.setValue(null);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.updateChipsList({
      name: event.option.viewValue,
      color: event.option.value,
    });
    this.chipInput.nativeElement.value = '';
    this.validation.chips.setValue(null);
    setTimeout(() => {
      this.autoComplete.openPanel();
    });
  }

  updateChipsList(chip): void {
    if (this.isChipSelected(chip.name)) {
      this.removeChip(chip);
    } else {
      this.addChipItem(chip);
    }
  }

  setData() {
    this.change.emit(this.finalChipsList);
  }

  removeChip(chip: ChipColor): void {
    const index = this.finalChipsList.map(x => x.name).indexOf(chip.name);
    if (index >= 0) {
      this.finalChipsList.splice(index, 1);
      this.change.emit(this.finalChipsList);
    }
  }

  addChipItem(chip: ChipColor) {
    this.finalChipsList.push(chip);
  }

  isChipSelected = (chip): boolean =>
    this.finalChipsList.map(x => x.name).indexOf(chip) >= 0;
}
