import { Component, OnInit, Input } from '@angular/core';
import { DetailsData } from 'src/app/interfaces/details-data';

@Component({
  selector: 'app-ship-details-sidebar',
  templateUrl: './ship-details-sidebar.component.html',
  styleUrls: ['./ship-details-sidebar.component.scss'],
})
export class ShipDetailsSidebarComponent implements OnInit {
  @Input() data: DetailsData;

  constructor() {}

  ngOnInit() {}
}
