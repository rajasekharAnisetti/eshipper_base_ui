import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShipDetailsSidebarComponent } from './ship-details-sidebar.component';

describe('ShipDetailsSidebarComponent', () => {
  let component: ShipDetailsSidebarComponent;
  let fixture: ComponentFixture<ShipDetailsSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShipDetailsSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipDetailsSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
