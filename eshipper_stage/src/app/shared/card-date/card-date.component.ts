import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-card-date',
  templateUrl: './card-date.component.html',
  styleUrls: ['./card-date.component.scss'],
})
export class CardDateComponent implements OnInit {
  @Output() change: EventEmitter<any> = new EventEmitter();

  @Input() date = {
    month: '',
    year: '',
  };

  constructor() {}

  ngOnInit() {}

  changeDate(date) {
    this.change.emit(date);
  }
}
