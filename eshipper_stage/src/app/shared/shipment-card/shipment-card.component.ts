import { Component, OnInit, Input } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { CURRENCIES_$ } from 'src/app/shared/const/currencies';

import {
  ValidationClassStateMatcher,
  QtyMatcher,
} from '../classes/validation-class';

@Component({
  selector: 'app-shipment-card',
  templateUrl: './shipment-card.component.html',
  styleUrls: ['./shipment-card.component.scss'],
})
export class ShipmentCardComponent implements OnInit {
  @Input() instantQuote = false;
  minDate = new Date();
  requiredText = 'Field is required';
  matcher = new ValidationClassStateMatcher();
  openMessage = true;
  // currency
  currencyValue = 'CAD $';
  currencyItems = CURRENCIES_$;
  systemValue = 'Metric';
  systemItems = ['Metric', 'Imperial'];
  history = [
    {
      title: 'Pallet',
      innerData: [
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
      ],
    },
    {
      title: 'Pallet',
      innerData: [
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
      ],
    },
    {
      title: 'Envelope',
      innerData: [],
    },
    {
      title: 'Package',
      innerData: [
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
      ],
    },
    {
      title: 'Pak',
      innerData: [],
    },
    {
      title: 'Pallet',
      innerData: [
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
      ],
    },
  ];

  insuranceTypes = [
    {
      value: 'eshipper',
      viewValue: 'eShipper',
    },
    {
      value: 'option2',
      viewValue: 'Option 2',
    },
    {
      value: 'option3',
      viewValue: 'Option 2',
    },
  ];

  packageTypes = [
    {
      value: 'envelope',
      viewValue: 'Envelope',
    },
    {
      value: 'pack',
      viewValue: 'Pack',
    },
    {
      value: 'package',
      viewValue: 'Package',
    },
    {
      value: 'pallet',
      viewValue: 'Pallet',
    },
  ];

  freightClasses = [
    {
      value: '50',
      viewValue: '50',
    },
    {
      value: '100',
      viewValue: '100',
    },
    {
      value: '150',
      viewValue: '150',
    },
  ];

  types = [
    {
      value: 'Roll',
      viewValue: 'Roll',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];

  packagesBooks = [
    {
      value: 'Standard Box',
      viewValue: 'Standard Box',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];

  shipment = {
    shippingDateControl: new FormControl('', [Validators.required]),
    shippingDateValue: '',
    packageTypeControl: new FormControl('', [Validators.required]),
    packageType: '',
    palletQtyControl: new FormControl('', [Validators.required]),
    palletQty: '1',
    totalWeight: 0,
    palletForm: [
      {
        lengthControl: new FormControl('', [Validators.required]),
        length: '',
        widthControl: new FormControl('', [Validators.required]),
        width: '',
        heightControl: new FormControl('', [Validators.required]),
        height: '',
        weightControl: new FormControl('', [Validators.required]),
        weight: '',
        freightClassControl: new FormControl('', [Validators.required]),
        freightClass: '',
        nmfc: '',
        typeControl: new FormControl('', [Validators.required]),
        typeClass: '',
        insurance: '',
        insuranceTooltip: 'Info about the field',
        description: '',
        button: 'All the Same',
      },
    ],
    packageQtyControl: new FormControl('', [Validators.required]),
    packageQty: '1',
    packageForm: [
      {
        packagesBookControl: new FormControl('', [Validators.required]),
        packagesBook: '',
        lengthControl: new FormControl('', [Validators.required]),
        length: '',
        widthControl: new FormControl('', [Validators.required]),
        width: '',
        heightControl: new FormControl('', [Validators.required]),
        height: '',
        weightControl: new FormControl('', [Validators.required]),
        weight: '',
        insurance: '',
        insuranceTooltip: 'Info about the field',
        description: '',
        button: 'All the Same',
      },
    ],
  };

  // Pallet Schedule Card

  palletSchedule = {
    stackable: true,
    form: {
      departureReadyTime: '',
      departureCloseTime: '',
      deliveryCloseTime: '',
    },
  };

  // Schedule Pickup block
  pickupLocations = [
    {
      value: 'Option 1',
      viewValue: 'Option 1',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];

  schedulePickup = {
    messageCheckboxText: 'Don`t show me again',
    message:
      'Please allow a minimum of 3 hour between pick up & closing times. Otherwise scheduling may fail.\n' +
      'Please Schedule a single pick up request with the carrier per day. The driver will pick up all the shipments under the same pick up request.',
    note: '',
    pickupDateControl: new FormControl('', [Validators.required]),
    pickupDateValue: '',
    pickupLocation: '',
  };

  qtyMatcher = new QtyMatcher();
  palletQtyMatcher = new QtyMatcher();

  constructor() { }

  ngOnInit() { }

  systemChange = val => console.log(val);
  currencyChange = val => console.log(val);

  getShippingDate = () => console.log(this.shipment.shippingDateValue);

  palletFunction() { }

  packageFunction() { }

  getPickTime = val => console.log(val);
  getClosingTime = val => console.log(val);

  getDepartureReadyTime = (time) => this.palletSchedule.form.departureReadyTime = time;
  getDepartureCloseTime = (time) => this.palletSchedule.form.departureCloseTime = time;
  getDeliveryCloseTime = (time) => this.palletSchedule.form.deliveryCloseTime = time;


  qtyFunction(qty, type) {
    switch (type) {
      case 'pallet':
        {
          this.shipment.palletForm = [
            {
              lengthControl: new FormControl('', [Validators.required]),
              length: '',
              widthControl: new FormControl('', [Validators.required]),
              width: '',
              heightControl: new FormControl('', [Validators.required]),
              height: '',
              weightControl: new FormControl('', [Validators.required]),
              weight: '',
              freightClassControl: new FormControl('', [Validators.required]),
              freightClass: '',
              nmfc: '',
              typeControl: new FormControl('', [Validators.required]),
              typeClass: '',
              insurance: '',
              insuranceTooltip: 'Info about the field',
              description: '',
              button: 'All the Same',
            },
          ];
          if (qty > 1 && qty <= 35) {
            for (let i = 1; i <= qty - 1; i++) {
              this.shipment.palletForm.push({
                lengthControl: new FormControl('', [Validators.required]),
                length: '',
                widthControl: new FormControl('', [Validators.required]),
                width: '',
                heightControl: new FormControl('', [Validators.required]),
                height: '',
                weightControl: new FormControl('', [Validators.required]),
                weight: '',
                freightClassControl: new FormControl('', [Validators.required]),
                freightClass: '',
                nmfc: '',
                typeControl: new FormControl('', [Validators.required]),
                typeClass: '',
                insurance: '',
                insuranceTooltip: 'Info about the field',
                description: '',
                button: 'Same As Above',
              });
            }
          }
        }

        break;
      case 'package':
        {
          this.shipment.packageForm = [
            {
              packagesBookControl: new FormControl('', [Validators.required]),
              packagesBook: '',
              lengthControl: new FormControl('', [Validators.required]),
              length: '',
              widthControl: new FormControl('', [Validators.required]),
              width: '',
              heightControl: new FormControl('', [Validators.required]),
              height: '',
              weightControl: new FormControl('', [Validators.required]),
              weight: '',
              insurance: '',
              insuranceTooltip: 'Info about the field',
              description: '',
              button: 'All the Same',
            },
          ];
          if (qty > 1 && qty <= 35) {
            for (let i = 1; i <= qty - 1; i++) {
              this.shipment.packageForm.push({
                packagesBookControl: new FormControl('', [Validators.required]),
                packagesBook: '',
                lengthControl: new FormControl('', [Validators.required]),
                length: '',
                widthControl: new FormControl('', [Validators.required]),
                width: '',
                heightControl: new FormControl('', [Validators.required]),
                height: '',
                weightControl: new FormControl('', [Validators.required]),
                weight: '',
                insurance: '',
                insuranceTooltip: 'Info about the field',
                description: '',
                button: 'Same As Above',
              });
            }
          }
        }

        break;
      default:
        break;
    }
  }
}
