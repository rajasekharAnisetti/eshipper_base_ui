import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { currency } from 'src/app/shared/functions/currency';
import { InfoList } from 'src/app/interfaces/shared';
import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';

@Component({
  selector: 'app-info-list',
  templateUrl: './info-list.component.html',
  styleUrls: ['./info-list.component.scss'],
})
export class InfoListComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  @Input() data: InfoList;
  @Input() right = false;
  constructor(public dialog: MatDialog) {}

  ngOnInit() {}

  info(data) {
    const dialogRef = this.dialog.open(InfoDialogComponent, {
      data: data,
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }

  currencyConvert = val => currency(val);

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
