import { MatDialog } from '@angular/material/dialog';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
} from '@angular/core';
import { Subscription } from 'rxjs';

import { PeriodDialogComponent } from 'src/app/dialogs/period-dialog/period-dialog.component';
@Component({
  selector: 'app-custom-dropdown',
  templateUrl: './custom-dropdown.component.html',
  styleUrls: ['./custom-dropdown.component.scss'],
})
export class CustomDropdownComponent implements OnInit, OnDestroy {
  suscribe = new Subscription();
  @Input() period = false;
  @Input() light = false;
  @Input() compareFlag = false;
  @Output() select = new EventEmitter();
  @Output() compare = new EventEmitter();
  @Input() options = [
    'ALL',
    'Today',
    'This Week',
    'This Month',
    'divider',
    'Custom Period',
  ];
  comparison = false;
  currentData: string;
  constructor(public dialog: MatDialog) {}

  ngOnInit() {
    this.currentData = this.options[0];
  }

  backOption(period) {
    if (period !== 'Custom Period') {
      this.select.emit(period);
      this.currentData = period;
    } else {
      const dialogRef = this.dialog.open(PeriodDialogComponent, {
        autoFocus: false,
        panelClass: 'period-dialog-width',
      });
      this.suscribe.add(
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.select.emit(result);
            this.currentData = result;
          }
        })
      );
    }
  }

  changeComparison(comparison) {
    this.compare.emit(comparison);
  }

  ngOnDestroy() {
    if (this.suscribe) {
      this.suscribe.unsubscribe();
    }
  }
}
