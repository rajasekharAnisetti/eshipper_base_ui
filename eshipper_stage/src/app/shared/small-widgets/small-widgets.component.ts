import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';

@Component({
  selector: 'app-small-widgets',
  templateUrl: './small-widgets.component.html',
  styleUrls: ['./small-widgets.component.scss'],
})
export class SmallWidgetsComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  @Input() smallWidgets = [];
  @Input() dashboardStyle = false;

  constructor(public dialog: MatDialog) {}

  ngOnInit() {}

  info(data) {
    const dialogRef = this.dialog.open(InfoDialogComponent, {
      data: data,
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
