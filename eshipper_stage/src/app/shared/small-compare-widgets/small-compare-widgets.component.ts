import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-small-compare-widgets',
  templateUrl: './small-compare-widgets.component.html',
  styleUrls: ['./small-compare-widgets.component.scss'],
})
export class SmallCompareWidgetsComponent implements OnInit {
  @Input() smallWidgets = [];

  constructor() {}

  ngOnInit() {}
}
