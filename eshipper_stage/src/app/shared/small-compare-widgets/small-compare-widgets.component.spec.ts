import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmallCompareWidgetsComponent } from './small-compare-widgets.component';

describe('SmallCompareWidgetsComponent', () => {
  let component: SmallCompareWidgetsComponent;
  let fixture: ComponentFixture<SmallCompareWidgetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmallCompareWidgetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmallCompareWidgetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
