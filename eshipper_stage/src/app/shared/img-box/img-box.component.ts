import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-img-box',
  templateUrl: './img-box.component.html',
  styleUrls: ['./img-box.component.scss'],
})
export class ImgBoxComponent implements OnInit {
  @Input() url = '';
  @Input() size = '40'; // 40, 64, 80
  @Input() icon = 'outline-local_shipping';
  constructor() {}

  ngOnInit() {}
}
