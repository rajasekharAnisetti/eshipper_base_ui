import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MainNavigation } from 'src/app/interfaces/shared';
@Component({
  selector: 'app-main-navigation',
  templateUrl: './main-navigation.component.html',
  styleUrls: ['./main-navigation.component.scss'],
})
export class MainNavigationComponent implements OnInit {
  @Output() close = new EventEmitter();
  mainMenu: MainNavigation;

  constructor() {}

  ngOnInit() {
    this.mainMenu = JSON.parse(localStorage.getItem('menuItems'));
  }

  // close navigation
  closeNavigation() {
    this.close.emit();
  }
  // end of close navigation
}
