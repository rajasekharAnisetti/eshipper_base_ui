import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialModules } from 'src/app/material-modules';

import { InstantQuoteDialogComponent } from 'src/app/instant-quote/instant-quote-dialog/instant-quote-dialog.component';

import { RegisterNewUserComponent } from './register-new-user/register-new-user.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';

@NgModule({
  declarations: [
    LoginComponent,
    ForgotPasswordComponent,
    InstantQuoteDialogComponent,
    RegisterNewUserComponent,
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    MaterialModules,
    SharedModule,
    RouterModule,
  ],
  exports: [LoginComponent],
  entryComponents: [ForgotPasswordComponent, InstantQuoteDialogComponent],
})
export class LoginModule {}
