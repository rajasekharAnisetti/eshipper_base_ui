import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login.component';
import { RegisterNewUserComponent } from './register-new-user/register-new-user.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    data: { loginFlag: true },
  },
  {
    path: 'register-new-user',
    component: RegisterNewUserComponent,
    data: { loginFlag: true },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginRoutingModule {}
