import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private http: HttpClient) {}

  getUsers(): Observable<any> {
    return this.http
      .get('../../content/data/users.json')
      .pipe(map(data => data));
  }

  // main menu

  getMenu(): Observable<any> {
    return this.http
      .get('../../content/data/main-menu.json')
      .pipe(map(data => data));
  }

  // super admin menu
  getSuperAdminMenu(): Observable<any> {
    return this.http
      .get('../../content/data/super-admin-menu.json')
      .pipe(map(data => data));
  }

  // customer menu
  getCustomerMenu(): Observable<any> {
    return this.http
      .get('../../content/data/main-customer-menu.json')
      .pipe(map(data => data));
  }
}
