import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { ValidationClassStateMatcher } from 'src/app/shared/classes/validation-class';
import { Countries } from 'src/app/shared/const/countries';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-register-new-user',
  templateUrl: './register-new-user.component.html',
  styleUrls: ['./register-new-user.component.scss'],
})
export class RegisterNewUserComponent implements OnInit {
  title = 'Register as New User';
  filteredCityes: Observable<any[]>;
  staticData = {
    countries: Countries.COUNTRIES,
    provinces: Countries.USASTATES,
    cities: Countries.USACITIES,
    stateLabel: 'State',
    zipLabel: 'Zip Code',
  };

  data = {
    country: '',
    zip: '',
    city: '',
    province: ''
  }

  places = ['Google', 'Bing', 'Yahoo'];
  monthlyVolumes = ['1 - 10', '11 - 30', '31 - 70', '71+'];
  industries = ['Option 1', 'Option 2', 'Option 3'];

  // form validation
  requiredText = 'Field is required';
  accountFormControl = new FormControl('', [Validators.required]);
  passwordFormControl = new FormControl('', [Validators.required]);
  confirmPasswordFormControl = new FormControl('', [Validators.required]);
  companyFormControl = new FormControl('', [Validators.required]);
  addressFormControl = new FormControl('', [Validators.required]);
  cityFormControl = new FormControl('', [Validators.required]);
  provinceFormControl = new FormControl('', [Validators.required]);
  zipFormControl = new FormControl('', [Validators.required]);
  countryFormControl = new FormControl('', [Validators.required]);
  nameFormControl = new FormControl('', [Validators.required]);
  phoneFormControl = new FormControl('', [Validators.required]);
  monthlyVolume = new FormControl('', [Validators.required]);
  industry = new FormControl('', [Validators.required]);

  matcher = new ValidationClassStateMatcher();
  // end of form validation

  constructor() { }

  ngOnInit() {
    // for city
    this.filteredCityes = this.cityFormControl.valueChanges.pipe(
      startWith(''),
      map(city => city ? this._filter(city) : this.staticData.cities.slice())
    )
  }

  // city autocomplete
  displayFn(city?: string): string | undefined {
    return city ? city : undefined;
  }

  private _filter(city: string) {
    const filterValue = city.toLowerCase();

    return this.staticData.cities.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
  }
  // emd of city autocomplete

  changeCountry(country) {
    this.data.city = '';
    switch (country) {
      case 'Canada':
        {
          this.staticData.stateLabel = 'Province';
          this.staticData.zipLabel = 'Postal Code';
          this.staticData.provinces = Countries.CANADAPROVINCES;
          this.staticData.cities = Countries.CANADACITIES;
        }
        break;
      case 'USA':
        {
          this.staticData.stateLabel = 'State';
          this.staticData.zipLabel = 'Zip Code';
          this.staticData.provinces = Countries.USASTATES;
          this.staticData.cities = Countries.USACITIES;
        }
        break;
      default:
        break;
    }
  }
}
