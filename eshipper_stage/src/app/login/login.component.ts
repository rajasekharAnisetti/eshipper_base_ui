import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { LoginService } from './login.service';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { InstantQuoteDialogComponent } from '../instant-quote/instant-quote-dialog/instant-quote-dialog.component';

import { ValidationClassStateMatcher } from 'src/app/shared/classes/validation-class';
import { UserAvatar } from 'src/app/interfaces/user';
import { TimeOutDialogComponent } from '../dialogs/time-out-dialog/time-out-dialog.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  userName: string;
  users: any;
  subscription = new Subscription();
  account: string;
  password: string;
  userAvatar: UserAvatar;

  accountFormControl = new FormControl('', [Validators.required]);
  passwordFormControl = new FormControl('', [Validators.required]);

  matcher = new ValidationClassStateMatcher();

  colors = [
    'light-blue',
    'blue',
    'dark-blue',
    'dark-green',
    'green',
    'light-purple',
    'purple',
    'red',
    'yellow',
    'light-orange',
    'orange',
    'brown',
  ];

  constructor(
    public dialog: MatDialog,
    private getData: LoginService,
    private router: Router
  ) {
    setTimeout(() => {
      {
        this.dialog.open(TimeOutDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
          data: {
            title: 'Time Out',
            message: 'Details text goes here...Learn How To Motivate Yourself',
          },
        });
      }
    }, 5000)
  }

  ngOnInit() {
    localStorage.clear();
  }

  openForgotPassword() {
    const dialogRef = this.dialog.open(ForgotPasswordComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscription.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }

  // login function
  login(account) {
    localStorage.clear();
    localStorage.setItem('account', account);
    this.subscription.add(
      this.getData.getUsers().subscribe(data => {
        this.users = data;
        for (const item of this.users) {
          if (item.account === account) {
            this.userName = item.name;
            localStorage.setItem('userPosition', item.position);
            localStorage.setItem('profileUrl', item.profile);
            this.generateAvatar(item.name, item.avatar);
          }
        }
        // placeholder (if !admin & !customer)
        if (!this.userName) {
          this.userName = this.users[1].name;
          localStorage.setItem('userPosition', this.users[1].position);
          localStorage.setItem('profileUrl', this.users[1].profile);
          this.generateAvatar(this.users[1].name, this.users[1].avatar);
        }
        // end of placeholder
        // main menu
        switch (account) {
          case 'admin':
            this.subscription.add(
              this.getData.getMenu().subscribe(menu => {
                localStorage.setItem('menuItems', JSON.stringify(menu));
                this.router.navigate(['/admin']);
              })
            );
            break;
          case 'superadmin':
            this.subscription.add(
              this.getData.getSuperAdminMenu().subscribe(superAdminMenu => {
                localStorage.setItem(
                  'menuItems',
                  JSON.stringify(superAdminMenu)
                );
                this.router.navigate(['/super-admin']);
              })
            );
            break;
          default:
            this.subscription.add(
              this.getData.getCustomerMenu().subscribe(customerMenu => {
                localStorage.setItem('menuItems', JSON.stringify(customerMenu));
                this.router.navigate(['/customer']);
              })
            );
            break;
        }
      })
    );
  }

  // generate avatar
  generateAvatar(name, url) {
    let color;
    if (url) {
      color = '';
    } else {
      color = this.colors[Math.floor(Math.random() * this.colors.length)];
    }
    this.userAvatar = {
      name: name,
      imageUrl: url,
      initialColor: color,
      initial: this.getInitial(name),
    };
    localStorage.setItem('userAvatar', JSON.stringify(this.userAvatar));
  }
  getInitial(name) {
    const matches = name.match(/\b(\w)/g);
    return matches.join('');
  }
  // end of generate avatar

  // end of login function

  openInstantQuoteDialog() {
    const dialogRef = this.dialog.open(InstantQuoteDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscription.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
