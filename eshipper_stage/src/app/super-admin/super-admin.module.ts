import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { SuperAdminTrackingModule } from './super-admin-tracking/super-admin-tracking.module';
import { SuperAdminRoutingModule } from './super-admin-routing.module';
import { TrackModule } from './track/track.module';

import { SuperAdminComponent } from './super-admin.component';
import { FranchiseTrackingComponent } from './franchise-tracking/franchise-tracking.component';
import { RevenueProfitReportComponent } from './revenue-profit-report/revenue-profit-report.component';
import { InsuranceReportComponent } from './insurance-report/insurance-report.component';

@NgModule({
  declarations: [
    SuperAdminComponent,
    FranchiseTrackingComponent,
    RevenueProfitReportComponent,
    InsuranceReportComponent,
  ],
  imports: [
    CommonModule,
    SuperAdminRoutingModule,
    PerfectScrollbarModule,
    SuperAdminTrackingModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
    TrackModule,
    MaterialModules,
    SharedModule,
  ],
  exports: [SuperAdminComponent],
})
export class SuperAdminModule {}
