import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserComponent } from 'src/app/shared/user/user.component';

import { SuperAdminComponent } from './super-admin.component';
import { SuperAdminTrackingComponent } from './super-admin-tracking/super-admin-tracking.component';
import { TrackComponent } from './track/track.component';
import { FranchiseTrackingComponent } from './franchise-tracking/franchise-tracking.component';
import { RevenueProfitReportComponent } from './revenue-profit-report/revenue-profit-report.component';
import { InsuranceReportComponent } from './insurance-report/insurance-report.component';

const superAdminRoutes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: 'dashboard',
    loadChildren: () =>
      import('./super-admin-dashboard/super-admin-dashboard.module').then(
        mod => mod.SuperAdminDashboardModule
      ),
  },
  {
    path: 'tracking',
    component: SuperAdminTrackingComponent,
    data: { title: 'Tracking' },
  },
  {
    path: 'franchise-tracking',
    component: FranchiseTrackingComponent,
    data: { title: 'Franchise Name', backBtn: true },
  },
  {
    path: 'track',
    component: TrackComponent,
    data: { title: 'Tracking', backBtn: true },
  },
  {
    path: 'track-details',
    loadChildren: () =>
      import('../order-details/order-details.module').then(
        mod => mod.OrderDetailsModule
      ),
    data: {
      title: 'Trans # SDJ123456789012',
      backBtn: true,
    },
  },
  {
    path: 'franchises',
    loadChildren: () =>
      import('./franchises/franchises.module').then(
        mod => mod.FranchisesModule
      ),
  },
  {
    path: 'markup',
    loadChildren: () =>
      import('../markup/markup.module').then(mod => mod.MarkupModule),
  },
  {
    path: 'franchise/new',
    loadChildren: () =>
      import('./franchises/franchise/franchise.module').then(
        mod => mod.FranchiseModule
      ),
    data: { title: 'New Franchice', backBtn: true, newFlag: true },
  },
  {
    path: 'franchise/edit',
    loadChildren: () =>
      import('./franchises/franchise/franchise.module').then(
        mod => mod.FranchiseModule
      ),
    data: { title: 'Franchice Name', backBtn: true, newFlag: false },
  },
  {
    path: 'user/new',
    component: UserComponent,
    data: { title: 'Add New User', backBtn: true, newFlag: true },
  },
  {
    path: 'user/edit',
    component: UserComponent,
    data: { title: 'User Name', backBtn: true },
  },
  {
    path: 'revenue-profit-report',
    component: RevenueProfitReportComponent,
    data: { title: 'Revenue & Profit' },
  },
  {
    path: 'insurance',
    component: InsuranceReportComponent,
    data: { title: 'Insurance' },
  },
  {
    path: 'profile',
    loadChildren: () =>
      import('../profile/profile.module').then(mod => mod.ProfileModule),
  },
];

const routes: Routes = [
  {
    path: '',
    component: SuperAdminComponent,
    children: superAdminRoutes,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuperAdminRoutingModule {}
