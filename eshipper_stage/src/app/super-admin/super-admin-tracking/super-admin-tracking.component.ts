import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { FranchiseTrackItem } from 'src/app/interfaces/franchise';
import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';
import { SuperAdminTrackingFilterDialogComponent } from 'src/app/filters/super-admin-tracking-filter-dialog/super-admin-tracking-filter-dialog.component';

import { SuperAdminTrackingService } from './super-admin-tracking.service';

@Component({
  selector: 'app-super-admin-tracking',
  templateUrl: './super-admin-tracking.component.html',
  styleUrls: ['./super-admin-tracking.component.scss'],
})
export class SuperAdminTrackingComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  dataEmpty = false;
  contentToolbarSettings = ['Export', 'Print Labels'];
  itemSettings = ['Export', 'Print Labels'];
  smallWidgetItems: Array<any>;
  franchiseItems: Array<FranchiseTrackItem>;

  constructor(
    private getData: SuperAdminTrackingService,
    private router: Router,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    // get small widgets
    this.subscribe.add(
      this.getData.getSuperAdminTrakingSmallWIdgets().subscribe(data => {
        if (data) {
          this.smallWidgetItems = data;
        }
      })
    );
    // get main list data
    this.subscribe.add(
      this.getData.getFranchiseItems().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        this.franchiseItems = data.content.length ? data.content : [];
      })
    );
  }

  // toolbar filter
  filterOpen() {
    const dialogRef = this.dialog.open(
      SuperAdminTrackingFilterDialogComponent,
      {
        panelClass: 'normal-dialog-width',
        autoFocus: false,
      }
    );
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of toolbar filter

  // toolbar search
  search(val) {
    console.log(val);
  }
  // end of toolbar search

  // toolbar settings
  settingsFunction(val) {
    console.log(val);
  }
  // end of toolbar settings

  // get period
  getPeriod(period) {
    console.log(period);
  }
  // end of get period

  // chage info dialog
  openChargeInfoDialog(data) {
    this.dialog.open(InfoDialogComponent, {
      data: {
        title: 'Charge Details',
        moreData: data,
      },
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
  }
  // end of chage info dialog

  // item settings function
  itemSettingsFunction(val, item) {
    console.log(val, item);
  }
  // end of item settings function

  // click on row
  clickOnRow(item) {
    console.log(item);
    const url = '/super-admin/franchise-tracking';
    this.router.navigate([url]);
  }
  // end of click on row

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
