import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperAdminTrackingComponent } from './super-admin-tracking.component';

describe('SuperAdminTrackingComponent', () => {
  let component: SuperAdminTrackingComponent;
  let fixture: ComponentFixture<SuperAdminTrackingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperAdminTrackingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperAdminTrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
