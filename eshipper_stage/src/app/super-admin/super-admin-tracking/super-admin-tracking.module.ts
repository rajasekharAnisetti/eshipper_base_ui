import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { SuperAdminTrackingComponent } from './super-admin-tracking.component';

@NgModule({
  declarations: [SuperAdminTrackingComponent],
  imports: [
    CommonModule,
    FormsModule,
    PerfectScrollbarModule,
    MaterialModules,
    SharedModule,
    RouterModule,
  ],
  exports: [SuperAdminTrackingComponent],
})
export class SuperAdminTrackingModule {}
