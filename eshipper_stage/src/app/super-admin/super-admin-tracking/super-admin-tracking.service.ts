import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SuperAdminTrackingService {
  constructor(private http: HttpClient) {}

  getSuperAdminTrakingSmallWIdgets(): Observable<any> {
    return this.http
      .get('content/data/super-admin/super-admin-tracking-small-widgets.json')
      .pipe(map(data => data));
  }

  getFranchiseItems(): Observable<any> {
    return this.http
      .get('content/data/super-admin/franchise-items.json')
      .pipe(map(data => data));
  }
}
