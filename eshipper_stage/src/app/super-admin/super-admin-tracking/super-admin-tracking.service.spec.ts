import { TestBed } from '@angular/core/testing';

import { SuperAdminTrackingService } from './super-admin-tracking.service';

describe('SuperAdminTrackingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SuperAdminTrackingService = TestBed.get(SuperAdminTrackingService);
    expect(service).toBeTruthy();
  });
});
