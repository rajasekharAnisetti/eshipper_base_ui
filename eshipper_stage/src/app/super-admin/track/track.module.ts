import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { TrackComponent } from './track.component';

@NgModule({
  declarations: [TrackComponent],
  imports: [
    CommonModule,
    FormsModule,
    PerfectScrollbarModule,
    MaterialModules,
    SharedModule,
    RouterModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDA_v-jmcXGmGO--6fJaEEZZot93E34rwM',
    }),
  ],
  exports: [TrackComponent],
})
export class TrackModule {}
