import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TrackService {
  constructor(private http: HttpClient) {}

  getFranchiseTrack(): Observable<any> {
    return this.http
      .get('content/data/super-admin/franchise-track.json')
      .pipe(map(data => data));
  }
}
