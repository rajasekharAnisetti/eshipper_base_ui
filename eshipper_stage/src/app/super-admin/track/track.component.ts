import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { TrackService } from './track.service';

import { FranchiseTrack } from 'src/app/interfaces/franchise';

@Component({
  selector: 'app-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.scss'],
})
export class TrackComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();

  // coordinates for example only
  lat = 43.727022;
  lng = -79.327631;
  // end of coordinates

  trackData = <FranchiseTrack>{};

  constructor(private getData: TrackService, private router: Router) {}

  ngOnInit() {
    this.subscribe.add(
      this.getData.getFranchiseTrack().subscribe(data => {
        this.trackData = data ? data : [];
      })
    );
  }

  // open details
  openDetails() {
    const url = '/super-admin/track-details';
    this.router.navigate([url]);
  }
  // end of open details

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
