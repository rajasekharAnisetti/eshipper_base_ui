import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranchiseTrackingComponent } from './franchise-tracking.component';

describe('FranchiseTrackingComponent', () => {
  let component: FranchiseTrackingComponent;
  let fixture: ComponentFixture<FranchiseTrackingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranchiseTrackingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranchiseTrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
