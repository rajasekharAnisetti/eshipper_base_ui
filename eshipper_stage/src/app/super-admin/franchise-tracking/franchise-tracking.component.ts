import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { TrackingFilterDialogComponent } from 'src/app/filters/tracking-filter-dialog/tracking-filter-dialog.component';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { SchedulePickupDialogComponent } from 'src/app/dialogs/schedule-pickup-dialog/schedule-pickup-dialog.component';

import { FranchiseTrakingServiceService } from './franchise-traking-service.service';

@Component({
  selector: 'app-franchise-tracking',
  templateUrl: './franchise-tracking.component.html',
  styleUrls: ['./franchise-tracking.component.scss'],
})
export class FranchiseTrackingComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  dataEmpty = true;
  trackings: Array<any>;
  itemSettings = [
    'Edit',
    'Print Label',
    'Schedule Pickup',
    'divider',
    'File a Claim',
    'Cancel',
  ];
  contentToolbarSettings = ['Export', 'Print Labels'];
  constructor(
    public dialog: MatDialog,
    private getData: FranchiseTrakingServiceService,
    private router: Router
  ) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData.getAdminTrackings().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        data.content.length
          ? (this.trackings = data.content)
          : console.log('Error!!!');
      })
    );
  }

  // filter
  filterOpen() {
    const dialogRef = this.dialog.open(TrackingFilterDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of filter

  // toolbar settings
  settingsFunction(val) {
    switch (val) {
      case 'Export':
        const dialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          dialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print Labels':
        console.log(val);
        break;
      default:
        break;
    }
  }
  // end of toolbar settings

  // item settings
  getItemSettingsValue(val) {
    console.log(val);
    switch (val) {
      case 'Edit':
        {
        }
        break;
      case 'Schedule Pickup':
        {
          const dialogRef = this.dialog.open(SchedulePickupDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;
      case 'Cancel':
        break;
      case 'File a Claim':
        {
          const url = '/file-a-claim';
          this.router.navigate([url]);
        }
        break;

      default:
        break;
    }
  }
  // end of item settings

  // get period
  getPeriod(period) {
    console.log(period);
  }
  // end of get period

  // click row functiom
  clickRowFunction(item) {
    console.log(item);
    const url = '/super-admin/track';
    this.router.navigate([url]);
  }
  // end of click row functiom

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
