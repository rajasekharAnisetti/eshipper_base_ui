import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class FranchiseTrakingServiceService {
  constructor(private http: HttpClient) {}

  // get admin trackings data
  getAdminTrackings(): Observable<any> {
    return this.http
      .get('/content/data/super-admin/admin-trackings.json')
      .pipe(map(data => data));
  }
}
