import { TestBed } from '@angular/core/testing';

import { FranchiseTrakingServiceService } from './franchise-traking-service.service';

describe('FranchiseTrakingServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FranchiseTrakingServiceService = TestBed.get(FranchiseTrakingServiceService);
    expect(service).toBeTruthy();
  });
});
