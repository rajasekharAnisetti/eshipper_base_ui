import { TestBed } from '@angular/core/testing';

import { InsuranceReportService } from './insurance-report.service';

describe('InsuranceReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InsuranceReportService = TestBed.get(InsuranceReportService);
    expect(service).toBeTruthy();
  });
});
