import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class InsuranceReportService {
  constructor(private http: HttpClient) {}

  // insurance list
  getInsuranceList(): Observable<any> {
    return this.http
      .get('content/data/reports/insurance.json')
      .pipe(map(data => data));
  }
}
