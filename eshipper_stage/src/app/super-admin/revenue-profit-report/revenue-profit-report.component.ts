import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';

import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { PrintDialogComponent } from 'src/app/dialogs/print-dialog/print-dialog.component';
import { RevenueProfitReport } from 'src/app/interfaces/reports';
import { SIMPLESETTINGS } from 'src/app/shared/const/dropdowns';

import { RevenueProfitReportService } from './revenue-profit-report.service';

@Component({
  selector: 'app-revenue-profit-report',
  templateUrl: './revenue-profit-report.component.html',
  styleUrls: ['./revenue-profit-report.component.scss'],
})
export class RevenueProfitReportComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  toolbarSettings = SIMPLESETTINGS;
  smallWidgetItems = [];
  complexWidgetItems = [
    {
      label: 'Total Shipment',
      value: '89,736.78/89,936.78',
      compareData: {
        warn: false,
        data: '+ 48 % ( + 60 )',
      },
    },
    {
      label: 'Cost',
      value: '$500.00/$430.00',
      compareData: {
        warn: true,
        data: '- 13 % ( - $ 267.00 )',
      },
    },
    {
      label: 'Charge',
      value: '$500.00/$600.00',
      compareData: {
        warn: false,
        data: '+ 29 % ( + $ 267.00 )',
      },
    },
    {
      label: 'Profit',
      value: '$500.00/$600.00',
      compareData: {
        warn: false,
        data: '+ 29 % ( + $ 267.00 )',
      },
    },
    {
      label: 'Revenue',
      value: '$1,233.00/$1,600.00',
      compareData: {
        warn: false,
        data: '+ 29 % ( + $ 267.00 )',
      },
    },
  ];
  compareFlag = false;
  carrierList: Array<RevenueProfitReport> = [];
  firstCompareTab = 'All';
  secondCompareTab = 'All';

  // chart widget data
  widget = {
    name: 'Widget Name',
    settings: {
      chartValue: false,
      chartTypes: ['Type 1', 'Type 2', 'Type 3'],
    },
  };

  // chart
  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      },
    },
    maintainAspectRatio: false
  };
  public barChartLabels: Label[] = [
    '2006',
    '2007',
    '2008',
    '2009',
    '2010',
    '2011',
    '2012',
  ];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
  ];
  // end of chart widget data

  constructor(
    private dialog: MatDialog,
    private getDate: RevenueProfitReportService
  ) { }

  ngOnInit() {
    this.subscribe.add(
      this.getDate.getRevenueProfitData().subscribe(data => {
        this.smallWidgetItems = data.smallWidgets.length
          ? data.smallWidgets
          : [];

        this.carrierList = data.content.length ? data.content : [];
      })
    );
  }

  // compare
  getPeriod(period) {
    console.log(period);
    this.firstCompareTab = period;
  }
  changeCompareFlag(flag) {
    this.compareFlag = flag;
    this.secondCompareTab = 'All';
  }
  getComparePeriod(comparePeriod) {
    console.log(comparePeriod);
    this.secondCompareTab = comparePeriod;
  }
  // end of compare

  // toolbar settings
  toolbarSettingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print':
        const printDialogRef = this.dialog.open(PrintDialogComponent, {
          autoFocus: false,
          panelClass: 'small-dialog-width',
        });
        this.subscribe.add(
          printDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }
  // end of toolbar settings

  // sum function
  sum(item, key) {
    let sum = 0;
    for (const i of item.collapceData) {
      sum = sum + i[key];
    }
    return sum;
  }
  // end of sum function

  ngOnDestroy() { }
}
