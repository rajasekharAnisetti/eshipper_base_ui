import { TestBed } from '@angular/core/testing';

import { RevenueProfitReportService } from './revenue-profit-report.service';

describe('RevenueProfitReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RevenueProfitReportService = TestBed.get(RevenueProfitReportService);
    expect(service).toBeTruthy();
  });
});
