import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class RevenueProfitReportService {
  constructor(private http: HttpClient) {}

  getRevenueProfitData(): Observable<any> {
    return this.http
      .get('content/data/super-admin/revenue-profit-report.json')
      .pipe(map(data => data));
  }
}
