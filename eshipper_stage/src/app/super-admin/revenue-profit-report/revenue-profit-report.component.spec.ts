import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevenueProfitReportComponent } from './revenue-profit-report.component';

describe('RevenueProfitReportComponent', () => {
  let component: RevenueProfitReportComponent;
  let fixture: ComponentFixture<RevenueProfitReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevenueProfitReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevenueProfitReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
