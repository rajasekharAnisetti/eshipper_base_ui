import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule } from '@angular/forms';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { SuperAdminDashboardRoutingModule } from './super-admin-dashboard-routing.module';
import { SuperAdminDashboardComponent } from './super-admin-dashboard.component';

@NgModule({
  declarations: [SuperAdminDashboardComponent],
  imports: [
    CommonModule,
    FormsModule,
    PerfectScrollbarModule,
    MaterialModules,
    SharedModule,
    SuperAdminDashboardRoutingModule,
  ],
  exports: [SuperAdminDashboardComponent],
})
export class SuperAdminDashboardModule {}
