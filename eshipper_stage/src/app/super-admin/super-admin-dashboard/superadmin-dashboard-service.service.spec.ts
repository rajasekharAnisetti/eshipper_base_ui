import { TestBed } from '@angular/core/testing';

import { SuperadminDashboardServiceService } from './superadmin-dashboard-service.service';

describe('SuperadminDashboardServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SuperadminDashboardServiceService = TestBed.get(SuperadminDashboardServiceService);
    expect(service).toBeTruthy();
  });
});
