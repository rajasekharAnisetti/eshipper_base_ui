import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SuperadminDashboardServiceService {
  constructor(private http: HttpClient) {}

  getDashboardSmallWidgets(): Observable<any> {
    return this.http
      .get('content/data/dashboard-small-widgets.json')
      .pipe(map(data => data));
  }
}
