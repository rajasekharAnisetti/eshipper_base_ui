import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { SuperadminDashboardServiceService } from './superadmin-dashboard-service.service';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { WidgetSettingsDialogComponent } from 'src/app/dialogs/widget-settings-dialog/widget-settings-dialog.component';

@Component({
  selector: 'app-super-admin-dashboard',
  templateUrl: './super-admin-dashboard.component.html',
  styleUrls: ['./super-admin-dashboard.component.scss'],
})
export class SuperAdminDashboardComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  smallWidgetItems: Array<any>;
  toolbarSettings = ['Export', 'Settings'];
  widgetsSettingsOptions = [
    {
      value: 'Widget Name',
      check: false,
    },
    {
      value: 'Widget Name',
      check: false,
    },
    {
      value: 'Widget Name',
      check: false,
    },
    {
      value: 'Widget Name',
      check: false,
    },
    {
      value: 'Widget Name',
      check: false,
    },
    {
      value: 'Widget Name',
      check: false,
    },
  ];
  dashboardCustomMenu = ['Franchise', 'Option 2', 'Option 3'];

  constructor(private getData: SuperadminDashboardServiceService, public dialog: MatDialog) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData.getDashboardSmallWidgets().subscribe(data => {
        this.smallWidgetItems = data;
      })
    );
  }

  // get period
  getPeriod(period) {
    console.log(period);
  }
  // end of get period

  // get Custom Option
  getCustomOption(val) {
    console.log(val);
  }
  // end of get Custom Option

  // toolbar settings function
  toolbarSettingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Settings':
        const settingsDialogRef = this.dialog.open(WidgetSettingsDialogComponent, {
          panelClass: 'small-dialog',
          autoFocus: false,
          data: {
            widgets: this.widgetsSettingsOptions
          }
        });
        this.subscribe.add(
          settingsDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }
  // end of toolbar settings function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
