import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { ValidationClassStateMatcher } from 'src/app/shared/classes/validation-class';

import { KeyContact } from 'src/app/interfaces/franchise';
@Component({
  selector: 'app-key-contact-dialog',
  templateUrl: './key-contact-dialog.component.html',
  styleUrls: ['./key-contact-dialog.component.scss'],
})
export class KeyContactDialogComponent implements OnInit {
  modalData = <KeyContact>{};
  matcher = new ValidationClassStateMatcher();
  validation = {
    name: new FormControl(),
    position: new FormControl(),
    mobilePhone: new FormControl(),
    workPhone: new FormControl(),
    otherPhone: new FormControl(),
    primaryEmail: new FormControl('', [Validators.required, Validators.email]),
    secondaryEmail: new FormControl('', [
      Validators.required,
      Validators.email,
    ]),
  };
  positions = ['CEO', 'Position'];

  constructor(
    public dialogRef: MatDialogRef<KeyContactDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data.keyContactItem) {
      this.modalData = data.keyContactItem;
    }
  }

  ngOnInit() {}

  closeDialog() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.modalData);
  }
}
