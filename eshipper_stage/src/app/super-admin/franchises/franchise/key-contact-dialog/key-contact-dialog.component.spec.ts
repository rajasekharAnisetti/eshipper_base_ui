import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyContactDialogComponent } from './key-contact-dialog.component';

describe('KeyContactDialogComponent', () => {
  let component: KeyContactDialogComponent;
  let fixture: ComponentFixture<KeyContactDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeyContactDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyContactDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
