import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { Markup } from 'src/app/interfaces/markup';
import { MarkupsFilterComponent } from 'src/app/filters/markups-filter/markups-filter.component';

import { FranchiseService } from '../franchise.service';

@Component({
  selector: 'app-franchise-markups',
  templateUrl: './franchise-markups.component.html',
  styleUrls: ['./franchise-markups.component.scss'],
})
export class FranchiseMarkupsComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  markupsList: Array<Markup>;
  dataEmpty = true;
  contentToolbarUsaLocation = false;
  contentToolbarCaLocation = false;
  selection = false;
  toolbarIndeterminate = false;
  selectAll = false;
  carrirersItems = ['Carrier', 'Carrier 2']

  constructor(
    private getData: FranchiseService,
    public dialog: MatDialog,
    public router: Router
  ) { }

  ngOnInit() {
    // get markups list
    this.subscribe.add(
      this.getData.getMarkupsList().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        this.markupsList = data.content.length ? data.content : [];
      })
    );
  }

  // search function
  searchFunction(val) {
    console.log(val);
  }
  // end of search function

  // content toolbar settings
  contentToolbarSettings(val) {
    console.log(val);
  }
  // end of content toolbar settings

  // filter function
  filter() {
    const dialogRef = this.dialog.open(MarkupsFilterComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of filter function

  // carriers filter
  getCarriresFilter(val) {
    console.log(val)
  }
  // end of carriers filter

  // get sort option
  getSortData(data) {
    console.log(data);
  }
  // end of get sort option

  // toolbar edit markup function
  toolbarEditMarkupFunction() {
    const url = '/super-admin/markup';
    this.router.navigate([url]);
  }
  // end of toolbar edit markup function

  // row click function
  rowClickFunction(item) {
    const url = '/super-admin/markup';
    this.router.navigate([url]);
  }
  // end of row click function

  // reset default
  resetDefaultFunction(item) {
    console.log(item);
  }
  // and of content toolbar settings

  // get location function
  getLocation(item) {
    let location = '';
    if (item.caFlag && item.usaFlag) {
      location = 'CA, USA';
    } else if (item.caFlag) {
      location = 'CA';
    } else if (item.usaFlag) {
      location = 'USA';
    }
    return location;
  }
  // end of get location function

  // select functions
  selectAllFunction(flag) {
    const select = flag ? true : false;
    this.selection = select ? true : false;
    for (const i of this.markupsList) {
      i.selected = select;
    }
  }

  selectRow(data) {
    const n = this.markupsList.length;
    let item = 0;
    for (const i of this.markupsList) {
      if (i.selected) {
        item++;
      }
    }
    this.selectAll = n === item ? true : false;
    this.toolbarIndeterminate = n > item && item !== 0 ? true : false;
    this.selection = this.selectAll || this.toolbarIndeterminate ? true : false;
  }
  // end of select functions

  ngOnDestroy() {
    // unsubscribe
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
