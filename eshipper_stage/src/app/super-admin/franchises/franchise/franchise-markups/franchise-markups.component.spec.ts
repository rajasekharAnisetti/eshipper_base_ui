import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranchiseMarkupsComponent } from './franchise-markups.component';

describe('FranchiseMarkupsComponent', () => {
  let component: FranchiseMarkupsComponent;
  let fixture: ComponentFixture<FranchiseMarkupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranchiseMarkupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranchiseMarkupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
