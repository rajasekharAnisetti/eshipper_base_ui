import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-franchise',
  templateUrl: './franchise.component.html',
  styleUrls: ['./franchise.component.scss'],
})
export class FranchiseComponent implements OnInit {
  tabLinks = [
    {
      route: 'details',
      name: 'Details',
    },
    {
      route: 'settings',
      name: 'Settings',
    },
    {
      route: 'roles',
      name: 'Roles',
    },
    {
      route: 'users',
      name: 'Users',
    },
    {
      route: 'markups',
      name: 'Markups',
    },
  ];
  activeLink = this.tabLinks[0];

  constructor() {}

  ngOnInit() {}
}
