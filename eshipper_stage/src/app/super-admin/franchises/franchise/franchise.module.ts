import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ColorPickerModule } from 'ngx-color-picker';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { FranchiseRoutingModule } from './franchise-routing.module';
import { FranchiseComponent } from './franchise.component';
import { FranchiseDetailsComponent } from './franchise-details/franchise-details.component';
import { FranchiseSettingsComponent } from './franchise-settings/franchise-settings.component';
import { FranchiseRolesComponent } from './franchise-roles/franchise-roles.component';
import { FranchiseUsersComponent } from './franchise-users/franchise-users.component';
import { FranchiseMarkupsComponent } from './franchise-markups/franchise-markups.component';
import { KeyContactDialogComponent } from './key-contact-dialog/key-contact-dialog.component';

@NgModule({
  declarations: [
    FranchiseComponent,
    FranchiseDetailsComponent,
    FranchiseSettingsComponent,
    FranchiseRolesComponent,
    FranchiseUsersComponent,
    FranchiseMarkupsComponent,
    KeyContactDialogComponent,
  ],
  imports: [
    CommonModule,
    FranchiseRoutingModule,
    MaterialModules,
    SharedModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    ColorPickerModule,
  ],
  exports: [FranchiseComponent],
  entryComponents: [KeyContactDialogComponent],
})
export class FranchiseModule {}
