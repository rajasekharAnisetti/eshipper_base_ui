import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

import { Franchise } from 'src/app/interfaces/franchise';
import { Countries } from 'src/app/shared/const/countries';
import { ValidationClassStateMatcher } from 'src/app/shared/classes/validation-class';
import { VideoDialogComponent } from 'src/app/dialogs/video-dialog/video-dialog.component';
import { CURRENCIES_$ } from 'src/app/shared/const/currencies';

import { FranchiseService } from '../franchise.service';
import { KeyContactDialogComponent } from '../key-contact-dialog/key-contact-dialog.component';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-franchise-details',
  templateUrl: './franchise-details.component.html',
  styleUrls: ['./franchise-details.component.scss'],
})
export class FranchiseDetailsComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  franchiseDetails = <Franchise>{};
  filteredCityes: Observable<any[]>;
  staticData = {
    countries: Countries.COUNTRIES,
    states: Countries.USASTATES,
    cities: Countries.USACITIES,
    stateLabel: 'State',
    zipLabel: 'Zip Code',
  };
  matcher = new ValidationClassStateMatcher();
  validation = {
    franchiseName: new FormControl(),
    address1: new FormControl(),
    address2: new FormControl(),
    city: new FormControl(),
    stateOrProvince: new FormControl(),
    zipOrPostal: new FormControl(),
    country: new FormControl(),
    contactName: new FormControl(),
    phone: new FormControl(),
    email: new FormControl('', [Validators.required, Validators.email]),
    financeEmail: new FormControl('', [Validators.required, Validators.email]),
    operationsEmail1: new FormControl('', [
      Validators.required,
      Validators.email,
    ]),
    operationsEmail2: new FormControl('', [
      Validators.required,
      Validators.email,
    ]),
    operationsEmail3: new FormControl('', [
      Validators.required,
      Validators.email,
    ]),
    primaryColor: new FormControl(),
    secondaryColor: new FormControl(),
    urlForLogin: new FormControl(),
    urlForRegister: new FormControl(),
  };

  currencies = CURRENCIES_$;
  keyContactItemSettings = ['Delete'];
  documentItemSettings = ['Download', 'Print', 'Delete'];

  constructor(
    private getData: FranchiseService,
    private activatedRoute: ActivatedRoute,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    const newFlag = this.activatedRoute.parent.snapshot.data.newFlag;
    if (!newFlag) {
      this.subscribe.add(
        this.getData.getFranchiseDetails().subscribe(data => {
          this.franchiseDetails = data;
        })
      );
    }
    this.franchiseDetails.primaryColor = '#472F92';
    this.franchiseDetails.secondaryColor = '#2FB5C0';

    // for city
    this.filteredCityes = this.validation.city.valueChanges.pipe(
      startWith(''),
      map(city => city ? this._filter(city) : this.staticData.cities.slice())
    )
  }

  // change currency
  currencyChange(val) {
    this.franchiseDetails.currency = val;
  }
  // end of change currency

  // city autocomplete
  displayFn(city?: string): string | undefined {
    return city ? city : undefined;
  }

  private _filter(city: string) {
    const filterValue = city.toLowerCase();

    return this.staticData.cities.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
  }
  // emd of city autocomplete

  // change country
  changeCountry(country) {
    console.log(country);
    switch (country) {
      case 'Canada':
        {
          this.staticData.stateLabel = 'Province';
          this.staticData.zipLabel = 'Postal Code';
          this.staticData.states = Countries.CANADAPROVINCES;
          this.staticData.cities = Countries.CANADACITIES;
        }
        break;
      case 'USA':
        {
          this.staticData.stateLabel = 'State';
          this.staticData.zipLabel = 'Zip Code';
          this.staticData.states = Countries.USASTATES;
          this.staticData.cities = Countries.USACITIES;
        }
        break;
      default:
        break;
    }
  }
  // end of change country

  // upload avatar
  upload() {
    console.log('Upload');
  }
  // end of upload avatar

  // key contact item settings
  keyContactItemSettingsFunction(val, item) {
    console.log(val, item);
  }
  // end of key contact item settings

  // edit key contact
  editKeyContact(item) {
    const dialogRef = this.dialog.open(KeyContactDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Edit Key Contact',
        actionButton: 'Edit',
        keyContactItem: item,
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of edit key contact

  // add key contact
  addKeyContact() {
    const dialogRef = this.dialog.open(KeyContactDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'New Key Contact',
        actionButton: 'Add',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.franchiseDetails.keyContactList.push(result);
        }
      })
    );
  }
  // end of add key contact

  // open url for login info
  openUrlForLoginInfo() {
    this.dialog.open(VideoDialogComponent, {
      panelClass: 'video-dialog',
      autoFocus: false,
      data: {
        youtubeVideoId: 'GRxofEmo3HA',
      },
    });
  }
  // end of open url for login info

  // open url for register info
  openUrlForRegisterInfo() {
    this.dialog.open(VideoDialogComponent, {
      panelClass: 'video-dialog',
      autoFocus: false,
      data: {
        youtubeVideoId: 'GRxofEmo3HA',
      },
    });
  }
  // end of open url for register info

  // document item settings
  documentItemSettingsFunction(val, item) {
    console.log(val, item);
  }
  // end of document item settings

  // add document
  addDocument() { }
  // end of add document

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
