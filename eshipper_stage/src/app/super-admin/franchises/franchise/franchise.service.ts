import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class FranchiseService {
  constructor(private http: HttpClient) {}

  // get franchise details
  getFranchiseDetails(): Observable<any> {
    return this.http
      .get('content/data/super-admin/franchise-details.json')
      .pipe(map(data => data));
  }

  // get franchise settings
  getFranchiseSettings(): Observable<any> {
    return this.http
      .get('content/data/super-admin/franchise-settings.json')
      .pipe(map(data => data));
  }

  // get franchise roles
  getFranchiseRoles(): Observable<any> {
    return this.http
      .get('content/data/super-admin/franchise-roles.json')
      .pipe(map(data => data));
  }
  // role permissions
  getRolePermissions(): Observable<any> {
    return this.http
      .get('content/data/account/role-permissions.json')
      .pipe(map(data => data));
  }

  // users list
  getUsersList(): Observable<any> {
    return this.http
      .get('content/data/admin-account/users-list.json')
      .pipe(map(data => data));
  }

  // markup list
  getMarkupsList(): Observable<any> {
    return this.http
      .get('content/data/admin-account/customer/markups.json')
      .pipe(map(data => data));
  }
}
