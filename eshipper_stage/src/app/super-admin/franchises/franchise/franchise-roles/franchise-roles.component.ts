import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

import { AdminRole, RolePermissions } from 'src/app/interfaces/roles';

import { FranchiseService } from '../franchise.service';

@Component({
  selector: 'app-franchise-roles',
  templateUrl: './franchise-roles.component.html',
  styleUrls: ['./franchise-roles.component.scss'],
})
export class FranchiseRolesComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  dataEmpty = true;
  toolbarSettings = ['Option 1', 'Option 2'];
  sectionItemSettings = ['Edit', 'Delete'];
  itemSettings = ['Edit', 'Delete'];
  rolePermissions: Array<RolePermissions>;
  adminRolesList: Array<AdminRole>;

  constructor(public router: Router, private getData: FranchiseService) { }

  ngOnInit() {
    // get role permissions
    this.subscribe.add(
      this.getData
        .getRolePermissions()
        .subscribe(data => (this.rolePermissions = data))
    );
    // get roles list
    this.subscribe.add(
      this.getData.getFranchiseRoles().subscribe(data => {
        this.adminRolesList = data.content;
        this.dataEmpty = data.content ? false : true;
      })
    );
  }

  // search
  searchFunction(val) {
    console.log(val);
  }
  // end of search

  // toolbar settings
  settingsFunction(val) {
    console.log(val);
  }
  // end of toolbar settings

  // section settings
  sectionItemSettingsFunction(val, section) {
    console.log(val, section);
  }
  // end of section settings

  // edit role
  editRole(item, role) {
    const url = '/super-admin/franchise/edit/roles/role/edit';
    localStorage.setItem('roleAdminFlag', 'admin');
    const data = {
      permissions: role.permissions,
      roleName: role.roleName,
      sectionName: item.sectionTitle
    };
    localStorage.setItem('roleData', JSON.stringify(data));
    this.router.navigate([url]);
  }
  // end of edit role

  // item settings function
  itemSettingsFunction(val, item, role) {
    switch (val) {
      case 'Edit':
        this.editRole(item, role);
        break;

      default:
        break;
    }
  }
  // end of item settings function

  // add role function
  addRole() {
    const url = '/super-admin/franchise/edit/roles/role/new';
    const newData = {
      permissions: this.rolePermissions,
      roleName: '',
      sectionName: ''
    };
    localStorage.setItem('roleAdminFlag', 'admin');
    localStorage.setItem('roleData', JSON.stringify(newData));
    this.router.navigate([url]);
  }
  // end of add role function

  // drop animation
  dropList(event: CdkDragDrop<string[]>, section) {
    moveItemInArray(section, event.previousIndex, event.currentIndex);
  }
  // end of drop animation

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
