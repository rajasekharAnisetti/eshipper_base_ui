import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranchiseRolesComponent } from './franchise-roles.component';

describe('FranchiseRolesComponent', () => {
  let component: FranchiseRolesComponent;
  let fixture: ComponentFixture<FranchiseRolesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranchiseRolesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranchiseRolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
