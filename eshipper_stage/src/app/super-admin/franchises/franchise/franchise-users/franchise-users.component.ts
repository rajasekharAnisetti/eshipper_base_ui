import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { User } from 'src/app/interfaces/user';

import { FranchiseService } from '../franchise.service';

@Component({
  selector: 'app-franchise-users',
  templateUrl: './franchise-users.component.html',
  styleUrls: ['./franchise-users.component.scss'],
})
export class FranchiseUsersComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  usersList: Array<User>;
  dataEmpty = true;
  roleFilterItems = ['All Roles', 'User Role 1', 'User Role 2'];
  toolbarSettings = ['Option 1', 'Option 2', 'Option 3'];
  itemSettings = ['Delete'];
  newFlag = false;

  constructor(private router: Router, private getData: FranchiseService) {}

  ngOnInit() {
    // get users list
    this.subscribe.add(
      this.getData.getUsersList().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        this.usersList = data.content.length ? data.content : [];
      })
    );
  }

  // user role filter
  getRoleFilter(val) {
    console.log(val);
  }
  // end of user role filter

  //  search function
  searchFunction(val) {
    console.log(val);
  }
  // end of search function

  // toolbar settings function
  toolbarSettingsFunction(val) {
    console.log(val);
  }
  // end of toolbar settings function

  // item settings function
  itemSettingsFunction(val, item) {
    console.log(val, item);
  }
  // end of item settings function

  // click on row function
  clickOnRow() {
    const url = '/super-admin/user/edit';
    this.router.navigate([url]);
  }
  // end of click on row function

  // add new user
  newUser() {
    const url = '/super-admin/user/new';
    this.router.navigate([url]);
  }
  // end of add new user

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
