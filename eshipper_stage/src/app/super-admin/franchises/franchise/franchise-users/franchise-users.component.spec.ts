import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranchiseUsersComponent } from './franchise-users.component';

describe('FranchiseUsersComponent', () => {
  let component: FranchiseUsersComponent;
  let fixture: ComponentFixture<FranchiseUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranchiseUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranchiseUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
