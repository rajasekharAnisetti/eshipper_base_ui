import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranchiseSettingsComponent } from './franchise-settings.component';

describe('FranchiseSettingsComponent', () => {
  let component: FranchiseSettingsComponent;
  let fixture: ComponentFixture<FranchiseSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FranchiseSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranchiseSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
