import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

import { FranchiseSettings } from 'src/app/interfaces/franchise-settings';
import { CarrierAccountDialogComponent } from 'src/app/dialogs/carrier-account-dialog/carrier-account-dialog.component';

import { FranchiseService } from '../franchise.service';

@Component({
  selector: 'app-franchise-settings',
  templateUrl: './franchise-settings.component.html',
  styleUrls: ['./franchise-settings.component.scss'],
})
export class FranchiseSettingsComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  data = <FranchiseSettings>{};

  validation = {
    subFranchiseOf: new FormControl(),
    paymentNetTerms: new FormControl(),
    paymentNetWarning: new FormControl(),
    paymentDisableAfter: new FormControl(),
    transactionalCostType: new FormControl(),
    transactionalTransactionFeeTiers: new FormControl(),
    recurringFeeFrequency: new FormControl(),
    recurringFeeRecurringFee: new FormControl(),
    recurringFeeOriginalLicense: new FormControl(),
    recurringFeeSetupCost: new FormControl(),
    oneTimeFeeOriginalLicense: new FormControl(),
    oneTimeFeeSetupCost: new FormControl(),
  };

  franchises = ['Option 1', 'Option 2', 'Option 3'];
  itemCarrierSettings = ['Edit', 'Delete'];
  costTypes = ['Option 1', 'Option 2', 'Option 3'];
  frequencies = ['Option 1', 'Option 2', 'Option 3'];

  constructor(
    private getData: FranchiseService,
    private dialog: MatDialog,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    const newFlag = this.activatedRoute.parent.snapshot.data.newFlag;
    if (!newFlag) {
      this.subscribe.add(
        this.getData.getFranchiseSettings().subscribe(data => {
          this.data = data;
        })
      );
    }
  }

  // add carrier account
  addCarrierAccount() {
    const dialogRef = this.dialog.open(CarrierAccountDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'New Carrier Account',
        actionButton: 'Add',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.data.carrierAccountsList.push(result);
        }
      })
    );
  }
  // end of add carrier account

  // carrier account item settings
  carrierAccountItemSettingsFunction(val, item) {
    switch (val) {
      case 'Edit':
        {
          const dialogRef = this.dialog.open(CarrierAccountDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              title: 'Edit Carrier Account',
              actionButton: 'Edit',
              modalData: item,
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                item = result;
              }
            })
          );
        }
        break;
      case 'Delete':
        {
          const index: number = this.data.carrierAccountsList.indexOf(item);
          if (index !== -1) {
            this.data.carrierAccountsList.splice(index, 1);
          }
        }
        break;
      default:
        break;
    }
  }
  // end of carrier account item settings

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
