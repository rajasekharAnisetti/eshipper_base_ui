import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FranchiseComponent } from './franchise.component';
import { FranchiseDetailsComponent } from './franchise-details/franchise-details.component';
import { FranchiseSettingsComponent } from './franchise-settings/franchise-settings.component';
import { FranchiseRolesComponent } from './franchise-roles/franchise-roles.component';
import { FranchiseUsersComponent } from './franchise-users/franchise-users.component';
import { FranchiseMarkupsComponent } from './franchise-markups/franchise-markups.component';

const routes: Routes = [
  {
    path: '',
    component: FranchiseComponent,
    children: [
      {
        path: '',
        redirectTo: 'details',
        pathMatch: 'full',
      },
      {
        path: 'details',
        component: FranchiseDetailsComponent,
      },
      {
        path: 'settings',
        component: FranchiseSettingsComponent,
      },
      {
        path: 'roles',
        component: FranchiseRolesComponent,
      },
      {
        path: 'roles/role/edit',
        loadChildren: () =>
          import('src/app/role/role.module').then(mod => mod.RoleModule),
        data: { title: 'Edit Role', backBtn: true, },
      },
      {
        path: 'roles/role/new',
        loadChildren: () =>
          import('src/app/role/role.module').then(mod => mod.RoleModule),
        data: { title: 'Create Role', backBtn: true },
      },
      {
        path: 'users',
        component: FranchiseUsersComponent,
      },
      {
        path: 'markups',
        component: FranchiseMarkupsComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FranchiseRoutingModule { }
