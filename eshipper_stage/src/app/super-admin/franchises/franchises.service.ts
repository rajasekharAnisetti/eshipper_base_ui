import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class FranchisesService {
  constructor(private http: HttpClient) {}

  getFranchisesList(): Observable<any> {
    return this.http
      .get('content/data/super-admin/franchises.json')
      .pipe(map(data => data));
  }
}
