import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FranchisesComponent } from './franchises.component';

const routes: Routes = [
  {
    path: '',
    component: FranchisesComponent,
    data: {
      title: 'Franchises',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FranchisesRoutingModule {}
