import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { Franchise } from 'src/app/interfaces/franchise';

import { FranchisesService } from './franchises.service';

@Component({
  selector: 'app-franchises',
  templateUrl: './franchises.component.html',
  styleUrls: ['./franchises.component.scss'],
})
export class FranchisesComponent implements OnInit, OnDestroy {
  franchises: Array<Franchise>;
  subscribe = new Subscription();
  dataEmpty = true;
  toolbarSettings = ['Option 1', 'Option 2'];
  itemSettings = ['Delete'];

  constructor(private getData: FranchisesService, private router: Router) {}

  ngOnInit() {
    this.subscribe.add(
      this.getData.getFranchisesList().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        this.franchises = data.content;
      })
    );
  }

  // toolbar settings function
  toolbarSettingsFunction(val) {
    console.log(val);
  }
  // end of toolbar settings function

  // item settings function
  itemSettingsFunction(i, item) {
    console.log(i, item);
  }
  // end of item settings function

  // add new franchise
  newFranchise() {
    const url = '/super-admin/franchise/new';
    this.router.navigate([url]);
  }
  // end of add new franchise

  // login to customer
  loginToCustomer(item) {
    console.log(item);
  }
  // end of login to customer

  // click on row
  clickOnRowItem(item) {
    console.log(item);
    const url = '/super-admin/franchise/edit';
    this.router.navigate([url]);
  }
  // end of click on row

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
