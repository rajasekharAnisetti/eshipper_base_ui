import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule } from '@angular/forms';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { FranchisesRoutingModule } from './franchises-routing.module';
import { FranchisesComponent } from './franchises.component';

@NgModule({
  declarations: [FranchisesComponent],
  imports: [
    CommonModule,
    FormsModule,
    PerfectScrollbarModule,
    FranchisesRoutingModule,
    MaterialModules,
    SharedModule,
  ],
  exports: [FranchisesComponent],
})
export class FranchisesModule {}
