import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { Countries } from 'src/app/shared/const/countries';
import { COLORS } from 'src/app/shared/const/colors';

@Component({
  selector: 'app-agent-dialog',
  templateUrl: './agent-dialog.component.html',
  styleUrls: ['./agent-dialog.component.scss'],
})
export class AgentDialogComponent implements OnInit {
  colors = COLORS;

  modalData = {
    company: '',
    agentName: '',
    emails: [],
    serviceRegionList: [],
    phone: '',
    phone2: '',
    fax: '',
    country: '',
    note: '',
  };

  countries = Countries.COUNTRIES;

  // form validation
  requiredText = 'Field is required';

  validation = {
    company: new FormControl('', [Validators.required]),
    agentName: new FormControl('', [Validators.required]),
    email: new FormControl(),
    phone: new FormControl('', [Validators.required]),
    phone2: new FormControl(),
    fax: new FormControl(),
    country: new FormControl('', [Validators.required]),
    note: new FormControl(),
  };
  // end of form validation

  constructor(
    public dialogRef: MatDialogRef<AgentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  // region list
  getRegions(regions) {
    this.modalData.serviceRegionList = regions;
  }
  // end of region list

  closeDialog() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.modalData);
  }
}
