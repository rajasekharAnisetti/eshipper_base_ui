import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-approved-dialog',
  templateUrl: './approved-dialog.component.html',
  styleUrls: ['./approved-dialog.component.scss'],
})
export class ApprovedDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ApprovedDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  ok = () => this.dialogRef.close(true);

  action(val) {
    this.dialogRef.close(val);
  }
}
