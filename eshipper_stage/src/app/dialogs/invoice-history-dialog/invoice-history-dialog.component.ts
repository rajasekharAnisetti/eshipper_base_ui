import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-invoice-history-dialog',
  templateUrl: './invoice-history-dialog.component.html',
  styleUrls: ['./invoice-history-dialog.component.scss'],
})
export class InvoiceHistoryDialogComponent implements OnInit {
  settings = ['Download', 'Print'];

  constructor(
    public dialogRef: MatDialogRef<InvoiceHistoryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  itemSettingsFunction(val, item) {
    console.log(val, item);
  }

  closeDialog = () => this.dialogRef.close();
}
