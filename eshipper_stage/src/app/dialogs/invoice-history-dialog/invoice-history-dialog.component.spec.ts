import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceHistoryDialogComponent } from './invoice-history-dialog.component';

describe('InvoiceHistoryDialogComponent', () => {
  let component: InvoiceHistoryDialogComponent;
  let fixture: ComponentFixture<InvoiceHistoryDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceHistoryDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceHistoryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
