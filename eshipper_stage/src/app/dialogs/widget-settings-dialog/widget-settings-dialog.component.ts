import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-widget-settings-dialog',
  templateUrl: './widget-settings-dialog.component.html',
  styleUrls: ['./widget-settings-dialog.component.scss']
})
export class WidgetSettingsDialogComponent implements OnInit {

  modalData: Array<any>

  constructor(public dialogRef: MatDialogRef<WidgetSettingsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    console.log(data.widgets);
    if (data.widgets) {
      this.modalData = data.widgets;
    }
  }

  ngOnInit() {

  }


  // close modal
  closeDialog = () => this.dialogRef.close();

  // action modal
  save = () => this.dialogRef.close(this.modalData);
}
