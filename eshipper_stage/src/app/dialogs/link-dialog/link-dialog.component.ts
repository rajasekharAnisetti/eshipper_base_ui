import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-link-dialog',
  templateUrl: './link-dialog.component.html',
  styleUrls: ['./link-dialog.component.scss'],
})
export class LinkDialogComponent implements OnInit {
  modalData = {
    customer: '',
  };

  validation = {
    customer: new FormControl('', [Validators.required]),
  };

  customers = [
    {
      value: 'Customer 1',
      viewValue: 'Customer 1',
    },
    {
      value: 'Customer 2',
      viewValue: 'Customer 2',
    },
    {
      value: 'Customer 3',
      viewValue: 'Customer 3',
    },
  ];

  constructor(
    public dialogRef: MatDialogRef<LinkDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  link = () => this.dialogRef.close(this.modalData);
}
