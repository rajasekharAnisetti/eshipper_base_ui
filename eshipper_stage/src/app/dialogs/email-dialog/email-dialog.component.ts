import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-email-dialog',
  templateUrl: './email-dialog.component.html',
  styleUrls: ['./email-dialog.component.scss'],
})
export class EmailDialogComponent implements OnInit {
  emailInfo = {
    shippingLabel: false,
    orderDetails: false,
    returnLabel: false,
    customsInvoice: false,
    emails: [],
  };

  constructor(
    public dialogRef: MatDialogRef<EmailDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  closeDialog = () => this.dialogRef.close();

  send() {
    this.dialogRef.close(this.emailInfo);
  }

  ngOnInit() {}
}
