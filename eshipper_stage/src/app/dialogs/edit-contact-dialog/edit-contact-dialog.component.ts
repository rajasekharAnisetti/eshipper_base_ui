import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { ValidationClassStateMatcher } from '../../shared/classes/validation-class';

@Component({
  selector: 'app-edit-contact-dialog',
  templateUrl: './edit-contact-dialog.component.html',
  styleUrls: ['./edit-contact-dialog.component.scss'],
})
export class EditContactDialogComponent implements OnInit {
  modalData = {
    companyName: '',
    attention: '',
    phone: '',
    email: '',
    brokerName: '',
    recipientTaxId: '',
    taxId: '',
  };
  // form validation
  requiredText = 'Field is required';
  matcher = new ValidationClassStateMatcher();
  validation = {
    companyName: new FormControl('', [Validators.required]),
    attention: new FormControl('', [Validators.required]),
    phone: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    brokerName: new FormControl('', [Validators.required]),
    recipientTaxId: new FormControl('', [Validators.required]),
    taxId: new FormControl('', [Validators.required]),
  };
  // end of form validation

  constructor(
    public dialogRef: MatDialogRef<EditContactDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data.fields) {
      this.modalData = data.fields;
    }
  }

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  save() {
    const error =
      this.validation.companyName.hasError('required') &&
      this.validation.attention.hasError('required') &&
      this.validation.phone.hasError('required') &&
      this.validation.email.hasError('required');

    if (!error) {
      this.dialogRef.close(this.modalData);
    }
  }
}
