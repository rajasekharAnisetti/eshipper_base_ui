import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { CarrierSttings } from 'src/app/interfaces/carier-settings';

@Component({
  selector: 'app-carrier-account-dialog',
  templateUrl: './carrier-account-dialog.component.html',
  styleUrls: ['./carrier-account-dialog.component.scss'],
})
export class CarrierAccountDialogComponent implements OnInit {
  modalData = <CarrierSttings>{
    carrierName: '',
    region: '',
    accountOwner: '',
    accountNumber: '',
    meterNumber: '',
    key: '',
    password: '',
  };

  validation = {
    carrierName: new FormControl(),
    region: new FormControl(),
    accountOwner: new FormControl(),
    accountNumber: new FormControl(),
    meterNumber: new FormControl(),
    key: new FormControl(),
    password: new FormControl(),
  };

  carrierNames = ['FedEx Webservice', 'UPS', 'Some Service'];
  accountOwners = ['Own Account', 'Option 2', 'Option 3'];
  regions = ['USA', 'Canada', 'Region'];

  constructor(
    public dialogRef: MatDialogRef<CarrierSttings>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data.modalData) {
      this.modalData = data.modalData;
    }
  }

  ngOnInit() {}

  // close dialog
  closeDialog = () => this.dialogRef.close();
  // end of close dialog

  // send modal data back to component
  send = () => this.dialogRef.close(this.modalData);
  // end of send modal data back to component
}
