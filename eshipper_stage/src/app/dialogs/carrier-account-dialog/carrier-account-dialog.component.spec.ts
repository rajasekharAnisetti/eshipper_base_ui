import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarrierAccountDialogComponent } from './carrier-account-dialog.component';

describe('CarrierAccountDialogComponent', () => {
  let component: CarrierAccountDialogComponent;
  let fixture: ComponentFixture<CarrierAccountDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarrierAccountDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarrierAccountDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
