import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { PaymentCard } from 'src/app/interfaces/payment-card';
import { Countries } from 'src/app/shared/const/countries';
import { Observable, Subscription } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { PayDialogComponent } from '../pay-dialog/pay-dialog.component';

@Component({
  selector: 'app-credit-card-dialog',
  templateUrl: './credit-card-dialog.component.html',
  styleUrls: ['./credit-card-dialog.component.scss'],
})
export class CreditCardDialogComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  modalData = <PaymentCard>{
    default: false,
    logo: '',
    number: '',
    expireMonth: '',
    expireYear: '',
    cvc: '',
    cardHolderName: '',
    address1: '',
    address2: '',
    city: '',
    stateOrProvince: '',
    zipOrPostal: '',
    country: '',
    privacy: false,
  };

  validation = {
    number: new FormControl(),
    cvc: new FormControl(),
    cardHolderName: new FormControl(),
    address1: new FormControl(),
    address2: new FormControl(),
    city: new FormControl(),
    stateOrProvince: new FormControl(),
    zipOrPostal: new FormControl(),
    country: new FormControl(),
  };
  filteredCityes: Observable<any[]>;
  staticData = {
    countries: Countries.COUNTRIES,
    states: Countries.USASTATES,
    cities: Countries.USACITIES,
    stateLabel: 'State',
    zipLabel: 'Zip Code',
  };

  cardDate = {
    month: '',
    year: '',
  };


  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<CreditCardDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data.modalData) {
      this.modalData = data.modalData;
      this.cardDate = {
        month: this.modalData.expireMonth,
        year: this.modalData.expireYear,
      };
    }
  }

  ngOnInit() {
    // for city
    this.filteredCityes = this.validation.city.valueChanges.pipe(
      startWith(''),
      map(city => city ? this._filter(city) : this.staticData.cities.slice())
    )
  }

  // get card date
  getCardDate(date) {
    this.modalData.expireMonth = date.month;
    this.modalData.expireYear = date.year;
  }
  // end of get card date

  // city autocomplete
  displayFn(city?: string): string | undefined {
    return city ? city : undefined;
  }

  private _filter(city: string) {
    const filterValue = city.toLowerCase();

    return this.staticData.cities.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
  }
  // emd of city autocomplete

  // change country
  changeCountry(country) {
    switch (country) {
      case 'Canada':
        {
          this.staticData.stateLabel = 'Province';
          this.staticData.zipLabel = 'Postal Code';
          this.staticData.states = Countries.CANADAPROVINCES;
          this.staticData.cities = Countries.CANADACITIES;
        }
        break;
      case 'USA':
        {
          this.staticData.stateLabel = 'State';
          this.staticData.zipLabel = 'Zip Code';
          this.staticData.states = Countries.USASTATES;
          this.staticData.cities = Countries.USACITIES;
        }
        break;
      default:
        break;
    }
  }
  // end of change country

  // close dialog
  closeDialog = () => this.dialogRef.close();
  // end of close dialog

  // send modal data back to component
  send() {
    if (this.data.dialogFlag) {
      this.dialogRef.close(this.modalData);
      const dialogRef = this.dialog.open(PayDialogComponent, {
        panelClass: 'normal-dialog-width',
        autoFocus: false,
        data: {
          title: 'Invoice #CWS123456789',
          newCardInfo: true
        },
      });
      this.subscribe.add(
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            console.log(result);
          }
        })
      );
    } else {
      this.dialogRef.close(this.modalData);
    }
  }
  // end of send modal data back to component

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
