import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-segment-dialog',
  templateUrl: './segment-dialog.component.html',
  styleUrls: ['./segment-dialog.component.scss'],
})
export class SegmentDialogComponent implements OnInit {
  carriers = ['Air Canada', 'Carrier Name', 'Carrier Name'];
  minDate = new Date();

  modalData = {
    type: 'Air',
    carrier: '',
    bol: '',
    paps: '',
    pars: '',
    pro: '',
    airwayBill: '',
    tracking: '',
    flights: '',
    service: '',
    carrierSpotRate: '',
    customerAccount: '',
    departure: {
      airport: '',
      date: '',
      time: '',
    },
    arrival: {
      airport: '',
      date: '',
      time: '',
    },
  };

  validation = {
    type: new FormControl(),
    carrier: new FormControl(),
    bol: new FormControl(),
    paps: new FormControl(),
    pars: new FormControl(),
    pro: new FormControl(),
    airwayBill: new FormControl(),
    tracking: new FormControl(),
    flights: new FormControl(),
    service: new FormControl(),
    carrierSpotRate: new FormControl(),
    customerAccount: new FormControl(),
    departure: {
      airport: new FormControl(),
      date: new FormControl(),
      time: new FormControl(),
    },
    arrival: {
      airport: new FormControl(),
      date: new FormControl(),
      time: new FormControl(),
    },
  };

  constructor(
    public dialogRef: MatDialogRef<SegmentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data.modalData) {
      this.modalData = data.modalData;
    }
  }

  ngOnInit() { }
  getArrivalTime = val => (this.modalData.arrival.time = val);
  getDepartureTime = val => (this.modalData.departure.time = val);

  closeDialog = () => this.dialogRef.close();

  save = () => this.dialogRef.close(this.modalData);
}
