import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { PaymentCard } from 'src/app/interfaces/payment-card';
import { ProfileService } from 'src/app/profile/profile.service';
import { Subscription } from 'rxjs';
import { CreditCardDialogComponent } from '../credit-card-dialog/credit-card-dialog.component';

@Component({
  selector: 'app-pay-dialog',
  templateUrl: './pay-dialog.component.html',
  styleUrls: ['./pay-dialog.component.scss'],
})
export class PayDialogComponent implements OnInit, OnDestroy {
  minDate = new Date();
  subscribe = new Subscription();
  cardList: Array<PaymentCard>;
  account: string;
  customer: boolean;
  cardFlag = false;
  modalData = {
    amount: null,
    totalAmount: 50500.5,
    payment: 500.0,
    balanceDue: 0,
    paymentMethod: '',
    checkNumber: '',
    lastPaymentDate: '',
    comment: '',
    notifyCustomer: true,
  };

  validation = {
    paymentMethod: new FormControl(),
    checkNumber: new FormControl(),
    lastPaymentDate: new FormControl(),
  };

  paymentMethods = ['Credit Card', 'Value 2', 'Value 3'];

  constructor(
    private getData: ProfileService,
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<PayDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  ngOnInit() {
    this.account = localStorage.getItem('account');
    this.customer = this.account === 'admin' ? false : true;
    this.modalData.balanceDue =
      this.modalData.totalAmount - this.modalData.payment;
    // get cards
    this.subscribe.add(
      this.getData.getPaymentCardsData().subscribe(data => {
        this.cardList = data.content.length ? data.content : [];
      })
    );
    this.modalData.paymentMethod = this.data.newCardInfo ? 'Credit Card' : ''
  }

  // change the card
  changeCard(item) {
    for (const i of this.cardList) {
      i.default = false;
    }
    item.default = true;
  }
  // end of change the card

  newPaymentMethod() {
    this.dialogRef.close();
    const dialogRef = this.dialog.open(CreditCardDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Add Credit Card',
        actionButton: 'Add',
        dialogFlag: true
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }

  // close dialog
  closeDialog = () => this.dialogRef.close();

  // send data
  pay = () => this.dialogRef.close(this.modalData);

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
