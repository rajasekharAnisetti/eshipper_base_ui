import { TestBed } from '@angular/core/testing';

import { RatesDialogsService } from './rates-dialogs.service';

describe('RatesDialogsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RatesDialogsService = TestBed.get(RatesDialogsService);
    expect(service).toBeTruthy();
  });
});
