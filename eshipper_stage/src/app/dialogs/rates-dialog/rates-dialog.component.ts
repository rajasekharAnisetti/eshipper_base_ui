import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { RatesDialogsService } from './rates-dialogs.service';
import { Rate } from 'src/app/interfaces/shipping';
import { sortAscDesc } from 'src/app/shared/functions/sort-asc-desc';

@Component({
  selector: 'app-rates-dialog',
  templateUrl: './rates-dialog.component.html',
  styleUrls: ['./rates-dialog.component.scss'],
})
export class RatesDialogComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  sortMenu = ['Carrier', 'Est. Days', 'Price'];
  fullscreen = false;
  rates: Array<Rate>;

  constructor(
    public dialogRef: MatDialogRef<RatesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private getData: RatesDialogsService
  ) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData.getRates().subscribe(data => (this.rates = data))
    );
  }

  // sort data
  getSortData(val) {
    let sortKey = '';
    switch (val.name) {
      case 'Carrier':
        sortKey = 'title'
        break;
      case 'Est. Days':
        sortKey = 'date'
        break;
      case 'Price':
        sortKey = 'value'
        break;
      default:
        break;
    }
    sortAscDesc(this.rates, val.direction, sortKey)
  }
  // end of sort data

  closeDialog = () => this.dialogRef.close();

  selectRate = item => this.dialogRef.close(item);

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
