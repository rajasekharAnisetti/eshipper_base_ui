import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class RatesDialogsService {
  constructor(private http: HttpClient) {}

  getRates(): Observable<any> {
    return this.http
      .get('content/data/rates.json')
      .pipe(map(data => data));
  }
}
