import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-print-dialog',
  templateUrl: './print-dialog.component.html',
  styleUrls: ['./print-dialog.component.scss'],
})
export class PrintDialogComponent implements OnInit {
  printInfo = {
    shippingLabel: false,
    customsInvoice: false,
    orderDetails: false,
    packingList: false,
  };

  constructor(
    public dialogRef: MatDialogRef<PrintDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  closeDialog = () => this.dialogRef.close();

  print() {
    this.dialogRef.close(this.printInfo);
    console.log(this.printInfo);
  }

  ngOnInit() {}
}
