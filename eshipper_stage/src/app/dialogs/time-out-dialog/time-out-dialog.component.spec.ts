import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeOutDialogComponent } from './time-out-dialog.component';

describe('TimeOutDialogComponent', () => {
  let component: TimeOutDialogComponent;
  let fixture: ComponentFixture<TimeOutDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeOutDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeOutDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
