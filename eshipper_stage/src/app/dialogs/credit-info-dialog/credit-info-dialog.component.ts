import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-credit-info-dialog',
  templateUrl: './credit-info-dialog.component.html',
  styleUrls: ['./credit-info-dialog.component.scss'],
})
export class CreditInfoDialogComponent implements OnInit {
  total = 0;
  constructor(
    public dialogRef: MatDialogRef<CreditInfoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    for (const item of data.moreData) {
      this.total = this.total + item.amount;
    }
  }

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();
}
