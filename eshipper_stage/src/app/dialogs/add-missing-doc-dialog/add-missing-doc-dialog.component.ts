import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-add-missing-doc-dialog',
  templateUrl: './add-missing-doc-dialog.component.html',
  styleUrls: ['./add-missing-doc-dialog.component.scss'],
})
export class AddMissingDocDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<AddMissingDocDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  documentTypes = ['Custom', 'Doc', 'Pdf', 'Xls'];

  modalData = {
    documentType: '',
    documentName: '',
    notifyClient: false,
  };
  validation = {
    documentType: new FormControl(),
    documentName: new FormControl(),
    notifyClient: new FormControl(),
  };

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  add = () => this.dialogRef.close(this.modalData);
}
