import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMissingDocDialogComponent } from './add-missing-doc-dialog.component';

describe('AddMissingDocDialogComponent', () => {
  let component: AddMissingDocDialogComponent;
  let fixture: ComponentFixture<AddMissingDocDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMissingDocDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMissingDocDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
