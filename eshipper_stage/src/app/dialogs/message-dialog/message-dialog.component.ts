import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { Message } from 'src/app/interfaces/wo';

@Component({
  selector: 'app-message-dialog',
  templateUrl: './message-dialog.component.html',
  styleUrls: ['./message-dialog.component.scss'],
})
export class MessageDialogComponent implements OnInit {
  modalData = <Message>{};

  validation = {
    type: new FormControl(),
    message: new FormControl(),
  };

  types = ['Pick Up', 'In Transit', 'Undelivered', 'Expired', 'Not Found'];

  constructor(
    public dialogRef: MatDialogRef<MessageDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data.modalData) {
      this.modalData = data.modalData;
    }
  }

  ngOnInit() {}

  closeDialog() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.modalData);
  }
}
