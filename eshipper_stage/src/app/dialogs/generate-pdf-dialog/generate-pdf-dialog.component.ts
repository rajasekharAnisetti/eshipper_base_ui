import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-generate-pdf-dialog',
  templateUrl: './generate-pdf-dialog.component.html',
  styleUrls: ['./generate-pdf-dialog.component.scss'],
})
export class GeneratePdfDialogComponent implements OnInit {
  modalData = {
    fileName: 'Flight Segment.pdf',
    emails: [],
  };
  constructor(
    public dialogRef: MatDialogRef<GeneratePdfDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  send = () => this.dialogRef.close(this.modalData);
}
