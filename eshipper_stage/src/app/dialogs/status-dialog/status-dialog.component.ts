import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Countries } from 'src/app/shared/const/countries';
import { STATUSES } from 'src/app/shared/const/dropdowns';

@Component({
  selector: 'app-status-dialog',
  templateUrl: './status-dialog.component.html',
  styleUrls: ['./status-dialog.component.scss'],
})
export class StatusDialogComponent implements OnInit {
  minDate = new Date();
  filteredCityes: Observable<any[]>;
  statuses = STATUSES;
  countries = Countries.COUNTRIES;
  cities = Countries.CANADACITIES;
  messageTypes = ['Option 1', 'Option 2', 'Option 3'];
  messages = ['Message 1', 'Message 2', 'Message 3'];
  modalData = {
    statusAlert: '',
    country: '',
    city: '',
    messageType: '',
    message: '',
    note: '',
  };
  validation = {
    statusAlert: new FormControl(),
    country: new FormControl(),
    city: new FormControl(),
    messageType: new FormControl(),
    message: new FormControl(),
    note: new FormControl(),
  };

  constructor(
    public dialogRef: MatDialogRef<StatusDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    // for city
    this.filteredCityes = this.validation.city.valueChanges.pipe(
      startWith(''),
      map(city => city ? this._filter(city) : this.cities.slice())
    )
  }

  // city autocomplete
  displayFn(city?: string): string | undefined {
    return city ? city : undefined;
  }

  private _filter(city: string) {
    const filterValue = city.toLowerCase();

    return this.cities.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
  }
  // emd of city autocomplete

  // change country
  changeCountry(country) {
    this.modalData.city = '';
    switch (country) {
      case 'Canada':
        {
          this.cities = Countries.CANADACITIES;
        }
        break;
      case 'USA':
        {
          this.cities = Countries.USACITIES;
        }
        break;
      default:
        break;
    }
  }
  // end of change country

  getPickDate() { }

  closeDialog() {
    this.dialogRef.close();
  }

  update() {
    this.dialogRef.close(this.modalData);
  }
}
