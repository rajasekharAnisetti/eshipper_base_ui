import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ServiceDialogService {
  constructor(private http: HttpClient) {}
  getWoLibraryServicesAutocomplete(): Observable<any> {
    return this.http
      .get('../../content/data/wo-library-services-autocomplete.json')
      .pipe(map(data => data));
  }
}
