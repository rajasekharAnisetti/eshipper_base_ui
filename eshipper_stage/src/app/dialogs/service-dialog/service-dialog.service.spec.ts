import { TestBed } from '@angular/core/testing';

import { ServiceDialogService } from './service-dialog.service';

describe('ServiceDialogService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceDialogService = TestBed.get(ServiceDialogService);
    expect(service).toBeTruthy();
  });
});
