import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { ServiceDialogService } from './service-dialog.service';
import { Service } from 'src/app/interfaces/shared';
@Component({
  selector: 'app-service-dialog',
  templateUrl: './service-dialog.component.html',
  styleUrls: ['./service-dialog.component.scss'],
})
export class ServiceDialogComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  filteredServiceName: Observable<Service[]>;
  services = [];

  modalData = {
    serviceName: '',
    subServiceName: '',
    mapping: '',
    chargeType: '',
    cost: '',
    charge: '',
    note: '',
  };
  mappings = [
    {
      value: 'Option 1',
      viewValue: 'Option 1',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];
  chargeTypes = [
    {
      value: 'Option 1',
      viewValue: 'Option 1',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];

  // form validation
  requiredText = 'Field is required';
  validation = {
    serviceName: new FormControl('', [Validators.required]),
    subServiceName: new FormControl('', [Validators.required]),
    mapping: new FormControl('', [Validators.required]),
    chargeType: new FormControl('', [Validators.required]),
    cost: new FormControl('', [Validators.required]),
    charge: new FormControl('', [Validators.required]),
    note: new FormControl(),
  };
  // end of form validation

  constructor(
    public dialogRef: MatDialogRef<ServiceDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private getData: ServiceDialogService
  ) {}

  private _servicesFilterStates(value: string): Service[] {
    const filterValue = value.toLowerCase();
    return this.services.filter(
      service => service.name.toLowerCase().indexOf(filterValue) === 0
    );
  }

  initAutocomplete() {
    this.filteredServiceName = this.validation.serviceName.valueChanges.pipe(
      startWith(''),
      map(service =>
        service ? this._servicesFilterStates(service) : this.services.slice()
      )
    );
  }

  ngOnInit() {
    // get services autocomplete data
    this.subscribe.add(
      this.getData.getWoLibraryServicesAutocomplete().subscribe(data => {
        this.services = data;
        // init autocomplete
        this.initAutocomplete();
      })
    );
  }

  closeDialog() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.modalData);
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
