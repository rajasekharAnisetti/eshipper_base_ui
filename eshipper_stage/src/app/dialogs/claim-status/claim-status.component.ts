import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-claim-status',
  templateUrl: './claim-status.component.html',
  styleUrls: ['./claim-status.component.scss'],
})
export class ClaimStatusComponent implements OnInit {
  minDate = new Date();
  modalData = {
    solution: '',
    statusAlert: '',
    comment: '',
  };
  solutions = ['Option 1', 'Option 2', 'Option 2'];
  // form validation
  validation = {
    solution: new FormControl(),
    statusAlert: new FormControl(),
    comment: new FormControl(),
  };
  // end of form validation

  constructor(
    public dialogRef: MatDialogRef<ClaimStatusComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() { }

  getPickDate() { }

  closeDialog() {
    this.dialogRef.close(false);
  }

  update() {
    this.dialogRef.close(this.modalData);
  }
}
