import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulePickupDialogComponent } from './schedule-pickup-dialog.component';

describe('SchedulePickupDialogComponent', () => {
  let component: SchedulePickupDialogComponent;
  let fixture: ComponentFixture<SchedulePickupDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedulePickupDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulePickupDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
