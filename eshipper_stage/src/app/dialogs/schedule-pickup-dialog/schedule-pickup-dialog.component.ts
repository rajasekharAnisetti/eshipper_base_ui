import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { ValidationClassStateMatcher } from 'src/app/shared/classes/validation-class';

@Component({
  selector: 'app-schedule-pickup-dialog',
  templateUrl: './schedule-pickup-dialog.component.html',
  styleUrls: ['./schedule-pickup-dialog.component.scss'],
})
export class SchedulePickupDialogComponent implements OnInit {
  pickupDateControl = new FormControl();
  minDate = new Date();
  pickupDateValue: any;
  modalData = {
    note: '',
    pickupLocation: '',
    pickupDate: '',
    pickupTime: '',
    closingTime: '',
  };
  pickupLocations = ['Option 1', 'Option 2', 'Option 3'];

  messageCheckboxText = 'Don`t show me again';
  message =
    'Please allow a minimum of 3 hour between pick up & closing times. Otherwise scheduling may fail. \n' +
    'Please Schedule a single pick up request with the carrier per day. The driver will pick up all the shipments under the same pick up request.';

  // form validation
  requiredText = 'Field is required';

  noteFormControl = new FormControl('', []);

  pickupLocationFormControl = new FormControl('', []);

  matcher = new ValidationClassStateMatcher();
  // end of form validation

  constructor(
    public dialogRef: MatDialogRef<SchedulePickupDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() { }

  getPickDate = () => (this.modalData.pickupDate = this.pickupDateValue);

  getPickTime = val => (this.modalData.pickupTime = val);
  getClosingTime = val => (this.modalData.closingTime = val);

  closeDialog = () => this.dialogRef.close();

  confirm = () => this.dialogRef.close(this.modalData);
}
