import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { currency } from '../../shared/functions/currency';

@Component({
  selector: 'app-info-dialog',
  templateUrl: './info-dialog.component.html',
  styleUrls: ['./info-dialog.component.scss'],
})
export class InfoDialogComponent implements OnInit {
  total = 0;
  currencyValue = false;
  weightValue = false;

  constructor(
    public dialogRef: MatDialogRef<InfoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    for (const item of data.moreData) {
      if (this.isNumber(item.value)) {
        this.currencyValue =
          data.infoFlag && data.infoFlag === 'weight' ? false : true;
        this.weightValue =
          data.infoFlag && data.infoFlag === 'weight' ? true : false;
        this.total = this.total + item.value;
      } else {
        return;
      }
    }
  }

  ngOnInit() {}

  isNumber = val => typeof val === 'number';

  convertValue(val) {
    if (this.isNumber(val) && !this.weightValue) {
      return currency(val);
    } else {
      return val;
    }
  }

  closeDialog = () => this.dialogRef.close();
}
