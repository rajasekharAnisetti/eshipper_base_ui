import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-period-dialog',
  templateUrl: './period-dialog.component.html',
  styleUrls: ['./period-dialog.component.scss'],
})
export class PeriodDialogComponent implements OnInit {
  inlineRange;

  constructor(
    public dialogRef: MatDialogRef<PeriodDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() { }

  inlineRangeChange($event) {
    this.inlineRange = $event;
  }

  savePeriod() {
    const format = 'MM/dd/yyyy';
    const locale = 'en-US';
    const start = formatDate(this.inlineRange.begin, format, locale);
    const end = formatDate(this.inlineRange.end, format, locale);
    this.dialogRef.close(start + ' - ' + end);
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
