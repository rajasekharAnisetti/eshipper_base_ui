import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.scss'],
})
export class UserDialogComponent implements OnInit {
  modalData = {
    userID: '',
    userName: '',
    userAvatar: '',
    userRole: '',
    userEmail: '',
    userPhone: '',
    userMarkup: '',
  };

  roles = ['Customer Admin', 'User Role'];

  validation = {
    userName: new FormControl(),
    userRole: new FormControl(),
    userEmail: new FormControl('', [Validators.required, Validators.email]),
    userPhone: new FormControl(),
    userMarkup: new FormControl(),
  };

  constructor(
    public dialogRef: MatDialogRef<UserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data.uresData) {
      this.modalData = data.uresData;
    }
  }

  ngOnInit() {}

  // close modal
  closeDialog = () => this.dialogRef.close();

  // action modal
  save = () => this.dialogRef.close(this.modalData);
}
