import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { NgxMaskModule } from 'ngx-mask';

import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialModules } from '../material-modules';

import { ExportDialogComponent } from './export-dialog/export-dialog.component';
import { GeneratePdfDialogComponent } from './generate-pdf-dialog/generate-pdf-dialog.component';
import { InfoDialogComponent } from './info-dialog/info-dialog.component';
import { CreditInfoDialogComponent } from './credit-info-dialog/credit-info-dialog.component';
import { WarningDialogComponent } from './warning-dialog/warning-dialog.component';
import { VideoDialogComponent } from './video-dialog/video-dialog.component';
import { PrintDialogComponent } from './print-dialog/print-dialog.component';
import { TextDialogComponent } from './text-dialog/text-dialog.component';
import { PeriodDialogComponent } from './period-dialog/period-dialog.component';
import { RenameDialogComponent } from './rename-dialog/rename-dialog.component';
import { LinkDialogComponent } from './link-dialog/link-dialog.component';
import { EmailDialogComponent } from './email-dialog/email-dialog.component';
import { MessageDialogComponent } from './message-dialog/message-dialog.component';
import { RejectDialogComponent } from './reject-dialog/reject-dialog.component';
import { ApprovedDialogComponent } from './approved-dialog/approved-dialog.component';
import { SetingDialogComponent } from './seting-dialog/seting-dialog.component';
import { UserDialogComponent } from './user-dialog/user-dialog.component';
import { SegmentDialogComponent } from './segment-dialog/segment-dialog.component';
import { ServiceDialogComponent } from './service-dialog/service-dialog.component';
import { AddressBookDialogComponent } from './address-book-dialog/address-book-dialog.component';
import { InvoiceHistoryDialogComponent } from './invoice-history-dialog/invoice-history-dialog.component';
import { AgentDialogComponent } from './agent-dialog/agent-dialog.component';
import { CarrierAccountDialogComponent } from './carrier-account-dialog/carrier-account-dialog.component';
import { EditContactDialogComponent } from './edit-contact-dialog/edit-contact-dialog.component';
import { PayDialogComponent } from './pay-dialog/pay-dialog.component';
import { RatesDialogComponent } from './rates-dialog/rates-dialog.component';
import { StatusDialogComponent } from './status-dialog/status-dialog.component';
import { SchedulePickupDialogComponent } from './schedule-pickup-dialog/schedule-pickup-dialog.component';
import { CarrierDialogComponent } from './carrier-dialog/carrier-dialog.component';
import { ClaimStatusComponent } from './claim-status/claim-status.component';
import { AddMissingDocDialogComponent } from './add-missing-doc-dialog/add-missing-doc-dialog.component';
import { AddCommentComponent } from './add-comment/add-comment.component';
import { WidgetSettingsDialogComponent } from './widget-settings-dialog/widget-settings-dialog.component';
import { CreditCardDialogComponent } from './credit-card-dialog/credit-card-dialog.component';
import { ErrorDialogComponent } from './error-dialog/error-dialog.component';
import { TimeOutDialogComponent } from './time-out-dialog/time-out-dialog.component';

@NgModule({
  declarations: [
    ExportDialogComponent,
    GeneratePdfDialogComponent,
    InfoDialogComponent,
    CreditInfoDialogComponent,
    WarningDialogComponent,
    VideoDialogComponent,
    PrintDialogComponent,
    TextDialogComponent,
    PeriodDialogComponent,
    RenameDialogComponent,
    LinkDialogComponent,
    EmailDialogComponent,
    MessageDialogComponent,
    RejectDialogComponent,
    ApprovedDialogComponent,
    SetingDialogComponent,
    UserDialogComponent,
    SegmentDialogComponent,
    ServiceDialogComponent,
    AddressBookDialogComponent,
    InvoiceHistoryDialogComponent,
    AgentDialogComponent,
    CarrierAccountDialogComponent,
    EditContactDialogComponent,
    PayDialogComponent,
    RatesDialogComponent,
    StatusDialogComponent,
    SchedulePickupDialogComponent,
    CarrierDialogComponent,
    ClaimStatusComponent,
    AddMissingDocDialogComponent,
    AddCommentComponent,
    WidgetSettingsDialogComponent,
    CreditCardDialogComponent,
    ErrorDialogComponent,
    TimeOutDialogComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    NgxMaskModule,
    MaterialModules,
    SharedModule,
  ],
  entryComponents: [
    ExportDialogComponent,
    GeneratePdfDialogComponent,
    InfoDialogComponent,
    CreditInfoDialogComponent,
    WarningDialogComponent,
    VideoDialogComponent,
    PrintDialogComponent,
    TextDialogComponent,
    PeriodDialogComponent,
    RenameDialogComponent,
    LinkDialogComponent,
    EmailDialogComponent,
    MessageDialogComponent,
    RejectDialogComponent,
    ApprovedDialogComponent,
    SetingDialogComponent,
    UserDialogComponent,
    SegmentDialogComponent,
    ServiceDialogComponent,
    AddressBookDialogComponent,
    InvoiceHistoryDialogComponent,
    AgentDialogComponent,
    CarrierAccountDialogComponent,
    EditContactDialogComponent,
    PayDialogComponent,
    RatesDialogComponent,
    StatusDialogComponent,
    SchedulePickupDialogComponent,
    CarrierDialogComponent,
    ClaimStatusComponent,
    AddMissingDocDialogComponent,
    AddCommentComponent,
    WidgetSettingsDialogComponent,
    CreditCardDialogComponent,
    ErrorDialogComponent,
    TimeOutDialogComponent,
  ],
})
export class DialogsModule { }
