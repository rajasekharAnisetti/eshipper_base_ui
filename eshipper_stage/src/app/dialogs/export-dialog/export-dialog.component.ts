import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-export-dialog',
  templateUrl: './export-dialog.component.html',
  styleUrls: ['./export-dialog.component.scss'],
})
export class ExportDialogComponent implements OnInit {
  filesArray = [
    {
      type: 'csv',
      preview: 'content/images/files/csv.svg',
      active: false,
    },
    {
      type: 'pdf',
      preview: 'content/images/files/pdf.svg',
      active: false,
    },
    {
      type: 'xls',
      preview: 'content/images/files/xls.svg',
      active: false,
    },
  ];

  fileType: string;

  constructor(
    public dialogRef: MatDialogRef<ExportDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  selectType(type) {
    this.fileType = type;
    for (const i of this.filesArray) {
      i.active = false;
    }
  }

  closeDialog = () => this.dialogRef.close();

  export = () => {
    this.dialogRef.close(this.fileType);
  };

  ngOnInit() {}
}
