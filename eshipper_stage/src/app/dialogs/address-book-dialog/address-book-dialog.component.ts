import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { ValidationClassStateMatcher } from 'src/app/shared/classes/validation-class';
import { Countries } from 'src/app/shared/const/countries';

@Component({
  selector: 'app-address-book-dialog',
  templateUrl: './address-book-dialog.component.html',
  styleUrls: ['./address-book-dialog.component.scss'],
})
export class AddressBookDialogComponent implements OnInit {
  filteredCityes: Observable<any[]>;
  staticData = {
    countries: Countries.COUNTRIES,
    provinces: Countries.USASTATES,
    cities: Countries.USACITIES,
    stateLabel: 'State',
    zipLabel: 'Zip Code',
  };
  modalData = {
    companyName: '',
    address1: '',
    address2: '',
    city: '',
    zip: '',
    country: '',
    province: '',
    attention: '',
    phone: '',
    email: '',
    taxId: '',
  };
  saveToAddressBook = false;
  // form validation
  requiredText = 'Field is required';
  matcher = new ValidationClassStateMatcher();
  validation = {
    companyName: new FormControl('', [Validators.required]),
    address: new FormControl('', [Validators.required]),
    city: new FormControl(),
    zip: new FormControl('', [Validators.required]),
    country: new FormControl('', [Validators.required]),
    province: new FormControl('', [Validators.required]),
    attention: new FormControl('', [Validators.required]),
    phone: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email])
  };
  // end of form validation

  constructor(
    public dialogRef: MatDialogRef<AddressBookDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data.fields) {
      this.modalData = data.fields;
    }
  }

  closeDialog = () => this.dialogRef.close();
  apply() {
    const error =
      this.validation.companyName.hasError('required') &&
      this.validation.address.hasError('required') &&
      this.validation.city.hasError('required') &&
      this.validation.zip.hasError('required') &&
      this.validation.country.hasError('required') &&
      this.validation.province.hasError('required') &&
      this.validation.attention.hasError('required') &&
      this.validation.phone.hasError('required') &&
      this.validation.email.hasError('required')
    if (!error) {
      this.dialogRef.close(this.modalData);
    }
  }

  // change country
  changeCountry(country) {
    switch (country) {
      case 'Canada':
        {
          this.staticData.stateLabel = 'Province';
          this.staticData.zipLabel = 'Postal Code';
          this.staticData.provinces = Countries.CANADAPROVINCES;
          this.staticData.cities = Countries.CANADACITIES;
          this.modalData.city = '';
        }
        break;
      case 'USA':
        {
          this.staticData.stateLabel = 'State';
          this.staticData.zipLabel = 'Zip Code';
          this.staticData.provinces = Countries.USASTATES;
          this.staticData.cities = Countries.USACITIES;
          this.modalData.city = '';
        }
        break;
      default:
        break;
    }
  }

  // end of change city

  ngOnInit() {
    this.filteredCityes = this.validation.city.valueChanges.pipe(
      startWith(''),
      map(city => city ? this._filter(city) : this.staticData.cities.slice())
    )
  }

  // city autocomplete
  displayFn(city?: string): string | undefined {
    return city ? city : undefined;
  }

  private _filter(city: string) {
    const filterValue = city.toLowerCase();

    return this.staticData.cities.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
  }
  // emd of city autocomplete


}
