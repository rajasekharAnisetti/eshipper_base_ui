import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarrierDialogComponent } from './carrier-dialog.component';

describe('CarrierDialogComponent', () => {
  let component: CarrierDialogComponent;
  let fixture: ComponentFixture<CarrierDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarrierDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarrierDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
