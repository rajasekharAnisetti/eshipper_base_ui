import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { ValidationClassStateMatcher } from 'src/app/shared/classes/validation-class';
import { Carrier } from 'src/app/interfaces/carrier';

@Component({
  selector: 'app-carrier-dialog',
  templateUrl: './carrier-dialog.component.html',
  styleUrls: ['./carrier-dialog.component.scss'],
})
export class CarrierDialogComponent implements OnInit {
  segmentTypes = ['Flight', 'Option 2'];
  modalData = <Carrier>{};
  matcher = new ValidationClassStateMatcher();
  validation = {
    name: new FormControl('', [Validators.required]),
    type: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    phone: new FormControl('', [Validators.required]),
    fax: new FormControl('', [Validators.required]),
  };
  constructor(
    public dialogRef: MatDialogRef<CarrierDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data.carrier) {
      this.modalData = data.carrier;
    }
  }

  ngOnInit() {}

  closeDialog() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.modalData);
  }
}
