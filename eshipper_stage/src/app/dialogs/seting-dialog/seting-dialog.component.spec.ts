import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetingDialogComponent } from './seting-dialog.component';

describe('SetingDialogComponent', () => {
  let component: SetingDialogComponent;
  let fixture: ComponentFixture<SetingDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SetingDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
