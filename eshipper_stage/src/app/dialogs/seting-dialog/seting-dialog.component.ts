import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Area } from 'src/app/interfaces/wo';
import { COLORS } from 'src/app/shared/const/colors';

@Component({
  selector: 'app-seting-dialog',
  templateUrl: './seting-dialog.component.html',
  styleUrls: ['./seting-dialog.component.scss'],
})
export class SetingDialogComponent implements OnInit {
  modalData: Array<Area> = [];
  colors = COLORS;

  constructor(
    public dialogRef: MatDialogRef<SetingDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.modalData = [...this.data.dataList];
    if (this.data.new) {
      this.addNewArea();
    }
  }

  deleteCategory(i) {
    this.modalData.splice(i, 1);
  }

  addNewArea() {
    this.modalData.push({
      color: 'red',
      name: '',
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.modalData);
  }
}
