import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { RolePermissions } from 'src/app/interfaces/roles';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss']
})
export class RoleComponent implements OnInit {
  rolePermissions: Array<RolePermissions>;
  roleAdminFlag = false;
  roleFormControl = new FormControl();
  sectionFormControl = new FormControl();
  sectionName = '';
  roleName = '';

  constructor() { }

  ngOnInit() {
    const data = localStorage.getItem('roleData');
    const adminFlag = localStorage.getItem('roleAdminFlag');
    if (adminFlag) {
      this.roleAdminFlag = adminFlag === 'admin' ? true : false;
    }
    if (data) {
      const role = JSON.parse(data);
      this.rolePermissions = role.permissions;
      this.roleName = role.roleName;
      this.sectionName = role.sectionName ? role.sectionName : ''
    }
  }

  // get Indeterminate
  getIndeterminate = item =>
    (item.subPermissions.permissionA &&
      item.subPermissions.permissionB &&
      item.subPermissions.permissionC &&
      item.subPermissions.permissionD) ||
      !(
        item.subPermissions.permissionA &&
        item.subPermissions.permissionB &&
        item.subPermissions.permissionC &&
        item.subPermissions.permissionD
      )
      ? false
      : true;
  // end of get Indeterminate

  // change main option
  changeMainOption(item) {
    item.subPermissions.permissionA = item.subPermissions.permissionB = item.subPermissions.permissionC = item.subPermissions.permissionD = item.sectionPermissions
      ? true
      : false;
  }

  // change inner option
  changeInnerOption(item) {
    item.sectionPermissions =
      item.subPermissions.permissionA &&
        item.subPermissions.permissionB &&
        item.subPermissions.permissionC &&
        item.subPermissions.permissionD
        ? true
        : false;

    item.toolbarIndeterminate =
      !item.sectionPermissions &&
        (item.subPermissions.permissionA ||
          item.subPermissions.permissionB ||
          item.subPermissions.permissionC ||
          item.subPermissions.permissionD)
        ? true
        : false;
  }

  // save function
  save() { }
  // end of save function

}
