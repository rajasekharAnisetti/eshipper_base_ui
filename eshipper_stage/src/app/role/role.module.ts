import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { RoleComponent } from './role.component';
import { RoleRoutingModule } from './role-routing.module';

@NgModule({
  declarations: [RoleComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    MaterialModules,
    SharedModule,
    RoleRoutingModule
  ],
  exports: [RoleComponent]
})
export class RoleModule { }
