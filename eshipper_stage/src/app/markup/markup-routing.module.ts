import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MarkupComponent } from './markup.component';

const routes: Routes = [
  {
    path: '',
    component: MarkupComponent,
    data: { title: 'Priority Mail Legal Flat Rate Envelope', backBtn: true },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MarkupRoutingModule {}
