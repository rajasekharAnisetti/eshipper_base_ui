import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { MarkupRoutingModule } from './markup-routing.module';

import { MarkupComponent } from './markup.component';

@NgModule({
  declarations: [MarkupComponent],
  imports: [
    CommonModule,
    MarkupRoutingModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule,
    MaterialModules,
    SharedModule,
  ],
  exports: [MarkupComponent],
})
export class MarkupModule {}
