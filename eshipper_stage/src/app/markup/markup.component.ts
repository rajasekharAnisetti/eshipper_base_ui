import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Markup } from 'src/app/interfaces/markup';

@Component({
  selector: 'app-markup',
  templateUrl: './markup.component.html',
  styleUrls: ['./markup.component.scss'],
})
export class MarkupComponent implements OnInit {
  markup = <Markup>{};
  validation = {
    direction: new FormControl(),
    markupMin: new FormControl(),
    markupDom: new FormControl(),
    markupIntl: new FormControl(),
    flatRate: new FormControl(),
    commCourierOpExp: new FormControl(),
  };
  contentToolbarUsaLocation = true;
  contentToolbarCaLocation = true;
  positionsList = ['Primary', 'Quaternary', 'Secondary', 'Tertiary'];
  positions = {
    lane: 'Primary',
    country: 'Quaternary',
    zip: 'Secondary',
    weight: 'Tertiary',
  };

  constructor() {
    this.markup.lines = [
      {
        from: '',
        to: '',
        markup: '',
      },
    ];
    this.markup.countries = [
      {
        location: '',
        markup: '',
      },
    ];
    this.markup.codeRange = [
      {
        from: '',
        to: '',
        markup: '',
      },
    ];
  }

  ngOnInit() { }

  // change position
  changePosition(val, item) {
    item = val;
  }
  // end of change position

  // dynamic row
  addLinesRow() {
    this.markup.lines.push({
      from: '',
      to: '',
      markup: '',
    });
  }
  addCountriesRow() {
    this.markup.countries.push({
      location: '',
      markup: '',
    });
  }
  addZipRow() {
    this.markup.codeRange.push({
      from: '',
      to: '',
      markup: '',
    });
  }
  removeRow(index, array) {
    array.splice(index, 1);
  }
  // end of dynamic row

  // content toolbar settings
  contentToolbarSettings(val) {
    console.log(val);
  }
  // end of content toolbar settings
}
