import { TestBed } from '@angular/core/testing';

import { MarkupService } from './markup.service';

describe('MarkupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MarkupService = TestBed.get(MarkupService);
    expect(service).toBeTruthy();
  });
});
