import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { DashboardService } from './dashboard.service';
import { ExportDialogComponent } from '../dialogs/export-dialog/export-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { WidgetSettingsDialogComponent } from '../dialogs/widget-settings-dialog/widget-settings-dialog.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  smallWidgetItems: Array<any>;
  toolbarSettings = ['Export', 'Settings'];
  widgetsSettingsOptions = [
    {
      value: 'Widget Name',
      check: false,
    },
    {
      value: 'Widget Name',
      check: false,
    },
    {
      value: 'Widget Name',
      check: false,
    },
    {
      value: 'Widget Name',
      check: false,
    },
    {
      value: 'Widget Name',
      check: false,
    },
    {
      value: 'Widget Name',
      check: false,
    },
  ];

  constructor(private getData: DashboardService, public dialog: MatDialog) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData.getDashboardSmallWidgets().subscribe(data => {
        this.smallWidgetItems = data;
      })
    );
  }

  // get period
  getPeriod(period) {
    console.log(period);
  }

  // toolbar settings function
  toolbarSettingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Settings':
        const settingsDialogRef = this.dialog.open(WidgetSettingsDialogComponent, {
          panelClass: 'small-dialog',
          autoFocus: false,
          data: {
            widgets: this.widgetsSettingsOptions
          }
        });
        this.subscribe.add(
          settingsDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }
  // end of toolbar settings function

  // widgets settings
  widgetsSettings(data) {
    console.log(data);
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
