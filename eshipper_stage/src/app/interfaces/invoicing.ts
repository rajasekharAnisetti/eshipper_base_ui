export interface Category {
  name: string;
  color: string;
  edit: boolean;
}

export interface CategoryItem {
  payeeType: string;
  categories: [
    {
      name: string;
    }
  ];
}

export interface Cheque {
  id: string;
  paidDate: string;
  paidAmount: number;
  currency: string;
  comment: string;
}

export interface Details {
  id: string;
  payee: string;
  payeeType: string;
  payeeCategory: string;
  dateCreated: string;
  payment: string;
  paymentId: string;
  lastPaymentDate: string;
  paymentAmount: number;
  amountPaid: number;
  creditNote: number;
  totalAmount: number;
  currency: string;
  balanceDue: number;
  status: {
    name: string;
    color: string;
  };
  dateSubmitted: string;
  dueDate: string;
  creditFlag: boolean;
  creditLabelAmount: number;
  creditNoteFlag: boolean;
  disputeFlag: boolean;
  subTotal: number;
  invoices: [
    {
      id: string;
      invoicedDate: string;
      comment: string;
      total: number;
    }
  ];
  taxes: [
    {
      tax: string;
      taxValue: number;
    }
  ];
  creditNotesHistory: [
    {
      id: string;
      invoiceId: string;
      currency: string;
      amount: number;
      newAmount: number;
    }
  ];
}

export interface Payable {
  selected: boolean;
  id: string;
  payee: string;
  payeeCategory: string;
  dateCreated: string;
  payment: string;
  totalAmount: number;
  currency: string;
  totalDetails: [
    {
      label: string;
      value: number;
    }
  ];
  balanceDue: number;
  dateSubmitted: string;
  paymentStatus: string;
  lastPaymentDate: string;
}

export interface Payee {
  payeeId: string;
  payeeType: string;
  companyName: string;
  payeePayment: string;
  payeePhone: string;
  payeeEmail: string;
}

export interface InvoiceActyvity {
  woInvoiceId: string;
  dateCreated: string;
  activityType: string;
  comment: string;
  activityData: {
    checkNo: string;
    currency: string;
    amount: number;
  };
}

export interface COD {
  woId: string;
  currency: string;
  profit: number;
  totalAmount: number;
  podSignedBy: string;
  podReportTo: string;
  podDate: string;
  podTime: string;
  podInternalComments: string;
  chargesOrginalCost: number;
  chargesOrginalCharge: number;
  chargesFuelCost: number;
  chargesFuelCharge: number;
  additionalServices: [
    {
      delivery: string;
      deliveryDocs: string;
      cost: number;
      charge: number;
      noOfShipments: string;
      totalPrice: number;
    }
  ];
  addInSubtotalFlag: boolean;
  subTotal: number;
  taxes: [
    {
      tax: string;
      taxValue: number;
    }
  ];
  details: {
    service: string;
    quote: number;
    fromComapnyName: string;
    fromAddress: string;
    fromDate: string;
    toComapnyName: string;
    toAddress: string;
    toDate: string;
    package: string;
    quantity: string;
    dimensionWeight: string;
    dimensionWeightDetais: Array<any>;
    specialInstructions: string;
  };
  documents: [
    {
      link: string;
      documentName: string;
      permission: string;
    }
  ];
}

export interface DraftInvoice {
  id: string;
  companyName: string;
  invoicePrefix: string;
  dateCreated: string;
  noOfShipments: number;
  totalAmount: number;
  currency: string;
  profit: number;
  dueDate: string;
  balanceDue: number;
  comment: string;
  commentPublicFlag: boolean;
  depositAmount: number;
  discountFlag: boolean;
  depositFlag: boolean;
  depositLabel: number;
  creditFlag: boolean;
  creditLabel: number;
  discount: string;
  discountValue: number;
  discountAmount: number;

  // unexpected fields from JSON example
  dateSubmitted: string;
  workOrdertype: string;
  lastPaymentDate: string;
  paymentStatus: string;
  depositCurrency: string;
  availableCreditNotes: string;
  creditNotesCurrency: string;
  // end of unexpected fields from JSON example
  workOrders: [
    {
      id: string;
      link: string;
      oriCost: number;
      oriCharge: number;
      profit: number;
    }
  ];
  subTotal: number;
  creditNotesHistory: [
    {
      id: string;
      invoiceId: string;
      currency: string;
      amount: number;
      newAmount: number;
    }
  ];
}

export interface InvoicesList {
  content: [
    {
      id: string;
      link: string;
      companyName: string;
      invoicePrefix: string;
      dateCreated: string;
      noOfShipments: number;
      totalAmount: number;
      currency: string;
      totalDetails: [
        {
          label: string;
          value: string;
          divider: boolean;
          active: boolean;
        }
      ];
      profit: number;
      selected: boolean;

      // unexpected fields from JSON example
      balanceDue: string;
      workOrdertype: string;
      dateSubmitted: string;
      dueDate: string;
      lastPaymentDate: string;
      PaymentStatus: string;
      // end of unexpected fields from JSON example
    }
  ];
  // unexpected fields from JSON example
  pageable: {
    sort: {
      sorted: boolean;
      unsorted: boolean;
    };
    offset: number;
    pageSize: number;
    pageNumber: number;
    paged: boolean;
    unpaged: boolean;
  };
  totalElements: number;
  totalPages: number;
  last: boolean;
  size: number;
  number: number;
  numberOfElements: number;
  sort: {
    sorted: boolean;
    unsorted: boolean;
  };
  first: boolean;
  // end of unexpected fields from JSON example
}

export interface AR {
  id: string;
  customerId: string;
  companyName: string;
  logo: string;
  currency: string;
  invoices: string;
  deposit: string;
  depositDetails: [
    {
      amount: number;
      paymentDetails: {
        currency: string;
        paymentType: string;
        checkNo: string;
        paidDate: string;
      };
    }
  ];
  credit: string;
  creditDetails: [
    {
      amount: number;
      paymentDetails: {
        currency: string;
        paymentType: string;
        checkNo: string;
        paidDate: string;
      };
    }
  ];
  receivable: string;
}
