export interface OrderDetails {
  company: {
    name: string;
    logo: string;
    info: string;
  };
  from: {
    label: string;
    title: string;
    description: string;
    secondDescription: string;
  };
  fromContact: {
    label: string;
    title: string;
    description: string;
    secondDescription: string;
  };
  to: {
    label: string;
    title: string;
    description: string;
    secondDescription: string;
  };
  toContact: {
    label: string;
    title: string;
    description: string;
    secondDescription: string;
  };
  additionalInfo: [
    {
      label: string;
      value: string;
      info: {
        title: string;
        moreData: [
          {
            label: string;
            value: string;
            divider: boolean;
            active: boolean;
          }
        ];
      };
    }
  ];
  info: string;
  paymentInfo: [
    {
      label: string;
      value: string;
      active: boolean;
    }
  ];
  documentsList: [
    {
      name: string;
      link: string;
    }
  ];
  markup: [
    {
      label: string;
      value: string;
      active: boolean;
    }
  ];
  creditCardInfo: [
    {
      label: string;
      value: string;
      active: boolean;
    }
  ];
}
