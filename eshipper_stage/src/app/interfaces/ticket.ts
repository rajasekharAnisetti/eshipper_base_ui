export interface Ticket {
  customer: string;
  ticket: string;
  type: {
    name: string;
    color: string; // color class
  };
  reason: string;
  contactPreference: string;
  tracking: string;
  receivedDate: string;
  sellingChannel: string;
  submitedDate: string;
  storeName: string;
  documents: Array<Doc>;
  documentsMissing: Array<Doc>;
  status: {
    name: string;
    color: string; // color class
  };
  subject: string;
  description: string;
  notifyMeForUpdates: boolean;
}

export interface Doc {
  name: string;
  url: string;
}
