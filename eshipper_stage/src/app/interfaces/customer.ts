export interface CustomerDetails {
  customerId: string;
  customerName: string;
  logo: string;
  dormantStage: string;
  lastShipment: string;
  salesPerson: string;
  created: string;
  active: false;
  customerType: string;
  address1: string;
  address2: string;
  city: string;
  stateOrProvince: string;
  zipOrPostal: string;
  country: string;
  contactPerson: string;
  phoneNumber: string;
  email: string;
  webCustomer: boolean;
  shipmentDefaultPackageType: string;
  shipmentDefaultPickupLocation: string;
  shipmentServiceCountryExcluded: string;
  insuranceType: string;
  insurance: string;
  salesRep: string;
  reference1: string;
  reference2: string;
  reference3: string;
  subAffiliateFlag: boolean;
  subAffiliateRebate: string;
  subAffiliateDiscount: string;
  linkToAffiliate: string;
  goalCompletionList: [
    {
      name: string;
      flag: boolean;
    }
  ];
}
