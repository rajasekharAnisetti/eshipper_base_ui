export interface EdiHeader {
  numberBox: {
    short: boolean;
    label: string;
    number: string;
    logo: string;
  };
  carrierInvoice: string;
  ediOrder: number;
  invoicedData: string;
  processedData: string;
  total: number;
  totalDetails: [
    {
      label: string;
      value: number;
    }
  ];
  selected: boolean;
}

export interface EdiOrphan {
  numberBox: {
    label: string;
    name: string;
    number: string;
    logo: string;
  };
  from: {
    companyName: string;
    companyAddress: string;
    date: string;
  };
  to: {
    companyName: string;
    companyAddress: string;
    date: string;
  };
  carrierInvoice: string;
  trans: string;
  selected: boolean;
}
