export interface Insurance {
  insuranceFile: string;
  period: string;
  totalValue: number;
  totalPremium: number;
}
