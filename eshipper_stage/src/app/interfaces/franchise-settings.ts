import { CarrierSttings } from 'src/app/interfaces/carier-settings';

export interface FranchiseSettings {
  subFranchiseOf: string;
  paymentNetTerms: string;
  paymentNetWarning: string;
  paymentDisable: boolean;
  paymentDisableAfter: number;
  useOwnCarrierAccount: boolean;
  useeShipperCarrierAccount: boolean;
  carrierAccountsList: Array<CarrierSttings>;
  billingTransactional: boolean;
  billingRecurringFee: boolean;
  billingOneTimeFee: boolean;
  transactionalCostType: string;
  transactionalTransactionFeeTiers: string;
  recurringFeeFrequency: string;
  recurringFeeRecurringFee: string;
  recurringFeeOriginalLicense: string;
  recurringFeeSetupCost: string;
  oneTimeFeeOriginalLicense: string;
  oneTimeFeeSetupCost: string;
}
