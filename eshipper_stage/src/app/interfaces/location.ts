export interface LocationData {
  companyName: string;
  address1: string;
  address2: string;
  city: string;
  province: string;
  zip: string;
  country: string;
  attention: string;
  phone: string;
  email: string;
  taxId: string;
}
