export interface PaymentCard {
  default: boolean;
  logo: string;
  number: string;
  expireMonth: string;
  expireYear: string;
  cvc: string;
  cardHolderName: string;
  address1: string;
  address2: string;
  city: string;
  stateOrProvince: string;
  zipOrPostal: string;
  country: string;
  privacy: boolean;
}
