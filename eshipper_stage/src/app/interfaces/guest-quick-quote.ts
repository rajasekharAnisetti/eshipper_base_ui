export interface GuestQuickQuote {
  companyName: string;
  contactPerson: string;
  shipments: number;
  currency: string;
  amount: number;
  dateCreated: string;
  registered: string;
  checkOut: string;
}
