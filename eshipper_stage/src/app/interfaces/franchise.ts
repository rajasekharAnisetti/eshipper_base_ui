import { Franchise } from './franchise';
export interface Franchise {
  franchiseId: string;
  franchiseLogo: string;
  franchiseName: string;
  email: string;
  phone: string;
  masterFlag: boolean;
  currency: string;
  address1: string;
  address2: string;
  city: string;
  stateOrProvince: string;
  zipOrPostal: string;
  country: string;
  contactName: string;
  financeEmail: string;
  operationsEmail1: string;
  operationsEmail2: string;
  operationsEmail3: string;
  primaryColor: string;
  secondaryColor: string;
  urlForLogin: string;
  urlForRegister: string;
  keyContactList: [
    {
      name: string;
      position: string;
      mobilePhone: string;
      workPhone: string;
      otherPhone: string;
      primaryEmail: string;
      secondaryEmail: string;
    }
  ];
  documents: [
    {
      name: string;
    }
  ];
}

export interface FranchiseTrackItem {
  id: string;
  logo: string;
  name: string;
  franchiseShipments: number;
  franchiseRevenue: number;
  franchiseCost: number;
  franchiseGrossProfit: number;
  franchiseNetProfit: number;
  franchiseCharge: number;
  franchiseChargeDetails: [
    {
      label: string;
      value: number;
    }
  ];
}

export interface FranchiseTrack {
  track: string;
  path: [
    {
      position: string;
      state: boolean;
    }
  ];
  locations: [
    {
      name: string;
      logo: string;
      link: string;
      date: string;
      time: string;
      state: boolean;
      current: boolean;
    }
  ];
}

export interface KeyContact {
  name: string;
  position: string;
  mobilePhone: string;
  workPhone: string;
  otherPhone: string;
  primaryEmail: string;
  secondaryEmail: string;
}
