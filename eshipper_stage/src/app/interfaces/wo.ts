import { currency } from '../shared/functions/currency';
export interface Segment {
  carrier: {
    type: string;
    name: string;
    description: string;
  };
  bigBlocks: [
    {
      label: string;
      value: string;
      date: string;
    }
  ];
  blocks: [
    {
      label: string;
      value: string;
    }
  ];
  showStops: boolean;
  agent: {
    service: string;
    name: string;
    phone: string;
  };
  stops: [
    {
      locationType: string;
      location: string;
      date: string;
      time: string;
      mission: string;
      active: boolean;
      current: boolean;
    }
  ];
  pickup: {
    locationType: string;
    location: string;
    contactName: string;
    phone: string;
    date: string;
    time: string;
  };
  delivery: {
    locationType: string;
    location: string;
    contactName: string;
    phone: string;
    date: string;
    time: string;
    note: string;
  };
}

export interface TrackDetails {
  company: {
    name: string;
    logo: string;
    info: string;
  };
  from: {
    label: string;
    title: string;
    description: string;
    secondDescription: string;
  };
  fromContact: {
    label: string;
    title: string;
    description: string;
    secondDescription: string;
  };
  to: {
    label: string;
    title: string;
    description: string;
    secondDescription: string;
  };
  toContact: {
    label: string;
    title: string;
    description: string;
    secondDescription: string;
  };
  additionalInfo: [
    {
      label: string;
      value: string | number;
      info: {
        title: string;
        moreData: [
          {
            label: string;
            value: string;
            divider: boolean;
            active: boolean;
          }
        ];
      };
    }
  ];
  charges: [
    {
      label: string;
      value: string | number;
      active: boolean;
    }
  ];
  documentsList: [
    {
      name: string;
      url: string;
    }
  ];
  progressReports: [
    {
      status: string;
      date: string;
      address: string;
      name: string;
      note: string;
    }
  ];
}

export interface Track {
  track: string;
  chip: {
    name: string;
    type: string;
  };
  pathToDetails: string;
  path: [
    {
      position: string;
      state: boolean;
    }
  ];
  locations: [
    {
      name: string;
      logo: string;
      link: string;
      date: string;
      time: string;
      state: boolean;
      current: boolean;
    }
  ];
}

export interface Agent {
  carrierName: string;
  name: string;
  phone: string;
  email: string;
  country: string;
  areas: [
    {
      name: string;
      color: string;
    }
  ];
}

export interface Area {
  name: string;
  color: string;
}

export interface Message {
  type: string;
  message: string;
}

export interface ServicesData {
  link: string;
  blocks: [
    {
      label: string;
      value: string;
      right: boolean;
      strong: boolean;
    }
  ];
}

export interface WOConfirmedData {
  company: {
    name: string;
    logo: string;
    info: string;
  };
  confirm: {
    number: string;
    text: string;
  };
  chargesInfo: [
    {
      label: string;
      value: string | number;
      right: boolean;
    }
  ];
  documentsList: [
    {
      name: string;
      link: string;
    }
  ];
  from: {
    label: string;
    title: string;
    description: string;
    secondDescription: string;
  };
  fromContact: {
    label: string;
    title: string;
    description: string;
    secondDescription: string;
  };
  to: {
    label: string;
    title: string;
    description: string;
    secondDescription: string;
  };
  toContact: {
    label: string;
    title: string;
    description: string;
    secondDescription: string;
  };
  additionalInfo: [
    {
      label: string;
      value: string | number;
      currency: boolean;
      info: {
        title: string;
        moreData: Array<any>;
      };
    }
  ];
}
