export interface DetailsData {
  from: {
    label: string;
    title: string;
    description: string;
    secondDescription: string;
  };
  to: {
    label: string;
    title: string;
    description: string;
  };
  package: {
    label: string;
    value: string;
  };
  quantity: {
    label: string;
    value: string;
  };
  dimension: {
    label: string;
    value: string;
    info: {
      title: string;
      moreData: [
        {
          label: string;
          value: string;
          divider: boolean;
          active: boolean;
        }
      ];
    };
  };
  instructions: {
    label: string;
    value: string;
  };
}
