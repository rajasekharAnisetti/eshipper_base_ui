export interface Files {
  name: string;
  process: boolean;
  success: boolean;
}

export interface ConfirmedData {
  company: {
    name: string;
    logo: string;
    info: string;
  };
  confirm: {
    number: string;
    text: string;
  };
  paymentInfo: [
    {
      label: string;
      value: string;
    }
  ];
  creditCardInfo: [
    {
      label: string;
      value: string;
    }
  ];
  from: {
    label: string;
    title: string;
    description: string;
    secondDescription: string;
  };
  fromContact: {
    label: string;
    title: string;
    description: string;
    secondDescription: string;
  };
  to: {
    label: string;
    title: string;
    description: string;
    secondDescription: string;
  };
  toContact: {
    label: string;
    title: string;
    description: string;
    secondDescription: string;
  };
  additionalInfo: [
    {
      label: string;
      value: string;
      currency: boolean;
      info: {
        title: string;
        moreData: Array<any>;
      };
    }
  ];

  customInfo: [
    {
      label: string;
      value: string;
    }
  ];
  documentsList: [
    {
      name: string;
      link: string;
    }
  ];
}

export interface Rate {
  image: string;
  title: string;
  description: string;
  date: string;
  value: string;
  info: string;
  selected: boolean;
}

export interface PendingBatch {
  id: string;
  numberBox: {
    label: string;
    name: string;
    number: string;
    logo: string;
  };
  pendingDistributor: string;
  pendingShipments: number;
  infoError: number;
  selected: boolean;
}

export interface BatchInner {
  id: string;
  numberBox: {
    label: string;
    name: string;
    number: string;
    logo: string;
  };
  from: {
    companyName: string;
    companyAddress: string;
    date: string;
  };
  to: {
    companyName: string;
    companyAddress: string;
    date: string;
  };
  errors: number;
  selected: boolean;
}

export interface RatesBatchTrack {
  id: string;
  numberBox: {
    label: string;
    name: string;
    number: string;
    logo: string;
  };
  from: {
    companyName: string;
    companyAddress: string;
    date: string;
  };
  to: {
    companyName: string;
    companyAddress: string;
    date: string;
  };
  errors: number;
  cost: number;
  costDetails: [
    {
      label: string;
      value: number;
    }
  ];
  status: {
    name: string;
    color: string;
  };
  selected: boolean;
}
