export interface CarrierReport {
  carrierId: string;
  carrierName: string;
  carrierLogo: string;
  collapceData: [
    {
      name: string;
      shipments: number;
      revenue: number;
      cost: number;
      grossProfit: number;
      commissions: number;
      netProfit: number;
      avgMargin: number;
    }
  ];
}

export interface CCAuth {
  orderId: string;
  authId: string;
  receiptId: string;
  customer: string;
  chargeType: string;
  entityType: string;
  currency: string;
  amount: number;
  status: string;
}

export interface ProgressReportData {
  status: string;
  date: string;
  address: string;
  name: string;
  note: string;
}

export interface RevenueProfitReport {
  id: string;
  name: string;
  logo: string;
  collapceData: [
    {
      name: string;
      shipments: number;
      revenue: number;
      cost: number;
      grossProfit: number;
      commissions: number;
      netProfit: number;
      avgMargin: number;
    }
  ];
}
