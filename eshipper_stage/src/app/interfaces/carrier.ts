export interface Carrier {
  name: string;
  type: string;
  email: string;
  phone: string;
  fax: string;
}
