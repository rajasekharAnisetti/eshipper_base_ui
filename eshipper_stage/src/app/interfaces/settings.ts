export interface Settings {
  name: string;
  value: number;
  info: string;
  system: boolean;
  email: boolean;
  frequency: string;
  active: boolean;
}
