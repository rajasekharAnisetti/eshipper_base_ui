export interface AccountSettings {
  creditPaymentCurrency: string;
  netTerms: string;
  netWarning: string;
  uspsAccountType: string;
  customerType: string;
  creditLimitFlag: boolean;
  creditLimitAmount: number;
  allowThirdPartyShipmentFlag: boolean;
  allowThirdPartyShipmentAmount: number;
  chargeCreditCardWhenInvoiceDueFlag: boolean;
  allowUpsCodFlag: boolean;
  receiveTransactionReceiptsFlag: boolean;
  smartePostPickupScheduledFlag: boolean;
  disableOnLatePaymentsFlag: boolean;
  costAccountFlag: boolean;
  codAccountFlag: boolean;
  storeFrontFlag: boolean;
  nmcfRequiredFlag: boolean;
  showNewsFlag: boolean;
  freightClassAutoPopulateFlag: boolean;
  customsInvoiceRequiredFlag: boolean;
  defaultShipmentsUseResidentialFlag: boolean;
  carrier: string;
  service: string;
  flatCharge: string;
  flatCost: string;
  invoiceTaxCurrency: string;
  generateInvoicesPer: string;
  chargeLevel: string;
  customsInvoiceNotRequired: Array<string>;
  invoiceEmail: Array<string>;
  refCodeMandatoryFlag: boolean;
  inBondCustomerFlag: boolean;
  applyTaxFlag: boolean;
  brokerCompanyName: string;
  taxId: string;
  brokerContactName: string;
  brokerEmail: string;
  brokerPhoneNumber: string;
  operationalExpensesCouriers: string;
  operationalExpensesPallets: string;
  operationalExpensesPostal: string;
  affiliateCommissionFor: Array<string>;
  transactionCommissionFor: Array<string>;
  useOwnCarrierAccountFlag: boolean;
  useEshipperCarrierAccountFlag: boolean;
  apiUsername: string;
  apiPassword: string;
}

export interface Carrier {
  id: string;
  serviceLogo: string;
  serviceName: string;
  packageTypes: [string];
  caFlag: boolean;
  usaFlag: boolean;
}

export interface AddressBook {
  id: string;
  companyName: string;
  address1: string;
  address2: string;
  city: string;
  stateOrProvince: string;
  zipOrPostal: string;
  country: string;
  contactPerson: string;
  contactPhone: string;
  contactEmail: string;
}

export interface Box {
  boxType: string;
  boxName: string;
  dimension: {
    l: string;
    w: string;
    h: string;
  };
  units: {
    weight: string;
    size: string;
  };
  weight: number;
  maxSupportWeight: number;
  description: string;
}

export interface Product {
  productId: string;
  productName: string;
  hsCode: string;
  skuCode: string;
  coo: string;
  unitPrice: number;
  quantity: number;
}
