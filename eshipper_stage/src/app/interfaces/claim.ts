export interface Claim {
  id: string;
  new: boolean;
  marker: string; // color class
  selected: boolean;
  numberBox: {
    label: string;
    name: string;
    subName: string;
    number: string;
    logo: string;
    type: {
      name: string;
      color: string; // color class
    };
  };
  insuredInfo: string;
  reason: string;
  submitedDate: string;
  trans: string;
  charge: number;
  total: number;
  status: {
    name: string;
    color: string; // color class
  };
  receivedDate: string;
  mailedDate: string;
  solution: string;
  assignTo: string;
  statusAlert: string;
  tracking: string;
  description: string;
  carrierRefundCurrency: string;
  carrierClaim: string;
  carrierRefundStatus: string;
  carrierRefundAmount: number;
  carrierRefundCheck: number;
  carrierRefundCheckNumber: string;
  carrierRefundDate: string;
  eShipperRefundCurrency: string;
  eShipperRefundAmount: number;
  eShipperRefundCheck: string;
  eShipperRefundDate: string;
  comments: [
    {
      type: string;
      date: string;
      comment: string;
    }
  ];
  notifyCustomerForUpdates: boolean;
}
