export interface Markup {
  markupId: string;
  markupLogo: string;
  markupName: string;
  direction: string;
  markupMin: number;
  markupDom: number;
  markupIntl: number;
  flatRate: number;
  commCourierOpExp: number;
  markupPrioritization: Array<string>;
  caFlag: boolean;
  usaFlag: boolean;
  selected: boolean;
  weight5: number;
  weight10: number;
  weight15: number;
  weightMax: number;
  lines: [
    {
      from: string;
      to: string;
      markup: string;
    }
  ];
  countries: [
    {
      location: string;
      markup: string;
    }
  ];
  codeRange: [
    {
      from: string;
      to: string;
      markup: string;
    }
  ];
}
