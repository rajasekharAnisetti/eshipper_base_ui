export interface CarrierSttings {
  carrierName: string;
  region: string;
  accountOwner: string;
  accountNumber: string;
  meterNumber: string;
  key: string;
  password: string;
}
