export interface News {
  newsDate: string;
  newsTitle: string;
  newsMessage: string;
  location: string;
  expired: string;
}
