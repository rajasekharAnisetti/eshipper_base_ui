export interface Invoice {
  selected: boolean;
  id: string;
  companyName: string;
  invoicePrefix: string;
  noOfShipments: number;
  dateSubmitted: string;
  aging: number;
  dueDate: string;
  currency: string;
  totalAmount: number;
  totalDetails: [
    {
      label: string;
      value: string;
      divider: boolean;
      active: boolean;
    }
  ];
  balanceDue: number;
  profit: number;
  status: {
    name: string;
    color: string;
  };
  lastPaymentDate: string;
  comment: string;
  commentPublicFlag: boolean;
  depositAmount: number;
  discountFlag: boolean;
  depositFlag: boolean;
  depositLabel: number;
  discount: string;
  discountValue: number;
  discountAmount: number;
  amountReceived: number;
  depositCurrency: string;
  availableCreditNotes: string;
  creditNotesCurrency: string;
  workOrders: [
    {
      id: string;
      link: string;
      oriCost: number;
      oriCharge: number;
      profit: number;
    }
  ];
  subTotal: number;
  docs: [
    {
      id: string;
      name: string;
      date: string;
      time: string;
      url: string;
    }
  ];

  creditLabel: string;
  dateCreated: string;
  workOrdertype: string;
}
