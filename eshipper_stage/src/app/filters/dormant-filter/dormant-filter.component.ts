import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-dormant-filter',
  templateUrl: './dormant-filter.component.html',
  styleUrls: ['./dormant-filter.component.scss'],
})
export class DormantFilterComponent implements OnInit {
  modalData = {
    salesperson: '',
    revenueRange: '',
    shipmentAmount: '',
  };

  validation = {
    salesperson: new FormControl(),
    revenueRange: new FormControl(),
    shipmentAmount: new FormControl(),
  };

  salespersons = ['Option 1', 'Option 2', 'Option 3'];
  revenueRanges = ['Option 1', 'Option 2', 'Option 3'];
  shipmentAmounts = ['Option 1', 'Option 2', 'Option 3'];

  constructor(
    public dialogRef: MatDialogRef<DormantFilterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.modalData);
  };
}
