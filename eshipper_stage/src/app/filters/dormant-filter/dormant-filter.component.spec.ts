import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DormantFilterComponent } from './dormant-filter.component';

describe('DormantFilterComponent', () => {
  let component: DormantFilterComponent;
  let fixture: ComponentFixture<DormantFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DormantFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DormantFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
