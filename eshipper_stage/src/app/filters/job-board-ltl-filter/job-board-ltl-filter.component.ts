import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { STATUSES } from 'src/app/shared/const/dropdowns';

@Component({
  selector: 'app-job-board-ltl-filter',
  templateUrl: './job-board-ltl-filter.component.html',
  styleUrls: ['./job-board-ltl-filter.component.scss'],
})
export class JobBoardLtlFilterComponent implements OnInit {
  modalData = {
    customer: '',
    ltlType: '',
    serviceAgent: '',
    status: '',
  };
  customers = ['Option 1', 'Option 2', 'Option 3'];
  ltlTypes = ['Option 1', 'Option 2', 'Option 3'];
  serviceAgents = ['Option 1', 'Option 2', 'Option 3'];
  statuses = STATUSES;
  validation = {
    customer: new FormControl(),
    ltlType: new FormControl(),
    serviceAgent: new FormControl(),
    status: new FormControl(),
  };

  constructor(
    public dialogRef: MatDialogRef<JobBoardLtlFilterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.modalData);
  };
}
