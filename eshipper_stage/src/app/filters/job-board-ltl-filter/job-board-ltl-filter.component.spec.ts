import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobBoardLtlFilterComponent } from './job-board-ltl-filter.component';

describe('JobBoardLtlFilterComponent', () => {
  let component: JobBoardLtlFilterComponent;
  let fixture: ComponentFixture<JobBoardLtlFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobBoardLtlFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobBoardLtlFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
