import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarrierFilterComponent } from './carrier-filter.component';

describe('CarrierFilterComponent', () => {
  let component: CarrierFilterComponent;
  let fixture: ComponentFixture<CarrierFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarrierFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarrierFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
