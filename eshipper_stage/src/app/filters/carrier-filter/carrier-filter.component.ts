import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-carrier-filter',
  templateUrl: './carrier-filter.component.html',
  styleUrls: ['./carrier-filter.component.scss'],
})
export class CarrierFilterComponent implements OnInit {
  modalData = {
    customer: '',
    program: '',
  };

  validation = {
    customer: new FormControl(),
    program: new FormControl(),
  };

  customers = ['Customer 1', 'Customer 2'];
  programs = ['Program 1', 'Program 2'];

  constructor(
    public dialogRef: MatDialogRef<CarrierFilterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.modalData);
  };
}
