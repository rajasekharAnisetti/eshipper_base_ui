import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTrackingFilterDialogComponent } from './admin-tracking-filter-dialog.component';

describe('AdminTrackingFilterDialogComponent', () => {
  let component: AdminTrackingFilterDialogComponent;
  let fixture: ComponentFixture<AdminTrackingFilterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTrackingFilterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTrackingFilterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
