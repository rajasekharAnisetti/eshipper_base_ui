import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { STATUSES } from 'src/app/shared/const/dropdowns';

@Component({
  selector: 'app-admin-tracking-filter-dialog',
  templateUrl: './admin-tracking-filter-dialog.component.html',
  styleUrls: ['./admin-tracking-filter-dialog.component.scss'],
})
export class AdminTrackingFilterDialogComponent implements OnInit {
  carriers = ['Option 1', 'Option 2', 'Option 3'];
  services = ['Option 1', 'Option 2', 'Option 3'];
  statuses = STATUSES;
  customers = ['Option 1', 'Option 2', 'Option 3'];
  shipmentTypes = ['Option 1', 'Option 2', 'Option 3'];
  paymentModes = ['Option 1', 'Option 2', 'Option 3'];
  markupTypes = ['Option 1', 'Option 2', 'Option 3'];
  salesReps = ['Option 1', 'Option 2', 'Option 3'];
  destinations = ['Option 1', 'Option 2', 'Option 3'];

  fiterData = {
    shipmentType: '',
    customer: '',
    carrier: '',
    service: '',
    status: '',
    paymentMode: '',
    markupType: '',
    salesRep: '',
    destination: '',
    originPostal: '',
    destNPostal: '',
    showCancelledShipments: '',
    showPredispatched: '',
  };

  filterArray: Array<string> = [];

  changeValue(val) {
    this.filterArray.push(val);
  }

  constructor(
    public dialogRef: MatDialogRef<AdminTrackingFilterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}
  closeDialog = () => this.dialogRef.close();

  filter = () => {
    const data = {
      velues: this.fiterData,
      array: this.filterArray,
    };
    this.dialogRef.close(data);
  };
}
