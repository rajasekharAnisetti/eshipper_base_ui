import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicesFilterDialogComponent } from './invoices-filter-dialog.component';

describe('InvoicesFilterDialogComponent', () => {
  let component: InvoicesFilterDialogComponent;
  let fixture: ComponentFixture<InvoicesFilterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicesFilterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicesFilterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
