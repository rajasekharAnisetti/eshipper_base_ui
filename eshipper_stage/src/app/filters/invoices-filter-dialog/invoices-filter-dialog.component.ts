import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { STATUSES } from 'src/app/shared/const/dropdowns';

@Component({
  selector: 'app-invoices-filter-dialog',
  templateUrl: './invoices-filter-dialog.component.html',
  styleUrls: ['./invoices-filter-dialog.component.scss'],
})
export class InvoicesFilterDialogComponent implements OnInit {
  account: string;
  customer: boolean;
  modalData = {
    customer: '',
    invoiceType: '',
    status: '',
  };
  customers = ['Option 1', 'Option 2', 'Option 3'];
  invoiceTypes = ['Option 1', 'Option 2', 'Option 3'];
  statuses = STATUSES;
  validation = {
    customer: new FormControl(),
    invoiceType: new FormControl(),
    status: new FormControl(),
  };

  constructor(
    public dialogRef: MatDialogRef<InvoicesFilterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.account = localStorage.getItem('account');
    this.customer = this.account === 'admin' ? true : false;
  }

  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.modalData);
  };
}
