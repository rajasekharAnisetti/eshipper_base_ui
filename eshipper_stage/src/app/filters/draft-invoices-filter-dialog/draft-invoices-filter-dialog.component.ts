import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-draft-invoices-filter-dialog',
  templateUrl: './draft-invoices-filter-dialog.component.html',
  styleUrls: ['./draft-invoices-filter-dialog.component.scss'],
})
export class DraftInvoicesFilterDialogComponent implements OnInit {
  modalData = {
    customer: '',
    invoiceType: '',
  };
  customers = ['Option 1', 'Option 2', 'Option 3'];
  invoiceTypes = ['Option 1', 'Option 2', 'Option 3'];
  validation = {
    customer: new FormControl(),
    invoiceType: new FormControl(),
  };

  constructor(
    public dialogRef: MatDialogRef<DraftInvoicesFilterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.modalData);
  };
}
