import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DraftInvoicesFilterDialogComponent } from './draft-invoices-filter-dialog.component';

describe('DraftInvoicesFilterDialogComponent', () => {
  let component: DraftInvoicesFilterDialogComponent;
  let fixture: ComponentFixture<DraftInvoicesFilterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DraftInvoicesFilterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DraftInvoicesFilterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
