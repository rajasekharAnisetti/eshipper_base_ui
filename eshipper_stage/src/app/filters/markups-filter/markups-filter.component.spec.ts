import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkupsFilterComponent } from './markups-filter.component';

describe('MarkupsFilterComponent', () => {
  let component: MarkupsFilterComponent;
  let fixture: ComponentFixture<MarkupsFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkupsFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkupsFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
