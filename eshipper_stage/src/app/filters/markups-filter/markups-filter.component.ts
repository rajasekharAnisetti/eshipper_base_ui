import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { STATUSES } from 'src/app/shared/const/dropdowns';

@Component({
  selector: 'app-markups-filter',
  templateUrl: './markups-filter.component.html',
  styleUrls: ['./markups-filter.component.scss'],
})
export class MarkupsFilterComponent implements OnInit {
  modalData = {
    markup: '',
    packageType: '',
    carriers: '',
    status: '',
    type: '',
  };

  markups = ['Default', 'Option 2', 'Option 3'];
  packageTypes = ['All', 'Option 2', 'Option 3'];
  carriersList = ['All', 'Option 2', 'Option 3'];
  statuses = STATUSES;
  types = ['Markup', 'Option 2', 'Option 3'];

  validation = {
    markup: new FormControl(),
    packageType: new FormControl(),
    carriers: new FormControl(),
    status: new FormControl(),
    type: new FormControl(),
  };

  constructor(
    public dialogRef: MatDialogRef<MarkupsFilterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.modalData);
  };
}
