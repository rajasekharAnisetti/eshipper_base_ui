import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdiFilterDialogComponent } from './edi-filter-dialog.component';

describe('EdiFilterDialogComponent', () => {
  let component: EdiFilterDialogComponent;
  let fixture: ComponentFixture<EdiFilterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdiFilterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdiFilterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
