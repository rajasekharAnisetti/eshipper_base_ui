import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-edi-filter-dialog',
  templateUrl: './edi-filter-dialog.component.html',
  styleUrls: ['./edi-filter-dialog.component.scss'],
})
export class EdiFilterDialogComponent implements OnInit {
  carriers = ['Option 1', 'Option 2', 'Option 3'];
  services = ['Option 1', 'Option 2', 'Option 3'];
  destinations = ['Option 1', 'Option 2', 'Option 3'];

  modalData = {
    carrier: '',
    service: '',
    destination: '',
    originPostal: '',
    destnPostal: '',
  };

  constructor(
    public dialogRef: MatDialogRef<EdiFilterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  filter = () => this.dialogRef.close(this.modalData);
}
