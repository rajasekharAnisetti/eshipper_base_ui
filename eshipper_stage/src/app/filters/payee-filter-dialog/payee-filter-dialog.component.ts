import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-payee-filter-dialog',
  templateUrl: './payee-filter-dialog.component.html',
  styleUrls: ['./payee-filter-dialog.component.scss'],
})
export class PayeeFilterDialogComponent implements OnInit {
  modalData = {
    payeeType: '',
    paymentMethod: '',
  };

  validation = {
    payeeType: new FormControl(),
    paymentMethod: new FormControl(),
  };

  payeeTypes = ['Option 1', 'Option 2', 'Option 3'];
  paymentMethods = ['Option 1', 'Option 2', 'Option 3'];

  constructor(
    public dialogRef: MatDialogRef<PayeeFilterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.modalData);
  };
}
