import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayeeFilterDialogComponent } from './payee-filter-dialog.component';

describe('PayeeFilterDialogComponent', () => {
  let component: PayeeFilterDialogComponent;
  let fixture: ComponentFixture<PayeeFilterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayeeFilterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayeeFilterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
