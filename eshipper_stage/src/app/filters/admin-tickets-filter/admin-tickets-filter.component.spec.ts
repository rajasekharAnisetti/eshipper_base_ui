import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTicketsFilterComponent } from './admin-tickets-filter.component';

describe('AdminTicketsFilterComponent', () => {
  let component: AdminTicketsFilterComponent;
  let fixture: ComponentFixture<AdminTicketsFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTicketsFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTicketsFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
