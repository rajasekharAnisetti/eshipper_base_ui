import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

import { STATUSES } from 'src/app/shared/const/dropdowns';

@Component({
  selector: 'app-admin-tickets-filter',
  templateUrl: './admin-tickets-filter.component.html',
  styleUrls: ['./admin-tickets-filter.component.scss'],
})
export class AdminTicketsFilterComponent implements OnInit {
  modalData = {
    reason: '',
    status: '',
    customer: '',
  };
  reasons = ['Feedback', 'Development Help', 'Option 3'];
  statuses = STATUSES;
  customers = [
    'Bashirian and Rutherford Hybrid Logistics Inc.',
    'Customer 2',
    'Customer 3',
  ];

  validation = {
    reason: new FormControl(),
    status: new FormControl(),
    customer: new FormControl(),
  };

  constructor(
    public dialogRef: MatDialogRef<AdminTicketsFilterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.modalData);
  };
}
