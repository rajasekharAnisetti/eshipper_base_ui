import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-draft-filter-dialog',
  templateUrl: './draft-filter-dialog.component.html',
  styleUrls: ['./draft-filter-dialog.component.scss'],
})
export class DraftFilterDialogComponent implements OnInit {
  modalData = {
    customer: '',
    woType: '',
    serviceType: '',
    agent: '',
  };

  customers = ['Option 1', 'Option 2', 'Option 3'];
  woTypes = ['Option 1', 'Option 2', 'Option 3'];
  serviceTypes = ['Option 1', 'Option 2', 'Option 3'];
  agents = ['Option 1', 'Option 2', 'Option 3'];

  validation = {
    customer: new FormControl(),
    woType: new FormControl(),
    serviceType: new FormControl(),
    agent: new FormControl(),
  };

  constructor(
    public dialogRef: MatDialogRef<DraftFilterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.modalData);
  };
}
