import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DraftFilterDialogComponent } from './draft-filter-dialog.component';

describe('DraftFilterDialogComponent', () => {
  let component: DraftFilterDialogComponent;
  let fixture: ComponentFixture<DraftFilterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DraftFilterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DraftFilterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
