import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

import { CURRENCIES } from 'src/app/shared/const/currencies';
import { STATUSES } from 'src/app/shared/const/dropdowns';

@Component({
  selector: 'app-profit-loss-fiter',
  templateUrl: './profit-loss-fiter.component.html',
  styleUrls: ['./profit-loss-fiter.component.scss'],
})
export class ProfitLossFiterComponent implements OnInit {
  modalData = {
    woType: '',
    status: '',
    currency: '',
  };

  validation = {
    woType: new FormControl(),
    status: new FormControl(),
    currency: new FormControl(),
  };

  woTypes = ['Option 1', 'Option 2', 'Option 3'];
  statuses = STATUSES;
  currencies = CURRENCIES;

  constructor(
    public dialogRef: MatDialogRef<ProfitLossFiterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.modalData);
  };
}
