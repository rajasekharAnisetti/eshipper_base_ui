import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfitLossFiterComponent } from './profit-loss-fiter.component';

describe('ProfitLossFiterComponent', () => {
  let component: ProfitLossFiterComponent;
  let fixture: ComponentFixture<ProfitLossFiterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfitLossFiterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfitLossFiterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
