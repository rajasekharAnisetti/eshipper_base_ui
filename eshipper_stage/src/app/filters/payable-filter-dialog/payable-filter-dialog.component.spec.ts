import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayableFilterDialogComponent } from './payable-filter-dialog.component';

describe('PayableFilterDialogComponent', () => {
  let component: PayableFilterDialogComponent;
  let fixture: ComponentFixture<PayableFilterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayableFilterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayableFilterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
