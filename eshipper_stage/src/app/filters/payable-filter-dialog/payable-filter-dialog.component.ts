import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { CURRENCIES } from 'src/app/shared/const/currencies';

@Component({
  selector: 'app-payable-filter-dialog',
  templateUrl: './payable-filter-dialog.component.html',
  styleUrls: ['./payable-filter-dialog.component.scss'],
})
export class PayableFilterDialogComponent implements OnInit {
  modalData = {
    payeeType: '',
    categories: '',
    payee: '',
    paymentMethod: '',
    paymentStatus: '',
    currency: '',
  };

  validation = {
    payeeType: new FormControl(),
    categories: new FormControl(),
    payee: new FormControl(),
    paymentMethod: new FormControl(),
    paymentStatus: new FormControl(),
    currency: new FormControl(),
  };

  payeeTypes = ['Option 1', 'Option 2', 'Option 3'];
  categories = ['Option 1', 'Option 2', 'Option 3'];
  payees = ['Option 1', 'Option 2', 'Option 3'];
  paymentMethods = ['Option 1', 'Option 2', 'Option 3'];
  paymentStatuses = ['Option 1', 'Option 2', 'Option 3'];
  currencies = CURRENCIES;

  constructor(
    public dialogRef: MatDialogRef<PayableFilterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.modalData);
  };
}
