import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-super-admin-tracking-filter-dialog',
  templateUrl: './super-admin-tracking-filter-dialog.component.html',
  styleUrls: ['./super-admin-tracking-filter-dialog.component.scss'],
})
export class SuperAdminTrackingFilterDialogComponent implements OnInit {
  shipmentTypes = ['Option 1', 'Option 2', 'Option 3'];
  franchises = ['Option 1', 'Option 2', 'Option 3'];
  carriers = ['Option 1', 'Option 2', 'Option 3'];
  services = ['Option 1', 'Option 2', 'Option 3'];
  paymentModes = ['Option 1', 'Option 2', 'Option 3'];
  salesRepList = ['Option 1', 'Option 2', 'Option 3'];
  customers = ['Option 1', 'Option 2', 'Option 3'];
  shipFormList = ['Option 1', 'Option 2', 'Option 3'];
  shipToList = ['Option 1', 'Option 2', 'Option 3'];

  fiterData = {
    shipmentType: '',
    franchise: '',
    carrier: '',
    service: '',
    paymentMode: '',
    salesRep: '',
    customer: '',
    shipForm: '',
    shipTo: '',
  };

  constructor(
    public dialogRef: MatDialogRef<SuperAdminTrackingFilterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.fiterData);
  };
}
