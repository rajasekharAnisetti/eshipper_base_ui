import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperAdminTrackingFilterDialogComponent } from './super-admin-tracking-filter-dialog.component';

describe('SuperAdminTrackingFilterDialogComponent', () => {
  let component: SuperAdminTrackingFilterDialogComponent;
  let fixture: ComponentFixture<SuperAdminTrackingFilterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperAdminTrackingFilterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperAdminTrackingFilterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
