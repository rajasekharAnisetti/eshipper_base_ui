import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobBoardWorkOrderFilterComponent } from './job-board-work-order-filter.component';

describe('JobBoardWorkOrderFilterComponent', () => {
  let component: JobBoardWorkOrderFilterComponent;
  let fixture: ComponentFixture<JobBoardWorkOrderFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobBoardWorkOrderFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobBoardWorkOrderFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
