import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { STATUSES } from 'src/app/shared/const/dropdowns';

@Component({
  selector: 'app-job-board-work-order-filter',
  templateUrl: './job-board-work-order-filter.component.html',
  styleUrls: ['./job-board-work-order-filter.component.scss'],
})
export class JobBoardWorkOrderFilterComponent implements OnInit {
  modalData = {
    customer: '',
    woType: '',
    serviceType: '',
    status: '',
    city: '',
    zip: '',
    agent: '',
  };

  customers = ['Option 1', 'Option 2', 'Option 3'];
  woTypes = ['Web', 'Option 2', 'Option 3'];
  serviceTypes = ['Option 1', 'Option 2', 'Option 3'];
  statuses = STATUSES;
  agents = ['Option 1', 'Option 2', 'Option 3'];

  validation = {
    customer: new FormControl(),
    woType: new FormControl(),
    serviceType: new FormControl(),
    status: new FormControl(),
    agent: new FormControl(),
    city: new FormControl(),
    zip: new FormControl(),
  };

  constructor(
    public dialogRef: MatDialogRef<JobBoardWorkOrderFilterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.modalData);
  };
}
