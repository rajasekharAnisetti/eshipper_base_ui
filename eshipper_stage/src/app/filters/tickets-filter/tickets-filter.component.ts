import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';
import { STATUSES } from 'src/app/shared/const/dropdowns';

@Component({
  selector: 'app-tickets-filter',
  templateUrl: './tickets-filter.component.html',
  styleUrls: ['./tickets-filter.component.scss'],
})
export class TicketsFilterComponent implements OnInit {
  modalData = {
    type: '',
    reason: '',
    status: '',
  };
  types = ['Claims', 'General', 'Technical'];
  reasons = ['Feedback', 'Development Help', 'Option 3'];
  statuses = STATUSES;

  validation = {
    type: new FormControl(),
    reason: new FormControl(),
    status: new FormControl(),
  };

  constructor(
    public dialogRef: MatDialogRef<TicketsFilterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.modalData);
  };
}
