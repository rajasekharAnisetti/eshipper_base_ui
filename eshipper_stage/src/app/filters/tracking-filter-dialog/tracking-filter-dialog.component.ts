import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { STATUSES } from 'src/app/shared/const/dropdowns';

@Component({
  selector: 'app-tracking-filter-dialog',
  templateUrl: './tracking-filter-dialog.component.html',
  styleUrls: ['./tracking-filter-dialog.component.scss'],
})
export class TrackingFilterDialogComponent implements OnInit {
  carriers = ['Option 1', 'Option 2', 'Option 3'];
  services = ['Option 1', 'Option 2', 'Option 3'];
  statuses = STATUSES;

  fiterData = {
    carrier: '',
    service: '',
    status: '',
  };

  constructor(
    public dialogRef: MatDialogRef<TrackingFilterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}
  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.fiterData);
  };
}
