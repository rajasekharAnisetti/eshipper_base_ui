import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackingFilterDialogComponent } from './tracking-filter-dialog.component';

describe('TrackingFilterDialogComponent', () => {
  let component: TrackingFilterDialogComponent;
  let fixture: ComponentFixture<TrackingFilterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackingFilterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackingFilterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
