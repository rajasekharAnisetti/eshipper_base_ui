import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-customer-filter',
  templateUrl: './customer-filter.component.html',
  styleUrls: ['./customer-filter.component.scss'],
})
export class CustomerFilterComponent implements OnInit {
  modalData = {
    paymentType: 'Credit Card',
    salesPerson: '',
  };

  validation = {
    paymentType: new FormControl(),
    salesPerson: new FormControl(),
  };

  salesPersons = ['Willie French', 'Person 2'];
  paymentTypes = ['Credit Card', 'Type 2'];

  constructor(
    public dialogRef: MatDialogRef<CustomerFilterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.modalData);
  };
}
