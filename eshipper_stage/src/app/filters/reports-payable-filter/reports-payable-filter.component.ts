import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-reports-payable-filter',
  templateUrl: './reports-payable-filter.component.html',
  styleUrls: ['./reports-payable-filter.component.scss'],
})
export class ReportsPayableFilterComponent implements OnInit {
  modalData = {
    payeeType: '',
    payee: '',
    paymentMethod: '',
    paymentStatus: '',
  };

  validation = {
    payeeType: new FormControl(),
    payee: new FormControl(),
    paymentMethod: new FormControl(),
    paymentStatus: new FormControl(),
  };

  payeeTypes = ['Option 1', 'Option 2', 'Option 3'];
  payees = ['Option 1', 'Option 2', 'Option 3'];
  paymentMethods = ['Option 1', 'Option 2', 'Option 3'];
  paymentStatuses = ['Option 1', 'Option 2', 'Option 3'];

  constructor(
    public dialogRef: MatDialogRef<ReportsPayableFilterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.modalData);
  };
}
