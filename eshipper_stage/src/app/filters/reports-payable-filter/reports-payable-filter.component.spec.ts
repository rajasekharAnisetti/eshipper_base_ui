import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsPayableFilterComponent } from './reports-payable-filter.component';

describe('ReportsPayableFilterComponent', () => {
  let component: ReportsPayableFilterComponent;
  let fixture: ComponentFixture<ReportsPayableFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsPayableFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsPayableFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
