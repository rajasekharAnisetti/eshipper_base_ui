import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CcAuthFilterDialogComponent } from './cc-auth-filter-dialog.component';

describe('CcAuthFilterDialogComponent', () => {
  let component: CcAuthFilterDialogComponent;
  let fixture: ComponentFixture<CcAuthFilterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CcAuthFilterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CcAuthFilterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
