import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

import { CURRENCIES } from 'src/app/shared/const/currencies';
import { STATUSES } from 'src/app/shared/const/dropdowns';

@Component({
  selector: 'app-cc-auth-filter-dialog',
  templateUrl: './cc-auth-filter-dialog.component.html',
  styleUrls: ['./cc-auth-filter-dialog.component.scss'],
})
export class CcAuthFilterDialogComponent implements OnInit {
  modalData = {
    customer: '',
    entityType: '',
    chargeType: '',
    status: '',
    currency: '',
  };

  customers = ['Option 1', 'Option 2', 'Option 3'];
  entityTypes = ['Option 1', 'Option 2', 'Option 3'];
  chargeTypes = ['Option 1', 'Option 2', 'Option 3'];
  statuses = STATUSES;
  currencies = CURRENCIES;

  validation = {
    customer: new FormControl(),
    entityType: new FormControl(),
    chargeType: new FormControl(),
    status: new FormControl(),
    currency: new FormControl(),
  };

  constructor(
    public dialogRef: MatDialogRef<CcAuthFilterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.modalData);
  };
}
