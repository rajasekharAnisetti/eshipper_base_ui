import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimsFilterDialogComponent } from './claims-filter-dialog.component';

describe('ClaimsFilterDialogComponent', () => {
  let component: ClaimsFilterDialogComponent;
  let fixture: ComponentFixture<ClaimsFilterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClaimsFilterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimsFilterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
