import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

import { STATUSES } from 'src/app/shared/const/dropdowns';

@Component({
  selector: 'app-claims-filter-dialog',
  templateUrl: './claims-filter-dialog.component.html',
  styleUrls: ['./claims-filter-dialog.component.scss'],
})
export class ClaimsFilterDialogComponent implements OnInit {
  modalData = {
    carrier: '',
    reason: '',
    status: '',
    customer: '',
  };
  reasons = ['Feedback', 'Development Help', 'Option 3'];
  statuses = STATUSES;
  customers = [
    'Bashirian and Rutherford Hybrid Logistics Inc.',
    'Customer 2',
    'Customer 3',
  ];
  carriers = ['Option 1', 'Option 2', 'Option 3'];

  validation = {
    carrier: new FormControl(),
    reason: new FormControl(),
    status: new FormControl(),
    customer: new FormControl(),
  };

  constructor(
    public dialogRef: MatDialogRef<ClaimsFilterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.modalData);
  };
}
