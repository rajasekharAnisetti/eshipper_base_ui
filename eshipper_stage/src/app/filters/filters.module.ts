import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxMaskModule } from 'ngx-mask';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModules } from '../material-modules';

import { PayableFilterDialogComponent } from './payable-filter-dialog/payable-filter-dialog.component';
import { PayeeFilterDialogComponent } from './payee-filter-dialog/payee-filter-dialog.component';
import { EdiFilterDialogComponent } from './edi-filter-dialog/edi-filter-dialog.component';
import { DraftInvoicesFilterDialogComponent } from './draft-invoices-filter-dialog/draft-invoices-filter-dialog.component';
import { InvoicesFilterDialogComponent } from './invoices-filter-dialog/invoices-filter-dialog.component';
import { TrackingFilterDialogComponent } from './tracking-filter-dialog/tracking-filter-dialog.component';
import { AdminTrackingFilterDialogComponent } from './admin-tracking-filter-dialog/admin-tracking-filter-dialog.component';
import { DraftFilterDialogComponent } from './draft-filter-dialog/draft-filter-dialog.component';
import { JobBoardLtlFilterComponent } from './job-board-ltl-filter/job-board-ltl-filter.component';
import { JobBoardWorkOrderFilterComponent } from './job-board-work-order-filter/job-board-work-order-filter.component';
import { CustomerFilterComponent } from './customer-filter/customer-filter.component';
import { MarkupsFilterComponent } from './markups-filter/markups-filter.component';
import { CcAuthFilterDialogComponent } from './cc-auth-filter-dialog/cc-auth-filter-dialog.component';
import { ReportsPayableFilterComponent } from './reports-payable-filter/reports-payable-filter.component';
import { DormantFilterComponent } from './dormant-filter/dormant-filter.component';
import { ProfitLossFiterComponent } from './profit-loss-fiter/profit-loss-fiter.component';
import { CarrierFilterComponent } from './carrier-filter/carrier-filter.component';
import { SuperAdminTrackingFilterDialogComponent } from './super-admin-tracking-filter-dialog/super-admin-tracking-filter-dialog.component';
import { TicketsFilterComponent } from './tickets-filter/tickets-filter.component';
import { AdminTicketsFilterComponent } from './admin-tickets-filter/admin-tickets-filter.component';
import { ClaimsFilterDialogComponent } from './claims-filter-dialog/claims-filter-dialog.component';
import { AdminTrackingBatchFilterDialogComponent } from './admin-tracking-batch-filter-dialog/admin-tracking-batch-filter-dialog.component';

@NgModule({
  declarations: [
    PayableFilterDialogComponent,
    PayeeFilterDialogComponent,
    EdiFilterDialogComponent,
    DraftInvoicesFilterDialogComponent,
    InvoicesFilterDialogComponent,
    TrackingFilterDialogComponent,
    AdminTrackingFilterDialogComponent,
    DraftFilterDialogComponent,
    JobBoardLtlFilterComponent,
    JobBoardWorkOrderFilterComponent,
    CustomerFilterComponent,
    MarkupsFilterComponent,
    CcAuthFilterDialogComponent,
    ReportsPayableFilterComponent,
    DormantFilterComponent,
    ProfitLossFiterComponent,
    CarrierFilterComponent,
    SuperAdminTrackingFilterDialogComponent,
    TicketsFilterComponent,
    AdminTicketsFilterComponent,
    ClaimsFilterDialogComponent,
    AdminTrackingBatchFilterDialogComponent,
  ],
  imports: [
    CommonModule,
    NgxMaskModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModules,
  ],
  entryComponents: [
    PayableFilterDialogComponent,
    PayeeFilterDialogComponent,
    EdiFilterDialogComponent,
    DraftInvoicesFilterDialogComponent,
    InvoicesFilterDialogComponent,
    TrackingFilterDialogComponent,
    AdminTrackingFilterDialogComponent,
    DraftFilterDialogComponent,
    JobBoardLtlFilterComponent,
    JobBoardWorkOrderFilterComponent,
    CustomerFilterComponent,
    MarkupsFilterComponent,
    CcAuthFilterDialogComponent,
    ReportsPayableFilterComponent,
    DormantFilterComponent,
    ProfitLossFiterComponent,
    CarrierFilterComponent,
    SuperAdminTrackingFilterDialogComponent,
    AdminTicketsFilterComponent,
    ClaimsFilterDialogComponent,
    AdminTrackingBatchFilterDialogComponent,
    TicketsFilterComponent,
  ],
})
export class FiltersModule { }
