import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminTrackingBatchFilterDialogComponent } from './admin-tracking-batch-filter-dialog.component';

describe('AdminTrackingBatchFilterDialogComponent', () => {
  let component: AdminTrackingBatchFilterDialogComponent;
  let fixture: ComponentFixture<AdminTrackingBatchFilterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminTrackingBatchFilterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminTrackingBatchFilterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
