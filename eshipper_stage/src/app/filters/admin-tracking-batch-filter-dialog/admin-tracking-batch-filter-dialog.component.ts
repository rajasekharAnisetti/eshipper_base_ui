import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { STATUSES } from 'src/app/shared/const/dropdowns';

@Component({
  selector: 'app-admin-tracking-batch-filter-dialog',
  templateUrl: './admin-tracking-batch-filter-dialog.component.html',
  styleUrls: ['./admin-tracking-batch-filter-dialog.component.scss']
})
export class AdminTrackingBatchFilterDialogComponent implements OnInit {
  customers = ['Customer 1', 'Customer 2', 'Customer 3'];
  carriers = ['Option 1', 'Option 2', 'Option 3'];
  services = ['Option 1', 'Option 2', 'Option 3'];
  statuses = STATUSES;
  payments = ['Option 1', 'Option 2', 'Option 3'];
  salesReps = ['Option 1', 'Option 2', 'Option 3'];
  markupTypes = ['Option 1', 'Option 2', 'Option 3'];

  fiterData = {
    customer: '',
    carrier: '',
    service: '',
    status: '',
    paymentMode: '',
    salesRep: '',
    markupType: ''
  };

  constructor(
    public dialogRef: MatDialogRef<AdminTrackingBatchFilterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() { }
  closeDialog = () => this.dialogRef.close();

  filter = () => {
    this.dialogRef.close(this.fiterData);
  };
}
