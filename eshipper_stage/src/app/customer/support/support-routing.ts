import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TicketsComponent } from './tickets/tickets.component';
import { TicketComponent } from './ticket/ticket.component';
import { SuppliesComponent } from './supplies/supplies.component';

const routes: Routes = [
  {
    path: 'tickets',
    component: TicketsComponent,
    data: {
      title: 'Tickets',
    },
  },
  {
    path: 'new-ticket',
    component: TicketComponent,
    data: {
      title: 'Add New Ticket',
      backBtn: true,
    },
  },
  {
    path: 'ticket',
    component: TicketComponent,
    data: {
      title: 'TK 89674590123456',
      backBtn: true,
    },
  },
  {
    path: 'ticket-claims',
    component: TicketComponent,
    data: {
      title: 'TK 89674590123456',
      backBtn: true,
    },
  },
  {
    path: 'ticket-general',
    component: TicketComponent,
    data: {
      title: 'TK 89674590123456',
      backBtn: true,
    },
  },
  {
    path: 'supplies',
    component: SuppliesComponent,
    data: {
      title: 'Supplies',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SupportRoutingModule {}
