import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { WarningInfoBlockComponent } from 'src/app/shared/warning-info-block/warning-info-block.component';

import { SupportRoutingModule } from './support-routing';

import { TicketsComponent } from './tickets/tickets.component';
import { SuppliesComponent } from './supplies/supplies.component';
import { SupportComponent } from './support.component';
import { TicketComponent } from './ticket/ticket.component';

@NgModule({
  declarations: [
    TicketsComponent,
    SuppliesComponent,
    SupportComponent,
    TicketComponent,
    WarningInfoBlockComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    SupportRoutingModule,
    NgxMaskModule,
    MaterialModules,
    SharedModule,
  ],
  exports: [SupportComponent],
})
export class SupportModule {}
