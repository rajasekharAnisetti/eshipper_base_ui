import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
} from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import * as _rollupMoment from 'moment';

const moment = _rollupMoment || _moment;

import { Ticket } from 'src/app/interfaces/ticket';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss'],
  providers: [
    // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
    // `MatMomentDateModule` in your applications root module. We provide it at the component level
    // here, due to limitations of our example generation script.
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
})
export class TicketComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  minDate = new Date();
  readonlyFlag = false;
  newTicketFlag = false;
  ticket: Ticket;
  validation = {
    type: new FormControl(),
    reason: new FormControl(),
    contactPreference: new FormControl(),
    storeName: new FormControl(),
    tracking: new FormControl(),
    sellingChannel: new FormControl(),
    receivedDate: new FormControl(),
    submitedDate: new FormControl(),
    subject: new FormControl(),
    description: new FormControl(),
  };
  types = ['Claims', 'General', 'Technical'];
  reasons = ['Damaged Shipment', 'Development Help', 'Feedback'];
  contactPreferences = ['Someone', 'Option 2', 'Option 3'];
  sellingChannels = ['Amazon', 'Option 2', 'Option 3'];
  storeNames = [
    'La-Z-Boy Furniture International in Toronto Galleries',
    'Option 2',
    'Option 3',
  ];
  trackings = ['CXDT1234567800', 'WTX890890654'];
  trackingsFiltered: Observable<any>;

  shipment = {
    logo: 'content/images/logos/ups.png',
    tracking: 'WTX890890654',
    valueOfGoods: 678900.0,
    shipData: '03/27/2017',
    cost: 789900.0,
    insurance: 'eshipper',
    amount: 789900.0,
    email: 'brigitte_eichmann@mafalda.me',
    phone: '111-060-9732',
    shipping: '700 Mitchell Flats Suite 253, Mississauga, L5B 1B8, CA',
    billing: 'Same as shipping address',
  };

  constructor(public router: Router) { }

  ngOnInit() {
    this.trackingsFiltered = this.validation.tracking.valueChanges.pipe(
      startWith(''),
      map(name => (name ? this._filter(name) : this.trackings.slice()))
    );

    // get ticket
    const tData = localStorage.getItem('ticketData');
    if (tData) {
      this.readonlyFlag = true;
      this.ticket = JSON.parse(tData);
      this.validation.receivedDate = new FormControl(
        moment(this.ticket.receivedDate)
      );
      this.validation.submitedDate = new FormControl(
        moment(this.ticket.submitedDate)
      );
    }
    this.newTicketFlag =
      localStorage.getItem('newTicketFlag') === 'true' ? true : false;
    this.readonlyFlag =
      localStorage.getItem('newTicketFlag') === 'true' ? false : true;
  }

  // create ticket
  createTicket() {
    localStorage.setItem('newTicketFlag', 'false');
    const url = '/customer/support/tickets';
    this.router.navigate([url]);
  }
  // end of create ticket

  // trackings autocomplete filter
  private _filter(item: string) {
    const filterValue = item.toLowerCase();

    return this.trackings.filter(
      option => option.toLowerCase().indexOf(filterValue) === 0
    );
  }
  // end of trackings autocomplete filter

  // change tracking
  changeTracking(val) {
    // this.shipmentFlag = val === '' ? false : true;
  }
  // end of change tracking

  // change documents list
  changeDocuments(items, docs) {
    docs = items;
  }
  // end of change documents list

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
