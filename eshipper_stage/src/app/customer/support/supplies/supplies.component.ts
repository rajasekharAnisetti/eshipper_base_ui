import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { SuppliesService } from './supplies.service';
import { ApprovedDialogComponent } from 'src/app/dialogs/approved-dialog/approved-dialog.component';

export interface Supplies {
  logo: string;
  name: string;
  carrier: string;
  counter: number;
  check: boolean;
}

@Component({
  selector: 'app-supplies',
  templateUrl: './supplies.component.html',
  styleUrls: ['./supplies.component.scss'],
})
export class SuppliesComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  suppliesList: Array<Supplies>;
  toolbarSettings = ['Export'];
  carriersList = ['Carrier', 'Carrier 2'];
  dataEmpty = true;
  toolbarIndeterminate = false;
  selectAll = false;
  selection = false;
  summ = 0;

  constructor(private getData: SuppliesService, public dialog: MatDialog) { }

  ngOnInit() {
    // get data
    this.subscribe.add(
      this.getData.getSupplies().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        this.suppliesList = data.content.length ? data.content : [];
      })
    );
  }

  // carriers function
  carriersFunction(val) {
    console.log(val);
  }
  // end of carriers function

  // content toolbar settings
  settingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;

      default:
        break;
    }
  }
  // end of content toolbar settings

  // search function
  searchFunction(val) {
    console.log(val);
  }
  // end of search function

  // selection
  selectAllFunction(flag) {
    const select = flag ? true : false;
    this.selection = select ? true : false;
    for (const i of this.suppliesList) {
      i.check = select;
      this.summ = select
        ? i.check
          ? this.summ + i.counter
          : this.summ - i.counter
        : 0;
    }
  }

  selectCard(el) {
    const n = this.suppliesList.length;
    this.summ = el.check ? this.summ + el.counter : this.summ - el.counter;
    let item = 0;
    for (const i of this.suppliesList) {
      if (i.check) {
        item++;
      }
    }
    this.selectAll = n === item ? true : false;
    this.toolbarIndeterminate = n > item && item !== 0 ? true : false;
    this.selection = this.toolbarIndeterminate || this.selectAll ? true : false;
  }

  clickOnCard(item) {
    item.check = !item.check;
    this.selectCard(item);
  }
  // end of selection

  // submit function
  submit() {
    const dialogRef = this.dialog.open(ApprovedDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Payment Succeed.',
        content: 'Details text goes here...',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of submit function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
