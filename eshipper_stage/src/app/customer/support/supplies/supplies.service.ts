import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SuppliesService {
  constructor(private http: HttpClient) {}

  getSupplies(): Observable<any> {
    return this.http
      .get('content/data/customer-support/customer-supplies.json')
      .pipe(map(data => data));
  }
}
