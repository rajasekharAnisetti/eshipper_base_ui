import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';

import { Ticket } from 'src/app/interfaces/ticket';
import { TicketsFilterComponent } from 'src/app/filters/tickets-filter/tickets-filter.component';

import { TicketsService } from './tickets.service';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { WarningDialogComponent } from 'src/app/dialogs/warning-dialog/warning-dialog.component';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss'],
})
export class TicketsComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  dataEmpty = true;
  ticketsList: Array<Ticket>;
  itemSettings = ['Edit', 'Delete'];
  toolbarSettings = ['Export'];

  constructor(
    private getData: TicketsService,
    public dialog: MatDialog,
    public router: Router
  ) { }

  ngOnInit() {
    // get tickets
    this.subscribe.add(
      this.getData.getTickets().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        this.ticketsList = data.content.length ? data.content : [];
      })
    );
  }

  // period function
  period(period) {
    console.log(period);
  }
  // end of period function

  // search
  searchFunction(val) {
    console.log(val);
  }
  // end of search

  // filter
  filter() {
    const dialogRef = this.dialog.open(TicketsFilterComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of filter

  // toolbar settings
  settingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;

      default:
        break;
    }
  }
  // end of toolbar settings

  // get sort option
  getSortData(data) {
    console.log(data);
  }
  // end of get sort option

  // item settings
  itemSettingsFunction(val, item) {
    switch (val) {
      case 'Edit':
        {
          let url;
          switch (item.type.name) {
            case 'Claim':
              {
                url = '/customer/support/ticket-claims';
              }
              break;
            case 'General':
              {
                url = '/customer/support/ticket-general';
                localStorage.setItem('ticketFlag', 'general');
              }
              break;
            default:
              {
                url = '/customer/support/ticket';
              }
              break;
          }
          localStorage.setItem('newTicketFlag', 'false');
          localStorage.setItem('ticketData', JSON.stringify(item));
          this.router.navigate([url]);
        }
        break;
      case 'Delete':
        {
          const warningDialogRef = this.dialog.open(WarningDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              title: 'Warning',
              message: 'Warning message for edit default markup goes here...',
              action: 'Delete',
            },
          });
          this.subscribe.add(
            warningDialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;
      default:
        break;
    }
  }
  // end of item settings

  // click on row
  clickOnRow(item) {
    let url;
    switch (item.type.name) {
      case 'Claim':
        {
          url = '/customer/support/ticket-claims';
        }
        break;
      case 'General':
        {
          url = '/customer/support/ticket-general';
          localStorage.setItem('ticketFlag', 'general');
        }
        break;
      default:
        {
          url = '/customer/support/ticket';
        }
        break;
    }
    localStorage.setItem('newTicketFlag', 'false');
    localStorage.setItem('ticketData', JSON.stringify(item));
    this.router.navigate([url]);
  }
  // end of click on row

  // add new ticket
  addTicket() {
    localStorage.setItem(
      'ticketData',
      JSON.stringify({
        customer: '',
        ticket: '',
        type: {
          name: '',
          color: '',
        },
        reason: '',
        contactPreference: '',
        tracking: '',
        storeName: '',
        sellingChannel: '',
        receivedDate: '',
        submitedDate: '',
        documents: [],
        documentsMissing: [],
        status: {
          name: '',
          color: '',
        },
        subject: '',
        description: '',
        notifyMeForUpdates: false,
      })
    );
    localStorage.setItem('newTicketFlag', 'true');
    const url = '/customer/support/new-ticket';
    this.router.navigate([url]);
  }
  // end of add new ticket

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
