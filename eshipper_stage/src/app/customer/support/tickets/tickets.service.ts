import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TicketsService {
  constructor(private http: HttpClient) {}

  getTickets(): Observable<any> {
    return this.http
      .get('content/data/support/tickets.json')
      .pipe(map(data => data));
  }
}
