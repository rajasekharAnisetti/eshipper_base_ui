import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModules } from '../../../material-modules';

import { PendingRatesSpecialtyRequestsModule } from './pending-rates-specialty-requests/pending-rates-specialty-requests.module';
import { PendingRatesBatchInnerModule } from './pending-rates-batch-inner/pending-rates-batch-inner.module';
import { PendingRatesBatchModule } from './pending-rates-batch/pending-rates-batch.module';

import { PendingRatesComponent } from './pending-rates.component';

@NgModule({
  declarations: [PendingRatesComponent],
  imports: [
    CommonModule,
    MaterialModules,
    PendingRatesSpecialtyRequestsModule,
    PendingRatesBatchInnerModule,
    PendingRatesBatchModule,
  ],
  exports: [PendingRatesComponent],
})
export class PendingRatesModule {}
