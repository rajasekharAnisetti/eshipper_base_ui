import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingRatesBatchComponent } from './pending-rates-batch.component';

describe('PendingRatesBatchComponent', () => {
  let component: PendingRatesBatchComponent;
  let fixture: ComponentFixture<PendingRatesBatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingRatesBatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingRatesBatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
