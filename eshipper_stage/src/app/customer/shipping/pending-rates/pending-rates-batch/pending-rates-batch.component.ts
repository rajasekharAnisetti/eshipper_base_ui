import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { ApprovedDialogComponent } from 'src/app/dialogs/approved-dialog/approved-dialog.component';
import { PrintDialogComponent } from 'src/app/dialogs/print-dialog/print-dialog.component';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';
import {
  SIMPLESETTINGS,
  BATCHITEMSETTINGS,
} from 'src/app/shared/const/dropdowns';

import { PendingRatesBatchService } from './pending-rates-batch.service';

@Component({
  selector: 'app-pending-rates-batch',
  templateUrl: './pending-rates-batch.component.html',
  styleUrls: ['./pending-rates-batch.component.scss'],
})
export class PendingRatesBatchComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  disableShip = true;
  selection = false;
  batchEmpty = false;
  selectAll = false;
  toolbarIndeterminate = false;
  pendingRatesBatches: Array<any>;
  toolbarSettings = SIMPLESETTINGS;
  itemSettings = BATCHITEMSETTINGS;

  constructor(
    private getData: PendingRatesBatchService,
    public dialog: MatDialog,
    public router: Router
  ) { }

  ngOnInit() {
    // get batches
    this.subscribe.add(
      this.getData.getPendingRatesBatches().subscribe(data => {
        this.batchEmpty = data.content.length ? false : true;
        data.content
          ? (this.pendingRatesBatches = data.content)
          : console.log('Error!');
      })
    );
  }

  // filter
  filter() { }
  // end of filter

  // selecting
  selectAllFunction(flag) {
    const select = flag ? true : false;
    this.selection = select ? true : false;
    for (const i of this.pendingRatesBatches) {
      i.selected = select;
    }
  }

  selectRow() {
    const n = this.pendingRatesBatches.length;
    let item = 0;
    let disableStatus = 0;

    for (const i of this.pendingRatesBatches) {
      if (i.selected) {
        item++;
        if (i.status.name === 'Rate Error' || i.status.name === 'Processing') {
          disableStatus++;
        }
      }
    }
    this.disableShip = disableStatus || !item ? true : false;
    this.selectAll = n === item ? true : false;
    this.toolbarIndeterminate = n > item && item !== 0 ? true : false;
    this.selection = this.toolbarIndeterminate || this.selectAll ? true : false;
  }
  // end of selecting

  shipFunction() {
    const dialogRef = this.dialog.open(ApprovedDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Shipment Submitted',
        content: 'Please check on the tracking sections for the status.',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }

  searchFunction(val) {
    console.log(val);
  }

  settingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print':
        const printDialogRef = this.dialog.open(PrintDialogComponent, {
          autoFocus: false,
          panelClass: 'small-dialog-width',
        });
        this.subscribe.add(
          printDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }

  getItemSettings(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print':
        const printDialogRef = this.dialog.open(PrintDialogComponent, {
          autoFocus: false,
          panelClass: 'small-dialog-width',
        });
        this.subscribe.add(
          printDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }

  // click on row
  clickOnRow() {
    const url = '/customer/shipping/pending-rates-batch-inner';
    this.router.navigate([url]);
  }
  // end of click on row

  // total info dialog
  openTotalDetailsDialog(data) {
    this.dialog.open(InfoDialogComponent, {
      data: {
        title: 'Charge Details',
        moreData: data,
      },
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
  }
  // end of total info dialog

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
