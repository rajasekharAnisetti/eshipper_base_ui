import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PendingRatesBatchService {
  constructor(private http: HttpClient) {}

  getPendingRatesBatches(): Observable<any> {
    return this.http
      .get('../../content/data/pending-rates-batches.json')
      .pipe(map(data => data));
  }
}
