import { TestBed } from '@angular/core/testing';

import { PendingRatesBatchService } from './pending-rates-batch.service';

describe('PendingRatesBatchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PendingRatesBatchService = TestBed.get(PendingRatesBatchService);
    expect(service).toBeTruthy();
  });
});
