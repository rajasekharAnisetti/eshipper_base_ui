import { TestBed } from '@angular/core/testing';

import { PendingRatesBatchInnerService } from './pending-rates-batch-inner.service';

describe('PendingRatesBatchInnerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PendingRatesBatchInnerService = TestBed.get(PendingRatesBatchInnerService);
    expect(service).toBeTruthy();
  });
});
