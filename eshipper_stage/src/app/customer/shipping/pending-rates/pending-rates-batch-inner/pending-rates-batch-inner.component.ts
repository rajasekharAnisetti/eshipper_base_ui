import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';
import { RatesBatchTrack } from 'src/app/interfaces/shipping';
import {
  RATESBATCHINNERTOOLBARSETTINGS,
  RATESBATCHINNERITEMSETTINGS,
} from 'src/app/shared/const/dropdowns';

import { PendingRatesBatchInnerService } from './pending-rates-batch-inner.service';
import { ApprovedDialogComponent } from 'src/app/dialogs/approved-dialog/approved-dialog.component';

@Component({
  selector: 'app-pending-rates-batch-inner',
  templateUrl: './pending-rates-batch-inner.component.html',
  styleUrls: ['./pending-rates-batch-inner.component.scss'],
  providers: [PendingRatesBatchInnerService],
})
export class PendingRatesBatchInnerComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  @Input() selection = false;
  disableShip = true;
  dataEmpty = false;
  selectAll = false;
  toolbarIndeterminate = false;
  trackings: Array<RatesBatchTrack>;
  smallWidgetItems: Array<any>;
  toolbarSettings = RATESBATCHINNERTOOLBARSETTINGS;
  itemSettings = RATESBATCHINNERITEMSETTINGS;

  constructor(
    private getData: PendingRatesBatchInnerService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    // get list data
    this.subscribe.add(
      this.getData.getPendingRatesBatchInner().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        data.content ? (this.trackings = data.content) : console.log('Error!');
      })
    );
    // get small widgets
    this.subscribe.add(
      this.getData
        .getBatchSmallWidgets()
        .subscribe(data =>
          data
            ? (this.smallWidgetItems = data)
            : console.log('Data File Error!!!')
        )
    );
  }

  // search 
  searchFunction(val) {
    console.log(val);
  }
  // end of search

  // period
  selectPeriod(val) {
    console.log(val)
  }
  // end fo period

  // sort function
  getSortData(item) {
    console.log(item)
  }
  // end of sort function

  // filter
  filter() { }
  // end of filter

  settingsFunction(val) {
    console.log(val);
  }

  getItemSettings(val) {
    console.log(val);
  }

  // select functions
  selectAllFunction(flag) {
    const select = flag ? true : false;
    this.selection = select ? true : false;
    for (const i of this.trackings) {
      i.selected = select;
    }
  }

  selectRow() {
    const n = this.trackings.length;
    let item = 0;
    let disableStatus = 0;

    for (const i of this.trackings) {
      if (i.selected) {
        item++;
        if (i.status.name === 'Rate Error' || i.status.name === 'Processing') {
          disableStatus++;
        }
      }
    }
    this.disableShip = disableStatus || !item ? true : false;
    this.selectAll = n === item ? true : false;
    this.toolbarIndeterminate = n > item && item !== 0 ? true : false;
    this.selection = this.toolbarIndeterminate || this.selectAll ? true : false;
  }
  // end of select functions

  // open details
  openCostDetailsDialog(data) {
    this.dialog.open(InfoDialogComponent, {
      data: {
        title: 'Charge Details',
        moreData: data,
      },
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
  }
  // end of open details

  // item settings
  batchSettingsFunction(item) {
    console.log(item)
  }
  // end of item settings

  // ship function
  shipFunction() {
    const dialogRef = this.dialog.open(ApprovedDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Shipment Submitted',
        content: 'Please check on the tracking sections for the status.',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of ship function



  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
