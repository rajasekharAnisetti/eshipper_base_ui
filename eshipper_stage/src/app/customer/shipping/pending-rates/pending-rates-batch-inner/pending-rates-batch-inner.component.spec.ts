import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingRatesBatchInnerComponent } from './pending-rates-batch-inner.component';

describe('PendingRatesBatchInnerComponent', () => {
  let component: PendingRatesBatchInnerComponent;
  let fixture: ComponentFixture<PendingRatesBatchInnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingRatesBatchInnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingRatesBatchInnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
