import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable()
export class PendingRatesBatchInnerService {
  constructor(private http: HttpClient) {}

  getPendingRatesBatchInner(): Observable<any> {
    return this.http
      .get('../../../../content/data/pending-rates-batch-inner.json')
      .pipe(map(data => data));
  }

  getBatchSmallWidgets(): Observable<any> {
    return this.http
      .get('../../../../content/data/batch-small-widgets.json')
      .pipe(map(data => data));
  }
}
