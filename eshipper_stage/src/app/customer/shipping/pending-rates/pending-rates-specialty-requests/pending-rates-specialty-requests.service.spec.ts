import { TestBed } from '@angular/core/testing';

import { PendingRatesSpecialtyRequestsService } from './pending-rates-specialty-requests.service';

describe('PendingRatesSpecialtyRequestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PendingRatesSpecialtyRequestsService = TestBed.get(PendingRatesSpecialtyRequestsService);
    expect(service).toBeTruthy();
  });
});
