import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingRatesSpecialtyRequestsComponent } from './pending-rates-specialty-requests.component';

describe('PendingRatesSpecialtyRequestsComponent', () => {
  let component: PendingRatesSpecialtyRequestsComponent;
  let fixture: ComponentFixture<PendingRatesSpecialtyRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingRatesSpecialtyRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingRatesSpecialtyRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
