import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { PrintDialogComponent } from 'src/app/dialogs/print-dialog/print-dialog.component';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { SIMPLESETTINGS } from 'src/app/shared/const/dropdowns';
import { RejectDialogComponent } from 'src/app/dialogs/reject-dialog/reject-dialog.component';
import { ApprovedDialogComponent } from 'src/app/dialogs/approved-dialog/approved-dialog.component';

import { PendingRatesSpecialtyRequestsService } from './pending-rates-specialty-requests.service';
import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';

@Component({
  selector: 'app-pending-rates-specialty-requests',
  templateUrl: './pending-rates-specialty-requests.component.html',
  styleUrls: ['./pending-rates-specialty-requests.component.scss'],
})
export class PendingRatesSpecialtyRequestsComponent
  implements OnInit, OnDestroy {
  srStatuses = ['Accept', 'Reject'];
  subscribe = new Subscription();
  dataEmpty = false;
  toolbarSettings = SIMPLESETTINGS;
  specialtyRequests: Array<any>;
  itemSettings = ['Edit', 'Cancel'];

  constructor(
    private getData: PendingRatesSpecialtyRequestsService,
    private router: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    // get list data
    this.subscribe.add(
      this.getData.getPendingRatesSpecialtyRequests().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        data.content
          ? (this.specialtyRequests = data.content)
          : console.log('Error!');
      })
    );
  }

  searchFunction(val) {
    console.log(val);
  }

  // filter
  filter() { }
  // end of filter

  // sort function
  getSortData(val) {
    console.log(val)
  }
  // end of sort function

  settingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print':
        const printDialogRef = this.dialog.open(PrintDialogComponent, {
          autoFocus: false,
          panelClass: 'small-dialog-width',
        });
        this.subscribe.add(
          printDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      default:
        break;
    }
  }

  getItemSettingsValue(val) {
    switch (val) {
      case 'Edit':
        {
          this.router.navigate(['/customer/shipping/specialty-requests']);
        }
        break;
      default:
        break;
    }
  }

  // status function
  statusFunction(status, item) {
    switch (status) {
      case 'Accept':
        {
          const dialogRef = this.dialog.open(ApprovedDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              title: 'Quote Accepted',
              content: 'We will process the shipment shortly.',
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
                item.status = {
                  name: 'Accept',
                  color: 'green',
                };
                console.log(item);
              }
            })
          );
        }
        break;
      case 'Reject':
        {
          const dialogRef = this.dialog.open(RejectDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
                item.status = {
                  name: 'Reject',
                  color: 'red',
                };
                console.log(item);
              }
            })
          );
        }
        break;
      default:
        break;
    }
  }
  // end of status function

  // click on row
  clickOnRow(item) {
    console.log(item);
  }
  // end of click on row

  // open quote details dialog
  openQuoteDetailsDialog(data) {
    this.dialog.open(InfoDialogComponent, {
      data: {
        title: 'Quote Details',
        moreData: data,
      },
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
  }
  // nd of open quote details dialog

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
