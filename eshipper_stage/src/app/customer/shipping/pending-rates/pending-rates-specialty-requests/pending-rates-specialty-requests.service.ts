import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PendingRatesSpecialtyRequestsService {
  constructor(private http: HttpClient) {}

  getPendingRatesSpecialtyRequests(): Observable<any> {
    return this.http
      .get('../../content/data/pending-rates-specialty-requests.json')
      .pipe(map(data => data));
  }
}
