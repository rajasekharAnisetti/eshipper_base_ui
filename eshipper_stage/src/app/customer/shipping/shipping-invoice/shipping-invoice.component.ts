import { ChangeDetectorRef, Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { MediaMatcher } from '@angular/cdk/layout';
import { Subscription } from 'rxjs';

import { DetailsData } from 'src/app/interfaces/details-data';
import { AddressBookDialogComponent } from 'src/app/dialogs/address-book-dialog/address-book-dialog.component';
import { EditContactDialogComponent } from 'src/app/dialogs/edit-contact-dialog/edit-contact-dialog.component';
import { CURRENCIES_$ } from 'src/app/shared/const/currencies';

import { ShippingInvoiceService } from './shipping-invoice.service';
import { QtyMatcher } from 'src/app/shared/classes/validation-class';

@Component({
  selector: 'app-shipping-invoice',
  templateUrl: './shipping-invoice.component.html',
  styleUrls: ['./shipping-invoice.component.scss'],
})
export class ShippingInvoiceComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  // data for steps
  shipSteps = [];
  stepsCreate = [
    {
      position: 'Details',
      state: true,
      link: '/customer/shipping/ship/details',
    },
    {
      position: 'Rate',
      state: true,
      link: '/customer/shipping/ship/rate',
    },
    {
      position: 'Invoice',
      state: false,
    },
    {
      position: 'Summary',
      state: false,
    },
  ];
  stepsEdit = [
    {
      position: 'Details',
      state: true,
      link: '/shipping/ship/edit-details',
    },
    {
      position: 'Rate',
      state: true,
      link: '/shipping/ship/edit-rate',
    },
    {
      position: 'Invoice',
      state: true,
      link: '/shipping/ship/edit-invoice',
    },
    {
      position: 'Summary',
      state: false,
    },
  ];

  // data for taxt card
  dutiables = [
    {
      value: 'No',
      viewValue: 'No',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];

  billToList = [
    {
      value: 'Receiver',
      viewValue: 'Receiver',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];
  productLists = [
    {
      value: 'Product 1',
      viewValue: 'Product 1',
    },
    {
      value: 'Product 2',
      viewValue: 'Product 2',
    },
    {
      value: 'Product 3',
      viewValue: 'Product 3',
    },
  ];

  taxes = {
    dutiable: '',
    dutiableFormControl: new FormControl('', [Validators.required]),
    billTo: '',
    billToFormControl: new FormControl('', [Validators.required]),
    account: '',
    sed: '',
  };

  // data for billong card
  billing = {
    companyName: 'MacAndrews & Forbes Holdings',
    address1: '350-80 St Marks Pl',
    address2: '',
    city: 'New York',
    province: 'NY',
    zip: '10003',
    country: 'USA',
    attention: '',
    phone: '',
    email: '',
    taxId: '',
  };

  contact = {
    companyName: 'MacAndrews & Forbes Holdings',
    attention: 'Marian Gray',
    phone: '641-146-0234',
    email: 'matteo.krajcik@reynolds.io',
    taxId: '',
    brokerName: '',
    recipientTaxId: '',
  };

  additionalContact = {
    tax: {
      label: 'TAX ID',
      value: '0000000000',
    },
    broker: {
      label: 'Broker Name',
      value: 'Theodore Wilkerson',
    },
    recipient: {
      label: 'Recipient Tax ID',
      value: '0000000000',
    },
  };

  // data for product card
  currencyValue = 'CAD $';
  currencyItems = CURRENCIES_$;
  qtyMatcher = new QtyMatcher();
  products = [
    {
      productList: '',
      productListFormControl: new FormControl(),
      description: '',
      descriptionFormControl: new FormControl(),
      hsCode: '',
      hsCodeFormControl: new FormControl(),
      hsCodeTooltip: 'Tooltip',
      qty: '',
      qtyFormControl: new FormControl(),
      unit: '',
      unitFormControl: new FormControl(),
      price: '',
      priceFormControl: new FormControl(),
      removable: false,
    },
  ];
  total = 0;

  // sidebar data
  invoiceDetails = {
    logo: 'content/images/logos/ups.png',
    service: {
      label: 'Service',
      value: 'Next Flight Out',
      strong: true,
    },
    quote: {
      label: 'Quote',
      value: '$567.88',
      strong: true,
    },
  };
  detailsData: DetailsData;

  // edit mode
  urlData: any;
  editMode = false;
  // end of edit mode

  constructor(
    public dialog: MatDialog,
    private getData: ShippingInvoiceService,
    private router: Router,
    private routerData: ActivatedRoute,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 1024px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    this.subscribe.add(
      this.getData
        .getShipDetailsSidebar()
        .subscribe(data => (this.detailsData = data))
    );

    this.contact.taxId = this.additionalContact.tax.value;
    this.contact.brokerName = this.additionalContact.broker.value;
    this.contact.recipientTaxId = this.additionalContact.recipient.value;

    // get edit mode flag
    this.subscribe.add(
      (this.urlData = this.routerData.data.subscribe(data => {
        if (data) {
          this.editMode = data.editMode;
        }
        this.shipSteps = this.editMode ? this.stepsEdit : this.stepsCreate;
      }))
    );
    // end of get edit mode flag
  }

  currencyChange = val => (this.currencyValue = val);

  editBilling() {
    const dialogRef = this.dialog.open(AddressBookDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Edit Billing',
        fields: this.billing,
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.billing = result;
        }
      })
    );
  }

  addRow() {
    this.products.push({
      productList: '',
      productListFormControl: new FormControl(),
      description: '',
      descriptionFormControl: new FormControl(),
      hsCode: '',
      hsCodeFormControl: new FormControl(),
      hsCodeTooltip: 'Tooltip',
      qty: '',
      qtyFormControl: new FormControl(),
      unit: '',
      unitFormControl: new FormControl(),
      price: '',
      priceFormControl: new FormControl(),
      removable: true,
    });
  }
  removeRow(index) {
    this.products.splice(index, 1);
  }

  editContact() {
    const dialogRef = this.dialog.open(EditContactDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        fields: this.contact,
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.contact = result;

          this.additionalContact.tax.value = result.taxId;
          this.additionalContact.broker.value = result.brokerName;
          this.additionalContact.recipient.value = result.recipientTaxId;
        }
      })
    );
  }

  processingFunction() {
    localStorage.setItem('processing', '/customer/shipping/ship/confirmed');
    this.router.navigate(['/processing']);
  }

  ngOnDestroy() {
    this.mobileQuery.removeListener(this._mobileQueryListener);
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
