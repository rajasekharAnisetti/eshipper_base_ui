import { TestBed } from '@angular/core/testing';

import { ShippingInvoiceService } from './shipping-invoice.service';

describe('ShippingInvoiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ShippingInvoiceService = TestBed.get(ShippingInvoiceService);
    expect(service).toBeTruthy();
  });
});
