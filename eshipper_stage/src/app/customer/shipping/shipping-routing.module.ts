import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShipRateComponent } from './ship/ship-rate/ship-rate.component';
import { ShipDetailsComponent } from './ship/ship-details/ship-details.component';
import { OrderConfirmedComponent } from './order-confirmed/order-confirmed.component';
import { ShippingInvoiceComponent } from './shipping-invoice/shipping-invoice.component';
import { ShippingComponent } from './shipping.component';
import { QuickQuoteComponent } from './quick-quote/quick-quote.component';
import { QuickQuoteProceedComponent } from './quick-quote/quick-quote-proceed/quick-quote-proceed.component';
import { QuickQouteProceedDomesticComponent } from './quick-quote/quick-qoute-proceed-domestic/quick-qoute-proceed-domestic.component';
import { ShipComponent } from './ship/ship.component';
import { BatchShippingComponent } from './batch-shipping/batch-shipping.component';
import { BatchPendingInnerComponent } from './batch-shipping/batch-pending-inner/batch-pending-inner.component';
import { SpecialtyRequestsComponent } from './specialty-requests/specialty-requests.component';
import { SpecialtyRequestsConfirmedComponent } from './specialty-requests-confirmed/specialty-requests-confirmed.component';
import { PendingRatesComponent } from './pending-rates/pending-rates.component';
import { PendingRatesBatchInnerComponent } from './pending-rates/pending-rates-batch-inner/pending-rates-batch-inner.component';
import { SavedQuotesComponent } from './saved-quotes/saved-quotes.component';

const shippingRoutes: Routes = [
  {
    path: '',
    redirectTo: 'quick-quote',
    pathMatch: 'full',
  },
  {
    path: 'quick-quote',
    component: QuickQuoteComponent,
    data: { title: 'Quick Quote' },
  },
  {
    path: 'quick-quote-proceed',
    component: QuickQuoteProceedComponent,
    data: { title: 'Quick Quote' },
  },
  {
    path: 'quick-quote-confirmed',
    component: OrderConfirmedComponent,
    data: { title: 'Quick Quote' },
  },
  {
    path: 'quick-quote-proceed-domestic',
    component: QuickQouteProceedDomesticComponent,
    data: { title: 'Quick Quote' },
  },
  {
    path: 'ship',
    component: ShipComponent,
    data: { title: 'Ship' },
    children: [
      {
        path: '',
        redirectTo: 'details',
        pathMatch: 'full',
      },
      {
        path: 'details',
        component: ShipDetailsComponent,
        data: { title: 'Ship' },
      },
      {
        path: 'rate',
        component: ShipRateComponent,
        data: { title: 'Ship' },
      },
      {
        path: 'invoice',
        component: ShippingInvoiceComponent,
        data: { title: 'Ship' },
      },
      {
        path: 'confirmed',
        component: OrderConfirmedComponent,
        data: { title: 'Ship' },
      },
      // edit mode
      {
        path: 'edit-details',
        component: ShipDetailsComponent,
        data: {
          title: 'Ship # 123456',
          backBtn: true,
          editMode: true,
        },
      },
      {
        path: 'edit-rate',
        component: ShipRateComponent,
        data: {
          title: 'Ship # 123456',
          backBtn: true,
          editMode: true,
        },
      },
      {
        path: 'edit-invoice',
        component: ShippingInvoiceComponent,
        data: {
          title: 'Ship # 123456',
          backBtn: true,
          editMode: true,
        },
      },
    ],
  },
  {
    path: 'batch-shipping',
    component: BatchShippingComponent,
    data: { title: 'Batch Shipping' },
  },
  {
    path: 'pending-batch',
    component: BatchPendingInnerComponent,
    data: {
      title: 'Batch #1234567890',
      backBtn: true,
    },
  },
  {
    path: 'specialty-requests',
    component: SpecialtyRequestsComponent,
    data: { title: 'Specialty Requests' },
  },
  {
    path: 'specialty-requests-confirmed',
    component: SpecialtyRequestsConfirmedComponent,
    data: { title: 'Specialty Requests' },
  },
  {
    path: 'pending-rates',
    component: PendingRatesComponent,
    data: { title: 'Pending Rates' },
  },
  {
    path: 'pending-rates-batch-inner',
    component: PendingRatesBatchInnerComponent,
    data: {
      title: 'Batch #1234567890',
      backBtn: true,
    },
  },
  {
    path: 'saved-quotes',
    component: SavedQuotesComponent,
    data: { title: 'Saved Quotes' },
  },
];

const shipping: Routes = [
  {
    path: '',
    component: ShippingComponent,
    data: { title: 'Shipping' },
    children: shippingRoutes,
  },
];

@NgModule({
  imports: [RouterModule.forChild(shipping)],
  exports: [RouterModule],
})
export class ShippingRoutingModule {}
