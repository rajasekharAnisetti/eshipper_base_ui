import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { ActivatedRoute } from '@angular/router';

import { ShipDetailsService } from './ship-details.service';

import { AddressBookDialogComponent } from 'src/app/dialogs/address-book-dialog/address-book-dialog.component';
import { LocationData } from 'src/app/interfaces/location';

@Component({
  selector: 'app-ship-details',
  templateUrl: './ship-details.component.html',
  styleUrls: ['./ship-details.component.scss'],
})
export class ShipDetailsComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  @ViewChild('fromSpecialInstructions, toSpecialInstructions', {
    static: false,
  })
  autosize: CdkTextareaAutosize;
  fromFilteredLocations: Observable<LocationData[]>;
  toFilteredLocations: Observable<LocationData[]>;
  // steps
  shipSteps = [];
  stepsCreate = [
    {
      position: 'Details',
      state: false,
    },
    {
      position: 'Rate',
      state: false,
    },
    {
      position: 'Summary',
      state: false,
    },
  ];
  stepsEdit = [
    {
      position: 'Details',
      state: true,
      link: '/customer/shipping/ship/edit-details',
    },
    {
      position: 'Rate',
      state: true,
      link: '/customer/shipping/ship/edit-rate',
    },
    {
      position: 'Summary',
      state: false,
    },
  ];
  //

  // locations
  locations = [];

  workingData = {
    from: {
      values: {
        value: '',
        specialInstructions: '',
        data: {
          companyName: '',
          address1: '',
          address2: '',
          city: '',
          province: '',
          zip: '',
          country: '',
          attention: '',
          phone: '',
          email: '',
          taxId: '',
        },
      },
      locationControl: new FormControl(),
      checkboxes: [
        {
          checked: false,
          label: 'Residential',
        },
        {
          checked: false,
          label: 'Return',
        },
        {
          checked: false,
          label: 'Notify',
        },
      ],
    },
    to: {
      values: {
        value: '',
        specialInstructions: '',
        data: {
          companyName: '',
          address1: '',
          address2: '',
          city: '',
          province: '',
          zip: '',
          country: '',
          attention: '',
          phone: '',
          email: '',
          taxId: '',
        },
      },
      locationControl: new FormControl(),
      checkboxes: [
        {
          checked: false,
          label: 'Residential',
        },
        {
          checked: false,
          label: 'Notify',
        },
      ],
    },
  };

  fromFlag = false;
  toFlag = false;

  // edit mode
  urlData: any;
  editMode = false;
  // end of edit mode

  constructor(
    public dialog: MatDialog,
    private routerData: ActivatedRoute,
    private getData: ShipDetailsService
  ) {}

  ngOnInit() {
    this.subscribe.add(
      this.getData.getShippingLocations().subscribe(data => {
        this.locations = data;
        this.initAutocomplete();
        this.initFromData(data);
      })
    );

    // get edit mode flag
    this.subscribe.add(
      (this.urlData = this.routerData.data.subscribe(data => {
        if (data) {
          this.editMode = data.editMode;
        }
        this.shipSteps = this.editMode ? this.stepsEdit : this.stepsCreate;
      }))
    );
    // end of get edit mode flag
  }

  private _fromFilterStates(value: string): LocationData[] {
    const filterValue = value.toLowerCase();
    this.workingData.from.values.data = this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    )[0];
    return this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    );
  }

  private _toFilterStates(value: string): LocationData[] {
    const filterValue = value.toLowerCase();
    this.workingData.to.values.data = this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    )[0];
    return this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    );
  }

  initAutocomplete() {
    this.fromFilteredLocations = this.workingData.from.locationControl.valueChanges.pipe(
      startWith(''),
      map(location =>
        location ? this._fromFilterStates(location) : this.locations.slice()
      )
    );
    this.toFilteredLocations = this.workingData.to.locationControl.valueChanges.pipe(
      startWith(''),
      map(location =>
        location ? this._toFilterStates(location) : this.locations.slice()
      )
    );
  }

  initFromData = data => {
    this.workingData.from.values.data = data[0];
    this.workingData.from.values.value = data[0].companyName;
    this.fromFlag = true;
  };

  newLocation(val) {
    const dialogRef = this.dialog.open(AddressBookDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'New Location',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          switch (val) {
            case 'from':
              this.locations.push(result);
              this.workingData.from.values.data = result;
              break;
            case 'to':
              this.locations.push(result);
              this.workingData.to.values.data = result;
              break;
            default:
              break;
          }
          this.initAutocomplete();
        }
      })
    );
  }

  editAddress() {
    const dialogRef = this.dialog.open(AddressBookDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Edit Address',
        fields: this.workingData.from.values.data,
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.workingData.from.values.data = result;
        }
      })
    );
  }

  changePosition() {
    const values = this.workingData.from.values;
    this.workingData.from.values = this.workingData.to.values;
    this.workingData.to.values = values;
  }

  // change flom location field
  changeFrom() {
    this.fromFlag = false;
    for (const item of this.locations) {
      if (this.workingData.from.values.value === item.companyName) {
        this.fromFlag = true;
      }
    }
  }
  // change To location field
  changeTo() {
    this.toFlag = false;
    for (const item of this.locations) {
      if (this.workingData.to.values.value === item.companyName) {
        this.toFlag = true;
      }
    }
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
