import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ShipDetailsService {
  constructor(private http: HttpClient) {}

  getShippingLocations(): Observable<any> {
    return this.http
      .get('content/data/shipping-locations.json')
      .pipe(map(data => data));
  }
}
