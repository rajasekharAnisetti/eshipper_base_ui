import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShipRateComponent } from './ship-rate.component';

describe('ShipRateComponent', () => {
  let component: ShipRateComponent;
  let fixture: ComponentFixture<ShipRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShipRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
