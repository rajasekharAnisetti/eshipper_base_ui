import { ChangeDetectorRef, Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MediaMatcher } from '@angular/cdk/layout';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { Rate } from 'src/app/interfaces/shipping';
import { DetailsData } from 'src/app/interfaces/details-data';
import { TextDialogComponent } from 'src/app/dialogs/text-dialog/text-dialog.component';

import { ShipRateService } from './ship-rate.service';
import { sortAscDesc } from 'src/app/shared/functions/sort-asc-desc';

@Component({
  selector: 'app-ship-rate',
  templateUrl: './ship-rate.component.html',
  styleUrls: ['./ship-rate.component.scss'],
})
export class ShipRateComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;

  shipSteps = [];
  stepsCreate = [
    {
      position: 'Details',
      state: true,
      link: '/customer/shipping/ship/details',
    },
    {
      position: 'Rate',
      state: false,
    },
    {
      position: 'Invoice',
      state: false,
    },
    {
      position: 'Summary',
      state: false,
    },
  ];
  stepsEdit = [
    {
      position: 'Details',
      state: true,
      link: '/customer/shipping/ship/edit-details',
    },
    {
      position: 'Rate',
      state: true,
      link: '/customer/shipping/ship/edit-rate',
    },
    {
      position: 'Invoice',
      state: true,
      link: '/customer/shipping/ship/edit-invoice',
    },
    {
      position: 'Summary',
      state: false,
    },
  ];

  sortMenu = ['Carrier', 'Est. Days', 'Price'];
  rates: Rate[] = [];
  detailsData: DetailsData;

  // edit mode
  urlData: any;
  editMode = false;
  // end of edit mode

  constructor(
    private getData: ShipRateService,
    public dialog: MatDialog,
    private routerData: ActivatedRoute,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 1024px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    this.subscribe.add(
      this.getData.getRates().subscribe(data => (this.rates = data))
    );
    this.subscribe.add(
      this.getData
        .getShipDetailsSidebar()
        .subscribe(data => (this.detailsData = data))
    );

    // get edit mode flag
    this.subscribe.add(
      (this.urlData = this.routerData.data.subscribe(data => {
        if (data) {
          this.editMode = data.editMode;
        }
        this.shipSteps = this.editMode ? this.stepsEdit : this.stepsCreate;
      }))
    );
    // end of get edit mode flag
  }

  // sort data
  getSortData(val) {
    let sortKey = '';
    switch (val.name) {
      case 'Carrier':
        sortKey = 'title'
        break;
      case 'Est. Days':
        sortKey = 'date'
        break;
      case 'Price':
        sortKey = 'value'
        break;
      default:
        break;
    }
    sortAscDesc(this.rates, val.direction, sortKey)
  }
  // end of sort data

  selectRate(item) {
    if (item.selected) {
      item.selected = false;
    } else {
      for (const n of this.rates) {
        n.selected = false;
        item.selected = true;
      }
    }
    console.log(item);
  }

  openTextDialog() {
    const dialogRef = this.dialog.open(TextDialogComponent, {
      autoFocus: false,
      panelClass: 'normal-dialog-width',
      data: {
        title: 'Disclaimer',
        text:
          'Living in today’s metropolitan world of cellular phones, mobile computers and other high-tech gadgets is not just hectic but very impersonal.' +
          'We make money and then invest our time and effort in making more money. Does it end? Not usually because we are never satisfied. How many times ' +
          'have we convinced ourselves that if only we had some more money, life would be so sweet? But then, after receiving a substantial raise, we ' +
          'realize that it wasn’t enough and that we need more?\n\n' +
          'What Should You Do? \n\n' +
          'I have read many books on life such as Robin Sharma’s Monk says this and the monk says that, and they all seem to say that money is not ' +
          'necessary. But it is. Can you do without cash and a lot of it? I know I can’t. So, I went to the neighbourhood Rabbi and asked for advice ' +
          'that will help me find my true way in life. The rabbi nodded and took me to the window. “What do you see?” he asked me.',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }

  ngOnDestroy() {
    this.mobileQuery.removeListener(this._mobileQueryListener);
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
