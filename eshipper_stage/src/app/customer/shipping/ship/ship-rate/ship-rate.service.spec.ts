import { TestBed } from '@angular/core/testing';

import { ShipRateService } from './ship-rate.service';

describe('ShipRateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ShipRateService = TestBed.get(ShipRateService);
    expect(service).toBeTruthy();
  });
});
