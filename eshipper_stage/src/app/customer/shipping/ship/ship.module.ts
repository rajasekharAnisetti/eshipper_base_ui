import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { ShipRateModule } from './ship-rate/ship-rate.module';
import { ShipDetailsModule } from './ship-details/ship-details.module';

import { ShipComponent } from './ship.component';

@NgModule({
  declarations: [ShipComponent],
  imports: [CommonModule, ShipRateModule, ShipDetailsModule, RouterModule],
  exports: [ShipComponent],
})
export class ShipModule {}
