import { TestBed } from '@angular/core/testing';

import { OrderConfirmedService } from './order-confirmed.service';

describe('OrderConfirmedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OrderConfirmedService = TestBed.get(OrderConfirmedService);
    expect(service).toBeTruthy();
  });
});
