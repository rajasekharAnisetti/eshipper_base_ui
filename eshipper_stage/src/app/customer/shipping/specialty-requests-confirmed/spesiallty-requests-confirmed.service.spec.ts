import { TestBed } from '@angular/core/testing';

import { SpesialltyRequestsConfirmedService } from './spesiallty-requests-confirmed.service';

describe('SpesialltyRequestsConfirmedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SpesialltyRequestsConfirmedService = TestBed.get(SpesialltyRequestsConfirmedService);
    expect(service).toBeTruthy();
  });
});
