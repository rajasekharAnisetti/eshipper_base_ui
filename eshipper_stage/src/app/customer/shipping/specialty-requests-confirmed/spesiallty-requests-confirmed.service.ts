import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SpesialltyRequestsConfirmedService {
  constructor(private http: HttpClient) {}

  getSpecialtyRequestsConfirmed(): Observable<any> {
    return this.http
      .get('content/data/specialty-requests-confirmed.json')
      .pipe(map(data => data));
  }
}
