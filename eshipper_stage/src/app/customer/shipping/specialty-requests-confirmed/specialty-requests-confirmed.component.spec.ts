import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialtyRequestsConfirmedComponent } from './specialty-requests-confirmed.component';

describe('SpecialtyRequestsConfirmedComponent', () => {
  let component: SpecialtyRequestsConfirmedComponent;
  let fixture: ComponentFixture<SpecialtyRequestsConfirmedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialtyRequestsConfirmedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialtyRequestsConfirmedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
