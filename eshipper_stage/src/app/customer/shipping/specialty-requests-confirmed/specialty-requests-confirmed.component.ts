import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { ConfirmedData } from 'src/app/interfaces/shipping';

import { SpesialltyRequestsConfirmedService } from './spesiallty-requests-confirmed.service';

@Component({
  selector: 'app-specialty-requests-confirmed',
  templateUrl: './specialty-requests-confirmed.component.html',
  styleUrls: ['./specialty-requests-confirmed.component.scss'],
})
export class SpecialtyRequestsConfirmedComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  confirmedData: ConfirmedData;

  constructor(
    private getData: SpesialltyRequestsConfirmedService,
  ) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData
        .getSpecialtyRequestsConfirmed()
        .subscribe(data => (this.confirmedData = data))
    );
  }

  // email function
  emailFunction() { }
  // end of email function

  // print function
  printFunction() { }
  // end of print function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
