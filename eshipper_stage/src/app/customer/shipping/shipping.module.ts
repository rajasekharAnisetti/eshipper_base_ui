import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShippingRoutingModule } from './shipping-routing.module';
import { SpecialtyRequestsConfirmedModule } from './specialty-requests-confirmed/spesiallty-requests-confirmed.module';
import { SpecialtyRequestsModule } from './specialty-requests/specialty-request.module';
import { ShippingInvoiceModule } from './shipping-invoice/shipping-invoice.module';
import { ShipModule } from './ship/ship.module';
import { SavedQuotesModule } from './saved-quotes/saved-quotes.module';
import { QuickQuoteModule } from './quick-quote/quick-quote.model';
import { PendingRatesModule } from './pending-rates/pending-rates.module';
import { OrderConfirmedModule } from './order-confirmed/order-confirmed.module';
import { BatchShippingModule } from './batch-shipping/batch-shipping.module';

import { ShippingComponent } from './shipping.component';

@NgModule({
  declarations: [ShippingComponent],
  imports: [
    CommonModule,
    ShippingRoutingModule,
    SpecialtyRequestsConfirmedModule,
    SpecialtyRequestsModule,
    ShippingInvoiceModule,
    ShipModule,
    SavedQuotesModule,
    QuickQuoteModule,
    PendingRatesModule,
    OrderConfirmedModule,
    BatchShippingModule,
  ],
  exports: [ShippingComponent],
})
export class ShippingModule {}
