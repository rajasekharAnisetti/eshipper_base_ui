import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { QuickQuoteProceedModule } from './quick-quote-proceed/quick-quote-proceed.module';
import { QuickQouteProceedDomesticModule } from './quick-qoute-proceed-domestic/quick-qoute-proceed-domestic.model';
import { QuickQuoteComponent } from './quick-quote.component';

@NgModule({
  declarations: [QuickQuoteComponent],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MaterialModules,
    QuickQuoteProceedModule,
    QuickQouteProceedDomesticModule,
    SharedModule,
  ],
  exports: [QuickQuoteComponent],
})
export class QuickQuoteModule {}
