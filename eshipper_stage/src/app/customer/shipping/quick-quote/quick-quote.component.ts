import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';

import { LocationData } from 'src/app/interfaces/location';
import { AddressBookDialogComponent } from 'src/app/dialogs/address-book-dialog/address-book-dialog.component';
import { RatesDialogComponent } from 'src/app/dialogs/rates-dialog/rates-dialog.component';
import { Countries } from 'src/app/shared/const/countries';

import { QuickQuoteService } from './quick-quote.service';

@Component({
  selector: 'app-quick-quote',
  templateUrl: './quick-quote.component.html',
  styleUrls: ['./quick-quote.component.scss'],
})
export class QuickQuoteComponent implements OnInit, OnDestroy {
  filteredCityes: Observable<any[]>;
  subscribe = new Subscription();
  autosize: CdkTextareaAutosize;
  fromFilteredLocations: Observable<LocationData[]>;
  toFilteredLocations: Observable<LocationData[]>;

  // locations
  locations = [];
  cities: Array<string>;
  countries: Array<string>;
  provinces: Array<string>;
  zipLabel: string;

  workingData = {
    from: {
      values: {
        value: '',
        data: {
          companyName: '',
          address1: '',
          address2: '',
          city: '',
          province: '',
          zip: '',
          country: '',
          attention: '',
          phone: '',
          email: '',
          taxId: '',
        },
      },
      locationControl: new FormControl(),
      checkboxes: [
        {
          checked: false,
          label: 'Residential',
        },
        {
          checked: false,
          label: 'Return',
        },
      ],
    },
    to: {
      values: {
        value: '',
        data: {
          companyName: '',
          address1: '',
          address2: '',
          city: '',
          province: '',
          zip: '',
          country: '',
          attention: '',
          phone: '',
          email: '',
          taxId: '',
        },
      },
      locationControl: new FormControl(),
      countryFormControl: new FormControl(),
      cityFormControl: new FormControl(),
      zipFormControl: new FormControl(),
      checkboxes: [
        {
          checked: false,
          label: 'Residential',
        },
      ],
    },
  };

  fromFlag = false;
  emptyData = {
    companyName: '',
    address1: '',
    address2: '',
    city: '',
    province: '',
    zip: '',
    country: '',
    attention: '',
    phone: '',
    email: '',
    taxId: '',
  };
  // qoute
  qoute: Object;

  constructor(public dialog: MatDialog, private getData: QuickQuoteService) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData.getShippingLocations().subscribe(data => {
        this.locations = data;
        this.initAutocomplete();
        this.initFromData(data);
      })
    );
    // countries
    this.countries = Countries.COUNTRIES;
    this.provinces = Countries.CANADAPROVINCES;
    this.cities = Countries.CANADACITIES;
    this.zipLabel = 'Postal';
    // for city
    this.filteredCityes = this.workingData.to.cityFormControl.valueChanges.pipe(
      startWith(''),
      map(city => city ? this._filter(city) : this.cities.slice())
    )
  }

  // city autocomplete
  displayFn(city?: string): string | undefined {
    return city ? city : undefined;
  }

  private _filter(city: string) {
    const filterValue = city.toLowerCase();

    return this.cities.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
  }
  // emd of city autocomplete

  changeCountry(country) {
    this.workingData.to.values.data.city = '';
    switch (country) {
      case 'Canada':
        this.cities = Countries.CANADACITIES;
        this.zipLabel = 'Postal';
        break;
      case 'USA':
        this.cities = Countries.USACITIES;
        this.zipLabel = 'Zip';
        break;
      default:
        break;
    }
  }

  private _fromFilterStates(value: string): LocationData[] {
    const filterValue = value.toLowerCase();
    this.workingData.from.values.data = this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    )[0];
    return this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    );
  }

  private _toFilterStates(value: string): LocationData[] {
    const filterValue = value.toLowerCase();
    const filteredVal = this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    );
    this.workingData.to.values.data = filteredVal.length
      ? filteredVal[0]
      : [this.emptyData];
    return this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    );
  }

  initFromData = data => {
    this.workingData.from.values.data = data[0];
    this.workingData.from.values.value = data[0].companyName;
    this.fromFlag = true;
  };

  initAutocomplete() {
    this.fromFilteredLocations = this.workingData.from.locationControl.valueChanges.pipe(
      startWith(''),
      map(location =>
        location ? this._fromFilterStates(location) : this.locations.slice()
      )
    );
    this.toFilteredLocations = this.workingData.to.locationControl.valueChanges.pipe(
      startWith(''),
      map(location =>
        location ? this._toFilterStates(location) : this.locations.slice()
      )
    );
  }

  newLocation(val) {
    const dialogRef = this.dialog.open(AddressBookDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'New Location',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          switch (val) {
            case 'from':
              this.locations.push(result);
              this.workingData.from.values.data = result;
              break;
            case 'to':
              this.locations.push(result);
              this.workingData.to.values.data = result;
              break;
            default:
              break;
          }
          this.initAutocomplete();
        }
      })
    );
  }

  editAddress() {
    const dialogRef = this.dialog.open(AddressBookDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Edit Address',
        fields: this.workingData.from.values.data,
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.workingData.from.values.data = result;
        }
      })
    );
  }

  changePosition() {
    // const values = this.workingData.from.values;
    // this.workingData.from.values = this.workingData.to.values;
    // this.workingData.to.values = values;
    console.log('Change Content');
  }

  // rates dialog
  openRatesDialog() {
    const dialogRef = this.dialog.open(RatesDialogComponent, {
      panelClass: 'big-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.qoute = result;
        }
      })
    );
  }

  // change flom location field
  changeFrom() {
    this.fromFlag = false;
    for (const item of this.locations) {
      if (this.workingData.from.values.value === item.companyName) {
        this.fromFlag = true;
      }
    }
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
