import { TestBed } from '@angular/core/testing';

import { QuickQuoteService } from './quick-quote.service';

describe('QuickQuoteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: QuickQuoteService = TestBed.get(QuickQuoteService);
    expect(service).toBeTruthy();
  });
});
