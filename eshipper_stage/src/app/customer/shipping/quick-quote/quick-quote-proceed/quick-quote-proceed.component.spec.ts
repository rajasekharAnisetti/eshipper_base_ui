import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuickQuoteProceedComponent } from './quick-quote-proceed.component';

describe('QuickQuoteProceedComponent', () => {
  let component: QuickQuoteProceedComponent;
  let fixture: ComponentFixture<QuickQuoteProceedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuickQuoteProceedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickQuoteProceedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
