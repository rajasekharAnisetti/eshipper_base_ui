import { TestBed } from '@angular/core/testing';

import { QuickQuoteProceedService } from './quick-quote-proceed.service';

describe('QuickQuoteProceedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: QuickQuoteProceedService = TestBed.get(QuickQuoteProceedService);
    expect(service).toBeTruthy();
  });
});
