import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';

import { LocationData } from 'src/app/interfaces/location';
import { ValidationClassStateMatcher } from 'src/app/shared/classes/validation-class';
import { AddressBookDialogComponent } from 'src/app/dialogs/address-book-dialog/address-book-dialog.component';
import { RatesDialogComponent } from 'src/app/dialogs/rates-dialog/rates-dialog.component';

import { QuickQuoteProceedService } from './quick-quote-proceed.service';
import { Countries } from 'src/app/shared/const/countries';

@Component({
  selector: 'app-quick-quote-proceed',
  templateUrl: './quick-quote-proceed.component.html',
  styleUrls: ['./quick-quote-proceed.component.scss'],
})
export class QuickQuoteProceedComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  filteredCityes: Observable<any[]>;
  staticData = {
    countries: Countries.COUNTRIES,
    provinces: Countries.USASTATES,
    cities: Countries.USACITIES,
    stateLabel: 'State',
    zipLabel: 'Zip Code',
  };
  @ViewChild('fromSpecialInstructions, toSpecialInstructions', {
    static: false,
  })
  autosize: CdkTextareaAutosize;
  fromFilteredLocations: Observable<LocationData[]>;
  toFilteredLocations: Observable<LocationData[]>;
  matcher = new ValidationClassStateMatcher();
  // steps
  shipSteps = [
    {
      position: 'Details',
      state: false,
    },
    {
      position: 'Invoice',
      state: false,
    },
    {
      position: 'Summary',
      state: false,
    },
  ];
  //

  // locations
  locations = [];

  workingData = {
    from: {
      values: {
        value: '',
        specialInstructions: '',
        data: {
          companyName: '',
          address1: '',
          address2: '',
          city: '',
          province: '',
          zip: '',
          country: '',
          attention: '',
          phone: '',
          email: '',
          taxId: '',
        },
      },
      locationControl: new FormControl(),
      checkboxes: [
        {
          checked: false,
          label: 'Residential',
        },
        {
          checked: false,
          label: 'Return',
        },
        {
          checked: false,
          label: 'Notify',
        },
      ],
    },
    to: {
      values: {
        value: 'MacAndrews & Forbes Holdings USA',
        specialInstructions: '',
        data: {
          companyName: '',
          address1: '268 Lake Rd',
          address2: '',
          city: 'Toronto',
          province: 'ON',
          zip: '3H4 2G5',
          country: 'Canada',
          attention: 'Tyler Figueroa',
          phone: '888 000 8888',
          email: 'braden_bode@yahoo.com',
          taxId: '',
        },
      },
      locationControl: new FormControl(),
      address1FormControl: new FormControl(),
      address2FormControl: new FormControl(),
      countryFormControl: new FormControl(),
      stateFormControl: new FormControl(),
      cityFormControl: new FormControl(),
      zipFormControl: new FormControl(),
      attentionFormControl: new FormControl(),
      phoneFormControl: new FormControl(),
      emailFormControl: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      checkboxes: [
        {
          checked: false,
          label: 'Residential',
        },
        {
          checked: false,
          label: 'Notify',
        },
      ],
    },
  };

  fromFlag = false;
  emptyData = {
    companyName: '',
    address1: '',
    address2: '',
    city: '',
    province: '',
    zip: '',
    country: '',
    attention: '',
    phone: '',
    email: '',
    taxId: '',
  };
  // qoute
  qoute = {
    image: 'content/images/logos/DHL.png',
    title: 'DHL',
    description: 'Worldwide Express Plus',
    date: '40 Est. Days',
    value: 2469.25,
    details: [
      {
        label: 'Base Charge',
        value: 456.5,
      },
      {
        label: 'Border Fee',
        value: 50.5,
      },
      {
        label: 'Other (16%)',
        value: 12.5,
      },
      {
        label: 'Fuel Surcharges',
        value: 90.2,
      },
      {
        label: 'HST (13 %)',
        value: 78.34,
      },
    ],
  };

  constructor(
    public dialog: MatDialog,
    private getData: QuickQuoteProceedService
  ) { }

  ngOnInit() {
    // get locations from json file
    this.subscribe.add(
      this.getData.getShippingLocations().subscribe(data => {
        this.locations = data;
        this.initAutocomplete();
      })
    );

    // for city
    this.filteredCityes = this.workingData.to.cityFormControl.valueChanges.pipe(
      startWith(''),
      map(city => city ? this._filter(city) : this.staticData.cities.slice())
    )
  }

  // city autocomplete
  displayFn(city?: string): string | undefined {
    return city ? city : undefined;
  }

  private _filter(city: string) {
    const filterValue = city.toLowerCase();

    return this.staticData.cities.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
  }
  // emd of city autocomplete

  changeCountry(country) {
    this.workingData.to.values.data.city = '';
    switch (country) {
      case 'Canada':
        {
          this.staticData.stateLabel = 'Province';
          this.staticData.zipLabel = 'Postal Code';
          this.staticData.provinces = Countries.CANADAPROVINCES;
          this.staticData.cities = Countries.CANADACITIES;
        }
        break;
      case 'USA':
        {
          this.staticData.stateLabel = 'State';
          this.staticData.zipLabel = 'Zip Code';
          this.staticData.provinces = Countries.USASTATES;
          this.staticData.cities = Countries.USACITIES;
        }
        break;
      default:
        break;
    }
  }

  private _fromFilterStates(value: string): LocationData[] {
    const filterValue = value.toLowerCase();
    this.workingData.from.values.data = this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    )[0];
    return this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    );
  }

  private _toFilterStates(value: string): LocationData[] {
    const filterValue = value.toLowerCase();
    const filteredVal = this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    );
    this.workingData.to.values.data = filteredVal.length
      ? filteredVal[0]
      : [this.emptyData];
    return this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    );
  }

  initAutocomplete() {
    this.fromFilteredLocations = this.workingData.from.locationControl.valueChanges.pipe(
      startWith(''),
      map(location =>
        location ? this._fromFilterStates(location) : this.locations.slice()
      )
    );
    this.toFilteredLocations = this.workingData.to.locationControl.valueChanges.pipe(
      startWith(''),
      map(location =>
        location ? this._toFilterStates(location) : this.locations.slice()
      )
    );
  }

  newLocation(val) {
    const dialogRef = this.dialog.open(AddressBookDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'New Location',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          switch (val) {
            case 'from':
              this.locations.push(result);
              this.workingData.from.values.data = result;
              break;
            case 'to':
              this.locations.push(result);
              this.workingData.to.values.data = result;
              break;
            default:
              break;
          }
          this.initAutocomplete();
        }
      })
    );
  }

  editAddress() {
    const dialogRef = this.dialog.open(AddressBookDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Edit Address',
        fields: this.workingData.from.values.data,
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.workingData.from.values.data = result;
        }
      })
    );
  }

  changePosition() {
    // const values = this.workingData.from.values;
    // this.workingData.from.values = this.workingData.to.values;
    // this.workingData.to.values = values;
    console.log('Change Content');
  }

  // rates dialog
  openRatesDialog() {
    const dialogRef = this.dialog.open(RatesDialogComponent, {
      panelClass: 'big-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.qoute = result;
        }
      })
    );
  }

  // change flom location field
  changeFrom() {
    this.fromFlag = false;
    for (const item of this.locations) {
      if (this.workingData.from.values.value === item.companyName) {
        this.fromFlag = true;
      }
    }
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
