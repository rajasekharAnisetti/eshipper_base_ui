import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuickQouteProceedDomesticComponent } from './quick-qoute-proceed-domestic.component';

describe('QuickQouteProceedDomesticComponent', () => {
  let component: QuickQouteProceedDomesticComponent;
  let fixture: ComponentFixture<QuickQouteProceedDomesticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuickQouteProceedDomesticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickQouteProceedDomesticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
