import { TestBed } from '@angular/core/testing';

import { QuickQouteProceedDomesticService } from './quick-qoute-proceed-domestic.service';

describe('QuickQouteProceedDomesticService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: QuickQouteProceedDomesticService = TestBed.get(QuickQouteProceedDomesticService);
    expect(service).toBeTruthy();
  });
});
