import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';

import { LocationData } from 'src/app/interfaces/location';
import { ValidationClassStateMatcher } from 'src/app/shared/classes/validation-class';
import { AddressBookDialogComponent } from 'src/app/dialogs/address-book-dialog/address-book-dialog.component';
import { RatesDialogComponent } from 'src/app/dialogs/rates-dialog/rates-dialog.component';

import { QuickQouteProceedDomesticService } from './quick-qoute-proceed-domestic.service';

@Component({
  selector: 'app-quick-qoute-proceed-domestic',
  templateUrl: './quick-qoute-proceed-domestic.component.html',
  styleUrls: ['./quick-qoute-proceed-domestic.component.scss'],
})
export class QuickQouteProceedDomesticComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  @ViewChild('fromSpecialInstructions, toSpecialInstructions', {
    static: false,
  })
  autosize: CdkTextareaAutosize;
  fromFilteredLocations: Observable<LocationData[]>;
  toFilteredLocations: Observable<LocationData[]>;
  matcher = new ValidationClassStateMatcher();

  // locations
  locations = [];
  cities = [
    'Toronto',
    'Montreal',
    'Vancouver',
    'Calgary',
    'Edmonton',
    'Ottawa',
    'Quebec',
    'Hamilton',
  ];
  countries = ['Canada', 'USA'];
  states = ['ON', 'QC', 'NS', 'NB', 'MB', 'BC', 'PE', 'SK', 'AB', 'NL'];

  workingData = {
    from: {
      values: {
        value: 'MacAndrews & Forbes Holdings USA',
        specialInstructions: '',
        data: {
          companyName: '',
          address1: '268 Lake Rd',
          address2: '',
          city: 'Toronto',
          province: 'ON',
          zip: '3H4 2G5',
          country: 'Canada',
          attention: 'Tyler Figueroa',
          phone: '888 000 8888',
          email: 'braden_bode@yahoo.com',
          taxId: '',
        },
      },
      locationControl: new FormControl(),
      checkboxes: [
        {
          checked: false,
          label: 'Residential',
        },
        {
          checked: false,
          label: 'Return',
        },
        {
          checked: false,
          label: 'Notify',
        },
      ],
    },
    to: {
      values: {
        value: 'MacAndrews & Forbes Holdings USA',
        specialInstructions: '',
        data: {
          companyName: '',
          address1: '268 Lake Rd',
          address2: '',
          city: 'Toronto',
          province: 'ON',
          zip: '3H4 2G5',
          country: 'Canada',
          attention: 'Tyler Figueroa',
          phone: '888 000 8888',
          email: 'braden_bode@yahoo.com',
          taxId: '',
        },
      },
      locationControl: new FormControl(),
      address1FormControl: new FormControl(),
      address2FormControl: new FormControl(),
      countryFormControl: new FormControl(),
      stateFormControl: new FormControl(),
      cityFormControl: new FormControl(),
      zipFormControl: new FormControl(),
      attentionFormControl: new FormControl(),
      phoneFormControl: new FormControl(),
      emailFormControl: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      checkboxes: [
        {
          checked: false,
          label: 'Residential',
        },
        {
          checked: false,
          label: 'Notify',
        },
      ],
    },
  };

  // qoute
  qoute = {
    image: 'content/images/logos/ups.png',
    title: 'Carrier Name',
    description: 'Worldwide Express Plus',
    date: '40 Est. Days',
    value: 2469.25,
    info:
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
  };

  constructor(
    public dialog: MatDialog,
    private getData: QuickQouteProceedDomesticService
  ) {}

  ngOnInit() {
    // get locations from json file
    this.subscribe.add(
      this.getData.getShippingLocations().subscribe(data => {
        this.locations = data;
        this.initAutocomplete();
      })
    );
  }

  private _fromFilterStates(value: string): LocationData[] {
    const filterValue = value.toLowerCase();
    this.workingData.from.values.data = this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    )[0];
    return this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    );
  }

  private _toFilterStates(value: string): LocationData[] {
    const filterValue = value.toLowerCase();
    this.workingData.to.values.data = this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    )[0];
    return this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    );
  }

  initAutocomplete() {
    this.fromFilteredLocations = this.workingData.from.locationControl.valueChanges.pipe(
      startWith(''),
      map(location =>
        location ? this._fromFilterStates(location) : this.locations.slice()
      )
    );
    this.toFilteredLocations = this.workingData.to.locationControl.valueChanges.pipe(
      startWith(''),
      map(location =>
        location ? this._toFilterStates(location) : this.locations.slice()
      )
    );
  }

  newLocation(val) {
    const dialogRef = this.dialog.open(AddressBookDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'New Location',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          switch (val) {
            case 'from':
              this.locations.push(result);
              this.workingData.from.values.data = result;
              break;
            case 'to':
              this.locations.push(result);
              this.workingData.to.values.data = result;
              break;
            default:
              break;
          }
          this.initAutocomplete();
        }
      })
    );
  }

  editAddress() {
    const dialogRef = this.dialog.open(AddressBookDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Edit Address',
        fields: this.workingData.from.values.data,
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.workingData.from.values.data = result;
        }
      })
    );
  }

  changePosition() {
    // const values = this.workingData.from.values;
    // this.workingData.from.values = this.workingData.to.values;
    // this.workingData.to.values = values;
    console.log('Change Content');
  }

  // rates dialog
  openRatesDialog() {
    const dialogRef = this.dialog.open(RatesDialogComponent, {
      panelClass: 'big-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.qoute = result;
        }
      })
    );
  }

  // change flom location field
  changeFrom() {}

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
