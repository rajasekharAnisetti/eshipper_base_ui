import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class BatchPendingInnerService {
  constructor(private http: HttpClient) {}

  getSelectionTracks(): Observable<any> {
    return this.http
      .get('../../content/data/batch-pending-inner-tracks.json')
      .pipe(map(data => data));
  }

  getBatchSmallWidgets(): Observable<any> {
    return this.http
      .get('../../content/data/batch-small-widgets.json')
      .pipe(map(data => data));
  }
}
