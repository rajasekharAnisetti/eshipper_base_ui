import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { ApprovedDialogComponent } from 'src/app/dialogs/approved-dialog/approved-dialog.component';
import {
  BATCHINNERTOOLBARSETTINGS,
  BATCHINNERINNERSETTINGS,
} from 'src/app/shared/const/dropdowns';
import { BatchInner } from 'src/app/interfaces/shipping';

import { BatchPendingInnerService } from './batch-pending-inner.service';

@Component({
  selector: 'app-batch-pending-inner',
  templateUrl: './batch-pending-inner.component.html',
  styleUrls: ['./batch-pending-inner.component.scss'],
})
export class BatchPendingInnerComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  batchEmpty = false;
  selection = false;
  toolbarIndeterminate = false;
  smallWidgetItems: Array<any>;
  trackings: Array<BatchInner>;
  selectAll = false;
  toolbarSettings = BATCHINNERTOOLBARSETTINGS;
  itemSettings = BATCHINNERINNERSETTINGS;
  settings = ['Rename', 'Delete'];

  constructor(
    private getData: BatchPendingInnerService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    // get batches
    this.subscribe.add(
      this.getData.getSelectionTracks().subscribe(data => {
        this.batchEmpty = data.content.length ? false : true;
        data.content ? (this.trackings = data.content) : console.log('Error!');
      })
    );
    // get small widgets
    this.subscribe.add(
      this.getData
        .getBatchSmallWidgets()
        .subscribe(data =>
          data
            ? (this.smallWidgetItems = data)
            : console.log('Data File Error!!!')
        )
    );
  }
  settingsFunction(val) {
    console.log(val);
  }
  searchFunction(val) {
    console.log(val);
  }
  getRateFunction() {
    const dialogRef = this.dialog.open(ApprovedDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Rate Requested',
        content: 'Please check on the pending rates sections for the rates.',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }

  // selection
  selectAllFunction(flag) {
    const select = flag ? true : false;
    this.selection = select ? true : false;
    for (const i of this.trackings) {
      i.selected = select;
    }
  }
  selectRow() {
    const n = this.trackings.length;
    let item = 0;
    for (const i of this.trackings) {
      if (i.selected) {
        item++;
      }
    }
    this.selectAll = n === item ? true : false;
    this.toolbarIndeterminate = n > item && item !== 0 ? true : false;
    this.selection = this.toolbarIndeterminate || this.selectAll ? true : false;
  }
  // end of slection

  batchSettingsFunction(val) {
    console.log(val);
  }



  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
