import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchPendingInnerComponent } from './batch-pending-inner.component';

describe('BatchPendingInnerComponent', () => {
  let component: BatchPendingInnerComponent;
  let fixture: ComponentFixture<BatchPendingInnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatchPendingInnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchPendingInnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
