import { TestBed } from '@angular/core/testing';

import { BatchPendingInnerService } from './batch-pending-inner.service';

describe('BatchPendingInnerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BatchPendingInnerService = TestBed.get(BatchPendingInnerService);
    expect(service).toBeTruthy();
  });
});
