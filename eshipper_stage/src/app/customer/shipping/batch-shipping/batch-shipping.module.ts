import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialModules } from 'src/app/material-modules';

import { BatchUploadModule } from './batch-upload/batch-upload.module';
import { BatchPendingModule } from './batch-pending/batch-pending.module';
import { BatchPendingInnerModule } from './batch-pending-inner/batch-pending-inner.module';

import { BatchShippingComponent } from './batch-shipping.component';

@NgModule({
  declarations: [BatchShippingComponent],
  imports: [
    CommonModule,
    MaterialModules,
    BatchUploadModule,
    BatchPendingModule,
    BatchPendingInnerModule,
    SharedModule,
  ],
  exports: [BatchShippingComponent],
})
export class BatchShippingModule {}
