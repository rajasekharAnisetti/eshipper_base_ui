import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-batch-shipping',
  templateUrl: './batch-shipping.component.html',
  styleUrls: ['./batch-shipping.component.scss'],
})
export class BatchShippingComponent implements OnInit {
  selectedIndex = 0;

  constructor() {}

  ngOnInit() {}

  selectedIndexChange(val: number) {
    this.selectedIndex = val;
  }
}
