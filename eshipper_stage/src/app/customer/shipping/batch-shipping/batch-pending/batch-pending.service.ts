import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class BatchPendingService {
  constructor(private http: HttpClient) {}

  getPendingBatches(): Observable<any> {
    return this.http
      .get('../../content/data/pending-batches.json')
      .pipe(map(data => data));
  }
}
