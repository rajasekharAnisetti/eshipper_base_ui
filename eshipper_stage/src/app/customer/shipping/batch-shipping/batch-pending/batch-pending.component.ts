import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { ApprovedDialogComponent } from 'src/app/dialogs/approved-dialog/approved-dialog.component';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { BATCHITEMSETTINGS } from 'src/app/shared/const/dropdowns';
import { PendingBatch } from 'src/app/interfaces/shipping';

import { BatchPendingService } from './batch-pending.service';

@Component({
  selector: 'app-batch-pending',
  templateUrl: './batch-pending.component.html',
  styleUrls: ['./batch-pending.component.scss'],
})
export class BatchPendingComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  batchEmpty = false;
  selection = false;
  selectAll = false;
  toolbarIndeterminate = false;
  pendingBatches: Array<PendingBatch>;
  itemSettings = BATCHITEMSETTINGS;
  settings = ['Rename', 'Delete'];

  constructor(
    private getData: BatchPendingService,
    public dialog: MatDialog,
    public router: Router
  ) { }

  ngOnInit() {
    // get batches
    this.subscribe.add(
      this.getData.getPendingBatches().subscribe(data => {
        this.batchEmpty = data.content.length ? false : true;
        data.content
          ? (this.pendingBatches = data.content)
          : console.log('Error!');
      })
    );
  }

  getRateFunction() {
    const dialogRef = this.dialog.open(ApprovedDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Rate Requested',
        content: 'Please check on the pending rates sections for the rates.',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }

  // selection
  selectAllFunction(flag) {
    const select = flag ? true : false;
    this.selection = select ? true : false;
    for (const i of this.pendingBatches) {
      i.selected = select;
    }
  }

  selectRow() {
    const n = this.pendingBatches.length;
    let item = 0;
    for (const i of this.pendingBatches) {
      if (i.selected) {
        item++;
      }
    }
    this.selectAll = n === item ? true : false;
    this.toolbarIndeterminate = n > item && item !== 0 ? true : false;
    this.selection = this.toolbarIndeterminate || this.selectAll ? true : false;
  }
  // end of selection

  getItemSettings(val) {
    switch (val) {
      case 'Export':
        const dialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          dialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;

      default:
        break;
    }
  }

  // click on row
  clickOnRow(item) {
    const url = '/customer/shipping/pending-batch';
    this.router.navigate([url]);
    console.log(item);
  }
  // end of click on row

  batchSettingsFunction(item) {

  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
