import { TestBed } from '@angular/core/testing';

import { BatchPendingService } from './batch-pending.service';

describe('BatchPendingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BatchPendingService = TestBed.get(BatchPendingService);
    expect(service).toBeTruthy();
  });
});
