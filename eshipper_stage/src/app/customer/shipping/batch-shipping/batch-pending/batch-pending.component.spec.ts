import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchPendingComponent } from './batch-pending.component';

describe('BatchPendingComponent', () => {
  let component: BatchPendingComponent;
  let fixture: ComponentFixture<BatchPendingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatchPendingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
