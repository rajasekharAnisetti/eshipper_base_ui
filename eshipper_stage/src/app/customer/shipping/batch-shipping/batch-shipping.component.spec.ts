import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BatchShippingComponent } from './batch-shipping.component';

describe('BatchShippingComponent', () => {
  let component: BatchShippingComponent;
  let fixture: ComponentFixture<BatchShippingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BatchShippingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BatchShippingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
