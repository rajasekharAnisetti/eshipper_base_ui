import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { ValidationClassStateMatcher } from 'src/app/shared/classes/validation-class';

@Component({
  selector: 'app-batch-upload',
  templateUrl: './batch-upload.component.html',
  styleUrls: ['./batch-upload.component.scss'],
})
export class BatchUploadComponent implements OnInit {
  @Output() generateClick = new EventEmitter();

  matcher = new ValidationClassStateMatcher();
  generateDisabled = true;
  // criteria data
  distributors = [
    {
      value: 'Option 1',
      viewValue: 'Option 1',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];
  carriers = [
    {
      value: 'Option 1',
      viewValue: 'Option 1',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];
  serviceTypes = [
    {
      value: 'By Cheapest Rate',
      viewValue: 'By Cheapest Rate',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];
  criteria = {
    distributor: '',
    distributorFormControl: new FormControl(),
    email: '',
    emailFormControl: new FormControl('', [
      Validators.required,
      Validators.email,
    ]),
    serviceType: '',
    serviceTypeFormControl: new FormControl(),
    carrier: '',
    carrierFormControl: new FormControl(),
  };
  // end of criteria data
  constructor() {}

  ngOnInit() {}

  getErrorMessage = () =>
    this.criteria.emailFormControl.hasError('required')
      ? 'You must enter a value'
      : this.criteria.emailFormControl.hasError('email')
      ? 'Not a valid email'
      : '';

  generate = () => this.generateClick.emit();
}
