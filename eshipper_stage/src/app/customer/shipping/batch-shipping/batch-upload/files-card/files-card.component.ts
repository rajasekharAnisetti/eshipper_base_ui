import {
  Component,
  OnInit,
  ViewChild,
  Output,
  EventEmitter,
  OnDestroy,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { Files } from 'src/app/interfaces/shipping';
import { RenameDialogComponent } from 'src/app/dialogs/rename-dialog/rename-dialog.component';

import { MappingDialogComponent } from '../mapping-dialog/mapping-dialog.component';

@Component({
  selector: 'app-files-card',
  templateUrl: './files-card.component.html',
  styleUrls: ['./files-card.component.scss'],
})
export class FilesCardComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  @Output() selectedFiles = new EventEmitter();
  messageCheckboxText = 'Don`t show me again';
  field: string;
  openMessage = true;
  @ViewChild('file', { static: false }) file;
  files: Array<Files> = [];

  fileSettings = ['Rename File', 'Download CSV', 'Delete'];
  data: Files[] = [
    {
      name: 'eShipper__Test_Batch_faster delivery.csv',
      process: false,
      success: true,
    },
    {
      name: 'eShipper__Test_Batch_faster delivery.csv',
      process: false,
      success: true,
    },
    {
      name: 'eShipper__Test_Batch_faster delivery.csv',
      process: false,
      success: true,
    },
    {
      name: 'eShipper__Test_Batch_faster delivery.csv',
      process: false,
      success: false,
    },
    {
      name: 'eShipper__Test_Batch_faster delivery.csv',
      process: false,
      success: false,
    },
  ];

  constructor(private dialog: MatDialog) { }

  ngOnInit() { }

  openMappingDialog() {
    const dialogRef = this.dialog.open(MappingDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
          for (const item of this.data) {
            item.process = false;
            item.success = true;
          }
        }
      })
    );
  }

  addFiles() {
    this.file.nativeElement.click();
  }

  onFilesAdded() {
    const files: { [key: string]: File } = this.file.nativeElement.files;
    for (const key in files) {
      if (!isNaN(parseInt(key, 10))) {
        this.data.unshift({
          name: files[key].name,
          process: true,
          success: false,
        });
      }
    }
    this.files = this.data;
    // activate generate button
    this.selectedFiles.emit();
  }

  fileSettingsFunction(val) {
    switch (val) {
      case 'Rename File':
        {
          const dialogRef = this.dialog.open(RenameDialogComponent, {
            autoFocus: false,
            panelClass: 'normal-dialog-width',
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;

      default:
        break;
    }
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
