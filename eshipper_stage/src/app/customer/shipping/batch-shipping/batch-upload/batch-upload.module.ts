import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { FilesCardComponent } from './files-card/files-card.component';
import { MappingDialogComponent } from './mapping-dialog/mapping-dialog.component';
import { BatchUploadComponent } from './batch-upload.component';

@NgModule({
  declarations: [
    BatchUploadComponent,
    FilesCardComponent,
    MappingDialogComponent,
  ],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MaterialModules,
    SharedModule,
  ],
  exports: [BatchUploadComponent],
  entryComponents: [MappingDialogComponent],
})
export class BatchUploadModule { }
