import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-mapping-dialog',
  templateUrl: './mapping-dialog.component.html',
  styleUrls: ['./mapping-dialog.component.scss'],
})
export class MappingDialogComponent implements OnInit {
  workingData = {
    fileName: 'Test_Batch_for abc company.csv',
    systemTitleEmail: '',
    systemTitleEmailFormControl: new FormControl('', [Validators.required]),
    systemTitlePhone: '',
    systemTitlePhoneFormControl: new FormControl('', [Validators.required]),
    systemTitleState: '',
    systemTitleStateFormControl: new FormControl('', [Validators.required]),
    systemTitleAddress: '',
    systemTitleAddressFormControl: new FormControl('', [Validators.required]),
    systemTitleBirthday: '',
    systemTitleBirthdayFormControl: new FormControl('', [Validators.required]),
  };

  systemTitles = [
    { value: 'email', viewValue: 'Email' },
    { value: 'phone', viewValue: 'Phone' },
    { value: 'state', viewValue: 'State' },
    { value: 'address', viewValue: 'Address' },
    { value: 'na', viewValue: 'N/A' },
  ];

  constructor(
    public dialogRef: MatDialogRef<MappingDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();
  submit = () => this.dialogRef.close(this.workingData);
}
