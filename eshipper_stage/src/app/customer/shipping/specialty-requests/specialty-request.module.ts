import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgxMaskModule } from 'ngx-mask';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { SpecialtyRequestsComponent } from './specialty-requests.component';

@NgModule({
  declarations: [SpecialtyRequestsComponent],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgxMaskModule,
    MaterialModules,
    SharedModule,
  ],
  exports: [SpecialtyRequestsComponent],
})
export class SpecialtyRequestsModule { }
