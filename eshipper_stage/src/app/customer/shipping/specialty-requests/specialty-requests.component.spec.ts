import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialtyRequestsComponent } from './specialty-requests.component';

describe('SpecialtyRequestsComponent', () => {
  let component: SpecialtyRequestsComponent;
  let fixture: ComponentFixture<SpecialtyRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialtyRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialtyRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
