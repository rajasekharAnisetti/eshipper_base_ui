import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';

import { LocationData } from 'src/app/interfaces/location';
import { AddressBookDialogComponent } from 'src/app/dialogs/address-book-dialog/address-book-dialog.component';
import { RenameDialogComponent } from 'src/app/dialogs/rename-dialog/rename-dialog.component';
import { CURRENCIES_$ } from 'src/app/shared/const/currencies';

import { SpecialtyRequestsService } from './specialty-requests.service';
import { QtyMatcher } from 'src/app/shared/classes/validation-class';
import { PACKAGETYPES } from 'src/app/shared/const/dropdowns';

@Component({
  selector: 'app-specialty-requests',
  templateUrl: './specialty-requests.component.html',
  styleUrls: ['./specialty-requests.component.scss'],
})
export class SpecialtyRequestsComponent implements OnInit, OnDestroy {
  minDate = new Date();
  subscribe = new Subscription();
  autosize: CdkTextareaAutosize;
  // from to data
  fromFilteredLocations: Observable<LocationData[]>;
  toFilteredLocations: Observable<LocationData[]>;
  locationTypes = [
    {
      value: 'address',
      viewValue: 'Address',
    },
    {
      value: 'option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'option 3',
      viewValue: 'Option 3',
    },
  ];
  history = [
    {
      title: 'Pallet',
      innerData: [
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
      ],
    },
    {
      title: 'Pallet',
      innerData: [
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
      ],
    },
    {
      title: 'Envelope',
      innerData: [],
    },
    {
      title: 'Package',
      innerData: [
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
      ],
    },
    {
      title: 'Pak',
      innerData: [],
    },
    {
      title: 'Pallet',
      innerData: [
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
        {
          label: '4 x 1 x 9 in (LxWxH)',
          value: '4lbs',
        },
      ],
    },
  ];
  locations = [];
  workingData = {
    from: {
      values: {
        value: '',
        locationType: '',
        specialInstructions: '',
        data: {
          companyName: '',
          address1: '',
          address2: '',
          city: '',
          province: '',
          zip: '',
          country: '',
          attention: '',
          phone: '',
          email: '',
          taxId: '',
        },
      },
      locationTypeControl: new FormControl(),
      locationControl: new FormControl(),
      checkboxes: [],
    },
    to: {
      values: {
        value: '',
        locationType: '',
        specialInstructions: '',
        data: {
          companyName: '',
          address1: '',
          address2: '',
          city: '',
          province: '',
          zip: '',
          country: '',
          attention: '',
          phone: '',
          email: '',
          taxId: '',
        },
      },
      locationTypeControl: new FormControl(),
      locationControl: new FormControl(),
      checkboxes: [
        {
          checked: false,
          label: 'Residential',
        },
      ],
    },
  };
  fromFlag = false;
  toFlag = false;
  // end of from to data
  // job details data
  serviceTypes = [
    {
      value: 'customs',
      viewValue: 'Customs Brokerage',
    },
    {
      value: 'ltl',
      viewValue: 'LTL',
    },
    {
      value: 'warehouse',
      viewValue: 'Warehouse/Distribution',
    },
  ];
  packageTypes = PACKAGETYPES;
  matRippleDisabled = false;
  qtyMatcher = new QtyMatcher();
  jobDetails = {
    date: '',
    time: '', // format 00:00
    specialInstructions: '',
    deliveryDeadline: '',
    deliveryDeadlineControl: new FormControl(),
    serviceType: '',
    serviceTypeControl: new FormControl(),
    customs: {
      productInformation: '',
      import: '',
      importControl: new FormControl(),
      export: '',
      exportControl: new FormControl(),
      value: '',
      valueControl: new FormControl(),
      modesOfTransport: [
        {
          value: 'Value',
          selected: false,
        },
        {
          value: 'Value',
          selected: false,
        },
        {
          value: 'Value',
          selected: false,
        },
        {
          value: 'Value',
          selected: false,
        },
        {
          value: 'Value',
          selected: false,
        },
      ],
    },
    ltl: {
      shipment: {
        currencyValue: '',
        systemValue: '',
        shippingDateValue: '',
        shippingDateControl: new FormControl(),
        qty: '1',
        qtyControl: new FormControl(),
        totalWeight: 10,
        palletForm: [
          {
            lengthControl: new FormControl(),
            length: '',
            widthControl: new FormControl(),
            width: '',
            heightControl: new FormControl(),
            height: '',
            weightControl: new FormControl(),
            weight: '',
            freightClassControl: new FormControl(),
            freightClass: '',
            nmfc: '',
            typeControl: new FormControl(),
            typeClass: '',
            insurance: '',
            insuranceTooltip: 'Info about the field',
            description: '',
            button: '',
          }
        ],
      },
      customs: {
        currencyValue: '',
        brokerName: '',
        brokerNameControl: new FormControl(),
        phoneNumber: '',
        phoneNumberControl: new FormControl(),
        declaredValue: '',
        declaredValueControl: new FormControl(),
        contact: '',
        contactControl: new FormControl(),
      },
      docs: [
        {
          docName: 'Document Name',
        },
        {
          docName: 'Document Name',
        },
        {
          docName: 'Document Name',
        },
      ],
    },
    warehouse: {
      storage: {
        flag: false,
        productInformation: '',
        packageType: '',
        packageTypeControl: new FormControl(),
        qty: '',
        qtyControl: new FormControl(),
        spaceRequirement: '',
        spaceRequirementControl: new FormControl(),
      },
      shippingInfo: {
        shippingServices: [
          {
            value: 'Postal',
            selected: false,
          },
          {
            value: 'Courier',
            selected: false,
          },
          {
            value: 'Trucking',
            selected: false,
          },
          {
            value: 'Air Freight',
            selected: false,
          },
          {
            value: 'Ocean',
            selected: false,
          },
          {
            value: 'Rail',
            selected: false,
          },
        ],
        monthlyVolume: '',
        monthlyVolumeControl: new FormControl(),
      },
      pickPack: {
        flag: false,
        skus: '',
        skusControl: new FormControl(),
        order: '',
        orderControl: new FormControl(),
        weight: '',
        weightControl: new FormControl(),
        size: '',
        sizeControl: new FormControl(),
        volume: '',
        volumeControl: new FormControl(),
        description: '',
        additionalServices: [
          {
            value: 'Inbound Per Box/Carton',
            description: '$1.50',
            selected: false,
          },
          {
            value: 'Inbound Pallets',
            description: '$5.00',
            selected: false,
          },
          {
            value: 'Outbound Pallets',
            description: '$5.00',
            selected: false,
          },
          {
            value: 'Container Unloading 20` Floor Loaded',
            description: '$30.00 (Per Hour / Per Person)',
            selected: false,
          },
          {
            value: 'Container Unloading 40` Floor Loaded',
            description: '$30.00 (Per Hour / Per Person)',
            selected: false,
          },
          {
            value: 'Container Unloading 20` Skid Loaded',
            description: '$125.00',
            selected: false,
          },
          {
            value: 'Container Unloading 40` Skid Loaded',
            description: '$180.00',
            selected: false,
          },
          {
            value: 'Pallet Shirnk & Wrap',
            description: '$15.00 (Per Pallet)',
            selected: false,
          },
          {
            value: 'Kitting',
            description: '$30.00 (Per Hour / Per Person)',
            selected: false,
          },
          {
            value: 'Pallet Shirnk & Wrap',
            description: '$15.00 (Per Pallet)',
            selected: false,
          },
          {
            value: 'Packaging Materials',
            description: 'Cost + 15%',
            selected: false,
          },
        ],
      },
    },
  };
  docSettings = ['Rename', 'Delete'];
  currencyItems = CURRENCIES_$;
  systemItems = ['Metric', 'Imperial'];
  insuranceTypes = [
    {
      value: 'eshipper',
      viewValue: 'eShipper',
    },
    {
      value: 'option2',
      viewValue: 'Option 2',
    },
    {
      value: 'option3',
      viewValue: 'Option 2',
    },
  ];
  qties = [
    {
      value: '1',
      viewValue: '1',
    },
    {
      value: '2',
      viewValue: '2',
    },
    {
      value: '3',
      viewValue: '3',
    },
  ];
  jobService = '';
  // job details data
  customsBrokerageFlag = false

  constructor(
    public dialog: MatDialog,
    private getData: SpecialtyRequestsService
  ) { }

  ngOnInit() {
    // from to functions
    this.subscribe.add(
      this.getData.getShippingLocations().subscribe(data => {
        this.locations = data;
        this.initAutocomplete();
        this.initFromData(data);
      })
    );
    // end of from to functions
  }

  // from to functions
  private _fromFilterStates(value: string): LocationData[] {
    const filterValue = value.toLowerCase();
    this.workingData.from.values.data = this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    )[0];
    return this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    );
  }

  private _toFilterStates(value: string): LocationData[] {
    const filterValue = value.toLowerCase();
    this.workingData.to.values.data = this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    )[0];
    return this.locations.filter(
      location => location.companyName.toLowerCase().indexOf(filterValue) === 0
    );
  }

  initAutocomplete() {
    this.fromFilteredLocations = this.workingData.from.locationControl.valueChanges.pipe(
      startWith(''),
      map(location =>
        location ? this._fromFilterStates(location) : this.locations.slice()
      )
    );
    this.toFilteredLocations = this.workingData.to.locationControl.valueChanges.pipe(
      startWith(''),
      map(location =>
        location ? this._toFilterStates(location) : this.locations.slice()
      )
    );
  }

  initFromData = data => {
    this.workingData.from.values.data = data[0];
    this.workingData.from.values.value = data[0].companyName;
    this.workingData.from.values.locationType = this.locationTypes[0].value;
    this.fromFlag = true;
  };

  newLocation(val) {
    const dialogRef = this.dialog.open(AddressBookDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'New Location',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          switch (val) {
            case 'from':
              this.locations.push(result);
              this.workingData.from.values.data = result;
              break;
            case 'to':
              this.locations.push(result);
              this.workingData.to.values.data = result;
              break;
            default:
              break;
          }
          this.initAutocomplete();
        }
      })
    );
  }

  editAddress() {
    const dialogRef = this.dialog.open(AddressBookDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Edit Address',
        fields: this.workingData.from.values.data,
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.workingData.from.values.data = result;
        }
      })
    );
  }

  changePosition() {
    const values = this.workingData.from.values;
    this.workingData.from.values = this.workingData.to.values;
    this.workingData.to.values = values;
  }

  // change flom location field
  changeFrom() {
    this.fromFlag = false;
    for (const item of this.locations) {
      if (this.workingData.from.values.value === item.companyName) {
        this.fromFlag = true;
      }
    }
  }
  // change To location field
  changeTo() {
    this.toFlag = false;
    for (const item of this.locations) {
      if (this.workingData.to.values.value === item.companyName) {
        this.toFlag = true;
      }
    }
  }
  // end of from to functions

  // job details functions
  jobDetailsTime(time) {
    this.jobDetails.time = time;
    console.log(time);
  }
  getDeliveryDeadline() { }
  changeServiceType(e) {
    this.jobService = e.value; // customs, ltl, warehouse
  }

  docSettingsFunction(val) {
    switch (val) {
      case 'Rename':
        {
          const dialogRef = this.dialog.open(RenameDialogComponent, {
            autoFocus: false,
            panelClass: 'normal-dialog-width',
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;

      default:
        break;
    }
  }
  customsCurrencyChange(val) {
    console.log(val);
  }
  shipmentCurrencyChange(val) {
    console.log(val);
  }
  shipmentSystemChange(val) {
    console.log(val);
  }
  getShippingDate() {
    console.log(this.jobDetails.ltl.shipment.shippingDateValue);
  }
  // end of job details functions

  // qty Function
  qtyFunction(qty) {
    console.log(qty);
    this.jobDetails.ltl.shipment.palletForm = [
      {
        lengthControl: new FormControl(),
        length: '',
        widthControl: new FormControl(),
        width: '',
        heightControl: new FormControl(),
        height: '',
        weightControl: new FormControl(),
        weight: '',
        freightClassControl: new FormControl(),
        freightClass: '',
        nmfc: '',
        typeControl: new FormControl(),
        typeClass: '',
        insurance: '',
        insuranceTooltip: 'Info about the field',
        description: '',
        button: '',
      }
    ];
    if (qty > 1 && qty <= 35) {
      this.jobDetails.ltl.shipment.palletForm = [
        {
          lengthControl: new FormControl(),
          length: '',
          widthControl: new FormControl(),
          width: '',
          heightControl: new FormControl(),
          height: '',
          weightControl: new FormControl(),
          weight: '',
          freightClassControl: new FormControl(),
          freightClass: '',
          nmfc: '',
          typeControl: new FormControl(),
          typeClass: '',
          insurance: '',
          insuranceTooltip: 'Info about the field',
          description: '',
          button: 'All the Same',
        }
      ];
      for (let i = 1; i <= qty - 1; i++) {
        this.jobDetails.ltl.shipment.palletForm.push({
          lengthControl: new FormControl(),
          length: '',
          widthControl: new FormControl(),
          width: '',
          heightControl: new FormControl(),
          height: '',
          weightControl: new FormControl(),
          weight: '',
          freightClassControl: new FormControl(),
          freightClass: '',
          nmfc: '',
          typeControl: new FormControl(),
          typeClass: '',
          insurance: '',
          insuranceTooltip: 'Info about the field',
          description: '',
          button: 'Same As Above',
        })
      }
    }
  }
  // end of qty Function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
