import { TestBed } from '@angular/core/testing';

import { SpecialtyRequestsService } from './specialty-requests.service';

describe('SpecialtyRequestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SpecialtyRequestsService = TestBed.get(SpecialtyRequestsService);
    expect(service).toBeTruthy();
  });
});
