import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedShipComponent } from './saved-ship.component';

describe('SavedShipComponent', () => {
  let component: SavedShipComponent;
  let fixture: ComponentFixture<SavedShipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedShipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedShipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
