import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

import { SavedShipService } from './saved-ship.service';
import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';
import { ApprovedDialogComponent } from 'src/app/dialogs/approved-dialog/approved-dialog.component';

@Component({
  selector: 'app-saved-ship',
  templateUrl: './saved-ship.component.html',
  styleUrls: ['./saved-ship.component.scss'],
})
export class SavedShipComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  dataEmpty = false;
  ships: Array<any>;
  contentToolbarSettings = ['Option 1', 'Option 2'];
  itemSettings = ['Edit', 'Cancel'];

  constructor(private getData: SavedShipService, private router: Router, public dialog: MatDialog) { }

  ngOnInit() {
    // get list data
    this.subscribe.add(
      this.getData.getSavedShips().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        data.content ? (this.ships = data.content) : console.log('Error!');
      })
    );
  }

  settingsFunction(val) {
    console.log(val);
  }

  getItemSettingsValue(val) {
    switch (val) {
      case 'Edit':
        this.router.navigate(['/customer/shipping/ship/edit-details']);
        break;

      default:
        break;
    }
  }

  // click on row function
  clickOnRow(item) {
    console.log(item);
    this.router.navigate(['/customer/shipping/ship/edit-details']);
  }
  // end of click on row function

  // open cost info dialog
  openCostInfoDialog(data) {
    this.dialog.open(InfoDialogComponent, {
      data: {
        title: 'Cost Details',
        moreData: data,
      },
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
  }
  // end of open cost info dialog

  // ship function
  shipFunction() {
    const dialogRef = this.dialog.open(ApprovedDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Shipment Submitted',
        content: 'Please check on the tracking sections for the status.',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // nd of ship function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
