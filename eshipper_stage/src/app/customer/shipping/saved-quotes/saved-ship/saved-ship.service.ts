import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SavedShipService {
  constructor(private http: HttpClient) {}

  getSavedShips(): Observable<any> {
    return this.http
      .get('../../content/data/saved-ships.json')
      .pipe(map(data => data));
  }
}
