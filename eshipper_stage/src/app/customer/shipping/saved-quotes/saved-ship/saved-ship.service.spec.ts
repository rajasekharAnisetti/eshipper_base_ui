import { TestBed } from '@angular/core/testing';

import { SavedShipService } from './saved-ship.service';

describe('SavedShipService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SavedShipService = TestBed.get(SavedShipService);
    expect(service).toBeTruthy();
  });
});
