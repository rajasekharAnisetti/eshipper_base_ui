import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedSpecialtyRequestsComponent } from './saved-specialty-requests.component';

describe('SavedSpecialtyRequestsComponent', () => {
  let component: SavedSpecialtyRequestsComponent;
  let fixture: ComponentFixture<SavedSpecialtyRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedSpecialtyRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedSpecialtyRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
