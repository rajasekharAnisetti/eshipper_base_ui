import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { SavedSpecialtyRequestsService } from './saved-specialty-requests.service';

@Component({
  selector: 'app-saved-specialty-requests',
  templateUrl: './saved-specialty-requests.component.html',
  styleUrls: ['./saved-specialty-requests.component.scss'],
})
export class SavedSpecialtyRequestsComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  dataEmpty = false;
  specialtyRequests: Array<any>;
  contentToolbarSettings = ['Option 1', 'Option 2'];
  itemSettings = ['Edit', 'Cancel'];

  constructor(
    private getData: SavedSpecialtyRequestsService,
    private router: Router
  ) { }

  ngOnInit() {
    // get list data
    this.subscribe.add(
      this.getData.getSavedSpecialtyRequests().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        data.content
          ? (this.specialtyRequests = data.content)
          : console.log('Error!');
      })
    );
  }

  settingsFunction(val) {
    console.log(val);
  }

  getItemSettingsValue(val) {
    switch (val) {
      case 'Edit':
        this.router.navigate(['/customer/shipping/ship/edit-details']);
        break;

      default:
        break;
    }
  }

  // click on row
  clickOnRow(item) {
    console.log(item);
    this.router.navigate(['/customer/shipping/ship/edit-details']);
  }
  // end of click on row

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
