import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SavedSpecialtyRequestsService {
  constructor(private http: HttpClient) {}

  getSavedSpecialtyRequests(): Observable<any> {
    return this.http
      .get('../../content/data/saved-specialty-requests.json')
      .pipe(map(data => data));
  }
}
