import { TestBed } from '@angular/core/testing';

import { SavedSpecialtyRequestsService } from './saved-specialty-requests.service';

describe('SavedSpecialtyRequestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SavedSpecialtyRequestsService = TestBed.get(SavedSpecialtyRequestsService);
    expect(service).toBeTruthy();
  });
});
