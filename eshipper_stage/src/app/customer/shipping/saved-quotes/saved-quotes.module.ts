import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModules } from '../../../material-modules';

import { SavedSpecialtyRequestsModule } from './saved-specialty-requests/saved-specialty-requests.module';
import { SavedShipModule } from './saved-ship/saved-ship.module';

import { SavedQuotesComponent } from './saved-quotes.component';

@NgModule({
  declarations: [SavedQuotesComponent],
  imports: [
    CommonModule,
    MaterialModules,
    SavedSpecialtyRequestsModule,
    SavedShipModule,
  ],
  exports: [SavedQuotesComponent],
})
export class SavedQuotesModule {}
