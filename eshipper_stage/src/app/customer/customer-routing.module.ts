import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerComponent } from './customer.component';

const customerRoutes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: 'tracking',
    loadChildren: () =>
      import('../tracking/tracking.module').then(mod => mod.TrackingModule),
  },
  {
    path: 'tracking/order-details',
    loadChildren: () =>
      import('../order-details/order-details.module').then(
        mod => mod.OrderDetailsModule
      ),
    data: {
      title: 'Tracking CVX12345678',
      backBtn: true,
    },
  },
  {
    path: 'shipping',
    loadChildren: () =>
      import('./shipping/shipping.module').then(mod => mod.ShippingModule),
  },
  {
    path: 'dashboard',
    loadChildren: () =>
      import('../dashboard/dashboard.module').then(mod => mod.DashboardModule),
  },
  {
    path: 'account',
    loadChildren: () =>
      import('./account/account.module').then(mod => mod.AccountModule),
  },
  {
    path: 'profile',
    loadChildren: () =>
      import('../profile/profile.module').then(mod => mod.ProfileModule),
  },

  {
    path: 'support',
    loadChildren: () =>
      import('./support/support.module').then(mod => mod.SupportModule),
  },
  {
    path: '**',
    loadChildren: () =>
      import('../unavailable-server/unavailable-server.module').then(mod => mod.UnavailableServerModule),
    data: {
      title: 'Unavailable',
    },
  }
];

const routes: Routes = [
  {
    path: '',
    component: CustomerComponent,
    children: customerRoutes,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerRoutingModule { }
