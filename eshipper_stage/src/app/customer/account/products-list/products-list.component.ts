import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { Product } from 'src/app/interfaces/account';

import { AccountService } from '../account.service';
import { ProductDialogComponent } from '../product-dialog/product-dialog.component';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss'],
})
export class ProductsListComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  productsList: Array<Product>;
  dataEmpty = true;
  toolbarSettings = ['Export'];
  itemSettings = ['Edit', 'Delete'];

  constructor(private getData: AccountService, private dialog: MatDialog) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData.getProductsList().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        this.productsList = data.content.length ? data.content : [];
      })
    );
  }

  // get search value
  searchFunction(val) {
    console.log(val);
  }
  // end of get search value

  // toolbar settings
  settingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;

      default:
        break;
    }
  }
  // end of toolbar settings

  // item settings function
  itemSettingsFunction(val, item) {
    switch (val) {
      case 'Edit':
        {
          const dialogRef = this.dialog.open(ProductDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              title: 'Edit Custom Item',
              actionButton: 'Edit',
              productData: item,
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;

      default:
        break;
    }
  }
  // end of item settings function

  // add product function
  addProduct() {
    const dialogRef = this.dialog.open(ProductDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'New Custom Item',
        actionButton: 'Add',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of add product function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
