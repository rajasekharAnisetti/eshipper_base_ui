import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-box-manager-dialog',
  templateUrl: './box-manager-dialog.component.html',
  styleUrls: ['./box-manager-dialog.component.scss'],
})
export class BoxManagerDialogComponent implements OnInit {
  units = ['Metric', 'Imperial'];
  untitsPlaceholder = 'Metric';
  types = ['Box', 'Envelope', 'Skid', 'Type'];

  modalData = {
    units: {
      weight: 'kg',
      size: 'cm',
    },
    boxName: '',
    description: '',
    dimension: {
      l: '',
      w: '',
      h: '',
    },
    weight: '',
    maxSupportWeight: '',
    boxType: '',
  };

  validation = {
    boxName: new FormControl(),
    description: new FormControl(),
    dimension: {
      l: new FormControl(),
      w: new FormControl(),
      h: new FormControl(),
    },
    weight: new FormControl(),
    maxSupportWeight: new FormControl(),
    boxType: new FormControl(),
  };

  constructor(
    public dialogRef: MatDialogRef<BoxManagerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data.boxData) {
      this.modalData = data.boxData;
      this.untitsPlaceholder =
        data.boxData.units.weight === 'kg' ? 'Metric' : 'Imperial';
    }
  }

  ngOnInit() {}

  // change system value
  systemChange(val) {
    switch (val) {
      case 'Metric':
        {
          this.modalData.units.weight = 'kg';
          this.modalData.units.size = 'cm';
        }
        break;
      case 'Imperial':
        {
          this.modalData.units.weight = 'lbs';
          this.modalData.units.size = 'in';
        }
        break;
      default:
        break;
    }
  }
  // end of change system value

  // close modal
  closeDialog = () => this.dialogRef.close();

  // action modal
  save = () => this.dialogRef.close(this.modalData);
}
