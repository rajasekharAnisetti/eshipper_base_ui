import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxManagerDialogComponent } from './box-manager-dialog.component';

describe('BoxManagerDialogComponent', () => {
  let component: BoxManagerDialogComponent;
  let fixture: ComponentFixture<BoxManagerDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoxManagerDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxManagerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
