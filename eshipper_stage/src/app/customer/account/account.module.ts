import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { AccountRoutingModule } from './account-routing.module';

import { AccountComponent } from './account.component';
import { CustomerUsersComponent } from './customer-users/customer-users.component';
import { AddressBookComponent } from './address-book/address-book.component';
import { AccountInvoicesComponent } from './account-invoices/account-invoices.component';
import { AccountCarriersComponent } from './account-carriers/account-carriers.component';
import { RolesComponent } from './roles/roles.component';
import { BoxManagerComponent } from './box-manager/box-manager.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { BoxManagerDialogComponent } from './box-manager-dialog/box-manager-dialog.component';
import { ProductDialogComponent } from './product-dialog/product-dialog.component';

@NgModule({
  declarations: [
    AccountComponent,
    CustomerUsersComponent,
    AddressBookComponent,
    AccountInvoicesComponent,
    AccountCarriersComponent,
    RolesComponent,
    BoxManagerComponent,
    ProductsListComponent,
    BoxManagerDialogComponent,
    ProductDialogComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    MaterialModules,
    RouterModule,
    SharedModule,
    AccountRoutingModule,
    NgxMaskModule,
  ],
  exports: [],
  entryComponents: [
    BoxManagerDialogComponent,
    ProductDialogComponent,
  ],
})
export class AccountModule { }
