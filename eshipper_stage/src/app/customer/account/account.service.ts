import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  constructor(private http: HttpClient) {}

  // role permissions
  getRolePermissions(): Observable<any> {
    return this.http
      .get('content/data/account/role-permissions.json')
      .pipe(map(data => data));
  }

  // roles list
  getRolesList(): Observable<any> {
    return this.http
      .get('content/data/account/roles-list.json')
      .pipe(map(data => data));
  }

  // users list
  getUsersList(): Observable<any> {
    return this.http
      .get('content/data/account/users-list.json')
      .pipe(map(data => data));
  }

  // address book list
  getAddressBookList(): Observable<any> {
    return this.http
      .get('content/data/account/address-book-list.json')
      .pipe(map(data => data));
  }

  // get account invoices
  getAccountInvoicesList(): Observable<any> {
    return this.http
      .get('content/data/account/account-invoices-list.json')
      .pipe(map(data => data));
  }

  // get box manager list
  getBoxManagerList(): Observable<any> {
    return this.http
      .get('content/data/account/box-manager-list.json')
      .pipe(map(data => data));
  }

  // get products list
  getProductsList(): Observable<any> {
    return this.http
      .get('content/data/account/products-list.json')
      .pipe(map(data => data));
  }

  // get carriers list
  getCarriersList(): Observable<any> {
    return this.http
      .get('content/data/account/carriers-list.json')
      .pipe(map(data => data));
  }
}
