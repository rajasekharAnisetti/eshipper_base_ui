import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { Box } from 'src/app/interfaces/account';

import { AccountService } from '../account.service';
import { BoxManagerDialogComponent } from '../box-manager-dialog/box-manager-dialog.component';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';

@Component({
  selector: 'app-box-manager',
  templateUrl: './box-manager.component.html',
  styleUrls: ['./box-manager.component.scss'],
})
export class BoxManagerComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  dataEmpty = true;
  toolbarSettings = ['Export'];
  itemSettings = ['Edit', 'Delete'];
  boxList: Array<Box>;

  constructor(public dialog: MatDialog, private getData: AccountService) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData.getBoxManagerList().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        this.boxList = data.content;
        console.log(this.boxList);
      })
    );
  }

  // search function
  searchFunction(val) {
    console.log(val);
  }
  // end of search function

  // toolbar settings function
  settingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;

      default:
        break;
    }
  }
  // end of toolbar settings function

  // item settings function
  itemSettingsFunction(val, item) {
    console.log(val, item);
    switch (val) {
      case 'Edit':
        {
          const dialogRef = this.dialog.open(BoxManagerDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              title: 'Edit Box',
              actionButton: 'Edit',
              boxData: item,
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;

      default:
        break;
    }
  }
  // end of item settings function

  // add box function
  addBox() {
    const dialogRef = this.dialog.open(BoxManagerDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'New Box',
        actionButton: 'Add',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of add box function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
