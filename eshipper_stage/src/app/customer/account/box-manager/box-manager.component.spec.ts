import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxManagerComponent } from './box-manager.component';

describe('BoxManagerComponent', () => {
  let component: BoxManagerComponent;
  let fixture: ComponentFixture<BoxManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoxManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
