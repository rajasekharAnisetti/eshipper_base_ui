import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { RolePermissions, Role } from 'src/app/interfaces/roles';

import { AccountService } from '../account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss'],
})
export class RolesComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  dataEmpty = true;
  toolbarSettings = ['Option 1', 'Option 2'];
  itemSettings = ['Edit', 'Delete'];
  rolePermissions: Array<RolePermissions>;
  rolesList: Array<Role>;

  constructor(private getData: AccountService, private router: Router) { }

  ngOnInit() {
    // get role permissions
    this.subscribe.add(
      this.getData
        .getRolePermissions()
        .subscribe(data => (this.rolePermissions = data))
    );
    // get roles list
    this.subscribe.add(
      this.getData.getRolesList().subscribe(data => {
        this.rolesList = data.content;
        this.dataEmpty = data.content ? false : true;
      })
    );
  }

  // search
  searchFunction(val) {
    console.log(val);
  }
  // end of search

  // toolbar settings
  settingsFunction(val) {
    console.log(val);
  }
  // end of toolbar settings

  // edit role
  editRole(item) {
    const url = '/customer/account/roles/role/edit';
    localStorage.setItem('roleAdminFlag', 'customer');
    localStorage.setItem('roleData', JSON.stringify(item));
    this.router.navigate([url]);
  }
  // end of edit role

  // item settings function
  itemSettingsFunction(val, item) {
    switch (val) {
      case 'Edit':
        this.editRole(item);
        break;

      default:
        break;
    }
  }
  // end of item settings function

  // add role function
  addRole() {
    const url = '/customer/account/roles/role/new';
    const newData = {
      permissions: this.rolePermissions,
      roleName: ''
    };
    localStorage.setItem('roleAdminFlag', 'customer');
    localStorage.setItem('roleData', JSON.stringify(newData));
    this.router.navigate([url]);
  }
  // end of add role function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
