import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { PACKAGETYPES } from 'src/app/shared/const/dropdowns';
import { Carrier } from 'src/app/interfaces/account';
import { AccountService } from '../account.service';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-account-carriers',
  templateUrl: './account-carriers.component.html',
  styleUrls: ['./account-carriers.component.scss'],
})
export class AccountCarriersComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  dataEmpty = true;
  carriersList: Array<Carrier>;
  packageTypes = PACKAGETYPES;
  toolbarSettings = ['Export'];

  constructor(private getData: AccountService, public dialog: MatDialog) { }

  ngOnInit() {
    this.subscribe.add(
      this.getData.getCarriersList().subscribe(data => {
        this.dataEmpty = data.content.length ? false : true;
        this.carriersList = data.content.length ? data.content : [];
      })
    );
  }

  // package types filter function
  packageTypesFilter(val) {
    console.log(val);
  }
  // end of package types filter function

  // search function
  searchFunction(val) {
    console.log(val);
  }
  // end of search function

  // toolbar settings function
  toolbarSettingsFunction(val) {
    switch (val) {
      case 'Export':
        const exportDialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          exportDialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;

      default:
        break;
    }
  }
  // end of toolbar settings function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
