import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountCarriersComponent } from './account-carriers.component';

describe('AccountCarriersComponent', () => {
  let component: AccountCarriersComponent;
  let fixture: ComponentFixture<AccountCarriersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountCarriersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountCarriersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
