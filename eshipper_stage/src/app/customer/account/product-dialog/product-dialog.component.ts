import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-product-dialog',
  templateUrl: './product-dialog.component.html',
  styleUrls: ['./product-dialog.component.scss'],
})
export class ProductDialogComponent implements OnInit {
  modalData = {
    productId: '',
    productName: '',
    hsCode: '',
    skuCode: '',
    coo: '',
    unitPrice: 0,
    quantity: 0,
  };

  validation = {
    productId: new FormControl(),
    productName: new FormControl(),
    hsCode: new FormControl(),
    skuCode: new FormControl(),
    coo: new FormControl(),
    unitPrice: new FormControl(),
    quantity: new FormControl(),
  };

  constructor(
    public dialogRef: MatDialogRef<ProductDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data.productData) {
      this.modalData = data.productData;
    }
  }

  ngOnInit() {}

  // close modal
  closeDialog = () => this.dialogRef.close();

  // action modal
  save = () => this.dialogRef.close(this.modalData);
}
