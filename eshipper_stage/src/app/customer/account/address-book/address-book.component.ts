import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { AddressBook } from 'src/app/interfaces/account';
import { AccountService } from '../account.service';
import { AddressBookDialogComponent } from 'src/app/dialogs/address-book-dialog/address-book-dialog.component';

@Component({
  selector: 'app-address-book',
  templateUrl: './address-book.component.html',
  styleUrls: ['./address-book.component.scss'],
})
export class AddressBookComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  dataEmpty = true;
  addressBookList: Array<AddressBook>;
  toolbarSettings = ['Option 1', 'Option 2'];
  itemSettings = ['Edit', 'Delete'];


  constructor(public dialog: MatDialog, private getData: AccountService) { }

  ngOnInit() {
    // get address book list
    this.subscribe.add(
      this.getData.getAddressBookList().subscribe(data => {
        this.dataEmpty = data.content ? false : true;
        this.addressBookList = data.content;
      })
    );
  }

  // search function
  searchFunction(val) {
    console.log(val);
  }
  // end of search function

  // content toolbar settings
  settingsFunction(val) {
    console.log(val);
  }
  // end of content toolbar settings


  // click on row
  clickOnRow(item) {
    const dialogRef = this.dialog.open(AddressBookDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Edit Address',
        fields: item,
        addressBookFlag: true
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of click on row

  // item settings
  itemSettingsFunction(val, item) {
    switch (val) {
      case 'Edit':
        {
          const dialogRef = this.dialog.open(AddressBookDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
            data: {
              title: 'Edit Address',
              fields: item,
              addressBookFlag: true
            },
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;

      default:
        break;
    }
  }
  // end of item settings

  // add new address
  addAddress() {
    const dialogRef = this.dialog.open(AddressBookDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'New Address',
        addressBookFlag: true
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of add new address

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
