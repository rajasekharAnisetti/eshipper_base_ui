import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountComponent } from './account.component';
import { CustomerUsersComponent } from './customer-users/customer-users.component';
import { AddressBookComponent } from './address-book/address-book.component';
import { RolesComponent } from './roles/roles.component';
import { BoxManagerComponent } from './box-manager/box-manager.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { AccountInvoicesComponent } from './account-invoices/account-invoices.component';
import { AccountCarriersComponent } from './account-carriers/account-carriers.component';

const accountRoutes: Routes = [
  {
    path: '',
    redirectTo: 'roles',
    pathMatch: 'full',
  },
  {
    path: 'roles',
    component: RolesComponent,
    data: { title: 'Roles' },
  },
  {
    path: 'roles/role/edit',
    loadChildren: () =>
      import('src/app/role/role.module').then(mod => mod.RoleModule),
    data: { title: 'Edit Role', backBtn: true, },
  },
  {
    path: 'roles/role/new',
    loadChildren: () =>
      import('src/app/role/role.module').then(mod => mod.RoleModule),
    data: { title: 'Create Role', backBtn: true },
  },
  {
    path: 'users',
    component: CustomerUsersComponent,
    data: { title: 'Users' },
  },
  {
    path: 'address-book',
    component: AddressBookComponent,
    data: { title: 'Addres Book' },
  },
  {
    path: 'invoices',
    component: AccountInvoicesComponent,
    data: { title: 'Invoices' },
  },
  {
    path: 'box-manager',
    component: BoxManagerComponent,
    data: { title: 'Box Manager' },
  },
  {
    path: 'products-list',
    component: ProductsListComponent,
    data: { title: 'Products List' },
  },
  {
    path: 'carriers',
    component: AccountCarriersComponent,
    data: { title: 'Carriers' },
  },
];

const routes: Routes = [
  {
    path: '',
    component: AccountComponent,
    data: { title: 'Account' },
    children: accountRoutes,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountRoutingModule { }
