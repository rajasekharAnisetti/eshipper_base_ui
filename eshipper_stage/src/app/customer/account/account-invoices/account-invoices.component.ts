import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { Invoice } from 'src/app/interfaces/invoice';
import { PayDialogComponent } from 'src/app/dialogs/pay-dialog/pay-dialog.component';
import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';
import { InvoicesFilterDialogComponent } from 'src/app/filters/invoices-filter-dialog/invoices-filter-dialog.component';

import { AccountService } from '../account.service';

@Component({
  selector: 'app-account-invoices',
  templateUrl: './account-invoices.component.html',
  styleUrls: ['./account-invoices.component.scss'],
})
export class AccountInvoicesComponent implements OnInit, OnDestroy {
  subscribe: Subscription = new Subscription();
  dataEmpty = true;
  toolbarSettings = ['Details PDF', 'Summary PDF'];
  itemSettings = [
    'Details PDF',
    'Summary PDF',
    'Payment Activities',
    'Created Ticket',
  ];
  invoicesList: Array<Invoice> = [];

  constructor(
    public dialog: MatDialog,
    private getData: AccountService,
    private router: Router
  ) { }

  ngOnInit() {
    // get invoices
    this.subscribe.add(
      this.getData.getAccountInvoicesList().subscribe(data => {
        this.invoicesList = data.content;
        this.dataEmpty = data.content.length ? false : true;
      })
    );
  }

  // period
  getPeriod(val) {
    console.log(val);
  }
  // end of period

  // search
  searchFunction(val) {
    console.log(val);
  }
  // end of search

  // item settings function
  itemSettingsFunction(val) {
    console.log(val);
  }
  // end of item settings function

  // filter function
  filterOpen() {
    const dialogRef = this.dialog.open(InvoicesFilterDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of filter function

  // toolbar settings
  settingsFunction(val) {
    console.log(val);
  }
  // end of toolbar settings

  // item pay function
  payFunction() {
    const dialogRef = this.dialog.open(PayDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
      data: {
        title: 'Invoice #CWS12345678',
        checkboxLabel: 'Notify customer by Email',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }
  // end of item pay function

  // click on invoice row function
  clickOnInvoiceItem() {
    const url = '/admin/invoicing/invoice-preview';
    this.router.navigate([url]);
  }
  // end of click on invoice row function

  // open total info dialog
  openTotalInfoDialog(data) {
    this.dialog.open(InfoDialogComponent, {
      data: {
        title: 'Price Details',
        moreData: data,
      },
      autoFocus: false,
      panelClass: 'small-dialog-width',
    });
  }
  // end of open total info dialog

  // get warn
  getWarnFunction(item) {
    switch (item.status.name) {
      case 'Unpaid':
        return true;
      case 'Partially':
        return true;
      case 'Partially Paid':
        return true;
      case 'Released':
        return true;
      default:
        break;
    }
  }
  // end of get warn

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
