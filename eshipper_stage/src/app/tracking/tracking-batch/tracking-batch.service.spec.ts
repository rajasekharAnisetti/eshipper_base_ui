import { TestBed } from '@angular/core/testing';

import { TrackingBatchService } from './tracking-batch.service';

describe('TrackingBatchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TrackingBatchService = TestBed.get(TrackingBatchService);
    expect(service).toBeTruthy();
  });
});
