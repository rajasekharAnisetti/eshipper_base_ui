import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TrackingBatchService {
  constructor(private http: HttpClient) {}

  // get admin batches data
  getAdminBatches(): Observable<any> {
    return this.http
      .get('../../content/data/admin-batches.json')
      .pipe(map(data => data));
  }

  // get customer batches data
  getCustomerBatches(): Observable<any> {
    return this.http
      .get('../../content/data/customer-batches.json')
      .pipe(map(data => data));
  }
}
