import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackingBatchComponent } from './tracking-batch.component';

describe('TrackingBatchComponent', () => {
  let component: TrackingBatchComponent;
  let fixture: ComponentFixture<TrackingBatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackingBatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackingBatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
