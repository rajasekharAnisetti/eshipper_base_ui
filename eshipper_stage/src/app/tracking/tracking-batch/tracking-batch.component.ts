import { MatDialog } from '@angular/material/dialog';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { TrackingFilterDialogComponent } from 'src/app/filters/tracking-filter-dialog/tracking-filter-dialog.component';
import { AdminTrackingBatchFilterDialogComponent } from 'src/app/filters/admin-tracking-batch-filter-dialog/admin-tracking-batch-filter-dialog.component';

import { TrackingBatchService } from './tracking-batch.service';

@Component({
  selector: 'app-tracking-batch',
  templateUrl: './tracking-batch.component.html',
  styleUrls: ['./tracking-batch.component.scss'],
})
export class TrackingBatchComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  account = localStorage.getItem('account');
  adminFlag = false;
  dataEmpty = false;
  batches: Array<any>;
  contentToolbarSettings = ['Export', 'Print Labels'];
  itemSettings = ['Export', 'Print', 'Cancel'];

  constructor(
    private getData: TrackingBatchService,
    public dialog: MatDialog,
    public router: Router
  ) { }

  ngOnInit() {
    this.adminFlag = this.account === 'admin' ? true : false;
    this.adminFlag
      ? // get admin batches
      this.subscribe.add(
        this.getData.getAdminBatches().subscribe(data => {
          this.dataEmpty = data.content.length ? false : true;
          data.content
            ? (this.batches = data.content)
            : console.log('Error!!!');
        })
      )
      : // get customer batches
      this.subscribe.add(
        this.getData.getCustomerBatches().subscribe(data => {
          this.dataEmpty = data.content.length ? false : true;
          data.content
            ? (this.batches = data.content)
            : console.log('Error!!!');
        })
      );
  }

  settingsFunction(val) {
    switch (val) {
      case 'Export':
        const dialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          dialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print Labels':
        console.log(val);
        break;
      default:
        break;
    }
  }

  filterOpen() {
    if (this.adminFlag) {
      const dialogRef = this.dialog.open(AdminTrackingBatchFilterDialogComponent, {
        panelClass: 'normal-dialog-width',
        autoFocus: false,
      });
      this.subscribe.add(
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            console.log(result);
          }
        })
      );
    } else {
      const dialogRef = this.dialog.open(TrackingFilterDialogComponent, {
        panelClass: 'normal-dialog-width',
        autoFocus: false,
      });
      this.subscribe.add(
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            console.log(result);
          }
        })
      );
    }
  }

  getItemSettings(val) {
    switch (val) {
      case 'Export':
        const dialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          dialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;

      default:
        break;
    }
  }

  // get period
  getPeriod(period) {
    console.log(period);
  }

  // click on row
  clickOnRow() {
    const url =
      this.account === 'admin'
        ? '/admin/tracking/batch'
        : '/customer/tracking/batch';
    this.router.navigate([url]);
  }
  // end of click on row

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
