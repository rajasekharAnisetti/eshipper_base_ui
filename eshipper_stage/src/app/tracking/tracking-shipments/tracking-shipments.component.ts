import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { AdminTrackingFilterDialogComponent } from 'src/app/filters/admin-tracking-filter-dialog/admin-tracking-filter-dialog.component';
import { TrackingFilterDialogComponent } from 'src/app/filters/tracking-filter-dialog/tracking-filter-dialog.component';
import { ExportDialogComponent } from 'src/app/dialogs/export-dialog/export-dialog.component';
import { SchedulePickupDialogComponent } from 'src/app/dialogs/schedule-pickup-dialog/schedule-pickup-dialog.component';

import { TrackingShipmentsService } from './tracking-shipments.service';
import { Subscription } from 'rxjs';
import { sortAscDesc } from 'src/app/shared/functions/sort-asc-desc';
import { InfoDialogComponent } from 'src/app/dialogs/info-dialog/info-dialog.component';

@Component({
  selector: 'app-tracking-shipments',
  templateUrl: './tracking-shipments.component.html',
  styleUrls: ['./tracking-shipments.component.scss'],
})
export class TrackingShipmentsComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  account = localStorage.getItem('account');
  adminFlag = false;
  dataEmpty = true;
  trackings: Array<any>;
  filteKeywords: Array<string> = [];
  itemSettings = [
    'Edit',
    'Print Label',
    'Schedule Pickup',
    'divider',
    'File a Claim',
    'Cancel',
  ];
  sortMenu = ['Status', 'Cost'];
  contentToolbarSettings = ['Export', 'Print Labels'];
  constructor(
    public dialog: MatDialog,
    private getData: TrackingShipmentsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.adminFlag = this.account === 'admin' ? true : false;
    this.adminFlag
      ? // get admin trackings
      this.subscribe.add(
        this.getData.getAdminTrackings().subscribe(data => {
          this.dataEmpty = data.content.length ? false : true;
          data ? (this.trackings = data.content) : console.log('Error!!!');
        })
      )
      : // get customer trackings
      this.subscribe.add(
        this.getData.getCustomerTrackings().subscribe(data => {
          this.dataEmpty = data.content.length ? false : true;
          data ? (this.trackings = data.content) : console.log('Error!!!');
        })
      );
  }

  // filter function
  filterOpen() {
    if (this.adminFlag) {
      const dialogRef = this.dialog.open(AdminTrackingFilterDialogComponent, {
        panelClass: 'normal-dialog-width',
        autoFocus: false,
      });
      this.subscribe.add(
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            console.log(result);
            this.filteKeywords = [...result.array];
          }
        })
      );
    } else {
      const dialogRef = this.dialog.open(TrackingFilterDialogComponent, {
        panelClass: 'normal-dialog-width',
        autoFocus: false,
      });
      this.subscribe.add(
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            console.log(result);
            this.filteKeywords.push('test');
          }
        })
      );
    }
  }

  changeFilterKeywords(keywords) {
    console.log(keywords);
  }
  // end of filter function

  // sort data
  getSortData(val) {
    sortAscDesc(this.trackings, val.direction, val.name)
  }
  // end of sort data

  settingsFunction(val) {
    switch (val) {
      case 'Export':
        const dialogRef = this.dialog.open(ExportDialogComponent, {
          panelClass: 'normal-dialog-width',
          autoFocus: false,
        });
        this.subscribe.add(
          dialogRef.afterClosed().subscribe(result => {
            if (result) {
              console.log(result);
            }
          })
        );
        break;
      case 'Print Labels':
        console.log(val);
        break;
      default:
        break;
    }
  }

  // click on row function
  clickOnRow() {
    const url = this.adminFlag ? '/admin/tracking/track' : '/customer/tracking/track';
    this.router.navigate([url]);
  }
  // end of click on row function

  getItemSettingsValue(val) {
    console.log(val);
    switch (val) {
      case 'Edit':
        {
          const url = this.adminFlag
            ? '/admin/work-order/accepted-work-order'
            : '/customer/shipping/ship/edit-details';
          this.router.navigate([url]);
        }
        break;
      case 'Schedule Pickup':
        {
          const dialogRef = this.dialog.open(SchedulePickupDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;
      case 'Cancel':
        break;
      case 'File a Claim':
        {
          const url = '/file-a-claim';
          this.router.navigate([url]);
        }
        break;

      default:
        break;
    }
  }
  // get period
  getPeriod(period) {
    console.log(period);
  }

  openCreditInfoDialog(data) {
    this.dialog.open(InfoDialogComponent, {
      data: {
        title: 'Cost Details',
        moreData: data,
      },
      autoFocus: false,
      panelClass: 'normal-dialog-width',
    });
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
