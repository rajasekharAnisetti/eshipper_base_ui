import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { TrackingShipmentsComponent } from './tracking-shipments.component';

@NgModule({
  declarations: [TrackingShipmentsComponent],
  imports: [
    CommonModule,
    MaterialModules,
    PerfectScrollbarModule,
    RouterModule,
    FormsModule,
    SharedModule,
  ],
  exports: [TrackingShipmentsComponent],
})
export class TrackingShipmentsModule {}
