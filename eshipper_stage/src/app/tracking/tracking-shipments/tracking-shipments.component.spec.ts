import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackingShipmentsComponent } from './tracking-shipments.component';

describe('TrackingShipmentsComponent', () => {
  let component: TrackingShipmentsComponent;
  let fixture: ComponentFixture<TrackingShipmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackingShipmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackingShipmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
