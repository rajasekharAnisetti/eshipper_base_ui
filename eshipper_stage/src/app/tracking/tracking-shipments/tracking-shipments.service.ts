import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TrackingShipmentsService {
  constructor(private http: HttpClient) {}

  // get customer tracking data
  getCustomerTrackings(): Observable<any> {
    return this.http
      .get('../../content/data/customer-trackings.json')
      .pipe(map(data => data));
  }

  // get admin trackings data
  getAdminTrackings(): Observable<any> {
    return this.http
      .get('../../content/data/admin-trackings.json')
      .pipe(map(data => data));
  }
}
