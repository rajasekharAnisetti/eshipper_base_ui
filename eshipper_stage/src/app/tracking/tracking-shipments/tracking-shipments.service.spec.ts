import { TestBed } from '@angular/core/testing';

import { TrackingShipmentsService } from './tracking-shipments.service';

describe('TrackingShipmentsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TrackingShipmentsService = TestBed.get(TrackingShipmentsService);
    expect(service).toBeTruthy();
  });
});
