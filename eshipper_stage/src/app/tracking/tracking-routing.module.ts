import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TrackingComponent } from './tracking.component';
import { TrackComponent } from './track/track.component';
import { BatchComponent } from './batch/batch.component';

const routes: Routes = [
  {
    path: '',
    component: TrackingComponent,
    data: { title: 'Tracking' },
  },
  {
    path: 'track',
    component: TrackComponent,
    data: {
      title: 'Tracking',
      backBtn: true,
    },
  },
  {
    path: 'batch',
    component: BatchComponent,
    data: {
      title: 'XCV1234567890123',
      backBtn: true,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TrackingRoutingModule {}
