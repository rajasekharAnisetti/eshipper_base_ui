import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModules } from '../material-modules';
import { TrackingRoutingModule } from './tracking-routing.module';
import { TrackingShipmentsModule } from './tracking-shipments/tracking-shipment.module';
import { TrackingBatchModule } from './tracking-batch/tracking-batch.module';
import { TrackModule } from './track/track.module';
import { BatchModule } from './batch/batch.module';

import { TrackingComponent } from './tracking.component';

@NgModule({
  declarations: [TrackingComponent],
  imports: [
    CommonModule,
    MaterialModules,
    TrackingRoutingModule,
    TrackingShipmentsModule,
    TrackingBatchModule,
    TrackModule,
    BatchModule,
  ],
  exports: [TrackingComponent],
})
export class TrackingModule {}
