import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { SchedulePickupDialogComponent } from 'src/app/dialogs/schedule-pickup-dialog/schedule-pickup-dialog.component';
import { TrackingFilterDialogComponent } from 'src/app/filters/tracking-filter-dialog/tracking-filter-dialog.component';
import { AdminTrackingFilterDialogComponent } from 'src/app/filters/admin-tracking-filter-dialog/admin-tracking-filter-dialog.component';

import {
  SIMPLESETTINGS,
  CUSTOMERBATCHITEMSETTINGS,
  ADMINBATCHITEMSETTINGS,
} from 'src/app/shared/const/dropdowns';

import { BatchService } from './batch.service';


@Component({
  selector: 'app-batch',
  templateUrl: './batch.component.html',
  styleUrls: ['./batch.component.scss'],
})
export class BatchComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  account = localStorage.getItem('account');
  adminFlag = false;
  dataEmpty = false;
  trackings: Array<any>;
  smallWidgetItems: Array<any>;
  itemSettings: Array<string> = [];
  contentToolbarSettings = SIMPLESETTINGS;

  constructor(public router: Router, public dialog: MatDialog, private getData: BatchService) { }

  ngOnInit() {
    this.adminFlag = this.account === 'admin' ? true : false;

    if (this.adminFlag) {
      // item settings
      this.itemSettings = ADMINBATCHITEMSETTINGS;
      // get admin trackings
      this.subscribe.add(
        this.getData.getAdminTrackings().subscribe(data => {
          this.dataEmpty = data.content.length ? false : true;
          data ? (this.trackings = data.content) : console.log('Data File Error!!!');
        })
      );
      // get admin small widgets
      this.subscribe.add(
        this.getData
          .getBatchSmallWidgetsAdmin()
          .subscribe(data =>
            data
              ? (this.smallWidgetItems = data)
              : console.log('Data File Error!!!')
          )
      );
    } else {
      // item settings
      this.itemSettings = CUSTOMERBATCHITEMSETTINGS;
      // get customer trackings
      this.subscribe.add(
        this.getData.getCustomerTrackings().subscribe(data => {
          this.dataEmpty = data.content.length ? false : true;
          data ? (this.trackings = data.content) : console.log('Data File Error!!!');
        })
      );
      // get customer small widgets
      this.subscribe.add(
        this.getData
          .getBatchSmallWidgets()
          .subscribe(data =>
            data
              ? (this.smallWidgetItems = data)
              : console.log('Data File Error!!!')
          )
      );
    }
  }

  // click on row function
  clickOnRow() {
    const url = this.adminFlag ? '/admin/tracking/track' : '/customer/tracking/track';
    this.router.navigate([url]);
  }
  // end of click on row function

  getItemSettingsValue(val) {
    console.log(val);
    switch (val) {
      case 'Edit':
        break;
      case 'Schedule Pickup':
        {
          const dialogRef = this.dialog.open(SchedulePickupDialogComponent, {
            panelClass: 'normal-dialog-width',
            autoFocus: false,
          });
          this.subscribe.add(
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                console.log(result);
              }
            })
          );
        }
        break;
      case 'Cancel':
        break;
      case 'File a Claim':
        {
          const url = '/file-a-claim';
          this.router.navigate([url]);
        }
        break;

      default:
        break;
    }
  }

  // filter function
  filterOpen() {
    if (this.adminFlag) {
      const dialogRef = this.dialog.open(AdminTrackingFilterDialogComponent, {
        panelClass: 'normal-dialog-width',
        autoFocus: false,
      });
      this.subscribe.add(
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            console.log(result);
          }
        })
      );
    } else {
      const dialogRef = this.dialog.open(TrackingFilterDialogComponent, {
        panelClass: 'normal-dialog-width',
        autoFocus: false,
      });
      this.subscribe.add(
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            console.log(result);
          }
        })
      );
    }
  }
  //  end of filter function

  // settings function
  settingsFunction(val) {
    console.log(val)
  }
  // end of settings function

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
