import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class BatchService {
  constructor(private http: HttpClient) {}

  // get customer tracking data
  getCustomerTrackings(): Observable<any> {
    return this.http
      .get('../../content/data/customer-trackings.json')
      .pipe(map(data => data));
  }

  // get admin trackings data
  getAdminTrackings(): Observable<any> {
    return this.http
      .get('../../content/data/admin-trackings.json')
      .pipe(map(data => data));
  }

  // Batch small widgets
  getBatchSmallWidgets(): Observable<any> {
    return this.http
      .get('../../content/data/batch-small-widgets.json')
      .pipe(map(data => data));
  }

  // Batch small widgets for admin
  getBatchSmallWidgetsAdmin(): Observable<any> {
    return this.http
      .get('../../content/data/batch-small-widgets-admin.json')
      .pipe(map(data => data));
  }
}
