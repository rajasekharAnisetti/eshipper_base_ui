import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { BatchComponent } from './batch.component';

@NgModule({
  declarations: [BatchComponent],
  imports: [
    CommonModule,
    MaterialModules,
    PerfectScrollbarModule,
    RouterModule,
    FormsModule,
    SharedModule,
  ],
  exports: [BatchComponent],
})
export class BatchModule {}
