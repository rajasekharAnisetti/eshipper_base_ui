import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Track } from 'src/app/interfaces/wo';
import { TrackingTrackService } from './tracking-track.service';

@Component({
  selector: 'app-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.scss'],
})
export class TrackComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  account = localStorage.getItem('account');

  // coordinates for example only
  lat = 43.727022;
  lng = -79.327631;
  // end of coordinates

  trackData = <Track>{};

  constructor(private getData: TrackingTrackService) {}

  ngOnInit() {
    if (this.account === 'admin') {
      this.subscribe.add(
        this.getData.getAdminTrack().subscribe(data => {
          data ? (this.trackData = data) : console.log('Data File Error!!!');
        })
      );
    } else {
      // get customer track
      this.subscribe.add(
        this.getData.getCustomerTrack().subscribe(data => {
          data ? (this.trackData = data) : console.log('Data File Error!!!');
        })
      );
    }
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
