import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class TrackingTrackService {
  constructor(private http: HttpClient) {}

  // get customer track data
  getCustomerTrack(): Observable<any> {
    return this.http
      .get('../../content/data/customer-track.json')
      .pipe(map(data => data));
  }

  // get admin track data
  getAdminTrack(): Observable<any> {
    return this.http
      .get('../../content/data/admin-track.json')
      .pipe(map(data => data));
  }
}
