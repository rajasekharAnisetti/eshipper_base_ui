import { TestBed } from '@angular/core/testing';

import { TrackingTrackService } from './tracking-track.service';

describe('TrackingTrackService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TrackingTrackService = TestBed.get(TrackingTrackService);
    expect(service).toBeTruthy();
  });
});
