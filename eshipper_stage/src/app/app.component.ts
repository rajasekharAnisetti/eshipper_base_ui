import { ChangeDetectorRef, Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { MediaMatcher } from '@angular/cdk/layout';
import { filter, map } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  mobileQuery: MediaQueryList;
  desctopQuery: MediaQueryList;
  title = 'Dashboard';
  backBtnFlag = false;
  customerActiveFlag: boolean;
  account: string;
  users = [];
  loginFlag = false;
  notificationsFlag = true;
  newNotifications: string;
  userPosition: boolean;

  private _mobileQueryListener: () => void;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 1420px)');
    this.desctopQuery = media.matchMedia('(max-width: 1420px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    // get data from routing
    this.subscribe.add(
      this.router.events
        .pipe(filter(event => event instanceof NavigationEnd))
        .pipe(
          map(() => {
            let child = this.activatedRoute.firstChild;
            while (child) {
              if (child.firstChild) {
                child = child.firstChild;
              } else if (child.snapshot.data) {
                if (
                  child.snapshot.data.title ||
                  child.snapshot.data.loginFlag
                ) {
                  return child.snapshot.data;
                } else {
                  return child.parent.snapshot.data;
                }
              } else {
                return null;
              }
            }
            return null;
          })
        )
        .subscribe(data => {
          this.title = data.title;
          this.backBtnFlag = data.backBtn ? true : false;
          this.loginFlag = data.loginFlag ? true : false;
        })
    );
    // get notifications count
    this.newNotifications = localStorage.getItem('newnotifications');
  }

  ngOnDestroy() {
    this.mobileQuery.removeListener(this._mobileQueryListener);
    this.desctopQuery.removeListener(this._mobileQueryListener);
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
