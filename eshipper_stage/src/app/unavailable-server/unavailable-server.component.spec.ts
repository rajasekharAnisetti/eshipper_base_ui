import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnavailableServerComponent } from './unavailable-server.component';

describe('UnavailableServerComponent', () => {
  let component: UnavailableServerComponent;
  let fixture: ComponentFixture<UnavailableServerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnavailableServerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnavailableServerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
