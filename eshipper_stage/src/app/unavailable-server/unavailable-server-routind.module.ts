import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UnavailableServerComponent } from './unavailable-server.component';

const routes: Routes = [
  {
    path: '',
    component: UnavailableServerComponent,
    data: { title: 'Unavailable' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UnavailableServerRoutingModule { }
