import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { MaterialModules } from 'src/app/material-modules';

import { UnavailableServerRoutingModule } from './unavailable-server-routind.module';
import { UnavailableServerComponent } from './unavailable-server.component';

@NgModule({
  declarations: [UnavailableServerComponent],
  imports: [
    CommonModule,
    MaterialModules,
    PerfectScrollbarModule,
    UnavailableServerRoutingModule
  ],
  exports: [UnavailableServerComponent],
})
export class UnavailableServerModule { }
