import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FileAClaimComponent } from './file-a-claim.component';

const roleRoutes: Routes = [
  {
    path: '',
    component: FileAClaimComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(roleRoutes)],
  exports: [RouterModule],
})
export class FileAClaimRoutingModule { }
