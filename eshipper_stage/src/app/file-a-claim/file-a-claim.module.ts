import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { FileAClaimComponent } from './file-a-claim.component';
import { FileAClaimRoutingModule } from './file-a-claim.routing.module';



@NgModule({
  declarations: [FileAClaimComponent],
  imports: [
    CommonModule,
    FileAClaimRoutingModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModules,
    SharedModule
  ],
  exports: [FileAClaimComponent],
})
export class FileAClaimModule { }
