import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-file-a-claim',
  templateUrl: './file-a-claim.component.html',
  styleUrls: ['./file-a-claim.component.scss']
})
export class FileAClaimComponent implements OnInit {
  minDate = new Date();
  reasons = ['Damaged Shipment', 'Development Help', 'Feedback'];
  contactPreferences = ['Someone', 'Option 2', 'Option 3'];
  claim = {
    reason: '',
    contactPreference: '',
    receivedDate: '',
    mailedDate: '',
    description: '',
    details: '',
    documents: '',
    notifyMeForUpdates: ''
  }
  shipment = {
    logo: 'content/images/logos/ups.png',
    tracking: 'WTX890890654',
    valueOfGoods: 678900.0,
    shipData: '03/27/2017',
    cost: 789900.0,
    insurance: 'eshipper',
    amount: 789900.0,
    customer: 'Duonamic Technology',
    email: 'brigitte_eichmann@mafalda.me',
    phone: '111-060-9732',
    shipping: '700 Mitchell Flats Suite 253, Mississauga, L5B 1B8, CA',
    billing: 'Same as shipping address',
  };

  validation = {
    reason: new FormControl(),
    contactPreference: new FormControl(),
    description: new FormControl(),
    details: new FormControl(),
    mailedDate: new FormControl(),
    receivedDate: new FormControl(),
  }


  constructor(public router: Router) { }

  ngOnInit() {
  }

  // create function
  createFileAClaim() { }
  // end of create function

  // change documents list
  changeDocuments(items, docs) {
    docs = items;
  }
  // end of change documents list

}
