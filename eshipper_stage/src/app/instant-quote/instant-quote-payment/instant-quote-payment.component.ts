import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { Subscription, Observable } from 'rxjs';
import { MediaMatcher } from '@angular/cdk/layout';

import { DetailsData } from 'src/app/interfaces/details-data';
import { TextDialogComponent } from 'src/app/dialogs/text-dialog/text-dialog.component';

import { InstantQuotePaymentService } from './instant-quote-payment.service';
import { Countries } from 'src/app/shared/const/countries';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-instant-quote-payment',
  templateUrl: './instant-quote-payment.component.html',
  styleUrls: ['./instant-quote-payment.component.scss'],
})
export class InstantQuotePaymentComponent implements OnInit, OnDestroy {
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;

  instantQuoteSteps = [
    {
      position: 'Details',
      state: true,
    },
    {
      position: 'Payment',
      state: false,
    },
    {
      position: 'Invoice',
      state: false,
    },
    {
      position: 'Summary',
      state: false,
    },
  ];

  workingData = {
    title: 'Instant Quote',
    backBtnFlag: true,
  };

  // payment data
  cards = [
    {
      title: 'Anerican Express',
      path: 'content/images/cards/am-express.svg',
    },
    {
      title: 'Master Card',
      path: 'content/images/cards/mastercard.svg',
    },
    {
      title: 'Visa',
      path: 'content/images/cards/visa.svg',
    },
  ];
  payment = {
    value: '',
    creditCard: {
      cardNumberFormControl: new FormControl(),
      cardNumber: '1234 1234 1234 0000',
      cardIcon: 'content/images/cards/mastercard.svg',
      date: {
        month: '',
        year: '',
      },
      cvcFormControl: new FormControl(),
      cvc: '',
      cvcIcon: 'content/images/cards/cvc.svg',
      cardHolderName: '',
      cardHolderNameFormControl: new FormControl(),
    },
  };
  // end of payment data

  filteredCityes: Observable<any[]>;
  staticData = {
    countries: Countries.COUNTRIES,
    provinces: Countries.USASTATES,
    cities: Countries.USACITIES,
    stateLabel: 'State',
    zipLabel: 'Zip Code',
  };

  // billing data
  billing = {
    sameSenderInformation: true,
    name: '',
    address1: '',
    address2: '',
    city: '',
    state: '',
    zip: '',
    country: '',
  };
  // end of billing data

  validation = {
    nameFormControl: new FormControl(),
    address1FormControl: new FormControl(),
    address2FormControl: new FormControl(),
    cityFormControl: new FormControl(),
    stateFormControl: new FormControl(),
    zipFormControl: new FormControl(),
    countryFormControl: new FormControl(),
  }

  // sidebar data
  subscribe = new Subscription();
  invoiceDetails = {
    logo: 'content/images/logos/ups.png',
    service: 'Next Flight Out',
    quote: 567.88,
  };
  detailsData = <DetailsData>{};

  constructor(
    private getData: InstantQuotePaymentService,
    public dialog: MatDialog,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 1024px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    this.subscribe.add(
      this.getData
        .getShipDetailsSidebar()
        .subscribe(data => (this.detailsData = data))
    );
    // for city
    this.filteredCityes = this.validation.cityFormControl.valueChanges.pipe(
      startWith(''),
      map(city => city ? this._filter(city) : this.staticData.cities.slice())
    )
  }

  // city autocomplete
  displayFn(city?: string): string | undefined {
    return city ? city : undefined;
  }

  private _filter(city: string) {
    const filterValue = city.toLowerCase();

    return this.staticData.cities.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
  }
  // emd of city autocomplete

  changeCountry(country) {
    this.billing.city = '';
    switch (country) {
      case 'Canada':
        {
          this.staticData.stateLabel = 'Province';
          this.staticData.zipLabel = 'Postal Code';
          this.staticData.provinces = Countries.CANADAPROVINCES;
          this.staticData.cities = Countries.CANADACITIES;
        }
        break;
      case 'USA':
        {
          this.staticData.stateLabel = 'State';
          this.staticData.zipLabel = 'Zip Code';
          this.staticData.provinces = Countries.USASTATES;
          this.staticData.cities = Countries.USACITIES;
        }
        break;
      default:
        break;
    }
  }

  openTextDialog() {
    const dialogRef = this.dialog.open(TextDialogComponent, {
      autoFocus: false,
      panelClass: 'normal-dialog-width',
      data: {
        title: 'Disclaimer',
        text:
          'Living in today’s metropolitan world of cellular phones, mobile computers and other high-tech gadgets is not just hectic but very impersonal.' +
          'We make money and then invest our time and effort in making more money. Does it end? Not usually because we are never satisfied. How many times ' +
          'have we convinced ourselves that if only we had some more money, life would be so sweet? But then, after receiving a substantial raise, we ' +
          'realize that it wasn’t enough and that we need more?\n\n' +
          'What Should You Do? \n\n' +
          'I have read many books on life such as Robin Sharma’s Monk says this and the monk says that, and they all seem to say that money is not ' +
          'necessary. But it is. Can you do without cash and a lot of it? I know I can’t. So, I went to the neighbourhood Rabbi and asked for advice ' +
          'that will help me find my true way in life. The rabbi nodded and took me to the window. “What do you see?” he asked me.',
      },
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }

  getCardDate(date) {
    this.payment.creditCard.date.month = date.month;
    this.payment.creditCard.date.year = date.year;
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
}
