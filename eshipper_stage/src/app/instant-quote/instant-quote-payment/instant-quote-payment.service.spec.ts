import { TestBed } from '@angular/core/testing';

import { InstantQuotePaymentService } from './instant-quote-payment.service';

describe('InstantQuotePaymentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InstantQuotePaymentService = TestBed.get(InstantQuotePaymentService);
    expect(service).toBeTruthy();
  });
});
