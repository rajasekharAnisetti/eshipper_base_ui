import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstantQuotePaymentComponent } from './instant-quote-payment.component';

describe('InstantQuotePaymentComponent', () => {
  let component: InstantQuotePaymentComponent;
  let fixture: ComponentFixture<InstantQuotePaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstantQuotePaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstantQuotePaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
