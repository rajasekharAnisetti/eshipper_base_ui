import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModules } from 'src/app/material-modules';
import { SharedModule } from 'src/app/shared/shared.module';

import { InstantQuoteRoutingModule } from './instant-quote-routing.module';

import { InstantQuoteComponent } from './instant-quote.component';
import { InstantQuoteProceedComponent } from './instant-quote-proceed/instant-quote-proceed.component';
import { InstantQuotePaymentComponent } from './instant-quote-payment/instant-quote-payment.component';
import { InstantQuoteInvoiceComponent } from './instant-quote-invoice/instant-quote-invoice.component';
import { InstantQuoteConfirmComponent } from './instant-quote-confirm/instant-quote-confirm.component';
import { LoginDialogComponent } from './login-dialog/login-dialog.component';
import { QuotesDialogComponent } from './quotes-dialog/quotes-dialog.component';

@NgModule({
  declarations: [
    InstantQuoteComponent,
    InstantQuoteProceedComponent,
    InstantQuotePaymentComponent,
    InstantQuoteInvoiceComponent,
    InstantQuoteConfirmComponent,
    LoginDialogComponent,
    QuotesDialogComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    MaterialModules,
    RouterModule,
    InstantQuoteRoutingModule,
    SharedModule,
  ],
  exports: [InstantQuoteComponent],
  entryComponents: [LoginDialogComponent, QuotesDialogComponent],
})
export class InstantQuoteModule {}
