import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Subscription, Observable } from 'rxjs';

import { Countries } from 'src/app/shared/const/countries';

import { QuotesDialogComponent } from './quotes-dialog/quotes-dialog.component';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-instant-quote',
  templateUrl: './instant-quote.component.html',
  styleUrls: ['./instant-quote.component.scss'],
})
export class InstantQuoteComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  filteredCityesFrom: Observable<any[]>;
  filteredCityesTo: Observable<any[]>;
  countries: Array<string>;
  cities = {
    from: [],
    to: [],
  };
  states = {
    from: [],
    to: [],
  };

  data = {
    title: 'Instant Quote',
    backBtnFlag: true,
    from: {
      countryFormControl: new FormControl(),
      stateFormControl: new FormControl(),
      zipFormControl: new FormControl(),
      cityFormControl: new FormControl(),
      values: {
        zipLabel: '',
        stateLabel: '',
        data: {
          city: '',
          province: '',
          zip: '',
          country: '',
        },
      },
      checkboxes: [
        {
          checked: false,
          label: 'Residential',
        },
      ],
    },
    to: {
      countryFormControl: new FormControl(),
      stateFormControl: new FormControl(),
      zipFormControl: new FormControl(),
      cityFormControl: new FormControl(),
      values: {
        zipLabel: '',
        stateLabel: '',
        data: {
          city: '',
          province: '',
          zip: '',
          country: '',
        },
      },
      checkboxes: [
        {
          checked: false,
          label: 'Residential',
        },
      ],
    },
  };

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
    this.data.from.values.zipLabel = 'Postal';
    this.data.to.values.zipLabel = 'Postal';
    this.data.from.values.stateLabel = 'Province';
    this.data.to.values.stateLabel = 'Province';
    this.countries = Countries.COUNTRIES;
    this.cities.from = Countries.CANADACITIES;
    this.cities.to = Countries.CANADACITIES;
    this.states.from = Countries.CANADAPROVINCES;
    this.states.to = Countries.CANADAPROVINCES;

    // for city from
    this.filteredCityesFrom = this.data.from.cityFormControl.valueChanges.pipe(
      startWith(''),
      map(city => city ? this._filterFrom(city) : this.cities.from.slice())
    )

    // for city to
    this.filteredCityesTo = this.data.to.cityFormControl.valueChanges.pipe(
      startWith(''),
      map(city => city ? this._filterTo(city) : this.cities.to.slice())
    )
  }

  // city autocomplete
  displayFn(city?: string): string | undefined {
    return city ? city : undefined;
  }

  private _filterFrom(city: string) {
    const filterValue = city.toLowerCase();

    return this.cities.from.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
  }
  private _filterTo(city: string) {
    const filterValue = city.toLowerCase();

    return this.cities.to.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
  }
  // emd of city autocomplete

  changeCountry(country, flag) {
    switch (country) {
      case 'Canada':
        switch (flag) {
          case 'to':
            {
              this.data.to.values.zipLabel = 'Postal';
              this.data.to.values.stateLabel = 'Province';
              this.states.to = Countries.CANADAPROVINCES;
              this.cities.to = Countries.CANADACITIES;
            }
            break;
          case 'from':
            {
              this.data.from.values.stateLabel = 'Province';
              this.data.from.values.zipLabel = 'Postal';
              this.states.from = Countries.CANADAPROVINCES;
              this.cities.from = Countries.CANADACITIES;
            }
            break;
          default:
            break;
        }
        break;
      case 'USA':
        switch (flag) {
          case 'to':
            {
              this.data.to.values.zipLabel = 'Zip';
              this.data.to.values.stateLabel = 'State';
              this.states.to = Countries.USASTATES;
              this.cities.to = Countries.USACITIES;
            }
            break;
          case 'from':
            {
              this.data.from.values.zipLabel = 'Zip';
              this.data.from.values.stateLabel = 'State';
              this.states.from = Countries.USASTATES;
              this.cities.from = Countries.USACITIES;
            }
            break;
          default:
            break;
        }
        break;
      default:
        break;
    }
  }

  changePosition() {
    console.log('change data');
  }

  openProcessDialog() {
    const dialogRef = this.dialog.open(QuotesDialogComponent, {
      autoFocus: false,
      panelClass: 'qoute-dialog-width',
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
