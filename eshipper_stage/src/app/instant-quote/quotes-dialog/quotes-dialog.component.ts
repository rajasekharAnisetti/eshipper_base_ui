import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialog,
} from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { LoginDialogComponent } from '../login-dialog/login-dialog.component';

@Component({
  selector: 'app-quotes-dialog',
  templateUrl: './quotes-dialog.component.html',
  styleUrls: ['./quotes-dialog.component.scss'],
})
export class QuotesDialogComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  proceedData: any;

  quotes = [
    {
      img: 'content/images/standard.svg',
      title: 'Standard',
      time: '30 Est. Days',
      price: 234.5,
      additionalInfo: [
        {
          label: 'Base',
          value: 456.5,
        },
        {
          label: 'Border Fee',
          value: 50.5,
        },
        {
          label: 'Other (16%)',
          value: 12.5,
        },
        {
          label: 'Fuel',
          value: 90.2,
        },
        {
          label: 'HST (13 %)',
          value: 78.34,
        },
      ],
    },
    {
      img: 'content/images/express.svg',
      title: 'Express',
      time: '6 Est. Days',
      price: 3678.5,
      additionalInfo: [
        {
          label: 'Base',
          value: 456.5,
        },
        {
          label: 'Border Fee',
          value: 50.5,
        },
        {
          label: 'Other (16%)',
          value: 12.5,
        },
        {
          label: 'Fuel',
          value: 90.2,
        },
        {
          label: 'HST (13 %)',
          value: 78.34,
        },
      ],
    },
  ];

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<QuotesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  closeDialog = () => this.dialogRef.close();

  selectCard(item) {
    console.log(item);
    this.dialogRef.close();
    const dialogRef = this.dialog.open(LoginDialogComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          console.log(result);
        }
      })
    );
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
