import { TestBed } from '@angular/core/testing';

import { InstantQuoteConfirmService } from './instant-quote-confirm.service';

describe('InstantQuoteConfirmService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InstantQuoteConfirmService = TestBed.get(InstantQuoteConfirmService);
    expect(service).toBeTruthy();
  });
});
