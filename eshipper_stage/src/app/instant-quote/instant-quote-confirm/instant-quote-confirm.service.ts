import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class InstantQuoteConfirmService {
  constructor(private http: HttpClient) {}

  getInstantQuoteConfirmed(): Observable<any> {
    return this.http
      .get('content/data/instant-quote-confirmed.json')
      .pipe(map(data => data));
  }
}
