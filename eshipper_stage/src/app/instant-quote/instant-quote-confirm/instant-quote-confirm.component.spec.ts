import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstantQuoteConfirmComponent } from './instant-quote-confirm.component';

describe('InstantQuoteConfirmComponent', () => {
  let component: InstantQuoteConfirmComponent;
  let fixture: ComponentFixture<InstantQuoteConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstantQuoteConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstantQuoteConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
