import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { ValidationClassStateMatcher } from 'src/app/shared/classes/validation-class';
import { Observable } from 'rxjs';
import { Countries } from 'src/app/shared/const/countries';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-instant-quote-proceed',
  templateUrl: './instant-quote-proceed.component.html',
  styleUrls: ['./instant-quote-proceed.component.scss'],
})
export class InstantQuoteProceedComponent implements OnInit {
  filteredCityesFrom: Observable<any[]>;
  filteredCityesTo: Observable<any[]>;
  matcher = new ValidationClassStateMatcher();
  instantQuoteSteps = [
    {
      position: 'Details',
      state: false,
    },
    {
      position: 'Payment',
      state: false,
    },
    {
      position: 'Invoice',
      state: false,
    },
    {
      position: 'Summary',
      state: false,
    },
  ];

  countries: Array<string>;
  cities = {
    from: [],
    to: [],
  };
  states = {
    from: [],
    to: [],
  };

  workingData = {
    title: 'Instant Quote',
    backBtnFlag: true,
    from: {
      values: {
        value: 'Fairmont Royal York',
        specialInstructions: '',
        zipLabel: 'Postal',
        stateLabel: 'Province',
        data: {
          companyName: '',
          address1: '100 Front St W',
          address2: '',
          city: 'Toronto',
          province: 'ON',
          zip: '3H4 2G5',
          country: 'Canada',
          attention: 'Tyler Figueroa',
          phone: '888 000 8888',
          email: 'braden_bode@yahoo.com',
          taxId: '',
        },
      },
      locationControl: new FormControl(),
      address1FormControl: new FormControl(),
      address2FormControl: new FormControl(),
      countryFormControl: new FormControl(),
      stateFormControl: new FormControl(),
      cityFormControl: new FormControl(),
      zipFormControl: new FormControl(),
      attentionFormControl: new FormControl(),
      phoneFormControl: new FormControl(),
      emailFormControl: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      checkboxes: [
        {
          checked: false,
          label: 'Residential',
        },
        {
          checked: false,
          label: 'Return',
        },
        {
          checked: false,
          label: 'Notify',
        },
      ],
    },
    to: {
      values: {
        value: 'MacAndrews & Forbes Holdings USA',
        specialInstructions: '',
        zipLabel: 'Postal',
        stateLabel: 'Province',
        data: {
          companyName: '',
          address1: '268 Lake Rd',
          address2: '',
          city: 'Toronto',
          province: 'ON',
          zip: '3H4 2G5',
          country: 'Canada',
          attention: 'Tyler Figueroa',
          phone: '888 000 8888',
          email: 'braden_bode@yahoo.com',
          taxId: '',
        },
      },
      locationControl: new FormControl(),
      address1FormControl: new FormControl(),
      address2FormControl: new FormControl(),
      countryFormControl: new FormControl(),
      stateFormControl: new FormControl(),
      cityFormControl: new FormControl(),
      zipFormControl: new FormControl(),
      attentionFormControl: new FormControl(),
      phoneFormControl: new FormControl(),
      emailFormControl: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      checkboxes: [
        {
          checked: false,
          label: 'Residential',
        },
        {
          checked: false,
          label: 'Notify',
        },
      ],
    },
  };

  qoute = {
    image: '',
    title: 'Standard',
    description: '',
    date: '40 Est. Days',
    value: 1897.36,
    details: [
      {
        label: 'Base Charge',
        value: 456.5,
      },
      {
        label: 'Border Fee',
        value: 50.5,
      },
      {
        label: 'Other (16%)',
        value: 12.5,
      },
      {
        label: 'Fuel Surcharges',
        value: 90.2,
      },
      {
        label: 'HST (13 %)',
        value: 78.34,
      },
    ],
    selected: false,
  };

  constructor() { }

  ngOnInit() {
    this.countries = Countries.COUNTRIES;
    this.cities.from = Countries.CANADACITIES;
    this.cities.to = Countries.CANADACITIES;
    this.states.from = Countries.CANADAPROVINCES;
    this.states.to = Countries.CANADAPROVINCES;
    // for city from
    this.filteredCityesFrom = this.workingData.from.cityFormControl.valueChanges.pipe(
      startWith(''),
      map(city => city ? this._filterFrom(city) : this.cities.from.slice())
    )

    // for city to
    this.filteredCityesTo = this.workingData.to.cityFormControl.valueChanges.pipe(
      startWith(''),
      map(city => city ? this._filterTo(city) : this.cities.to.slice())
    )
  }

  // city autocomplete
  displayFn(city?: string): string | undefined {
    return city ? city : undefined;
  }

  private _filterFrom(city: string) {
    const filterValue = city.toLowerCase();

    return this.cities.from.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
  }
  private _filterTo(city: string) {
    const filterValue = city.toLowerCase();

    return this.cities.to.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
  }
  // emd of city autocomplete

  changeCountry(country, flag) {
    switch (country) {
      case 'Canada':
        switch (flag) {
          case 'to':
            {
              this.workingData.to.values.zipLabel = 'Postal';
              this.workingData.to.values.stateLabel = 'Province';
              this.states.to = Countries.CANADAPROVINCES;
              this.cities.to = Countries.CANADACITIES;
            }
            break;
          case 'from':
            {
              this.workingData.from.values.stateLabel = 'Province';
              this.workingData.from.values.zipLabel = 'Postal';
              this.states.from = Countries.CANADAPROVINCES;
              this.cities.from = Countries.CANADACITIES;
            }
            break;
          default:
            break;
        }
        break;
      case 'USA':
        switch (flag) {
          case 'to':
            {
              this.workingData.to.values.zipLabel = 'Zip';
              this.workingData.to.values.stateLabel = 'State';
              this.states.to = Countries.USASTATES;
              this.cities.to = Countries.USACITIES;
            }
            break;
          case 'from':
            {
              this.workingData.from.values.zipLabel = 'Zip';
              this.workingData.from.values.stateLabel = 'State';
              this.states.from = Countries.USASTATES;
              this.cities.from = Countries.USACITIES;
            }
            break;
          default:
            break;
        }
        break;
      default:
        break;
    }
  }

  changePosition() {
    console.log('change data');
  }
}
