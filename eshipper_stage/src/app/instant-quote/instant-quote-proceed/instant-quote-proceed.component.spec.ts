import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstantQuoteProceedComponent } from './instant-quote-proceed.component';

describe('InstantQuoteProceedComponent', () => {
  let component: InstantQuoteProceedComponent;
  let fixture: ComponentFixture<InstantQuoteProceedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstantQuoteProceedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstantQuoteProceedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
