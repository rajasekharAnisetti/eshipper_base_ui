import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InstantQuoteComponent } from './instant-quote.component';
import { InstantQuoteProceedComponent } from './instant-quote-proceed/instant-quote-proceed.component';
import { InstantQuotePaymentComponent } from './instant-quote-payment/instant-quote-payment.component';
import { InstantQuoteInvoiceComponent } from './instant-quote-invoice/instant-quote-invoice.component';
import { InstantQuoteConfirmComponent } from './instant-quote-confirm/instant-quote-confirm.component';

const routes: Routes = [
  {
    path: '',
    component: InstantQuoteComponent,
    data: { loginFlag: true },
  },
  {
    path: 'instant-quote-proceed',
    component: InstantQuoteProceedComponent,
    data: { loginFlag: true },
  },
  {
    path: 'instant-quote-payment',
    component: InstantQuotePaymentComponent,
    data: { loginFlag: true },
  },
  {
    path: 'instant-quote-invoice',
    component: InstantQuoteInvoiceComponent,
    data: { loginFlag: true },
  },
  {
    path: 'instant-quote-confirm',
    component: InstantQuoteConfirmComponent,
    data: { loginFlag: true },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InstantQuoteRoutingModule {}
