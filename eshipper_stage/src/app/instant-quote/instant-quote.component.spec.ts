import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstantQuoteComponent } from './instant-quote.component';

describe('InstantQuoteComponent', () => {
  let component: InstantQuoteComponent;
  let fixture: ComponentFixture<InstantQuoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstantQuoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstantQuoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
