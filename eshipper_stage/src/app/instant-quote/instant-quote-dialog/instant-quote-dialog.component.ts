import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { ValidationClassStateMatcher } from 'src/app/shared/classes/validation-class';

@Component({
  selector: 'app-instant-quote-dialog',
  templateUrl: './instant-quote-dialog.component.html',
  styleUrls: ['./instant-quote-dialog.component.scss'],
})
export class InstantQuoteDialogComponent implements OnInit {
  modalData = {
    companyName: 'ABC Company',
    fullName: 'Value',
    phone: 'Value',
    email: 'email@email.com',
    monthlyVolume: '$ 100',
    industry: '',
  };
  monthlyVolumes = ['$ 100', '$ 500', '$ 1000'];
  industries = ['1 - 10', '11 - 30', '31 - 70', '71+'];

  requiredText = 'Field is required';
  matcher = new ValidationClassStateMatcher();
  validation = {
    companyName: new FormControl('', [Validators.required]),
    fullName: new FormControl('', [Validators.required]),
    phone: new FormControl('', [Validators.required]),
    monthlyVolume: new FormControl('', [Validators.required]),
    industry: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
  };

  readonlyFlag = true;

  constructor(
    public dialogRef: MatDialogRef<InstantQuoteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() { }

  closeDialog() {
    this.dialogRef.close();
  }

  next() {
    this.dialogRef.close(this.modalData);
  }
}
