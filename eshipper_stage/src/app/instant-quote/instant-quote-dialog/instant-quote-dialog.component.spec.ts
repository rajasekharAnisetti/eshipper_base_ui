import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstantQuoteDialogComponent } from './instant-quote-dialog.component';

describe('InstantQuoteDialogComponent', () => {
  let component: InstantQuoteDialogComponent;
  let fixture: ComponentFixture<InstantQuoteDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstantQuoteDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstantQuoteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
