import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialog,
} from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { ValidationClassStateMatcher } from 'src/app/shared/classes/validation-class';
import { ForgotPasswordComponent } from 'src/app/login/forgot-password/forgot-password.component';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.scss'],
})
export class LoginDialogComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  account: string;
  password: string;

  accountFormControl = new FormControl('', [Validators.required]);
  passwordFormControl = new FormControl('', [Validators.required]);

  matcher = new ValidationClassStateMatcher();

  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<LoginDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  openForgotPassword() {
    this.dialogRef.close();
    const dialogRef = this.dialog.open(ForgotPasswordComponent, {
      panelClass: 'normal-dialog-width',
      autoFocus: false,
    });
    this.subscribe.add(dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log(result);
      }
    }));
  }

  login(account) {
    localStorage.clear();
    localStorage.setItem('account', account);
  }

  closeDialog = () => this.dialogRef.close();

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
  }
}
