import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class InstantQuoteInvoiceService {
  constructor(private http: HttpClient) {}

  getShipDetailsSidebar(): Observable<any> {
    return this.http
      .get('../../content/data/ship-details-sidebar.json')
      .pipe(map(data => data));
  }
}
