import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstantQuoteInvoiceComponent } from './instant-quote-invoice.component';

describe('InstantQuoteInvoiceComponent', () => {
  let component: InstantQuoteInvoiceComponent;
  let fixture: ComponentFixture<InstantQuoteInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstantQuoteInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstantQuoteInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
