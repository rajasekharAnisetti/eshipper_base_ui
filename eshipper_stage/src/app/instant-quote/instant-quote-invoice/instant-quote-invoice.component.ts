import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { MediaMatcher } from '@angular/cdk/layout';

import { DetailsData } from 'src/app/interfaces/details-data';
import {
  ValidationClassStateMatcher,
  QtyMatcher,
} from 'src/app/shared/classes/validation-class';
import { CURRENCIES_$ } from 'src/app/shared/const/currencies';
import { InstantQuoteInvoiceService } from './instant-quote-invoice.service';

@Component({
  selector: 'app-instant-quote-invoice',
  templateUrl: './instant-quote-invoice.component.html',
  styleUrls: ['./instant-quote-invoice.component.scss'],
})
export class InstantQuoteInvoiceComponent implements OnInit, OnDestroy {
  subscribe = new Subscription();
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;

  instantQuoteSteps = [
    {
      position: 'Details',
      state: true,
    },
    {
      position: 'Payment',
      state: true,
    },
    {
      position: 'Invoice',
      state: false,
    },
    {
      position: 'Summary',
      state: false,
    },
  ];

  workingData = {
    title: 'Instant Quote',
    backBtnFlag: true,
  };

  // data for product card
  currencyValue = 'CAD $';
  currencyItems = CURRENCIES_$;
  products = [
    {
      description: '',
      descriptionFormControl: new FormControl(),
      hsCode: '',
      hsCodeFormControl: new FormControl(),
      hsCodeTooltip: 'Tooltip',
      qty: '',
      qtyFormControl: new FormControl(),
      unit: '',
      unitFormControl: new FormControl(),
      price: '',
      priceFormControl: new FormControl(),
      removable: false,
    },
  ];
  total = 0;

  // data for taxt card
  billToList = [
    {
      value: 'Receiver',
      viewValue: 'Receiver',
    },
    {
      value: 'Option 2',
      viewValue: 'Option 2',
    },
    {
      value: 'Option 3',
      viewValue: 'Option 3',
    },
  ];
  taxes = {
    billTo: '',
    billToFormControl: new FormControl('', [Validators.required]),
    account: '',
    sed: '',
  };

  // data for contact card
  matcher = new ValidationClassStateMatcher();
  contact = {
    name: '',
    nameFormControl: new FormControl('', [Validators.required]),
    phone: '',
    phoneFormControl: new FormControl('', [Validators.required]),
    email: '',
    emailFormControl: new FormControl('', [
      Validators.required,
      Validators.email,
    ]),
    brokerName: '',
    brokerNameFormControl: new FormControl(),
    tax: '',
    taxFormControl: new FormControl(),
    recipientTax: '',
    recipientTaxFormControl: new FormControl(),
  };

  // sidebar data
  invoiceDetails = {
    logo: 'content/images/logos/ups.png',
    service: 'Next Flight Out',
    quote: 567.88,
  };
  detailsData = <DetailsData>{};
  qtyMatcher = new QtyMatcher();

  constructor(
    private getData: InstantQuoteInvoiceService,
    private router: Router,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 1024px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    this.subscribe.add(
      this.getData
        .getShipDetailsSidebar()
        .subscribe(data => (this.detailsData = data))
    );
  }

  currencyChange = val => (this.currencyValue = val);
  addRow() {
    this.products.push({
      description: '',
      descriptionFormControl: new FormControl(),
      hsCode: '',
      hsCodeFormControl: new FormControl(),
      hsCodeTooltip: 'Tooltip',
      qty: '',
      qtyFormControl: new FormControl(),
      unit: '',
      unitFormControl: new FormControl(),
      price: '',
      priceFormControl: new FormControl(),
      removable: true,
    });
  }
  removeRow(index) {
    this.products.splice(index, 1);
  }

  processingFunction() {
    localStorage.setItem('processing', '/instant-quote/instant-quote-confirm');
    this.router.navigate(['/processing']);
  }

  ngOnDestroy() {
    if (this.subscribe) {
      this.subscribe.unsubscribe();
    }
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
}
