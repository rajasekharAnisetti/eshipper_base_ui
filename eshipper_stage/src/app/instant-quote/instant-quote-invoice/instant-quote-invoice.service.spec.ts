import { TestBed } from '@angular/core/testing';

import { InstantQuoteInvoiceService } from './instant-quote-invoice.service';

describe('InstantQuoteInvoiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InstantQuoteInvoiceService = TestBed.get(InstantQuoteInvoiceService);
    expect(service).toBeTruthy();
  });
});
