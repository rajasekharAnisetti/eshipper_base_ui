import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { NgxMaskModule } from 'ngx-mask';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { WebStorageModule } from 'ngx-store';
import { AgmCoreModule } from '@agm/core';

import { MaterialModules } from './material-modules';
import { DialogsModule } from './dialogs/dialogs.module';
// components
import { NotificationsComponent } from 'src/app/shared/notifications/notifications.component';
import { MainNavigationComponent } from 'src/app/shared/main-navigation/main-navigation.component';
import { UserItemComponent } from 'src/app/shared/user-item/user-item.component';

// modules
import { AppRoutingModule } from 'src/app/app-routing.module';
import { ProcessingModule } from 'src/app/processing/processing.module';
// end of modules

// shared
import { SharedModule } from 'src/app/shared/shared.module';
import { FiltersModule } from 'src/app/filters/filters.module';

import { AppComponent } from './app.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {};
@NgModule({
  declarations: [
    AppComponent,
    NotificationsComponent,
    MainNavigationComponent,
    UserItemComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModules,
    WebStorageModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    NgxMaskModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDA_v-jmcXGmGO--6fJaEEZZot93E34rwM',
    }),
    DialogsModule,
    SharedModule,
    FiltersModule,
    ProcessingModule,
    AppRoutingModule,
  ],
  exports: [
    WebStorageModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModules,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
    },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
